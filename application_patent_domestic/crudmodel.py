import datetime
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.views.decorators.csrf import csrf_exempt

from application_patent_domestic.forms import *
from application_patent_domestic.models import *
from common.codemultilanguage_choice import GetCodeMultpleLanguageForPatentMaster, \
    GetCodeWithMultpleLanguage
from utils.db_fieldset import get_fielddic



@login_required(login_url='People:login')
def MasterCreate(request):

    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Application_Patent_Domestic_Master')

    if request.method == 'POST':
        form = MasterModelForm(request.POST, labeldic=None, customcodechoice=None, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse('<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        nowdate = datetime.datetime.now()
        year = nowdate.strftime('%y')
        mon = nowdate.strftime('%m')
        ourlist = []
        f = Application_Patent_Domestic_Master.objects.filter(ourCompanyNumber__regex=r'(^L)(PD)('+year+')('+mon+')(-?)(\d{4})')
        #f = Application_Patent_Domestic_Master.objects.filter(ourCompanyNumber__regex=r'(^L)(P|T|D|R|I|C)(D|O|I)(1912)(-?)(\d{4})').all()
        serialnum = ''
        if f:
            for l in f:
                if '-' in l.ourCompanyNumber:
                    ourlist.append(int(l.ourCompanyNumber[8:12]))
                else:
                    ourlist.append(int(l.ourCompanyNumber[7:11]))
            serialnum = year+mon+ '-' + str(max(ourlist)+1).zfill(4)
        else:
            serialnum = year+mon+'-0001'

        pcodename = Code_Information.objects.filter(codeType=ALL_CODE_TYPE.PatentType.name).first().codeName
        ncodename = Code_Information.objects.filter(codeType=ALL_CODE_TYPE.PatentNationType.name).first().codeName
        serialnum = 'L'+pcodename + ncodename + serialnum
        customcodechoice = GetCodeMultpleLanguageForPatentMaster(request)
        form = MasterModelForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)


    return render(request, 'application_patent_domestic/master_form.html', {'form': form,'serialnum':serialnum})

@login_required(login_url='People:login')
def Master_SearchOurCom(request):
    numberforsearch = request.GET.get('searchournum')
    ourlist = []
    if len(numberforsearch) > 12:
        code1 = numberforsearch[0:7]
        code2 = numberforsearch[12:]
        f = Application_Patent_Domestic_Master.objects.filter(
            ourCompanyNumber__regex=r'(' + code1 + ')(-?)(\d{4})'+'('+ code2 +')')
        if f:
            for l in f:
                if '-' in l.ourCompanyNumber:
                    ourlist.append(int(l.ourCompanyNumber[8:12]))
                else:
                    ourlist.append(int(l.ourCompanyNumber[7:11]))
            serialnum = code1 + '-' + str(max(ourlist) + 1).zfill(4) + code2
        else:
            serialnum = code1 + '-0001' + code2
    else:
        numberforsearch = numberforsearch[0:7]
        f = Application_Patent_Domestic_Master.objects.filter(
            ourCompanyNumber__regex=r'('+numberforsearch +')(-?)(\d{4})')
        if f:
            for l in f:
                if '-' in l.ourCompanyNumber:
                    ourlist.append(int(l.ourCompanyNumber[8:12]))
                else:
                    ourlist.append(int(l.ourCompanyNumber[7:11]))
            serialnum = numberforsearch + '-' + str(max(ourlist) + 1).zfill(4)
        else:
            serialnum = numberforsearch + '-0001'
    data = {'serialnum': serialnum}
    return JsonResponse(data)

@login_required(login_url='People:login')
def MasterEdit(request, pk):
    master = get_object_or_404(Application_Patent_Domestic_Master, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Application_Patent_Domestic_Master')
    if request.method == 'POST':
        form = MasterModelForm(request.POST, request.FILES, instance=master, crud='u', request=request)  # NOTE: instance 인자(수정대상) 지정

        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeMultpleLanguageForPatentMaster(request)
        form = MasterModelForm(instance=master, labeldic=labeldic, customcodechoice=customcodechoice, request=request)
    return render(request, 'application_patent_domestic/master_form.html', {
        'form': form,
    })

"""
def MasterDelete(request):
    if request.is_ajax():
        selected_tests = request.POST.getlist('m_list_ids')
        m_list = ast.literal_eval(selected_tests[0])
        for ma_s in m_list:
            if ma_s != '':
                Application_Patent_Domestic_Master.objects.filter(id=int(ma_s)).delete()
        return HttpResponse(json.dumps({'status': 'ok'}), content_type="application/json")
"""
@login_required(login_url='People:login')
@csrf_exempt
def MasterDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Application_Patent_Domestic_Master.objects.get(id=pk).delete()
    return redirect('application_patent_domestic:patent_master')

@login_required(login_url='People:login')
def ApplicantCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Application_Patent_Domestic_Applicant')
    if request.method == 'POST':
        form = ApplicantModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse('<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeWithMultpleLanguage(request)
        form = ApplicantModelForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)

    return render(request, 'application_patent_domestic/applicant_form.html', {'form': form})

@login_required(login_url='People:login')
def ApplicantEdit(request, pk):
    applicant = get_object_or_404(Application_Patent_Domestic_Applicant, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Application_Patent_Domestic_Applicant')
    if request.method == 'POST':
        form = ApplicantModelForm(request.POST, request.FILES, instance=applicant, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeWithMultpleLanguage(request)
        form = ApplicantModelForm(instance=applicant, labeldic=labeldic, customcodechoice=customcodechoice, request=request)
    return render(request, 'application_patent_domestic/applicant_form.html', {
        'form': form,
    })
@login_required(login_url='People:login')
@csrf_exempt
def ApplicantDelete(request):
    if request.method =='POST':
        pk = request.POST.get('pk')
        Application_Patent_Domestic_Applicant.objects.get(id=pk).delete()
        return redirect('application_patent_domestic:applicant')

@login_required(login_url='People:login')
def MainInventorCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Main_Inventor')
    if request.method == 'POST':
        form = MainInventorModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = MainInventorModelForm(labeldic=labeldic, request=request)

    return render(request, 'application_patent_domestic/maininventor_form.html', {'form': form})

@login_required(login_url='People:login')
def MainInventorEdit(request, pk):
    maininvenotr = get_object_or_404(Main_Inventor, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Main_Inventor')
    if request.method == 'POST':
        form = MainInventorModelForm(request.POST, request.FILES, instance=maininvenotr, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = MainInventorModelForm(instance=maininvenotr, labeldic=labeldic, request=request)
    return render(request, 'application_patent_domestic/maininventor_form.html', {
        'form': form,
    })

@login_required(login_url='People:login')
@csrf_exempt
def MainInventorDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Main_Inventor.objects.get(id=pk).delete()
        return redirect('application_patent_domestic:inventorview')


@login_required(login_url='People:login')
def AgentCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Agent')
    if request.method == 'POST':
        form = AgentModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = AgentModelForm(labeldic=labeldic, request=request)

    return render(request, 'application_patent_domestic/agent_form.html', {'form': form})

@login_required(login_url='People:login')
def AgentEdit(request, pk):
    agent = get_object_or_404(Agent, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Agent')
    if request.method == 'POST':
        form = AgentModelForm(request.POST, request.FILES, instance=agent, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = AgentModelForm(instance=agent, labeldic=labeldic, request=request)
    return render(request, 'application_patent_domestic/agent_form.html', {
        'form': form,
    })

@login_required(login_url='People:login')
@csrf_exempt
def AgentDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Agent.objects.get(id=pk).delete()
        return redirect('application_patent_domestic:agentview')