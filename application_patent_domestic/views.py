import glob
import json
import os
from datetime import datetime
from pathlib import Path

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.files import File
from django.db.models import Q
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from Lipis_system_master import settings
from Lipis_system_master.views import HomeMixin

from application_patent_domestic.models import *



# Create your views here.
from common.codemultilanguage_choice import GetCodeInfoMultipleLanguage_all, GetCodeWithMultpleLanguage
from common.tableview_function import get_all_model_context_for_table
from message.models import Message, Patent_Document_Attachment, File, Patent_Document
from utils.api.api import callapi_readAppHistInfo
from utils.dir_mgr import make_brandimage_attachment_path
from utils.enums import PATENT_DOCU_PROCESS_STATUS, PATENT_FILTER_TYPE, UPLOAD_FILE_PATH


def makepatentfilter(filtertype, filtername):
    dic = {}
    if filtertype == 'casetype':
        dic['client__client_clienttype__typeName'] = filtername
    return dic



class MasterView(LoginRequiredMixin, HomeMixin, TemplateView):
    login_url = 'People:login'
    template_name = 'application_patent_domestic/patent_master.html'

    def get_context_data(self, **kwargs):
        ptype = self.request.GET.get('ptype')
        ntype = self.request.GET.get('ntype')
        context = super(MasterView, self).get_context_data(**kwargs)
        mfilter = {}
        penum = ''
        nenum = ''
        if ptype == 'all':
            mfilter = {}
            self.request.session['ptype'] = 'all'
            self.request.session['ntype'] = 'all'
        else:
            if ptype is None:
                if 'ptype' in self.request.session:
                    ptype = self.request.session['ptype']
                else:
                   penum = PATENT_TYPE_FOR_OURCOM.P.name
                   ptype= 'P'

            if ptype == 'P':
                penum = PATENT_TYPE_FOR_OURCOM.P.name
            elif ptype == 'T':
                penum = PATENT_TYPE_FOR_OURCOM.T.name
            elif ptype == 'D':
                penum = PATENT_TYPE_FOR_OURCOM.D.name
            elif ptype == 'R':
                penum = PATENT_TYPE_FOR_OURCOM.R.name
            elif ptype == 'all':
                penum = None

            if ntype is None:
                if 'ntype' in self.request.session:
                    ntype = self.request.session['ntype']
                else:
                    nenum = NATION_TYPE_FOR_OURCOM.D.name
                    ntype = 'D'
            if ntype == 'D':
                nenum = NATION_TYPE_FOR_OURCOM.D.name
            elif ntype == 'O':
                nenum = NATION_TYPE_FOR_OURCOM.O.name
            elif ntype == 'I':
                nenum = NATION_TYPE_FOR_OURCOM.I.name
            elif ptype is not None and ptype != 'all' and ntype == 'all':
                ntype = 'D'
                nenum = NATION_TYPE_FOR_OURCOM.D.name
            elif ntype == 'all':
                nenum = None

            self.request.session['ntype'] = ntype
            self.request.session['ptype'] = ptype
            if penum is None:
                mfilter = {}
            else:
                mfilter = {'patentType': penum, 'nationType': nenum}


        # 필터를 클릭 시 ajax 로 필터 타입을 세션에 저장 후 리로드 하게 된다. input name 과 enum filter 를 일치시키는 것을 전제로하고,
        # 세션에 enum name 이 있으면 enum value 의 filter 를 불러와 dictionary 만들어서 모델에 적용한다.
        for patenttype in PATENT_FILTER_TYPE:
            if patenttype.name in self.request.session:
                # input radio를 찾기 위해 context에 저장. 나중에 적용된 필터 전부 context 리스트로 보낸 다음에
                # frontend에서 ready 시 하나의 loop로 다수의 radio을 check 하도록 변경할 것.
                context[patenttype.name] = self.request.session[patenttype.name]
                if self.request.session[patenttype.name] != 'All':
                    mfilter[patenttype.value] = self.request.session[patenttype.name]


        #a = Application_Patent_Domestic_Master.objects.filter(client__client_clienttype__typeName='OverseaAgent')
        context = get_all_model_context_for_table(self, Application_Patent_Domestic_Master, 'Application_Patent_Domestic_Master',
                                         'Master', context, filter=mfilter)
        context['patentType'] = ptype
        context['nationType'] = ntype
        context['code_multiplelang_all'] = GetCodeInfoMultipleLanguage_all(self.request)
        context['countryCode'] = GetCodeWithMultpleLanguage(self.request)['countryCode']
        return context



class MasterDetailView(LoginRequiredMixin, HomeMixin, TemplateView):
    login_url = 'People:login'
    template_name = 'application_patent_domestic/patent_master_detail.html'
    def get_context_data(self, **kwargs):
        context = super(MasterDetailView, self).get_context_data(**kwargs)
        return context

    def get(self, request, pk):
        context = self.get_context_data()
        master = Application_Patent_Domestic_Master.objects.get(id=pk)
        msgs = Message.objects.filter(master_message=master)
        rcptlist = []
        if master.applicationNumber:
            result = callapi_readAppHistInfo(request, master.applicationNumber.replace('-',''))
            obj = json.loads(result)

            if obj['errorType'] == '000':
                for o in obj['resultListData']:
                    try:
                        pd = Patent_Document.objects.get(receiptSendCode=o['rcptSendNo'])
                        for ps in PATENT_DOCU_PROCESS_STATUS:
                            if o['procStDesc'] == ps.value:
                                pd.processingState = ps.name
                                pd.save()
                                pd.processingState = ps.value
                        pd.msgid = Message.objects.get(patent_docu=pd).id
                        rcptlist.append(pd)
                    except:
                        pd = Patent_Document()
                        pd.processingState = o['procStDesc']
                        pd.receiptSendDate = datetime.strptime(o['rcptSendDate'], '%Y%m%d')
                        pd.receiptSendCode = o['rcptSendNo']
                        pd.rcptSendDocNm = o['rcptSendDocNm']
                        rcptlist.append(pd)
                        #rcptlist.append({'processingsState':o['procStDesc'],'receiptSendDate':o['rcptSendDate'],'receiptSendCode':o['rcptSendNo']})

        if rcptlist:
            context['patent_document'] = rcptlist
        else:



            context['patent_document'] = Patent_Document.objects.filter(msg_patentdocu__master_message=master).all()







        master.msgs = msgs
        context['patentType'] = self.request.session['ptype']
        context['nationType'] = self.request.session['ntype']
        context['master'] = master


        return self.render_to_response(context)



class ApplicantsView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'application_patent_domestic/applicants.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):

        context = super(ApplicantsView, self).get_context_data(**kwargs)

        # And so on for more models
        return get_all_model_context_for_table(self, Application_Patent_Domestic_Applicant, 'Application_Patent_Domestic_Applicant', 'Applicant', context)

@csrf_exempt
def LinkPatentMasterApplicant(request):
    if request.method == 'POST':
        searchcontent = request.POST.get('searchContent')
        pk = request.POST.get('pid')
        applobj = Application_Patent_Domestic_Applicant.objects.get(id=pk)
        searchType = request.POST.get('searchType')

        if searchType == 'applicationNumber':
            masters = Application_Patent_Domestic_Master.objects.filter(applicationNumber=searchcontent)
        elif searchType == 'ourCompanyNumber':
            masters = Application_Patent_Domestic_Master.objects.filter(ourCompanyNumber=searchcontent)
        for master in masters:
            master.applicants.add(applobj)
    return HttpResponse('')

@login_required(login_url='People:login')
@csrf_exempt
def DelLinkPatentMasterApplicant(request):
    if request.method == 'POST':
        applicantid = request.POST.get('applid')
        masterid = request.POST.get('maid')
        applicant = Application_Patent_Domestic_Applicant.objects.get(id=applicantid)
        master = Application_Patent_Domestic_Master.objects.get(id=masterid)
        applicant.master.remove(master)
        applicant.save()
    return HttpResponse('')

@login_required(login_url='People:login')
@csrf_exempt
def UploadBrandImage(request, pk):
    if request.method == 'POST':
        files = request.FILES.getlist('files[]')

        for f in files:
            if 'image' in f.content_type:
                brandpath = make_brandimage_attachment_path(request.user.username)
                mobj = Application_Patent_Domestic_Master.objects.get(id=pk)
                customfilename = datetime.today().strftime('%Y%m%d%H%M%S%f') + '.' + f.name.split('.')[1]
                mypath = (settings.MEDIA_ROOT + '/' + brandpath + '/' + customfilename).replace('\\', '/')

                with open(mypath, 'wb') as dest:
                    for chunk in f.chunks():
                        dest.write(chunk)
                savepath = ('/' + brandpath + '/' + customfilename).replace('\\', '/')
                #mobj.brandImage.save(savepath, ContentFile(myfile))
                #리눅스에서 동작하기 위해.
                mobj.brandImage = savepath
                mobj.save()

    return HttpResponse('')

class MainInventorView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'application_patent_domestic/maininventors.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(MainInventorView, self).get_context_data(**kwargs)
        MIcontext = get_all_model_context_for_table(self, Main_Inventor,
                                                    'Main_Inventor',
                                                    'MainInventor', context)
        return MIcontext

class AgentView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'application_patent_domestic/agents.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(AgentView, self).get_context_data(**kwargs)
        MIcontext = get_all_model_context_for_table(self, Agent,
                                                    'Agent',
                                                    'Agent', context)
        return MIcontext


class KIPOGoodsView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'application_patent_domestic/kipo_goods.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


class KIPOGoodsSyncView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'application_patent_domestic/kipo_goods_sync.html'
    login_url = 'People:login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        file_list = glob.glob(str(Path(UPLOAD_FILE_PATH.kipogoods.value) / '*'))
        context['file_list'] = file_list
        return context

@login_required(login_url='People:login')
@csrf_exempt
def KIPOGoodsExcelFileUpload(request):
    dirc = UPLOAD_FILE_PATH.kipogoods.value
    if request.method == 'POST':
        files = request.FILES.getlist('files[]')
        if not os.path.exists(dirc):
            os.makedirs(dirc)
        for f in files:
            with open(dirc +'/'+ f.name, 'wb') as dest:
                for chunk in f.chunks():
                    dest.write(chunk)
        return HttpResponse('')