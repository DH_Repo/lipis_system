from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.http import JsonResponse

from application_patent_domestic.models import Application_Patent_Domestic_Master
from utils.enums import PATENT_FILTER_TYPE


@login_required(login_url='People:login')
def GetMasterRelation(request):
    patentid = request.GET.get('patentid')
    mobj = Application_Patent_Domestic_Master.objects.get(id=patentid)
    data = {'state': 'success'}
    managerofourcompany = []
    for oc in mobj.managerOfOurCompany.all():
        managerofourcompany.append(oc.id)
    client = []
    clientmgr = []
    for cl in mobj.client.all():
        for c in cl.manager.all():
            clientmgr.append(c.id)
        client.append(cl.id)
    data['managerofourcompany'] = managerofourcompany
    data['client'] = client
    data['clientmgr'] = clientmgr
    return JsonResponse(data)

@login_required(login_url='People:login')
def SetMasterFilter(request):

    for patentfilter in PATENT_FILTER_TYPE:
        filtername = request.GET.get(patentfilter.name, None)
        if filtername:
            request.session[patentfilter.name] = filtername
    data = {'state': 'success', 'result': 'success'}
    return JsonResponse(data)