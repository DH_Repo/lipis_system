from django.db import models
from django.db.models import signals
from phone_field import PhoneField
from django.forms.models import model_to_dict

import People

# Create your models here.
from management.models import BaseModel
from utils.enums import PATENT_STATE_CHOICE, TABLE_TYPE, \
    PATENT_TYPE_FOR_OURCOM, MakeWIPO_ST3_Dic, NATION_TYPE_FOR_OURCOM, MakeWIPO_ST3_Enum


class Application_Patent_Domestic_Master(BaseModel):
    patentType = models.CharField(max_length=30, choices=[(tag.name, tag.value) for tag in PATENT_TYPE_FOR_OURCOM])
    nationType = models.CharField(max_length=30, choices=[(tag.name, tag.value) for tag in NATION_TYPE_FOR_OURCOM])
    state = models.CharField(max_length=30, blank=True, choices=[(tag.name, tag.value) for tag in PATENT_STATE_CHOICE],
                             null=True)
    ourCompanyNumber = models.CharField(max_length=30, null=True, blank=True)
    inventNameKor = models.CharField(max_length=200, blank=True, null=True)
    inventNameEng = models.CharField(max_length=200, blank=True, null=True)
    inventNameOrigin = models.CharField(max_length=200, blank=True)
    receptionDate = models.DateTimeField(null=True, blank=True)
    priorityClaim = models.CharField(max_length=30, blank=True)
    priorityDate = models.DateTimeField(null=True, blank=True)
    applicationRequestFixedDate = models.DateTimeField(null=True, blank=True)
    draftFixedDate = models.DateTimeField(null=True, blank=True)
    draftSendDate = models.DateTimeField(null=True, blank=True)
    currentProgress = models.CharField(max_length=30, blank=True)
    currentProgressDate = models.CharField(max_length=30, blank=True)
    applicationNumber = models.CharField(max_length=30, blank=True)
    applicationDate = models.DateTimeField(null=True, blank=True)
    remarks = models.CharField(max_length=200, blank=True)
    clientCode = models.IntegerField(blank=True, null=True)
    foreignAgentCode = models.IntegerField(blank=True, null=True)
    holder = models.ManyToManyField(
        'application_patent_domestic.Application_Patent_Domestic_Applicant',
        through='application_patent_domestic.Application_Patent_Domestic_Master_Holder',
        related_name='patent_domestic_master_holder', blank=True
    )

    applicants = models.ManyToManyField(
        'application_patent_domestic.Application_Patent_Domestic_Applicant',
        through='application_patent_domestic.Application_Patent_Domestic_Master_Applicant',
        related_name='patent_domestic_master_applicant', blank=True
    )
    client = models.ManyToManyField(
        'People.Client',
        through='application_patent_domestic.Application_Patent_Domestic_Master_Client',
        related_name='patent_domestic_master_client', blank=True
    )
    mainInventor = models.ManyToManyField(
        'Main_Inventor',
        through='application_patent_domestic.Application_Patent_Domestic_Master_MainInventor',
        related_name='patent_domestic_master_maininventor', blank=True
    )
    managerOfApplicant = models.ManyToManyField(
        'People.Person',
        through='application_patent_domestic.Application_Patent_Domestic_Master_ManagerOfApplicant',
        related_name='patent_domestic_master_managerofapplicant', blank=True
    )
    managerOfOurCompany = models.ManyToManyField(
        'People.Person',
        through='application_patent_domestic.Application_Patent_Domestic_Master_ManagerOfOurCompany',
        related_name='patent_domestic_master_managerofourcompany', blank=True
    )
    agent = models.ManyToManyField(
        'Agent',
        through='application_patent_domestic.Application_Patent_Domestic_Master_Agent',
        related_name='patent_domestic_master_agent', blank=True
    )
    oversea_agent = models.ManyToManyField(
        'People.Client',
        through='application_patent_domestic.Application_Patent_Domestic_Master_OverseaAgent',
        related_name='patent_domestic_master_oversea_agent', blank=True
    )
    supporting_organization = models.ManyToManyField(
        'People.Client',
        through='application_patent_domestic.Application_Patent_Domestic_Master_SupportingOrganization',
        related_name='patent_domestic_master_supporting_organization', blank=True
    )
    supporting_organization_management = models.ManyToManyField(
        'People.Person',
        through='application_patent_domestic.Master_SupportingOrganization_Management',
        related_name='patent_domestic_master_supporting_organization_management', blank=True
    )
    nation_research_project = models.ManyToManyField(
        'NationResearchProject',
        through='ApplicationPatentDomesticMaster_NationResearchProject',
        related_name='patent_domestic_master_nation_research_project', blank=True
    )
    task = models.ManyToManyField('case.Task', through='Application_Patent_Domestic_Master_Task',
                                  related_name='master_task', blank=True)
    case = models.ManyToManyField('case.Case', through='Application_Patent_Domestic_Master_Case',
                                  related_name='master_case', blank=True)
    countryCode = models.CharField(max_length=20, null=True, blank=True,
                                   choices=[(tag.name, tag.value) for tag in MakeWIPO_ST3_Enum()])
    pctBaseApplication = models.CharField(max_length=30, null=True, blank=True)
    brandImage = models.ImageField(null=True, blank=True)
    brandName = models.CharField(max_length=100, null=True, blank=True)
    productClassification = models.CharField(max_length=50, null=True, blank=True)
    designatedProductOrigin = models.CharField(max_length=500, null=True, blank=True)
    designatedProductKor = models.CharField(max_length=500, null=True, blank=True)
    designatedProductEng = models.CharField(max_length=500, null=True, blank=True)
    registrationNumber = models.CharField(max_length=100, null=True, blank=True)
    registrationDate = models.DateTimeField(null=True, blank=True)
    abstractFigNum = models.CharField(max_length=20, null=True, blank=True)
    abstractFig = models.ImageField(null=True, blank=True)

    def __str__(self):
        return 'id:' + str(self.id) + ' // appno:' + self.applicationNumber + 'ourno:' + self.ourCompanyNumber

    class Meta:
        db_table = 'application_patent_domestic_master'


class Application_Patent_Domestic_Master_Case(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_case_master', null=True, blank=True)
    case = models.ForeignKey('case.Case', on_delete=models.CASCADE, related_name='master_case_case', null=True,
                             blank=True)
    caseSortCode = models.CharField(max_length=30, null=True, blank=True)

    class Meta:
        db_table = "application_patent_domestic_master_case"


class Application_Patent_Domestic_Master_Task(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_task_master', null=True, blank=True)
    task = models.ForeignKey('case.Task', on_delete=models.CASCADE,
                             related_name='master_task_task', null=True, blank=True)

    class Meta:
        db_table = "application_patent_domestic_master_task"


class Application_Patent_Domestic_Master_Holder(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_holder')
    applicant = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Applicant'

                                  , on_delete=models.CASCADE, related_name='master_holder')
    priority = models.IntegerField(null=True, blank=True)
    korAddress = models.CharField(max_length=50, null=True, blank=True)
    engAddress = models.CharField(max_length=50, null=True, blank=True)
    originAddress = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        db_table = "application_patent_domestic_master_holder"


class Application_Patent_Domestic_Master_Applicant(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_applicant')
    applicant = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Applicant'

                                  , on_delete=models.CASCADE, related_name='master_applicant')
    priority = models.IntegerField(null=True, blank=True)
    korAddress = models.CharField(max_length=50, null=True, blank=True)
    engAddress = models.CharField(max_length=50, null=True, blank=True)
    originAddress = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        db_table = "application_patent_domestic_master_applicant"


class Application_Patent_Domestic_Master_Client(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_client')
    client = models.ForeignKey('People.Client', on_delete=models.CASCADE, related_name='master_client')
    priority = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "application_patent_domestic_master_client"


class ApplicationPatentDomesticMaster_NationResearchProject(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_project_master')
    project = models.ForeignKey('NationResearchProject', on_delete=models.CASCADE,
                                related_name='master_project_project')
    priority = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "application_patent_domestic_master_nation_research_project"


class Application_Patent_Domestic_Master_MainInventor(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_maininventor')
    mainInventor = models.ForeignKey('Main_Inventor', on_delete=models.CASCADE, related_name='master_maininventor')
    priority = models.IntegerField(null=True, blank=True)
    korAddress = models.CharField(max_length=50, null=True, blank=True)
    engAddress = models.CharField(max_length=50, null=True, blank=True)
    originAddress = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        db_table = "application_patent_domestic_master_maininventor"


class Application_Patent_Domestic_Master_ManagerOfApplicant(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_managerofapplicant')
    managerOfApplicant = models.ForeignKey('People.Person', on_delete=models.CASCADE,
                                           related_name='master_managerofapplicant')
    priority = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "application_patent_domestic_master_managerofapplicant"


class Application_Patent_Domestic_Master_ManagerOfOurCompany(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_managerofourcompany')
    managerOfOurCompany = models.ForeignKey('People.Person', on_delete=models.CASCADE,
                                            related_name='master_managerofourcompany')
    priority = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "application_patent_domestic_master_managerofourcompany"


class Application_Patent_Domestic_Master_Agent(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_agent')
    agent = models.ForeignKey('Agent', on_delete=models.CASCADE, related_name='master_agent', null=True)
    priority = models.IntegerField(null=True, blank=True)
    korAddress = models.CharField(max_length=50, null=True, blank=True)
    engAddress = models.CharField(max_length=50, null=True, blank=True)
    originAddress = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        db_table = "application_patent_domestic_master_agent"


class Application_Patent_Domestic_Master_OverseaAgent(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_overseaagent_master')
    oversea_agent = models.ForeignKey('People.Client', on_delete=models.CASCADE,
                                      related_name='master_overseaagent_overseaagent')
    agentCode = models.CharField(max_length=30, null=True, blank=True)
    oversea_management = models.ManyToManyField(
        'People.Person',
        through='application_patent_domestic.Oversea_Agent_Management',
        related_name='patent_domestic_master_oversea_agent_oversea_management', blank=True
    )

    class Meta:
        db_table = "application_patent_domestic_master_oversea_agent"


class Oversea_Agent_Management(models.Model):
    master_oversea_agent = models.ForeignKey(
        'application_patent_domestic.Application_Patent_Domestic_Master_OverseaAgent'
        , on_delete=models.CASCADE, related_name='master_oversea_agent_oversea_management_master_oversea_agent')
    person_oversea_management = models.ForeignKey('People.Person', on_delete=models.CASCADE,
                                                  related_name='master_oversea_agent_oversea_management_master_person_oversea_management')

    class Meta:
        db_table = "oversea_agent_management"


class Application_Patent_Domestic_Master_SupportingOrganization(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE, related_name='master_supporting_organization_master')
    supporting_organization = models.ForeignKey('People.Client', on_delete=models.CASCADE,
                                                related_name='master_supporting_organization_supporting_organization')

    managementCode = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = "application_patent_domestic_master_supporting_organization"
        unique_together = ('master', 'supporting_organization')


class Master_SupportingOrganization_Management(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                               , on_delete=models.CASCADE,
                               related_name='master_supporting_organization_management_master')
    management = models.ForeignKey('People.Person', on_delete=models.CASCADE,
                                   related_name='master_supporting_organization_management_management')

    class Meta:
        db_table = "application_patent_master_supportingorganiztaion_management"
        unique_together = ('master', 'management')


class Application_Patent_Domestic_Applicant(BaseModel):
    code = models.CharField(max_length=50, blank=True)
    past_id = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)
    patentOfficeCode = models.CharField(max_length=10, blank=True,
                                        choices=[(tag.name, tag.value) for tag in MakeWIPO_ST3_Enum()])
    creationDate = models.DateTimeField(auto_now_add=True)
    agent = models.ManyToManyField('Agent', through='Applicant_Agent', blank=True,
                                   related_name='domesticapplicant_agent')
    master = models.ManyToManyField(
        'application_patent_domestic.Application_Patent_Domestic_Master',
        through='application_patent_domestic.Application_Patent_Domestic_Master_Applicant',
        related_name='applicant_patent_domestic_master', blank=True
    )
    isCorporation = models.BooleanField(null=True, blank=True)
    isCompany = models.BooleanField(null=False, default=True, blank=False)
    originLastName = models.CharField(max_length=50, blank=True)
    originFirstName = models.CharField(max_length=50, blank=True)
    originFullName = models.CharField(max_length=50, blank=True)
    originAddress = models.CharField(max_length=100, blank=True, null=True)
    engLastName = models.CharField(max_length=50, blank=True, null=True)
    engFirstName = models.CharField(max_length=50, blank=True, null=True)
    engFullName = models.CharField(max_length=50, blank=True, null=True)
    engAddress = models.CharField(max_length=100, blank=True, null=True)
    korLastName = models.CharField(max_length=20, blank=True, null=True)
    korFirstName = models.CharField(max_length=20, blank=True, null=True)
    korFullName = models.CharField(max_length=50, blank=True, null=True)
    korAddress = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        if self.originFullName is not None:
            orgin = self.originFullName
        else:
            orgin = 'None'
        if self.korFullName is not None:
            kor = self.korFullName
        else:
            kor = 'None'
        return orgin + '(' + kor + ')'

        # return self.korCorporationName

    class Meta:
        db_table = 'application_patent_domestic_applicant'


class Main_Inventor(BaseModel):
    nationality = models.CharField(max_length=10, null=True, blank=True)
    korId = models.CharField(max_length=20, null=True, blank=True)
    zipCode = models.CharField(max_length=10, null=True, blank=True)
    korName = models.CharField(max_length=10, null=True, blank=True)
    korAddress = models.CharField(max_length=100, null=True, blank=True)
    engName = models.CharField(max_length=50, null=True, blank=True)
    engAddress = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        restr = ''
        if self.korName is not None:
            restr += self.korName + ','
        if self.engName is not None:
            restr += self.engName
        return restr

    class Meta:
        db_table = 'main_inventor'


class Agent(BaseModel):
    agentCode = models.CharField(max_length=60, null=True, blank=True)
    korName = models.CharField(max_length=10, null=True, blank=True)
    korAddress = models.CharField(max_length=150, null=True, blank=True)
    engName = models.CharField(max_length=50, null=True, blank=True)
    engAddress = models.CharField(max_length=150, null=True, blank=True)
    management = models.ManyToManyField('People.Person', through='Agent_Management', related_name='agent_management',
                                        blank=True)
    isCorporation = models.BooleanField(null=True, blank=True)

    def __str__(self):
        restr = ''
        if self.korName is not None:
            restr += self.korName + ','
        if self.engName is not None:
            restr += self.engName
        return restr

    class Meta:
        db_table = 'agent'


class Applicant_Agent(models.Model):
    applicant = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Applicant'
                                  , on_delete=models.CASCADE, related_name='applicant_agent')
    agent = models.ForeignKey('Agent', on_delete=models.CASCADE, related_name='applicant_agent')
    priority = models.IntegerField(null=True, blank=True)
    GPOACode = models.CharField(max_length=20, null=True, blank=True)

    class Meta:
        db_table = "applicant_agent"


class Agent_Management(models.Model):
    agent = models.ForeignKey('Agent', on_delete=models.CASCADE, related_name='agent_management_agent')
    management = models.ForeignKey('People.Person', on_delete=models.CASCADE,
                                   related_name='agent_management_management')

    class Meta:
        db_table = "agent_management"


class Search_Parameter(models.Model):
    tableType = models.CharField(max_length=50, blank=True, choices=[(tag.name, tag.value) for tag in TABLE_TYPE],
                                 primary_key=True)
    colname = models.CharField(max_length=20, blank=True, null=True)
    searchname = models.CharField(max_length=20, blank=True, null=True)

    def __str__(self):
        return self.tableType

    class Meta:
        db_table = 'search_parameter'




class Priority_Claim(models.Model):
    countryCode = models.CharField(max_length=20, blank=True, null=True)
    applNo = models.CharField(max_length=30, blank=True, null=True)
    applicant = models.CharField(max_length=50, blank=True, null=True)
    appDate = models.DateField(blank=True, null=True)
    master = models.ForeignKey('Application_Patent_Domestic_Master', on_delete=models.CASCADE,
                           related_name='priority_claim_master', blank=True, null=True)
    priority = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'priority_claim'


class KIPOGoodsName(models.Model):
    noticeNumber = models.CharField(max_length=30, null=False, blank=False)
    effectiveDate = models.DateField(null=False, blank=False)
    goodsNameKor = models.CharField(max_length=1000, null=False, blank=False)
    classification = models.CharField(max_length=50, null=False, blank=False)
    similarGroupCode = models.CharField(max_length=500, null=False, blank=False)
    goodNameEng = models.CharField(max_length=1000, null=False, blank=False)
    sourcePatentOffice = models.CharField(max_length=50, null=True, blank=True)
    serviceClass = models.CharField(max_length=20, null=True, blank=True)

    class Meta:
        db_table = 'kipo_goods_name'


class NationResearchProject(models.Model):
    subjectID = models.CharField(max_length=30, null=True, blank=True)
    ministryName = models.CharField(max_length=30, null=True, blank=True)
    researchManageExpertMachine = models.CharField(max_length=30, null=True, blank=True)
    researchProjectName = models.CharField(max_length=50, null=True, blank=True)
    researchSubjectName = models.CharField(max_length=200, null=True, blank=True)
    contributionRatio = models.CharField(max_length=10, null=True, blank=True)
    managementMachinery = models.CharField(max_length=20, null=True, blank=True)
    researchPeriodStart = models.DateField(null=True, blank=True)
    researchPeriodEnd = models.DateField(null=True, blank=True)

    class Meta:
        db_table = 'applicant_patent_domestic_nation_research_project'
