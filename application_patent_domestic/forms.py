from abc import abstractmethod

from django import forms
from django.db.models import Case, When, Prefetch
from django.forms import ModelForm

from People.models import Client
from common.codemultilanguage_choice import MakeTagChoiceMultipleLanguage
from management.models import DB_Modifying_Field_Name, Code_Multiple_Language, Code_Information, Table_History
from utils.enums import ALL_CODE_TYPE, CRUD_TYPE
from .models import *
from django.shortcuts import render
class ParentForm():

	def init_argument(self, kwargs):
		if 'crud' in kwargs:
			self.crud = kwargs.pop('crud')
		else:
			self.crud = None
		if 'labeldic' in kwargs:
			self.labeldic = kwargs.pop('labeldic')
		else:
			self.labeldic = None
		if 'customcodechoice' in kwargs:
			self.customcodechoice = kwargs.pop('customcodechoice')

		else:
			self.customcodechoice = None
		if 'request' in kwargs:
			self.request = kwargs.pop('request')
		else:
			self.request = None

	def save_create(self):
		if self.crud == 'c':
			name = self.Meta.model.__name__
			Table_History.objects.create(crudType=CRUD_TYPE.C.name, tableName=name, primaryKey=self.instance.id, user=self.request.user)

	def arrange_and_createtablehistory(self):
		MakeTagChoiceMultipleLanguage(self)
		init = self.initial

		if self.is_bound:
			self.full_clean()

			if self.crud == 'u':
				name = self.Meta.model.__name__
				for cdata in self.changed_data:
					f = self.data.get(cdata, None)
					# changed_data는 있는데 self.data에 없는 경우 있어서 처리. 확인할 것.
					if f:
						c = str(init[cdata])
						Table_History.objects.create(crudType=CRUD_TYPE.U.name, tableName=name,
													 primaryKey=init['id'], fieldName=cdata, initialFieldValue=c,
													 modifiedFieldValue=f, user=self.request.user)

	def save(self, instance, commit):
		# Prepare a 'save_m2m' method for the form,
		old_save_m2m = self.save_m2m

		def save_m2m():
			try:
				# save m2m 수행하고
				old_save_m2m()
				# form에 tag가 있으면 현재 instance에서 tag 추출하여 through의 user를
				# 현재 user로 바꾼다.
				if 'tag' in self.cleaned_data:
					tags = self.cleaned_data['tag']
					for tag in tags:
						for ta in instance.tag.all():
							if ta == tag:
								for lt in ta.linked_tag_tag_list.all():
									lt.user = self.request.user
									lt.save()


			except Exception as e:
				print(e)

		self.save_m2m = save_m2m

		# Do we need to save all changes now?
		if commit:
			instance.save()
			self.save_m2m()

	def rearrange_field_order(self):

		original_fields = self.fields
		new_fields = {}

		for field_name in self.ordered_field_names:
			field = original_fields.get(field_name)
			if field:
				new_fields[field_name] = field
		for other_field in original_fields:
			if other_field not in self.ordered_field_names:
				field = original_fields.get(other_field)
				new_fields[other_field] = field
		self.fields = new_fields

# form.Form
class BasicFormBase(ParentForm, forms.Form):


	def __init__(self, *args, **kwargs):
		ParentForm.init_argument(self, kwargs)
		super(BasicFormBase, self).__init__(*args, **kwargs)
		ParentForm.arrange_and_createtablehistory(self)

	def save_create(self):
		ParentForm.save_create(self)

	def save(self, commit=True):
		instance = forms.Form
		ParentForm.save(self, instance, commit)
		#super(BasicFormBase, self)
		return instance

# ModelForm
class ModelFormBase(ParentForm, ModelForm):


	def __init__(self, *args, **kwargs):
		ParentForm.init_argument(self, kwargs)
		super().__init__(*args, **kwargs)
		try:
			if not self.after_create_history:
				ParentForm.arrange_and_createtablehistory(self)
		except AttributeError:
			ParentForm.arrange_and_createtablehistory(self)

	def save_create(self):
		ParentForm.save_create(self)

	def save(self, commit=True):
		instance = forms.ModelForm.save(self, False)
		ParentForm.save(self, instance, commit)
		return instance

class MasterModelForm(ModelFormBase):
	ordered_field_names = ['receptionDate', 'patentType', 'nationType', 'countryCode']
	def __init__(self, *args, **kwargs):
		super(MasterModelForm, self).__init__(*args, **kwargs)
		self.rearrange_field_order()
		if self.labeldic:
			for i, (k, v) in enumerate(self.labeldic.items()):
				self.fields[k].label = v

				"""
				if k == 'applicationCountrycode':
					countryval = ['KR', 'EN', 'JP', 'CN']
					order = Case(*[When(countryCode=ccode, then=pos) for pos, ccode in enumerate(countryval)])
					self.fields[k].queryset = self.fields[k].queryset.order_by(order)
				"""
		if self.customcodechoice:
			self.fields['patentType'].choices = self.customcodechoice[ALL_CODE_TYPE.PatentType.name]
			self.fields['nationType'].choices = self.customcodechoice[ALL_CODE_TYPE.PatentNationType.name]
			self.fields['state'].choices = self.customcodechoice[ALL_CODE_TYPE.ApplicationPatent_Status.name]
			self.fields['countryCode'].choices = self.customcodechoice['countryCode']
			self.fields['client'].queryset = Client.objects.filter(client_clienttype__typeName='Client')
			self.fields['oversea_agent'].queryset = Client.objects.filter(client_clienttype__typeName='OverseaAgent')
			MakeTagChoiceMultipleLanguage(self)




	class Meta:
		model = Application_Patent_Domestic_Master
		fields = '__all__'
		widgets = {
			'receptionDate': forms.DateInput(attrs={'type': 'date'}),
			'priorityDate': forms.DateInput(attrs={'type': 'date'}),
			'draftFixedDate': forms.DateInput(attrs={'type': 'date'}),
			'draftSendDate': forms.DateInput(attrs={'type': 'date'}),
			'currentProgressDate': forms.DateInput(attrs={'type': 'date'}),
			'applicationDate': forms.DateInput(attrs={'type': 'date'}),
		}



class ApplicantModelForm(ModelFormBase):
	def __init__(self, *args, **kwargs):
		super(ApplicantModelForm, self).__init__(*args, **kwargs)
		if self.labeldic:
			for i, (k, v) in enumerate(self.labeldic.items()):
				if k != 'creationDate':
					self.fields[k].label = v
		if self.customcodechoice:
			self.fields['patentOfficeCode'].choices = self.customcodechoice['countryCode']
			MakeTagChoiceMultipleLanguage(self)
	class Meta:
		model = Application_Patent_Domestic_Applicant
		fields = '__all__'

class MainInventorModelForm(ModelFormBase):
	def __init__(self, *args, **kwargs):
		super(MainInventorModelForm, self).__init__(*args, **kwargs)
		if self.labeldic:
			for i, (k,v) in enumerate(self.labeldic.items()):
				self.fields[k].label = v
		if self.customcodechoice:
			MakeTagChoiceMultipleLanguage(self)
	class Meta:
		model = Main_Inventor
		fields = '__all__'

class AgentModelForm(ModelFormBase):
	def __init__(self, *args, **kwargs):
		super(AgentModelForm, self).__init__(*args, **kwargs)
		if self.labeldic:
			for i, (k, v) in enumerate(self.labeldic.items()):
				self.fields[k].label = v
		if self.customcodechoice:
			MakeTagChoiceMultipleLanguage(self)
	class Meta:
		model = Agent
		fields = '__all__'




