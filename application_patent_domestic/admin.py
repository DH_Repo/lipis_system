from django.contrib import admin
from application_patent_domestic.models import *
from django.utils.html import format_html
from django.urls import reverse

class ApplicantInline(admin.TabularInline):
    model = Application_Patent_Domestic_Applicant
    extra = 3

# Register your models here.



class MasterAdmin(admin.ModelAdmin):
    model = Application_Patent_Domestic_Master
    list_display = ['state', 'clientCompany', 'ourCompanyNumber', 'inventName', 'mainInventor', 'managerOfApplicant',
                    'managerOfOurCompany', 'receptionDate', 'priorityClaim', 'priorityDate', 'applicationRequestFixedDate',
                    'draftFixedDate', 'draftSendDate', 'currentProgress', 'currentProgressDate', 'applicationNumber',
                    'applicationDate', 'remarks',]
    #list_display_links = ['applicants',]
    """
    list_editable = ['state', 'clientCompany', 'ourCompanyNumber', 'inventName', 'mainInventor', 'managerOfApplicant',
                    'managerOfOurCompany', 'receptionDate', 'preferenceOpinion', 'priorityDate', 'applicationRequestFixedDate',
                    'draftFixedDate', 'draftSendDate', 'currentProgress', 'currentProgressDate', 'applicationNumber',
                    'applicationDate', 'remarks',]
                    """






admin.site.register(Application_Patent_Domestic_Master)
#admin.site.register(Application_Patent_Domestic_Master, MasterAdmin)
admin.site.register(Application_Patent_Domestic_Applicant)
admin.site.register(Main_Inventor)
admin.site.register(Agent)


admin.site.register(Search_Parameter)



