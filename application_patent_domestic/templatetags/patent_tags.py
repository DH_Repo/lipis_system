import datetime
import operator
from functools import reduce

from django import template
from django.db.models import Q

from People.models import Work_Email
from application_patent_domestic.models import Application_Patent_Domestic_Master
from common.codemultilanguage_choice import GetCodeMultpleLanguageForCase
from common.tableview_function import get_model_query_context_for_table_nopage_nosearch
from message.models import Message
from utils.enums import ALL_CODE_TYPE

register = template.Library()




@register.inclusion_tag('forinclude/patent/patent_simpleinfo_th.html', name='include_patent_simpleinfo', takes_context=True)
def include_patent_simpleinfo(context):
    context['patents'] = get_model_query_context_for_table_nopage_nosearch(Application_Patent_Domestic_Master,
                                                                    'Application_Patent_Domestic_Master', context, )
    return context


