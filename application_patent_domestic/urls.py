from django.urls import path
from application_patent_domestic import crudmodel, ajax
from sync_file import kipo_goods
from . import views



app_name = 'application_patent_domestic'
urlpatterns = [
    path('patentmaster', views.MasterView.as_view(), name='patent_master'),
    path('master/create', crudmodel.MasterCreate, name='master_create'),
    path('master/edit/<pk>', crudmodel.MasterEdit, name='master_edit'),
    path('master/delete', crudmodel.MasterDelete, name='master_delete'),
    path('master/<pk>', views.MasterDetailView.as_view(), name='master_detail'),
    path('master/search/ourcom', crudmodel.Master_SearchOurCom, name='master_search_ourcom'),
    path('master/relation/get', ajax.GetMasterRelation, name='master_relation_get'),
    path('master/filter/ajax/set', ajax.SetMasterFilter, name='master_filter_set'),


    path('applicant', views.ApplicantsView.as_view(), name='applicant'),
    path('applicant/create', crudmodel.ApplicantCreate, name='applicant_create'),
    path('applicant/edit/<pk>', crudmodel.ApplicantEdit, name='applicant_edit'),
    path('applicant/delete', crudmodel.ApplicantDelete, name='applicant_delete'),
    path('applicant/addmaster', views.LinkPatentMasterApplicant, name='applicant_addmaster'),
    path('applicant/deletemaster', views.DelLinkPatentMasterApplicant, name='applicant_deletemaster'),
    path('upload_brandimage/<pk>', views.UploadBrandImage, name='upload_brandimage'),
    path('inventorview', views.MainInventorView.as_view(), name='inventorview'),
    path('maininventor/create', crudmodel.MainInventorCreate, name='maininventor_create'),
    path('maininventor/edit/<pk>', crudmodel.MainInventorEdit, name='maininventor_edit'),
    path('maininventor/delete', crudmodel.MainInventorDelete, name='maininventor_delete'),

    path('agentview', views.AgentView.as_view(), name='agentview'),
    path('agent/create', crudmodel.AgentCreate, name='agent_create'),
    path('agent/edit/<pk>', crudmodel.AgentEdit, name='agent_edit'),
    path('agent/delete', crudmodel.AgentDelete, name='agent_delete'),

    path('kipogoods', views.KIPOGoodsView.as_view(), name='kipo_goods'),
    path('kipogoods/sync', views.KIPOGoodsSyncView.as_view(), name='kipo_goods_sync'),
    path('kipogoods/fileupload', views.KIPOGoodsExcelFileUpload, name='kipo_goods_fileupload'),
    path('kipogoods/filesync', kipo_goods.sync_kipogoods, name='kipo_goods_filesync')

]

