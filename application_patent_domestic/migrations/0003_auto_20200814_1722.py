# Generated by Django 3.0.6 on 2020-08-14 08:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application_patent_domestic', '0002_auto_20200803_1530'),
    ]

    operations = [
        migrations.AlterField(
            model_name='search_parameter',
            name='tableType',
            field=models.CharField(blank=True, choices=[('Master', 'Master'), ('Applicant', 'Applicant'), ('Client', 'Client'), ('Person', 'Person'), ('Email', 'Email'), ('Message', 'Message'), ('File', 'File'), ('MainInventor', 'MainInventor'), ('Agent', 'Agent'), ('Case', 'Case'), ('Task', 'Task'), ('Work_Email', 'Work_Email'), ('Revenue_Invoice', 'Revenue_Invoice'), ('Purchase_Invoice', 'Purchase_Invoice'), ('Transaction_History', 'Transaction_History'), ('BankAccount_Information', 'BankAccount_Information'), ('ApplicantInformation', 'ApplicantInformation'), ('Task_Template', 'Task_Template')], max_length=50, primary_key=True, serialize=False),
        ),
    ]
