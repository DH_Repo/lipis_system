function deleteModel(url, pk) {
    var ok = confirm("삭제하시겠습니까?");
    if (ok) {
        let formData = new FormData()
        formData.append('pk', pk)
        formData.append('csrfmiddlewaretoken', '{{ csrf_token }}');
        fetch(url, {
            method: 'POST',
            body: formData,
            credentials: 'same-origin',
        })
            .then(response => {

                    location.reload()
                }
            )
            .catch(() => {
                alert('fail')
            })
    } else {

    }
}


function newPopup(url) {

    count = localStorage.getItem("popcount");
    if (count == null) {
        localStorage.setItem("popcount", 1);
    } else {
        count++;
        localStorage.setItem("popcount", count);
    }

    window.open(
        url, 'popUpWindow' + count, 'height=800,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

function windowClose() {
    count = localStorage.getItem("popcount");
    localStorage.setItem("popcount", count--);
    window.open('', '_parent', '');
    window.close();
}

function menuclick(myid) {

    localStorage.setItem("id", myid);
}

function mainleftmenuclick(leftmenuid, myid) {

    localStorage.setItem("mainleftid", leftmenuid);
    if (myid != '') {
        localStorage.setItem("id", myid);
    }

}

function call_ajax(url, data, needresult, needreload, method='GET')
{
   var csrftoken = $("[name=csrfmiddlewaretoken]").val();
            $.ajax({
                type: method,
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {

                    if (needresult)
                    {
                         alert(data.result)
                    }
                    if (needreload)
                    {
                        location.reload();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ' Error: ' + thrownError);
            }
            });
}

function getCheckedTable(classname) {


    var ok = confirm("삭제하시겠습니까?");

    if (ok) {

        var selected_rows = [];

        $("." + classname).find('tr').each(function () {
            var row = $(this);
            if (row.find('input[type="checkbox"]').is(':checked')) {
                selected_rows.push(row.attr('data-id'));
            }
            ;
        });
        return JSON.stringify(selected_rows);

    } else {

    }
}

function deleteTableData(classname, action) {

    var ok = confirm("삭제하시겠습니까?");

    if (ok) {

        var selected_rows = [];

        $("." + classname).find('tr').each(function () {
            var row = $(this);
            if (row.find('input[type="checkbox"]').is(':checked')) {
                selected_rows.push(row.attr('data-id'));
            }
            ;
        });
        //var json = {'m_list_ids': selected_rows, 'csrfmiddlewaretoken': '{{ csrf_token }}'};
        var datas = JSON.stringify(selected_rows);

        $.ajax({
            url: action,
            type: "post",
            data: {'m_list_ids': datas, 'csrfmiddlewaretoken': "{{ csrf_token }}"},
            success: function (data) {
                alert('ss');
                window.location.reload();
            },


        })


    } else {

    }
}

function HideShowUsing(classname) {

  $("."+classname).toggle();
}


