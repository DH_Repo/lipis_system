import json

import requests
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView

from Lipis_system_master.views import HomeMixin
from patentro.forms import UserCreationForm, APIAuthenticationForm
from patentro.models import PatentroWebapi, PatentroWebapiRequest, PatentroWebapiAuthenticationInformation


class PatentroAPISetting(LoginRequiredMixin, HomeMixin , FormView):
    template_name = 'patentro/patentro_api_setting.html'
    success_url = reverse_lazy('patentro:patentro_api_setting')
    form_class = APIAuthenticationForm

    def form_valid(self, form):
        pk = self.request.POST.get('pk')
        form.instance.id = pk
        myform = form.save(commit=False)
        myform.save()
        form.save_m2m()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PatentroAPISetting, self).get_context_data(**kwargs)
        p_auth = PatentroWebapiAuthenticationInformation.objects.all().first()
        form = context.get('form')
        form.initial = p_auth.__dict__
        context['form'] = form
        context['model'] = p_auth
        return context

class PatentroAPIList(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'patentro/patentro_api_list.html'
    def get_context_data(self, **kwargs):
        context = super(PatentroAPIList, self).get_context_data(**kwargs)
        appal = PatentroWebapi.objects.all().order_by('func_no')
        context['api_list'] = appal
        return context

#  <pk> get parameter 로 호출된 page에서 post 시 pk를 바로 받을 수 있다.
#  post 시 form 데이터로 작업 후, 다시 기존 form에 data 전달할 수 있다.
class PatentAPIDetail(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'patentro/patentro_api_detail.html'
    def get_context_data(self, **kwargs):
        context = super(PatentAPIDetail, self).get_context_data(**kwargs)
        appal = PatentroWebapi.objects.all().order_by('func_no')
        context['api_list'] = appal
        return context

    def get(self, request, pk, *args, **kwargs):
        context = self.get_context_data()
        inputlist = PatentroWebapiRequest.objects.filter(func_no=pk)
        context['form'] = UserCreationForm(extra=inputlist)
        context['pk'] = pk
        return self.render_to_response(context)

    def post(self, request, pk):
        context = self.get_context_data()
        appal = PatentroWebapi.objects.filter(func_no=pk)
        inputlist = PatentroWebapiRequest.objects.filter(func_no=pk)
        form = UserCreationForm(request.POST, extra=inputlist)
        if form.is_valid():
            for api in appal:
                rest_uri = api.rest_uri
                service = api.service_category
            url = "https://www.patent.go.kr/smart/webservice/" + service + "/" + rest_uri + "?"
            p_auth = PatentroWebapiAuthenticationInformation.objects.all().first()
            url += 'ctfctKey=' + p_auth.authKey + '&apagtCd=' + p_auth.apagtCd + '&'
            for (name, value) in (form.extra_input(extra=inputlist)):
                if name != "":
                    url = url + name + "=" + value + "&"

            urlstrip = url.strip('&')
            response = requests.get(urlstrip)
            context['result'] = json.loads(response.text)
            context['form'] = form
        return self.render_to_response(context)




