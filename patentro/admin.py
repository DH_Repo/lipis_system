from django.contrib import admin

# Register your models here.
from patentro.models import PatentroWebapi, PatentroWebapiRequest

admin.site.register(PatentroWebapi)
admin.site.register(PatentroWebapiRequest)
