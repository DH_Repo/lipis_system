from django.db import models



class PatentroWebapiAuthenticationInformation(models.Model):
    authKey = models.CharField(max_length=500, blank=True, null=True)
    apagtCd = models.CharField(max_length=100, blank=True, null=True)
    class Meta:
        app_label = 'patentro'
        db_table = 'patentro_webapi_authentication'
# Create your models here.
class PatentroWebapi(models.Model):
    func_no = models.IntegerField(primary_key=True)
    func_name = models.CharField(max_length=100, blank=True, null=True)
    service_name = models.CharField(max_length=500, blank=True, null=True)
    service_category = models.CharField(max_length=50, blank=True, null=True)
    service_desc = models.CharField(max_length=1000, blank=True, null=True)
    rest_uri = models.CharField(max_length=100, blank=True, null=True)
    response_last_item = models.CharField(max_length=100, blank=True, null=True)
    response_last_item_type = models.CharField(max_length=50, blank=True, null=True)
    response_last_item_desc = models.CharField(max_length=1000, blank=True, null=True)
    response_last_item_remark = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        app_label = 'patentro'
        db_table = 'patentro_webapi'




class PatentroWebapiRequest(models.Model):
    id = models.IntegerField(primary_key=True)
    func_no = models.IntegerField(blank=True, null=True)
    request_item = models.CharField(max_length=100, blank=True, null=True)
    request_item_type = models.CharField(max_length=50, blank=True, null=True)
    request_item_desc = models.CharField(max_length=500, blank=True, null=True)
    request_item_isessential = models.CharField(max_length=50, blank=True, null=True)
    request_item_remark = models.CharField(max_length=1000, blank=True, null=True)


    def save(self, **kwargs):
        raise NotImplementedError()
    class Meta:
        app_label = 'patentro'
        db_table = 'patentro_webapi_request'