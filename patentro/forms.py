from django import forms

from application_patent_domestic.forms import ModelFormBase
from patentro.models import PatentroWebapiAuthenticationInformation


class APIAuthenticationForm(ModelFormBase):
    def __init__(self, *args, **kwargs):
        super(APIAuthenticationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PatentroWebapiAuthenticationInformation
        fields = "__all__"

class UserCreationForm(forms.Form):
    def __init__(self, *args, **kwargs):
        extra = kwargs.pop('extra')
        super(UserCreationForm, self).__init__(*args, **kwargs)

        for i, input in enumerate(extra):
            if input.request_item == "signKey":
                pass
            else:
                if input.request_item_isessential == "OPTIONAL":
                    self.fields[input.request_item] = forms.CharField(label=input.request_item_desc, required=False)
                else:
                    self.fields[input.request_item] = forms.CharField(label=input.request_item_desc + ' *', required=True)

    def extra_input(self, extra=None):
        for i, input in enumerate(extra):
            if input.request_item == "signKey":
                pass
            elif input.request_item_isessential == "OPTIONAL":
                yield ("","")
            else:
                value = self.cleaned_data[input.request_item]
                yield (input.request_item, value)