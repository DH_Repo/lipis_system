
from django.urls import path

from patentro import views

app_name = 'patentro'
urlpatterns = [
            path('api/list', views.PatentroAPIList.as_view(), name='patentroapilist'),
            path('api/detail/<pk>', views.PatentAPIDetail.as_view(), name='patentroapidetail'),
            path('api/setting', views.PatentroAPISetting.as_view(), name='patentro_api_setting'),
    ]