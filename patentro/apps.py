from django.apps import AppConfig


class PatentroConfig(AppConfig):
    name = 'patentro'
