from __future__ import absolute_import, unicode_literals
import traceback
import imaplib

import django
import django_celery_beat
import django_celery_results
from celery import states
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import redirect
from django_celery_results.models import TaskResult

from Lipis_system_master.celery import app
from emailsystem.SyncEmail.sync_eml import eml_parse_frombyte
from emailsystem.models import Email_Info



@app.task()
def syncmail_task():

    print('---------task start----------')
    for ei in Email_Info.objects.filter(isActive=True).all():
        try:
            imapserver = ''
            authuser = ei.user
            user = ei.syncEmail
            password = ei.password
            imapsrv = ei.imapServer
            imapport = ei.imapPort
            #imaplib._MAXLINE = 20480
            imapserver = imaplib.IMAP4_SSL(imapsrv, imapport)
            emailcount = ei.syncEmailCount
            imapserver.login(user, password)
            imapserver.select('inbox')
            res, unseen_data = imapserver.search(None, '(ALL)')
            ids = unseen_data[0]
            id_list = ids.split()
            from django_celery_beat.models import PeriodicTask, CrontabSchedule
            pt = PeriodicTask.objects.get(name='sync_email_task')
            cs = CrontabSchedule.objects.get(id=pt.crontab.id)

            if emailcount == 0:
                cs.minute = '*/8'
                cs.save()
                id_list.reverse()
                latest_email_id= id_list
            else:
                if emailcount <= 20:
                    cs.minute = '*/2'
                    cs.save()
                elif emailcount <= 200:
                    cs.minute = '*/5'
                    cs.save()
                else:
                    cs.minute = '*/8'
                    cs.save()
                latest_email_id = id_list[-emailcount:]
            try:
                print("get email count : {}".format(len(latest_email_id)))
                for index ,each_mail in enumerate(latest_email_id):
                    # fetch the email body (RFC822) for the given ID
                    try:
                        result, data = imapserver.fetch(each_mail, "(RFC822)")
                        eml_parse_frombyte(data[0][1], authuser, ei, index, user)
                    except Exception as e:
                        print('exception email index {}'.format(index))
                        print(e)
                        traceback.print_exc()
                        continue
                #django_celery_results.backends.cleanup()
                #django_celery_results.managers.get_all_expired(3600)
                if TaskResult.objects.count() > 100:
                    tid = TaskResult.objects.first().id
                    TaskResult.objects.filter(~Q(id=tid)).delete()



            except Exception as e:
                print(e)
                imapshutdown(imapserver)
            imapshutdown(imapserver)
        except Exception as e:
            print(e)
            imapshutdown(imapserver)

def imapshutdown(imapserver):
    if imapserver:
        imapserver.logout()

@login_required(login_url='People:login')
def sync_email(request):
    print('start!!!!!!!!!!!!!!!')

    emailsettingid = request.GET.get('emailsettingid')
    if emailsettingid:
        ei = Email_Info.objects.get(id=emailsettingid)
        start_specific_sync_email(ei, request)
    else:
        for ei in Email_Info.objects.filter(isActive=True).all():
            start_specific_sync_email(ei, request)

def start_specific_sync_email(ei, request):
    try:
        imapserver = ''
        authuser = ei.user
        user = ei.syncEmail
        password = ei.password
        imapsrv = ei.imapServer
        imapport = ei.imapPort
        imaplib._MAXLINE = 20480
        imapserver = imaplib.IMAP4_SSL(imapsrv, imapport)
        emailcount = ei.syncEmailCount

        imapserver.login(user, password)

        #imapserver.select('inbox')
        #a = imapserver.getquotaroot('Inbox')
        #l = imapserver.list()
        imapserver.select('inbox')
        res, unseen_data = imapserver.search(None, '(ALL)')

        ids = unseen_data[0]
        id_list = ids.split()
        if emailcount == 0:
            id_list.reverse()
            latest_email_id = id_list
        else:
            latest_email_id = id_list[-emailcount:]
        try:

            for index, each_mail in enumerate(latest_email_id):
                # fetch the email body (RFC822) for the given ID
                try:
                    result, data = imapserver.fetch(each_mail, "(RFC822)")
                    eml_parse_frombyte(data[0][1], authuser, ei, index, user, request)
                except Exception as e:
                    print('exception email index {}'.format(index))
                    print(e)
                    traceback.print_exc()
                    continue

        except Exception as e:
            print(e)
            imapshutdown(imapserver)
        imapshutdown(imapserver)
    except Exception as e:
        print(e)
        imapshutdown(imapserver)
