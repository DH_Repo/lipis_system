import datetime
import os
from uuid import uuid4

from django.db import models

# Create your models here.
from django.utils import timezone
from django.utils.timezone import now

from management.models import BaseModel
from utils.enums import EMAILTYPE


class Email_Detail(BaseModel):
    emailId = models.CharField(max_length=500, blank=False, unique=True, null=False, default='')
    subject = models.CharField(max_length=300, blank=True, null=True)
    body = models.CharField(max_length=2000000, blank=True, null=True)
    textBody = models.CharField(max_length=2000000, blank=True, null=True)
    body_rtf = models.CharField(max_length=2000000, blank=True, null=True)
    fromEmail = models.ForeignKey('People.Work_Email', related_name='from_email', null=True, blank=True, default='', on_delete=models.SET_NULL)
    header = models.CharField(max_length=200000, blank=True, null=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    emailInfo = models.ForeignKey('Email_Info', on_delete=models.SET_NULL, null=True, blank=False, default='')
    isFromFile = models.BooleanField(null=False, blank=False, default=False)
    user = models.ForeignKey('auth.User', on_delete=models.SET_NULL, null=True, default='')
    duplicateReference = models.ManyToManyField('self', through='Email_DuplicateEmail',blank=True, related_name='email_duplicateemail')
    sentDate = models.DateTimeField(null=True, blank=True)
    confirmAdd = models.BooleanField(null=True, blank=True, default=True)
    emailFileName = models.CharField(max_length=300, null=True, blank=True)
    emailType = models.CharField(max_length=30, choices=[(tag.name, tag.value) for tag in EMAILTYPE], null=True, blank=True)
    to_person = models.ManyToManyField(
        'People.Work_Email',
        through='EmailTo_WorkEmail',
        related_name='to_workemail', blank=True
    )
    cc_person = models.ManyToManyField(
        'People.Work_Email',
        through='EmailCC_WorkEmail',
        related_name='cc_workemail', blank=True
    )
    client = models.ManyToManyField(
        'People.Client',
        through='EmailDetail_Client',
        related_name='emaildetail_client', blank=True
    )
    class Meta:
        db_table = 'email_detail'

class Email_DuplicateEmail(models.Model):
    childemail = models.ForeignKey('Email_Detail'
                               , on_delete=models.CASCADE, related_name='email_child')
    referenceemail = models.ForeignKey('Email_Detail'

                                  , on_delete=models.CASCADE, related_name='email_referecne')

    class Meta:
        db_table = 'email_duplicate_email'
        unique_together = ('childemail', 'referenceemail')

class EmailTo_WorkEmail(models.Model):
    emailTo = models.ForeignKey('Email_Detail'
                                                           , on_delete=models.CASCADE, related_name='emailto_workemail')
    work_email = models.ForeignKey('People.Work_Email'

                                                    , on_delete=models.CASCADE, related_name='emailto_workemail')
    priority = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = "emailto_workemail"

class EmailCC_WorkEmail(models.Model):
    emailCC = models.ForeignKey('Email_Detail'
                                                           , on_delete=models.CASCADE, related_name='emailcc_workemail')
    work_email = models.ForeignKey('People.Work_Email'

                                                    , on_delete=models.CASCADE, related_name='emailcc_workemail')
    priority = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = "emailcc_workemail"
"""
class Attachment(models.Model):

    def date_upload_to(instance, filename):
        # upload_to="%Y/%m/%d" 처럼 날짜로 세분화
        ymd_path = timezone.now().strftime('%Y/%m/%d')
        # 길이 32 인 uuid 값
        uuid_name = uuid4().hex
        # 확장자 추출
        extension = os.path.splitext(filename)[-1].lower()
        # 결합 후 return
        return '/'.join([
            'images', ymd_path,
            uuid_name + extension,
        ])

    path = models.FileField()
    createdDate = models.DateTimeField(auto_now_add=True)
    emailsystem = models.ForeignKey('Email', on_delete=models.CASCADE, null=False)
    class Meta:
        db_table = 'attachment'
"""
class Email_Info(BaseModel):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=False, default='')
    syncEmail = models.EmailField(blank=True)
    password = models.CharField(max_length=50, null=True, blank=True)
    imapServer = models.CharField(max_length=30, null=True, blank=True)
    imapPort = models.CharField(max_length=10, null=True, blank=True)
    popServer = models.CharField(max_length=30, null=True, blank=True)
    popPort = models.CharField(max_length=10, null=True, blank=True)
    isImap = models.BooleanField(null=True, blank=True)
    isActive = models.BooleanField(null=False, blank=False,default=True)
    syncEmailCount = models.IntegerField(null=True,blank=True)
    smtpServer = models.CharField(max_length=30, null=True, blank=True)
    smtpPort = models.CharField(max_length=10, null=True, blank=True)
    ack_content = models.ForeignKey('Email_ACK_Content', on_delete=models.SET_NULL, related_name='emailinfo_ackcontent', null=True)
    signature = models.ForeignKey('Email_Signature', on_delete=models.SET_NULL, related_name='emailinfo_signature',
                                    null=True)
    class Meta:
        db_table = 'email_info'
        unique_together = (('user', 'syncEmail'),)

class EmailDetail_Client(models.Model):
    email_detail = models.ForeignKey('Email_Detail', on_delete=models.CASCADE, related_name='emaildetail_client_emaildetail')
    client = models.ForeignKey('People.Client', on_delete=models.CASCADE, related_name='emaildetail_client_client')
    class Meta:
        db_table = "emaildetail_client"

class Email_ACK_Content(models.Model):
    ack_content = models.CharField(max_length=3000, null=True, blank=True)
    class Meta:
        db_table = "email_ack_content"

class Email_Signature(models.Model):
    signature = models.CharField(max_length=5000, null=True, blank=True)
    class Meta:
        db_table = "email_signature"


class Email_Option_User_Choice(models.Model):
    email_option_code = models.ForeignKey('Email_Option_Code', on_delete=models.CASCADE, null=True,
                                     related_name='email_option_user_choice_email_option_code')
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='email_option_user_choice_user')
    email_option = models.ForeignKey('Email_Option', on_delete=models.CASCADE,
                                     related_name='email_option_user_choice_email_option')
    class Meta:
        db_table = "email_option_user_choice"
        unique_together = ['user', 'email_option']
class Email_Option(models.Model):
    optionName = models.CharField(max_length=100, null=False, blank=False, default="option name")

    def __str__(self):
        return self.optionName
    class Meta:
        db_table = "email_option"

class Email_Option_Code(models.Model):
    code = models.CharField(max_length=50, null=False, blank=False, default="code name")
    explanation = models.CharField(max_length=50, null=False, blank=False, default="code name")
    email_option = models.ForeignKey('Email_Option', on_delete=models.CASCADE, related_name='emailoption_emailoptioncode')
    def __str__(self):
        return self.code + " (" +self.explanation+")"
    class Meta:
        db_table = "email_option_code"