import imaplib
import traceback
from datetime import datetime
from email import message_from_bytes
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import make_msgid
from textwrap import dedent
import smtplib
import eml_parser
import html2text as html2text
from django.shortcuts import redirect, render
from django.test import TestCase

# Create your tests here.
from application_patent_domestic.models import Application_Patent_Domestic_Master
from emailsystem.SyncEmail.sync_eml import eml_parse_frombyte
from emailsystem.models import Email_Info, Email_Detail


def syncemail(request):
    ERROR_KEY = "ERROR"
    d = ''

    return render(request, 'management/commoninfo_form.html', {'status': d})


def sendemail(request):
    ei = Email_Info.objects.get(user=request.user)
    content = ei.ack_content.ack_content
    plaincontent = html2text.html2text(content)
    signature = ei.signature.signature
    plainsignature = html2text.html2text(signature)
    ackhtml = content + '\n' + '===============================================================================\n' + \
              signature + '<p style="width:700px; word-break:normal; border:0.1px solid #d2d2d2"></p>'
    ackplain = plaincontent + '\n' + '===============================================================================\n' + \
               plainsignature + '<p style="width:700px; word-break:normal; border:0.1px solid #d2d2d2"></p>'

    emaildetail = Email_Detail.objects.get(id=32137)
    mymsg = emaildetail.msg_email.first()



    user = ei.syncEmail
    password = ei.password
    smtpsrv = ei.smtpServer
    smtpport = ei.smtpPort
    imaplib._MAXLINE = 20480

    smtp = smtplib.SMTP_SSL(smtpsrv, smtpport)
    smtp.login(user, password)


    mail = MIMEMultipart('alternative')
    mail['Message-ID'] = make_msgid(domain='liboip.com')
    mail['References'] = mail['In-Reply-To'] = emaildetail.emailId
    mail['Subject'] = 'Re: ' + emaildetail.subject
    mail['From'] = "libo@liboip.com"
    mail['To'] = "dh.lee@lipis.kr"
    #emaildetail.fromEmail.email
    mail.attach(MIMEText(dedent(ackplain+'\n'+emaildetail.textBody), 'plain'))
    mail.attach(MIMEText(ackhtml + '\n' + emaildetail.body, 'html'))
    smtp.sendmail(
        "libo@liboip.com", ["dh.lee@lipis.kr"],
        mail.as_bytes())

    """
    ids = data[0]
    id_list = ids.split()
    for index, each_mail in enumerate(id_list):
        try:
            result, data = smtp.fetch(each_mail, "(RFC822)")
            emlparser = eml_parser.eml_parser.EmlParser(include_raw_body=True,
                                                        include_attachment_data=True,
                                                        email_force_tld=True,
                                                        parse_attachments=True)
            parsed_eml = emlparser.decode_email_bytes(data[0][1])
            header = parsed_eml['header']
            msgid = header['header']['message-id'][0]
        except Exception as e:
            print('exception email index {}'.format(index))
            print(e)
            traceback.print_exc()
            continue
    """
    return redirect('management:common_mgr')







