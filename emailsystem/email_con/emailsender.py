import datetime
import smtplib
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import make_msgid, formatdate
from textwrap import dedent

import html2text
import pytz
from bs4 import BeautifulSoup


def send_ackemail(originemail, emailinfo):
    try:
        content = emailinfo.ack_content.ack_content
        plaincontent = html2text.html2text(content)
        signature = emailinfo.signature.signature
        plainsignature = html2text.html2text(signature)
        ackhtml = content + '\n' + '===============================================================================\n' + \
                  signature + '<p style="width:700px; word-break:normal; border:0.1px solid #d2d2d2"></p>'
        ackplain = plaincontent + '\n' + '===============================================================================\n' + \
                   plainsignature
        soup = BeautifulSoup(originemail.body, 'html.parser')
        fontfamily =''
        for tag in soup.find_all():
            if 'style' in tag.attrs:
                attrs = tag.attrs['style'].split(';')
                for attr in attrs:
                    if 'font-family' in attr:
                        fontfamily = attr+';'
                        break

        if not fontfamily:
            fontfamily = 'font-family:돋움,Dotum;'

        tohtml = ''
        toplaintext = ''
        toall = originemail.to_person.all()
        tocount = len(toall)
        for i,oe in enumerate(toall):
            if i == tocount - 1:
                tohtml = tohtml + oe.emailName + "&lt;"+ oe.email +"&gt;"
                toplaintext = toplaintext + oe.emailName + '<' + oe.email + '>'
            else:
                tohtml = tohtml + oe.emailName + "&lt;" + oe.email + "&gt;,"
                toplaintext = toplaintext + oe.emailName + '<' + oe.email + '>,'

        cchtml = ''
        ccplaintext = ''
        ccall = originemail.cc_person.all()
        cccount = len(ccall)
        for i, oe in enumerate(ccall):
            if i == cccount - 1:
                cchtml = cchtml + oe.emailName + " &lt;" + oe.email + "&gt;"
                ccplaintext = ccplaintext + oe.emailName + '<' + oe.email + '>'
            else:
                cchtml = cchtml + oe.emailName + " &lt;" + oe.email + "&gt;,"
                ccplaintext = ccplaintext + oe.emailName + '<' + oe.email + '>,'
        date = originemail.sentDate.strftime('%Y-%m-%d %H:%M:%S')
        referenceplaintext = "----- Original Message -----\n From: " + originemail.fromEmail.emailName + '<' + originemail.fromEmail.email + '> \n'+\
        "To: " + toplaintext + "\n" + \
        "Cc: " + ccplaintext + "\n" + \
        "Sent: " + date + "\n" + \
        "Subject: " + originemail.subject + "\n\n\n"

        referencehtml = "<font style = \""+ fontfamily + "font-size:10pt;\" > ----- Original Message ----- <br>" +\
        "From: " + originemail.fromEmail.emailName + " &lt;" + originemail.fromEmail.email + "&gt; <br>" +\
        "To: " + tohtml + "<br>"+ \
        "Cc: " + cchtml + "<br>" +\
        "Sent: " + date + "<br>" + \
        "Subject: " + originemail.subject + "<br> <br> <br> </font>"

        ackhtml = ackhtml + referencehtml
        ackplain = ackplain + referenceplaintext
        user = emailinfo.syncEmail
        password = emailinfo.password
        smtpsrv = emailinfo.smtpServer
        smtpport = emailinfo.smtpPort
        smtplib._MAXLINE = 20480

        for i in range(5):
            try:
                smtp = smtplib.SMTP_SSL(smtpsrv, smtpport)
                smtp.login(user, password)
                break
            except:
                time.sleep(1)
                continue


        domain = emailinfo.syncEmail.split('@')[1]
        mail = MIMEMultipart('alternative')
        mail['Message-ID'] = make_msgid(domain=domain)
        mail['References'] = mail['In-Reply-To'] = originemail.emailId
        mail['Subject'] = 'RE: ' + originemail.subject
        mail['From'] = emailinfo.syncEmail
        mail['To'] = originemail.fromEmail.email
        utc = pytz.utc.localize(datetime.datetime.utcnow())
        mail['Date'] = formatdate(usegmt=True)
        # emaildetail.fromEmail.email
        mail.attach(MIMEText(dedent(ackplain + '\n' + originemail.textBody), 'plain'))
        mail.attach(MIMEText(ackhtml + '\n' + originemail.body, 'html'))
        smtp.sendmail(
            emailinfo.syncEmail, [originemail.fromEmail.email],
            mail.as_bytes())



        mail = MIMEMultipart('alternative')
        mail['Message-ID'] = make_msgid(domain=domain)
        mail['References'] = mail['In-Reply-To'] = originemail.emailId
        mail['Subject'] = 'RE: ' + originemail.subject
        mail['From'] = emailinfo.syncEmail
        mail['To'] = originemail.fromEmail.email
        utc = pytz.utc.localize(datetime.datetime.utcnow())
        mail['Date'] = formatdate(usegmt=True)
        # emaildetail.fromEmail.email
        mail.attach(MIMEText(dedent(ackplain + '\n' + originemail.textBody), 'plain'))
        mail.attach(MIMEText(ackhtml + '\n' + originemail.body, 'html'))
        smtp.sendmail(
            emailinfo.syncEmail, emailinfo.syncEmail,
            mail.as_bytes())
        smtp.close()
        return "success"
    except Exception as e:
        smtp.close()
        return str(e)





