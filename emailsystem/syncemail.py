import glob
import json
import mimetypes
import os
import shutil
import smtplib
import imaplib
import poplib
import email
import tempfile
import time
import traceback
from email import policy
from email.header import decode_header
from email.message import EmailMessage, Message
from datetime import datetime, timedelta
from email.parser import BytesParser, Parser
from html.parser import HTMLParser
from os import path
from time import sleep

import compressed_rtf
import django.utils
import eml_parser
import extract_msg



from bs4 import BeautifulSoup
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse

from pytz import timezone
from django.shortcuts import get_object_or_404, redirect
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
import re

from six import unichr

import message
from Lipis_system_master.settings import BASE_DIR, STATICFILES_DIRS
from People.models import Person, Work_Email
from application_patent_domestic.models import Application_Patent_Domestic_Master
from emailsystem.models import Email_Info, Email_Detail, Email_DuplicateEmail

from message.models import File, Application_Patent_Domestic_Master_Message
from utils.dir_mgr import make_attachment_path
from utils.enums import FILETYPE, UPLOAD_FILE_PATH, MESSAGE_TYPE, MESSAGE_STATE, REGEX_APPLCODE_REFCODE




def check_similar_email(title, contents, sendeddate, to, cc, fromemail):

    start = sendeddate - timedelta(0, 3)
    end = sendeddate + timedelta(0, 3)

    to.sort(key=lambda x: x.id)
    cc.sort(key=lambda x: x.id)
    try:
        tolist = []
        cclist = []
        result = []
        emaild = Email_Detail.objects.filter(subject=title, body=contents, sentDate__range=[start, end], fromEmail=fromemail)
        if emaild.count() > 0:
            for eobj in emaild.all():
                for pemail in eobj.to_person.all():
                    tolist.append(pemail)
                for cemail in eobj.cc_person.all():
                    cclist.append(cemail)
                tolist.sort(key=lambda x: x.id)
                cclist.sort(key=lambda x: x.id)
                if to == tolist and cc == cclist:
                    result.append(eobj)
            return result
    except Exception as e:
        print(e)
        return result


def Link_applNo_ourCompanyNumber_toMaster(emailcontent):
    nContent = emailcontent.replace(' ','').replace('-','')
    matchobj = []
    regex = re.compile(REGEX_APPLCODE_REFCODE.RefOurCom.value)
    for tu in regex.findall(nContent):
        matchobj.append(tu[0])
    matchobj = list(set(matchobj))
    masterlist = []
    if matchobj:
        #tobj = matchobj.group()
        for obj in Application_Patent_Domestic_Master.objects.all():
            for match in matchobj:
                if obj.ourCompanyNumber.replace('-','') == match.replace('-',''):
                    masterlist.append(obj)


    regex = re.compile(REGEX_APPLCODE_REFCODE.applNo.value)
    matchobj = list(set(regex.findall(nContent)))
    if matchobj:
        #tobj = matchobj.group()
        for obj in Application_Patent_Domestic_Master.objects.all():
            for match in matchobj:
                if obj.applicationNumber.replace('-','') == match.replace('-',''):
                    masterlist.append(obj)


    return list(set(masterlist))

def set_message_for_email(email):

    msg = message.models.Message()
    msg.type = MESSAGE_TYPE.EM.name
    msg.email = email
    msg.state = MESSAGE_STATE.unspecified.name
    obj = Link_applNo_ourCompanyNumber_toMaster(email.subject+' '+email.body)
    msg.save()
    if obj:
        msg.isClassified = True
        msg.save()
        for m in obj:
        #msg.master_message.add(obj)
            Application_Patent_Domestic_Master_Message.objects.get_or_create(master=m, message=msg)
    return msg



def check_email_format_return(email):
    # pass the regualar expression
    # and the string in search() method
    if type(email) != str:
        return None

    regex = re.compile(r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
    rsearch = regex.search(email.replace('<','').replace('>',''))
    if rsearch:
        return rsearch.group()

def check_email_format(email):
    # pass the regualar expression
    # and the string in search() method
    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    if (re.search(regex, email)):
        return True

    else:
        return False

def json_serial(obj):
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial



class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        print("Encountered a start tag:", tag)

    def handle_endtag(self, tag):
        print("Encountered an end tag :", tag)

    def handle_data(self, data):
        print("Encountered some data  :", data)








def TestEmail(request):
    emaildetail1 = Email_Detail()
    emaildetail1.emailId = 't1'
    emaildetail1.subject = 'hhi'
    emaildetail1.body_rtf = 'contentt'
    emaildetail1.receivedDate = datetime.now()
    emaildetail1.createdDate = datetime.now()
    emaildetail1.fromEmail = 'email@email.com'
    emaildetail1.user = request.user
    emaildetail1.save()

    emaildetail2 = Email_Detail()
    emaildetail2.emailId = 't2'
    emaildetail2.subject = 'hhi'
    emaildetail2.body_rtf = 'contentt'
    emaildetail2.receivedDate = datetime.now()
    emaildetail2.createdDate = datetime.now()
    emaildetail2.fromEmail = 'email@email.com'
    emaildetail2.user = request.user
    emaildetail2.save()

    emaildetail = Email_Detail()
    emaildetail.emailId = 't3'
    emaildetail.subject = 'hhi'
    emaildetail.body_rtf = 'contentt'
    emaildetail.receivedDate = datetime.now()
    emaildetail.createdDate = datetime.now()
    emaildetail.fromEmail = 'email@email.com'
    emaildetail.user = request.user
    emaildetail.save()
    obj1, created = Email_DuplicateEmail.objects.get_or_create(childemail=emaildetail, referenceemail=emaildetail1)
    obj2, created = Email_DuplicateEmail.objects.get_or_create(childemail=emaildetail, referenceemail=emaildetail2)

    #emaildetail1.duplicateReference.set(obj2)
    #Email_DuplicateEmail.objects.get_or_create(childemail=emaildetail, referenceemail=emaildetail1)
    #Email_DuplicateEmail.objects.get_or_create(childemail=emaildetail, referenceemail=emaildetail2)




    return redirect('emailsystem:emailupload')

def get_part_filename(msg: EmailMessage):
    filename = msg.get_filename()
    if filename is not None and decode_header(filename)[0][1] is not None:
        filename = decode_header(filename)[0][0].decode(decode_header(filename)[0][1])

    return filename



@login_required(login_url='People:login')
def sync_email(request, pk):
    
    imaplib._MAXLINE = 20480

    emailinfo = get_object_or_404(Email_Info, id=pk)


    password = emailinfo.password
    if emailinfo.isImap:
        server = emailinfo.imapServer
        port = emailinfo.imapPort
        myserver = imaplib.IMAP4_SSL(server, port)

    else:
        server = emailinfo.imapServer
        port = emailinfo.imapPort
        myserver = poplib.IMAP4_SSL(server, port)

    myserver.login(request.user.email, password)
    myserver.select('inbox')
    res, unseen_data = myserver.search(None, '(ALL)')

    ids = unseen_data[0]
    id_list = ids.split()
    latest_email_id = id_list

    try:
        for each_mail in latest_email_id:
            # fetch the emailsystem body (RFC822) for the given ID
            result, data = myserver.fetch(each_mail, "(RFC822)")

            """
            id = email.message_from_bytes(data[0][0])
            tid = id.get_payload(decode=True)
            id = tid.decode('utf-8')
            print(id)
            mails = Email_Detail.objects.filter(emailId=id)
            if len(mails) != 0:
                continue
            """


            msg = email.message_from_bytes(data[0][1])
            emailId = decode_header(msg['Message-Id'])[0][0]
            # 아이디 입력
            ed = Email_Detail.objects.filter(emailId=emailId)
            if ed.count() > 0:
                continue
            emails = Email_Detail()
            emails.emailId = emailId
            # 수신 날짜 얻기
            rece = decode_header(msg['Received'])[0][0]
            datetime_object = datetime.strptime(rece.split(';')[1].strip(), '%d %b %Y %H:%M:%S %z')
            KST = timezone('Asia/Seoul')
            emails.receivedDate = datetime_object.astimezone(KST)

            filepath = make_attachment_path(request.user.username)
            checkattach = False
            ## 제목
            subject = decode_header(msg['subject'])
            subjectstr = ''

            for s in subject:
                try:
                    subjectstr = subjectstr + s[0].decode('utf-8')
                except:
                    subjectstr = subjectstr + s[0].decode('euc-kr')
            emails.subject = subjectstr

            ## 받는 사람
            to = decode_header(msg['to'])
            receiverstr = ''

            for r in to:
                if isinstance(r[0], str):
                    receiverstr = r[0]
                    break
                try:
                    receiverstr = receiverstr + r[0].decode('utf-8')
                except:
                    receiverstr = receiverstr + r[0].decode('euc-kr')
            if '<' in receiverstr:
                startindex = receiverstr.index('<')
                stopindex = receiverstr.index('>')
                receiverstr = receiverstr[startindex + 1:stopindex]
            emails.toEmail = receiverstr

            ## 보낸 사람
            sender = decode_header(msg['from'])
            sernderstr = ''
            for ss in sender:
                if isinstance(ss[0], str):
                    sernderstr = ss[0]
                    break
                try:
                    sernderstr = sernderstr + ss[0].decode('utf-8')
                except:
                    sernderstr = sernderstr + ss[0].decode('euc-kr')
            if '<' in sernderstr:
                startindex = sernderstr.index('<')
                stopindex = sernderstr.index('>')
                sernderstr = sernderstr[startindex + 1:stopindex]
            emails.fromEmail = sernderstr

            emails.emailInfo = emailinfo
            while msg.is_multipart():
                msg = msg.get_payload(0)

            content = msg.get_payload(decode=True)
            # savefile(content)
            try:
                txt_content = content.decode('UTF-8')
            except Exception as e:
                txt_content = content.decode('EUC-KR')
            emails.body = txt_content
            emails.isFromFile = False
            emails.save()

            msg = email.message_from_bytes(data[0][1])
            for part in msg.walk():

                # this part comes from the snipped I don't understand yet...
                if part.get_content_maintype() == 'multipart':
                    continue
                if part.get('Content-Disposition') is None:
                    continue
                attach = File()
                filename = get_part_filename(part)
                if filename is not None:
                    attach.originFileName = filename
                    customfilename = datetime.today().strftime('%Y%m%d%H%M%S%f')+'.'+filename.split('.')[1]
                    sv_path = os.path.join(filepath, customfilename)
                    attach.path = sv_path

                    if not os.path.isfile(sv_path):
                        fp = open(sv_path, 'wb')
                        fp.write(part.get_payload(decode=True))
                        fp.close()
                        attach.email = emails
                        attach.place = FILETYPE.sync_email.name
                        attach.save()


            set_message_for_email(emails)
            sleep(2)




    except Exception as e:
        print(e)
        print(traceback.format_exc())
    finally:
        myserver.close()
        myserver.logout()
        return redirect('emailsystem:emailsetting')
