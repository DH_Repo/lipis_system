import email
from datetime import datetime
import os
from email.header import decode_header

import compressed_rtf
import extract_msg


from People.models import Work_Email
from emailsystem.models import Email_Detail, Email_DuplicateEmail
from emailsystem.syncemail import check_email_format_return, check_email_format, check_similar_email, \
    set_message_for_email
from pytz import timezone

from message.models import File
from utils.dir_mgr import make_attachment_path
from utils.enums import FILETYPE



# eml parsing 부분만 업데이트 됨. 사용하지 않음.
def msg_parse(file, request):

    emails = Email_Detail()
    msg = extract_msg.Message(file)



    emailid = Email_Detail.objects.filter(emailId=msg.message_id)
    if emailid.count() != 0:
        print('exist EmailID')
        return msg
    if msg.message_id is None:
        emails.emailId = "NOID_" + datetime.today().strftime('%Y%m%d%H%M%S%f')
    else:
        emails.emailId = msg.message_id
    toworkemail = []
    if not msg.to or msg.to == 'undisclosed-recipients:;':
        dev = msg.header['To']
        if dev is None or dev == 'undisclosed-recipients:;':
            dev = msg.header['Delivered-To']
        print(dev)
        #emails.toName = dev.strip(' ').strip('"')

        try:
            obj = Work_Email.objects.get(email=dev.replace('<', '').replace('>', '').strip(' ').strip('"').replace(',',''))
            toworkemail.append(obj)
        except:
            toworkemail.append(Work_Email.objects.create(email=dev.replace('<', '').replace('>', '').strip(' ')
                                                         .strip('"').replace(',','')))



    else:
        tolist = decode_header(msg.to)
        tos = []

        for to in tolist:

            res = check_email_format_return(to[0])
            if res is not None:
                tos.append(res)
            elif type(to[0]) == str:
                tos.append(to[0])
            else:
                try:
                    tos.append(to[0].decode(encoding='utf-8'))
                except:
                    try:
                        tos.append(to[0].decode(encoding='euc-kr'))
                    except:
                        tos.append(to[0].decode(encoding=to[1]))

        for i in range(1, len(tos), 2):
            try:
                obj = Work_Email.objects.get(
                    email=tos[i].replace('<', '').replace('>', '').strip(' ').strip('"').replace(',',''))
                toworkemail.append(obj)
            except:
                toworkemail.append(Work_Email.objects.create(email=tos[i].replace('<', '').replace('>', '').strip(' ')
                                                             .strip('"').replace(',','')))

            #emails.toName = tos[0].strip(' ').strip('"').strip("'")


    ccworkemail = []
    if msg.cc is not None:
        cclist = decode_header(msg.cc)
        ccs = []
        for cc in cclist:

            if type(cc[0]) == str:
                ccs.append(cc[0])
            else:
                try:
                    ccs.append(cc[0].decode())
                except:
                    try:
                        ccs.append(cc[0].decode(encoding='euc-kr'))
                    except:
                        ccs.append(cc[0].decode(encoding=cc[1]))
        for i in range(1, len(ccs), 2):
            try:
                obj = Work_Email.objects.get(
                    email=ccs[i].replace('<', '').replace('>', '').strip(' ').strip('"').replace(',',''))
                ccworkemail.append(obj)
            except:
                ccworkemail.append(Work_Email.objects.create(email=ccs[i].replace('<', '').replace('>', '').strip(' ')
                                                             .strip('"').replace(',','')))

    senders = []
    if msg.sender is not None:
        senderlist = decode_header(msg.sender)
        for sender in senderlist:
            res = check_email_format_return(sender[0])
            if res is not None:
                senders.append(res)
            else:
                if type(sender[0]) is str:
                    senders.append(msg.header['Return-Path'])
                else:
                    if sender[1] is None:
                        sdecode = sender[0].decode()
                    else:
                        sdecode = sender[0].decode(sender[1])
                    sdecode = sdecode.replace('<', '').replace('>', '').strip('"').strip()
                    if sdecode:
                        senders.append(sdecode)
    else:
        if msg.header['From']:
            senders.append(msg.header['From'])

    for send in senders:
        if send is None:
            continue
        if check_email_format(send):
            try:
                obj = Work_Email.objects.get(
                    email=send.replace('<', '').replace('>', '').strip(' ').strip('"').replace(',',''))

            except:
                obj = Work_Email.objects.create(email=send.replace('<', '').replace('>', '').strip(' ')
                                                             .strip('"').replace(',',''))
            emails.fromEmail = obj

    #clr.AddReference(os.path.join(os.getcwd(), 'wrapper.dll'))
    #from wrapper import wrapper
    #wrapper = wrapper()
    #rtf = wrapper.Getrtf(os.path.join(os.getcwd(), file))

    rtf = compressed_rtf.decompress(msg.compressedRtf).decode()
    emails.body_rtf = rtf
    emails.body = msg.body
    emails.subject = msg.subject

    # 수신 날짜
    sent_date = msg.date
    datetime_object = datetime.strptime(sent_date, '%a, %d %b %Y %H:%M:%S %z')
    KST = timezone('Asia/Seoul')
    emails.receivedDate = datetime_object.astimezone(KST)

    emails.user = request.user
    emails.isFromFile = True

    # 조건에 해당하는 이메일이 있는지 확인한다.
    rescheckemail = check_similar_email(emails.subject, emails.body_rtf, emails.receivedDate)
    if rescheckemail is not None:

        for ch in rescheckemail:
            obj = Email_Detail.objects.get(emailId=ch.emailId)
            if emails.emailId != obj.emailId:
                obj.confirmAdd = False
                obj.save()
                emails.confirmAdd = False
                Email_DuplicateEmail.objects.get_or_create(childemail=emails, referenceemail=obj)

        emails.save()
    else:
        emails.save()

    for tw in toworkemail:
        emails.to_person.add(tw)
    for cw in ccworkemail:
        emails.cc_person.add(cw)


    files = msg.attachments
    for f in files:

        attach = File()
        if f.longFilename is None and f.shortFilename is None:
            if f.type == "msg":
                #customfilename = datetime.today().strftime('%Y%m%d%H%M%S%f') + '.' + 'msg'
                continue
        else:
            if f.shortFilename is not None and f.shortFilename:
                try:
                    customfilename = datetime.today().strftime('%Y%m%d%H%M%S%f') + '.' + f.shortFilename.split('.')[1]
                except Exception as e:
                    customfilename = datetime.today().strftime('%Y%m%d%H%M%S%f') + '.' + 'png'
                attach.originFileName = f.shortFilename
            elif f.longFilename is not None and f.longFilename:
                try:
                    customfilename = datetime.today().strftime('%Y%m%d%H%M%S%f') + '.' + f.longFilename.split('.')[1]
                except Exception as e:
                    customfilename = datetime.today().strftime('%Y%m%d%H%M%S%f') + '.' + 'png'
                attach.originFileName = f.longFilename

        print(f.type)
        if 'png' in f.type:
            print('check')
        attach.place = FILETYPE.file_email.name
        filepath = make_attachment_path(request.user.username)
        sv_path = os.path.join(filepath, customfilename)
        attach.path = sv_path
        print(f.cid)
        if f.cid is not None and f.cid in emails.body_rtf:
            emails.body_rtf = emails.body_rtf.replace('cid:' + f.cid, sv_path)
            emails.save()
        if f.type == "msg":
            continue

            #gen = email.generator.Generator(out)
            #gen.flatten(f.msg)
        else:
            f.save(customPath=filepath, customFilename=customfilename)
        attach.email = emails
        attach.save()

    set_message_for_email(emails)

    return msg