import base64
import json
import re
from datetime import datetime
import email
import os
from email.header import decode_header
from pathlib import Path
from xml import etree

from django.db import transaction
import eml_parser
import pytz
from bs4 import BeautifulSoup
from Lipis_system_master import settings
from People.models import Work_Email
from utils.crawling.drivers import chrome_driver
from emailsystem.models import Email_Detail
from emailsystem.syncemail import check_similar_email, get_part_filename, set_message_for_email

from message.models import File, ParentMessage_ChildMessage
from utils.dir_mgr import make_attachment_path
from utils.enums import FILETYPE, EMAILTYPE


def check_email_format(email):
    # pass the regualar expression
    # and the string in search() method
    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    if (re.search(regex, email)):
        return True

    else:
        return False

def TextBody(data):
    forplaintext = email.message_from_bytes(data)
    while forplaintext.is_multipart():
        forplaintext = forplaintext.get_payload(0)

    content = forplaintext.get_payload(decode=True)
    # savefile(content)
    try:
        txt_content = content.decode('UTF-8')
    except Exception as e:
        try:
            txt_content = content.decode('EUC-KR')
        except Exception as e:
            try:
                txt_content = content.decode('ks_c_5601-1987')
            except:
                soup = BeautifulSoup(content)
                txt_content = str(soup.contents[0])
    if not txt_content:
        return None
    if '<html' in txt_content or 'xmlns' in txt_content or '<HTML' in txt_content \
            or '<img' in txt_content or '<span' in txt_content or '<div' in txt_content \
            or '<tr' in txt_content or '<td' in txt_content:
        soup = BeautifulSoup(txt_content, "html.parser")
        soup.prettify()
        txt_content = soup.text
    return txt_content

def Subject(fortitle):

    if fortitle.get('subject') != None:
        subject = decode_header(fortitle['subject'])
        subjectstr = ''

        for s in subject:
            if type(s[0]) == str:
                subjectstr = subjectstr + s[0]
            else:
                if s[1] == None:
                    subjectstr = subjectstr + s[0].decode()
                else:
                    try:
                        subjectstr = subjectstr + s[0].decode(s[1])
                    except:
                        try:
                            subjectstr = subjectstr + s[0].decode('utf-8')
                        except:
                            subjectstr = subjectstr + s[0].decode('euc-kr')
    else:
        subjectstr = ''
    return subjectstr

def Body(rawbody):
    allbody = ''
    for rawb in rawbody:
        if 'html' in rawb[1]:
            allbody += rawb[1]

    if not allbody:
        for rawb in rawbody:
            allbody += rawb[1]
    soup = BeautifulSoup(allbody, "html.parser")
    prettifysoup = soup.prettify()
    # emails.body = str(prettifysoup)
    return allbody

def To(originHeader):
    toworkemail = []
    if 'to' in originHeader:
        to = originHeader['to'][0]
        tos = to.split(',')
        for t in tos:
            if '<' in t:
                place0 = t.find('<')
                place1 = t.find('>')
                searchemail = t[place0 + 1:place1]
                emailname = t[:place0]
                emailname = emailname.replace('<', '').replace('>', '').replace("'", '').replace('/', '').replace('"',
                                                                                                                  '').replace(
                    '\\', '')
                try:
                    obj = Work_Email.objects.get(
                        emailName=emailname.replace('<', '').replace('>', '').strip(' ').strip('"').replace(',', ''),
                        email=searchemail)
                    toworkemail.append(obj)
                except:
                    toworkemail.append(Work_Email.objects.create(
                        emailName=emailname.replace('<', '').replace('>', '').strip(' ').strip('"').replace(',', ''),
                        email=searchemail))

            elif '<' not in t and check_email_format(t):

                searchemail = t
                obj = Work_Email.objects.filter(email=searchemail)
                if obj:
                    toworkemail.append(obj.first())
                else:
                    toworkemail.append(Work_Email.objects.create(emailName=searchemail, email=searchemail))
    return toworkemail

def CC(originHeader):
    ccworkemail = []
    if 'cc' in originHeader:
        cc = originHeader['cc'][0]
        ccs = cc.split(',')
        for c in ccs:
            place0 = c.find('<')
            place1 = c.find('>')
            searchemail = c[place0 + 1:place1]
            emailname = c[:place0]
            emailname = emailname.replace('<', '').replace('>', '').replace("'", '').replace('/', '').replace('"',
                                                                                                              '').replace(
                '\\', '')
            try:
                obj = Work_Email.objects.get(
                    emailName=emailname.replace('<', '').replace('>', '').strip(' ').strip('"').replace(',', ''),
                    email=searchemail)
                ccworkemail.append(obj)
            except:
                ccworkemail.append(Work_Email.objects.create(
                    emailName=emailname.replace('<', '').replace('>', '').strip(' ').strip('"').replace(',', ''),
                    email=searchemail))
    return ccworkemail

def From(originHeader, emails):
    fromemail = Work_Email()
    if 'from' in originHeader:
        if type(originHeader['from']) == list:
            checkemail = originHeader['from'][0]
        else:
            checkemail = originHeader['from']

        if check_email_format(checkemail):

            originemail = checkemail
            fromemail = Work_Email.objects.filter(email=originemail).first()
            if fromemail is None:
                fromemail = Work_Email.objects.create(emailName=originemail, email=originemail)

        else:

            fromemail = checkemail
            if not '<' in fromemail:
                searchemail = ''
                emailname = fromemail.replace('>', '').replace("'", '').replace('/', '').replace('"', '').replace('\\',
                                                                                                                  '').strip().strip(
                    ' ').replace(',', '')
                try:
                    fromemail = Work_Email.objects.get(emailName=emailname)
                except:
                    fromemail = Work_Email.objects.create(emailName=emailname, email=searchemail)
            else:
                place0 = fromemail.find('<')
                place1 = fromemail.find('>')
                searchemail = fromemail[place0 + 1:place1]
                emailname = fromemail[:place0]
                emailname = emailname.replace('<', '').replace('>', '').replace("'", '').replace('/', '').replace('"',
                                                                                                                  '').replace(
                    '\\', '')
                try:
                    fromemail = Work_Email.objects.get(
                        emailName=emailname.replace('<', '').replace('>', '').strip(' ').strip('"').replace(',', ''),
                        email=searchemail)
                except:
                    fromemail = Work_Email.objects.create(
                        emailName=emailname.replace('<', '').replace('>', '').strip(' ').strip('"').replace(',', ''),
                        email=searchemail)
        emails.fromEmail = fromemail
    else:
        fromemail = None
        print('from error:')
        # fromemail을 못 찾는 경우가 있어서 return-path에서 이메일을 가져오도록 추가함.
    if not fromemail:
        if 'return-path' in originHeader:
            returnpath = originHeader['return-path'][0].replace('<', '').replace('>', '')

            wemail = Work_Email.objects.filter(email=returnpath).first()
            if not wemail:
                wobj = Work_Email.objects.create(email=returnpath, emailname=returnpath)
                emails.fromEmail = wobj
            else:
                emails.fromEmail = wemail
    return fromemail, emails

def Attachment(msg, emails, user, request):


    for part in msg.walk():

        # this part comes from the snipped I don't understand yet...
        if part.get_content_maintype() == 'multipart':
            continue

        if part.get('Content-Disposition') is None:
            if part.get('Content-Type') is None:
                continue
            else:
                if 'image' not in part.get('Content-Type'):
                    continue
        from io import StringIO
        filename = get_part_filename(part)
        if filename is not None:

            para = datetime.today().strftime('%Y%m%d%H%M%S%f')
            try:
                fname, file_extenstion = os.path.splitext(filename)
                if not file_extenstion:
                    print('')
                customfilename = para + file_extenstion
            except:
                customfilename = para + '.' + 'png'
            filepath = make_attachment_path(user.username)
            mediafilepath = os.path.join(str(Path(settings.BASE_DIR)), str(Path(settings.MEDIA_URL)))
            mediafilepath = os.path.join(mediafilepath, filepath)
            mediafilepath = os.path.join(mediafilepath, customfilename)
            print('check' + mediafilepath)

            # mediafilepath = str(Path(settings.MEDIA_URL) / filepath / customfilename)
            sv_path = str(Path(filepath) / customfilename)
            """ cid 의 src 바꾸기 위한 1안.
                        # 첫번째 cid 체크
            if 'cid:' + filename in emails.body:
                emails.body = emails.body.replace('cid:' + filename, sv_path)
                emails.save()
            # 두번 째 cid로 오는 첨부파일 인지 체크
            ctype = part.get('Content-Type')
            # image/jpg;\n\t
            cid = part.get('Content-Id')
            if 'image' in ctype and cid:
                rcid = cid.replace('<', '').replace('>', '')
            if 'cid:' + rcid in emails.body:
                emails.body = emails.body.replace('cid:'+rcid, sv_path)
                emails.save()
                        """
            ## 2번째 안은 cid 필드를 채우고 src는 url로 제공
            ctype = part.get('Content-Type')
            # image/jpg;\n\t
            cid = part.get('Content-Id')

            medaifull = (settings.MEDIA_ROOT + '/' + filepath + '/' + customfilename).replace('\\', '/')
            if not os.path.isfile(mediafilepath):
                fp = open(medaifull, 'wb')
                try:
                    fp.write(part.get_payload(decode=True))
                except Exception as e:
                    print('write error:')
                    f = StringIO()
                    g = email.generator.Generator(f, mangle_from_=True, maxheaderlen=60)
                    g.flatten(part._payload[0])
                    text = f.getvalue()
                    f.close()
                    continue

                fp.close()

                attach = File()
                attach.originFileName = filename
                attach.path = sv_path
                attach.place = FILETYPE.sync_email.name
                attach.email = emails
                try:
                    if 'image' in ctype and cid:
                        attach.cId = cid.replace('<', '').replace('>', '')
                except:
                    pass
                attach.save()
                if 'hometax.go.kr' in emails.fromEmail.email and filename == 'NTS_eTaxInvoice.html':
                    try:
                        driver, custompath = chrome_driver()
                        if request:
                            base_url = "{0}://{1}".format(request.scheme, request.get_host())
                        else:
                            base_url = settings.BASE_URL
                        driver.get(base_url + '/media/' + sv_path)
                        input = driver.find_element_by_id('idPcPwd')
                        input.send_keys("5820400515")
                        btn = driver.find_element_by_xpath(
                            './/table[@class ="Cri_table_layout"]/tbody/tr[3]/td[3]/button')
                        btn.click()
                        btn1 = driver.find_element_by_xpath('.//div[@class ="CriDiv"]/button[2]')
                        btn1.click()
                        btn2 = driver.find_element_by_xpath('.//table[@class ="attachPcTable"]/tbody/tr[2]/td/a')
                        btn2.click()
                        xmlname = btn2.text

                        attach = File()
                        attach.originFileName = xmlname
                        attach.path = custompath + '/' + xmlname
                        attach.place = FILETYPE.sync_email.name
                        attach.email = emails
                        attach.save()
                        driver.close()
                    except Exception as e:
                        print(e)
                        driver.close()

def SetRelation(originHeader, linkmsg):
    refermsgid = ''
    if 'references' in originHeader:
        referlist = originHeader['references']
        if '\t' in referlist[0]:
            refermsgid = referlist[0].split('\t')[-1]
        else:
            refermsgid = referlist[0].split(' ')[-1]
        print(refermsgid)
    elif 'references' not in originHeader and 'in-reply-to' in originHeader:
        inreplyto = originHeader['in-reply-to']
        refermsgid = inreplyto[0]
        print(refermsgid)
    if refermsgid:
        ed = Email_Detail.objects.filter(emailId=refermsgid)
        if ed:
            pc = ParentMessage_ChildMessage()
            pc.parent_message = ed.first().msg_email.first()
            pc.child_message = linkmsg
            # reply인지 forward 인지?
            pc.type = "RE"
            pc.save()

@transaction.atomic
def eml_parse_frombyte(data, user, emailinfo,index, useremail, request=None):
    print('start sync index {}'.format(index))
    emlparser = eml_parser.eml_parser.EmlParser(include_raw_body=True,
                                                include_attachment_data=True,
                                                email_force_tld=True,
                                                parse_attachments=True)
    rawbody = emlparser.get_raw_body_text(msg=email.message_from_bytes(data))
    emails = Email_Detail()
    emails.emailType = EMAILTYPE.FromServer.name
    emails.emailInfo = emailinfo
    parsed_eml = emlparser.decode_email_bytes(data)
    """
    try:
        parsed_eml = emlparser.decode_email_bytes(data)
    except:
        newparsed = {}
        parsed_eml = email.message_from_bytes(data)
        items = parsed_eml.items()
        rece = decode_header(parsed_eml['Return-Path'])
        for h in parsed_eml._headers:
            newparsed[h[0]] = decode_header(parsed_eml.get(h[0]))
    """
    strheader = str(parsed_eml['header'])
    jsondumps = json.dumps(strheader)
    jsonheader = json.loads(jsondumps)

    emails.header = jsonheader
    originHeader = parsed_eml['header']['header']

    txt_content = TextBody(data)
    if not txt_content:
        print('body is empty - fail')
        return
    emails.textBody = txt_content

    fortitle = email.message_from_bytes(data)
    subjectstr = Subject(fortitle)
    emails.subject = subjectstr
    print('subject : {}'.format(subjectstr))


    header = parsed_eml['header']
    if 'message-id' in header['header']:
        msgid = header['header']['message-id'][0]
    else:
        msgid = "NOID_" + datetime.today().strftime('%Y%m%d%H%M%S%f')

    emails.emailId = msgid

    emailid = Email_Detail.objects.filter(emailId=msgid)
    if emailid.count() != 0:
        print('duplicated id  %s' % msgid)
        return

    emails.isFromFile = False

    allbody = Body(rawbody)
    emails.body = allbody


    # emails.content = bp.get_body(preferencelist=('plain')).get_content()
    # for iter in bp.iter_attachments():
    #    print(iter)

    # get to. 없을 경우 Work_Email object 만들고 다대다 연결하기 위해 리스트 추가
    toworkemail = To(originHeader)

    # get cc
    ccworkemail = CC(originHeader)

    # get from
    fromemail, emails = From(originHeader, emails)

    emails.user = user
    # KST = timezone('Asia/Seoul')
    sent_date = header['date'].astimezone(pytz.utc)

    # datetime_object = datetime.strptime(sent_date, '%Y-%m-%d %H:%M:%S%z')
    # emails.sentDate = datetime.strptime(sent_date.strftime('%Y-%m-%d %H:%M:%S %z'), '%Y-%m-%d %H:%M:%S %z')
    emails.sentDate = sent_date
    rescheckemail = check_similar_email(emails.subject, emails.body, emails.sentDate, toworkemail, ccworkemail,
                                        fromemail)

    if rescheckemail:
        print("exist same subect,body,senddate,to,cc,from email.")
        return

    else:
        emails.save()



    # email save 이후 다대다 저장
    for tw in toworkemail:
        emails.to_person.add(tw)
    for cw in ccworkemail:
        emails.cc_person.add(cw)

    """
    # 첨부파일 1
    openfile = open(file)
    msg = email.message_from_file(openfile)
    openfile.close()



    for emlatt in parsed_eml['attachment']:
        filename = emlatt['filename']
        if filename is not None:

            para = datetime.today().strftime('%Y%m%d%H%M%S%f')
            try:
                fname, file_extenstion = os.path.splitext(filename)
                if not file_extenstion:
                    print('')
                customfilename = para + file_extenstion
            except:
                customfilename = para + '.' + 'png'

            filepath = make_attachment_path(request.user.username)
            mediafilepath = os.path.join(str(Path(settings.BASE_DIR)), str(Path(settings.MEDIA_URL)))
            mediafilepath = os.path.join(mediafilepath, filepath)
            mediafilepath = os.path.join(mediafilepath, customfilename)
            print('check' + mediafilepath)

            # mediafilepath = str(Path(settings.MEDIA_URL) / filepath / customfilename)
            sv_path = str(Path(filepath) / customfilename)
            if 'cid:' + filename in emails.body:
                emails.body = emails.body.replace('cid:' + filename, sv_path)
                emails.save()

            medaifull = (settings.MEDIA_ROOT + '/' + filepath + '/' + customfilename).replace('\\', '/')
            if not os.path.isfile(mediafilepath):
                fp = open(medaifull, 'wb')
                filebytes = emlatt['raw']
                fp.write(filebytes)
                fp.close()
                attach = File()
                attach.originFileName = filename
                attach.path = sv_path
                attach.place = FILETYPE.file_email
                attach.email = emails
                attach.save()
    """
    # 첨부파일 2
    msg = email.message_from_bytes(data)
    Attachment(msg, emails, user, request)



    linkmsg = set_message_for_email(emails)

    SetRelation(originHeader, linkmsg)


    return
@transaction.atomic
def eml_parse_fromfile(file, request):

    #em = open(file, 'rb')
    #bp = BytesParser(policy=policy.default).parse(em)
    #em.close()
    if '\\' in file:
        emailfilename= file.split('\\')[-1]
    else:
        emailfilename = file.split('/')[-1]

    fhdl = open(file, 'rb')
    raw_email = fhdl.read()
    fhdl.close()
    emlparser = eml_parser.eml_parser.EmlParser(include_raw_body= True,
                 include_attachment_data=True,
                 email_force_tld= True,
                 parse_attachments= True)
    mymsg = email.message_from_bytes(raw_email)
    #특수한 경우. header에 Resent-Date 가 오류가 나서 이부분만 제거 해주기 위해 적용함.
    """
    for header in mymsg._headers:
        if header[0].lower() == 'resent-date':
            mymsg._headers.remove(header)
    newbyte = mymsg.as_bytes()
    parsed_eml = emlparser.decode_email_bytes(newbyte)
    """
    parsed_eml = emlparser.decode_email_bytes(raw_email)

    rawbody = emlparser.get_raw_body_text(msg=mymsg)
    emails = Email_Detail()
    emails.emailType = EMAILTYPE.FromFile.name
    emails.emailFileName = emailfilename

    strheader = str(parsed_eml['header'])
    jsondumps = json.dumps(strheader)
    jsonheader = json.loads(jsondumps)

    emails.header = jsonheader
    #emails.header = parsed_eml['header']
    originHeader = parsed_eml['header']['header']


    txt_content = TextBody(raw_email)
    if not txt_content:
        print('body is empty - fail')
        return
    emails.textBody = txt_content


    try:
        openfile = open(file)
        fortitle = email.message_from_file(openfile)
        openfile.close()
        subjectstr = Subject(fortitle)
    except Exception as e:
        openfile.close()
        subjectstr = originHeader['subject']

    emails.subject = subjectstr


    header = parsed_eml['header']
    if 'message-id' in header['header']:
        msgid = header['header']['message-id'][0]
    else:
        msgid = "NOID_" + datetime.today().strftime('%Y%m%d%H%M%S%f')

    emails.emailId = msgid

    emailid = Email_Detail.objects.filter(emailId=msgid)
    if emailid.count() != 0:
        os.remove(file)
        print('duplicated id removed %s' % file)
        return

    emails.isFromFile = True

    allbody = Body(rawbody)
    ## 보통 body에 html 에 한글로 잘 디코딩 된 상태이나, body자체가 to, from, content 가 인코딩되어 있는 str 형식인
    ## 특수한 경우가 있다. 아래와 같이 디코딩 하나, 아주 예외적인 사항인듯
    #bodys = parsed_eml['body'][0]['content'].split('\n\n')
    #d = base64.b64decode(bodys[2])
    #dd = d.decode('utf-8')
    emails.body = allbody

    """
    b = email.message_from_bytes(raw_email)
    while b.is_multipart():
        b = b.get_payload(0)

    content = b.get_payload(decode=True)
    # savefile(content)
    try:
        txt_content = content.decode('UTF-8')
    except Exception as e:
        txt_content = content.decode('EUC-KR')
    emails.body_rtf = txt_content
    """

    #emails.content = bp.get_body(preferencelist=('plain')).get_content()
    #for iter in bp.iter_attachments():
    #    print(iter)


    # get to. 없을 경우 Work_Email object 만들고 다대다 연결하기 위해 리스트 추가
    toworkemail = To(originHeader)

    # get cc
    ccworkemail = CC(originHeader)

    # get from

    fromemail, emails = From(originHeader, emails)

    emails.emailId = msgid
    emails.user = request.user
    #KST = timezone('Asia/Seoul')
    sent_date = header['date'].astimezone(pytz.utc)

    #datetime_object = datetime.strptime(sent_date, '%Y-%m-%d %H:%M:%S%z')
    #emails.sentDate = datetime.strptime(sent_date.strftime('%Y-%m-%d %H:%M:%S %z'), '%Y-%m-%d %H:%M:%S %z')
    emails.sentDate = sent_date
    rescheckemail = check_similar_email(emails.subject, emails.body, emails.sentDate, toworkemail, ccworkemail, fromemail)


    if rescheckemail:
        print("exist same subect,body,senddate,to,cc,from email.")
        os.remove(file)
        return

    else:
        emails.save()

    # 다대다 저장
    for tw in toworkemail:
        emails.to_person.add(tw)
    for cw in ccworkemail:
        emails.cc_person.add(cw)

    """
    # 첨부파일 1
    openfile = open(file)
    msg = email.message_from_file(openfile)
    openfile.close()



    for emlatt in parsed_eml['attachment']:
        filename = emlatt['filename']
        if filename is not None:

            para = datetime.today().strftime('%Y%m%d%H%M%S%f')
            try:
                fname, file_extenstion = os.path.splitext(filename)
                if not file_extenstion:
                    print('')
                customfilename = para + file_extenstion
            except:
                customfilename = para + '.' + 'png'

            filepath = make_attachment_path(request.user.username)
            mediafilepath = os.path.join(str(Path(settings.BASE_DIR)), str(Path(settings.MEDIA_URL)))
            mediafilepath = os.path.join(mediafilepath, filepath)
            mediafilepath = os.path.join(mediafilepath, customfilename)
            print('check' + mediafilepath)

            # mediafilepath = str(Path(settings.MEDIA_URL) / filepath / customfilename)
            sv_path = str(Path(filepath) / customfilename)
            if 'cid:' + filename in emails.body:
                emails.body = emails.body.replace('cid:' + filename, sv_path)
                emails.save()

            medaifull = (settings.MEDIA_ROOT + '/' + filepath + '/' + customfilename).replace('\\', '/')
            if not os.path.isfile(mediafilepath):
                fp = open(medaifull, 'wb')
                filebytes = emlatt['raw']
                fp.write(filebytes)
                fp.close()
                attach = File()
                attach.originFileName = filename
                attach.path = sv_path
                attach.place = FILETYPE.file_email
                attach.email = emails
                attach.save()
    """
    # 첨부파일 2
    openfile = open(file)
    msg = email.message_from_file(openfile)
    openfile.close()
    Attachment(msg, emails, request.user, request)

    linkmsg = set_message_for_email(emails)
    # 이메일에서 이메일 참조하는 이메일 관계
    SetRelation(originHeader, linkmsg)

    os.remove(file)
    print('removed %s' % file)
    return


@transaction.atomic
def eml_parse_fromfile_custom(file, request):

    #em = open(file, 'rb')
    #bp = BytesParser(policy=policy.default).parse(em)
    #em.close()
    if '\\' in file:
        emailfilename= file.split('\\')[-1]
    else:
        emailfilename = file.split('/')[-1]

    fhdl = open(file, 'rb')
    raw_email = fhdl.read()
    fhdl.close()
    emlparser = eml_parser.eml_parser.EmlParser(include_raw_body= True,
                 include_attachment_data=True,
                 email_force_tld= True,
                 parse_attachments= True)
    mymsg = email.message_from_bytes(raw_email)
    #특수한 경우. header에 Resent-Date 가 오류가 나서 이부분만 제거 해주기 위해 적용함.
    """
    for header in mymsg._headers:
        if header[0].lower() == 'resent-date':
            mymsg._headers.remove(header)
    newbyte = mymsg.as_bytes()
    parsed_eml = emlparser.decode_email_bytes(newbyte)
    """
    parsed_eml = emlparser.decode_email_bytes(raw_email)

    rawbody = emlparser.get_raw_body_text(msg=mymsg)
    emails = Email_Detail()
    emails.emailType = EMAILTYPE.FromFile.name
    emails.emailFileName = emailfilename

    strheader = str(parsed_eml['header'])
    jsondumps = json.dumps(strheader)
    jsonheader = json.loads(jsondumps)

    emails.header = jsonheader
    #emails.header = parsed_eml['header']
    originHeader = parsed_eml['header']['header']


    txt_content = TextBody(raw_email)
    if not txt_content:
        print('body is empty - fail')
        return
    emails.textBody = txt_content


    try:
        openfile = open(file)
        fortitle = email.message_from_file(openfile)
        openfile.close()
        subjectstr = Subject(fortitle)
    except Exception as e:
        openfile.close()
        subjectstr = originHeader['subject']

    emails.subject = subjectstr


    header = parsed_eml['header']
    if 'message-id' in header['header']:
        msgid = header['header']['message-id'][0]
    else:
        msgid = "NOID_" + datetime.today().strftime('%Y%m%d%H%M%S%f')

    emails.emailId = msgid

    em = Email_Detail.objects.filter(emailId=msgid)
    """
    if em:
        os.remove(file)
        print('duplicated id removed %s' % file)
        return
    """
    if em:
        emails = em.first()
    emails.isFromFile = True

    allbody = Body(rawbody)
    ## 보통 body에 html 에 한글로 잘 디코딩 된 상태이나, body자체가 to, from, content 가 인코딩되어 있는 str 형식인
    ## 특수한 경우가 있다. 아래와 같이 디코딩 하나, 아주 예외적인 사항인듯
    #bodys = parsed_eml['body'][0]['content'].split('\n\n')
    #d = base64.b64decode(bodys[2])
    #dd = d.decode('utf-8')
    emails.body = allbody

    """
    b = email.message_from_bytes(raw_email)
    while b.is_multipart():
        b = b.get_payload(0)

    content = b.get_payload(decode=True)
    # savefile(content)
    try:
        txt_content = content.decode('UTF-8')
    except Exception as e:
        txt_content = content.decode('EUC-KR')
    emails.body_rtf = txt_content
    """

    #emails.content = bp.get_body(preferencelist=('plain')).get_content()
    #for iter in bp.iter_attachments():
    #    print(iter)


    # get to. 없을 경우 Work_Email object 만들고 다대다 연결하기 위해 리스트 추가
    toworkemail = To(originHeader)

    # get cc
    ccworkemail = CC(originHeader)

    # get from

    fromemail, emails = From(originHeader, emails)

    emails.emailId = msgid
    emails.user = request.user
    #KST = timezone('Asia/Seoul')
    sent_date = header['date'].astimezone(pytz.utc)

    #datetime_object = datetime.strptime(sent_date, '%Y-%m-%d %H:%M:%S%z')
    #emails.sentDate = datetime.strptime(sent_date.strftime('%Y-%m-%d %H:%M:%S %z'), '%Y-%m-%d %H:%M:%S %z')
    emails.sentDate = sent_date
    rescheckemail = check_similar_email(emails.subject, emails.body, emails.sentDate, toworkemail, ccworkemail, fromemail)


    if rescheckemail:
        print("exist same subect,body,senddate,to,cc,from email.")
        os.remove(file)
        return

    else:
        emails.save()

    # 다대다 저장
    for tw in toworkemail:
        emails.to_person.add(tw)
    for cw in ccworkemail:
        emails.cc_person.add(cw)

    """
    # 첨부파일 1
    openfile = open(file)
    msg = email.message_from_file(openfile)
    openfile.close()



    for emlatt in parsed_eml['attachment']:
        filename = emlatt['filename']
        if filename is not None:

            para = datetime.today().strftime('%Y%m%d%H%M%S%f')
            try:
                fname, file_extenstion = os.path.splitext(filename)
                if not file_extenstion:
                    print('')
                customfilename = para + file_extenstion
            except:
                customfilename = para + '.' + 'png'

            filepath = make_attachment_path(request.user.username)
            mediafilepath = os.path.join(str(Path(settings.BASE_DIR)), str(Path(settings.MEDIA_URL)))
            mediafilepath = os.path.join(mediafilepath, filepath)
            mediafilepath = os.path.join(mediafilepath, customfilename)
            print('check' + mediafilepath)

            # mediafilepath = str(Path(settings.MEDIA_URL) / filepath / customfilename)
            sv_path = str(Path(filepath) / customfilename)
            if 'cid:' + filename in emails.body:
                emails.body = emails.body.replace('cid:' + filename, sv_path)
                emails.save()

            medaifull = (settings.MEDIA_ROOT + '/' + filepath + '/' + customfilename).replace('\\', '/')
            if not os.path.isfile(mediafilepath):
                fp = open(medaifull, 'wb')
                filebytes = emlatt['raw']
                fp.write(filebytes)
                fp.close()
                attach = File()
                attach.originFileName = filename
                attach.path = sv_path
                attach.place = FILETYPE.file_email
                attach.email = emails
                attach.save()
    """
    # 첨부파일 2
    openfile = open(file)
    msg = email.message_from_file(openfile)
    openfile.close()
    Attachment(msg, emails, request.user, request)

    linkmsg = set_message_for_email(emails)
    # 이메일에서 이메일 참조하는 이메일 관계
    SetRelation(originHeader, linkmsg)

    os.remove(file)
    print('removed %s' % file)
    return