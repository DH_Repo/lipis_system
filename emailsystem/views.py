import datetime
import glob
import json
import operator
import os
import re
import shutil
import sys
import tempfile
import time
import traceback
from functools import reduce
from pathlib import Path
# 리눅스에서는 Lib 제거할 것.
from django.core import serializers
from django.core.serializers import serialize
from django.forms import model_to_dict

from People.models import Work_Email
from common.codemultilanguage_choice import GetCodeMultpleLanguageForTask, GetCodeMultpleLanguageForCase, \
    GetCodeMultpleLanguageForCaseTaskMessage
from emailsystem.email_con.emailsender import send_ackemail

if sys.platform == "linux":
    from xml.dom import minidom
else:
    from Lib.xml.dom import minidom
import extract_msg
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.sessions.backends import file
from django.http import HttpResponseForbidden, HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.utils.decorators import method_decorator
from django.views.decorators.clickjacking import xframe_options_sameorigin, xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, FormView

from Lipis_system_master import settings
from Lipis_system_master.settings import MEDIA_ROOT
from Lipis_system_master.views import HomeMixin
from application_patent_domestic.models import Application_Patent_Domestic_Master
from case.forms import CaseModelForm, Custom_CaseForm, TaskModelForm
from common.tableview_function import get_all_model_context_for_table, get_model_query_context_for_table_nopage_nosearch, \
    get_field_multilang
from emailsystem.SyncEmail.sync_eml import eml_parse_fromfile, eml_parse_fromfile_custom
from emailsystem.SyncEmail.sync_msg import msg_parse
from emailsystem.forms import EmailSettingForm, EmailFileUploadingForm, EmailACKForm, EmailSignatureForm
from emailsystem.models import Email_Info, Email_Detail, Email_DuplicateEmail, Email_ACK_Content, Email_Signature, \
    Email_Option, Email_Option_Code, Email_Option_User_Choice
from django.contrib import auth
from django.db.models import Q, Prefetch

from emailsystem.tasks import syncmail_task, sync_email
from message.models import Message, File, Application_Patent_Domestic_Master_Message, ParentMessage_ChildMessage
from utils.db_fieldset import get_feild_names, get_fielddic
from utils.dir_mgr import make_brandimage_attachment_path, make_attachment_path
from utils.enums import UPLOAD_FILE_PATH, FILETYPE, REGEX_APPLCODE_REFCODE, CASE_TASK_TYPE, ALL_CODE_TYPE


class EmaliUploadView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'emailsystem/emailupload2.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(EmaliUploadView, self).get_context_data(**kwargs)

        file_list = glob.glob(str(Path(UPLOAD_FILE_PATH.email.value) / '*'))
        child = Email_DuplicateEmail.objects.filter(childemail__isnull=False).distinct('childemail')
        eid = []
        for ch in child:
            eid.append(Email_Detail.objects.get(emailId=ch.childemail_id))

        context['duplicate_list'] = eid
        context['file_list'] = file_list
        return context
class SearchEmailView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'emailsystem/emails.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(SearchEmailView, self).get_context_data(**kwargs)

        search = self.request.GET.get('Search')
        search_column = self.request.GET.get('SearchColumn')
        myfilter = search_column + '__contains'
        makecontext = get_model_query_context_for_table_nopage_nosearch(self, Email_Detail, 'Email_Detail',
                                                                        context,
                                                                        Email_Detail.objects.filter(**{myfilter:search}).order_by('-sentDate'))
        return makecontext
class EmailListView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'emailsystem/emails.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):

        context = super(EmailListView, self).get_context_data(**kwargs)
        makecontext = get_all_model_context_for_table(self, Email_Detail, 'Email_Detail',
                               'Email', context)

        return makecontext

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context)
    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context)


def SearchOurCompanyNumberForEmail(email):
    regexlist = [REGEX_APPLCODE_REFCODE.RefOurCom.value]
    ourcomresult = []
    for r in regexlist:
        regex = re.compile(r)
        for r in regex.findall(email.subject + ' ' + email.textBody):
            ourcomresult.append(r[0])

    regexlist = [REGEX_APPLCODE_REFCODE.applNo.value]
    for r in regexlist:
        regex = re.compile(r)
        applresult = regex.findall(email.subject + ' ' + email.textBody)

    return list(set(ourcomresult)), list(set(applresult))

@login_required(login_url='People:login')
@csrf_exempt
def SetHTMLFilter(request):
    if request.method == 'POST':
        dic = request.POST.dict()
        if 'isHTML' in dic:
            request.session['ishtml'] = dic['isHTML']
        else:
            request.session['ishtml'] = "True"
    return HttpResponse('')


def emaildetail_get_context(self, request, pk):
    if 'ishtml' in self.request.session:
        ishtml = self.request.session['ishtml']
    else:
        ishtml = "True"
    context = self.get_context_data()
    context['ishtml'] = ishtml
    email = Email_Detail.objects.prefetch_related('msg_email', 'email_file').get(id=pk)
    decide = False
    xmlfile = ''
    msgs = email.msg_email.prefetch_related('master_message').all()
    mastermessage = msgs.first().master_message.all()

    if email.fromEmail.emailName == '국세청':
        for f in email.email_file.all():
            if f.originFileName == "NTS_eTaxInvoice.html":
                decide = True
            elif '.xml' in f.originFileName:
                xmlfile = f
    if xmlfile and decide:
        # 국세청에서 온 메일

        xmldoc = minidom.parse(MEDIA_ROOT + '/' + str(xmlfile.path))

        taxinvoicedocu = xmldoc.getElementsByTagName('TaxInvoiceDocument')
        dt = False
        nodes = taxinvoicedocu[0]._get_childNodes()
        for child in nodes:
            if child._get_tagName() == 'DescriptionText':
                dt = True
        if dt:
            itemlist = xmldoc.getElementsByTagName('DescriptionText')
            ourcomnumber = ''
            revenue_code = ''
            if '/' in itemlist[0].firstChild.data:
                numberlist = itemlist[0].firstChild.data.split('/')
                for num in numberlist:
                    regex = re.compile(REGEX_APPLCODE_REFCODE.RefOurCom.value)
                    matchobj = regex.search(num)
                    if matchobj:
                        ourcomnumber = num
                    regex = re.compile(REGEX_APPLCODE_REFCODE.RevenueInvoice_OurComNumber.value)
                    matchobj = regex.search(num)
                    if matchobj:
                        revenue_code = num
            else:
                content = itemlist[0].firstChild.data
                regex = re.compile(REGEX_APPLCODE_REFCODE.RefOurCom.value)
                matchobj = regex.search(content)
                if matchobj:
                    ourcomnumber = content
                regex = re.compile(REGEX_APPLCODE_REFCODE.RevenueInvoice_OurComNumber.value)
                matchobj = regex.search(content)
                if matchobj:
                    revenue_code = content


            context['searchedourcomnumber'] = [ourcomnumber]
            ourcomnumber_ = []
            ourcomnumber_.append(ourcomnumber)
            if '-' in ourcomnumber:
                ourcomnumber_.append(ourcomnumber.replace('-', ''))



            # 이메일의 관계 msg_email의 관계 master_message의 id를 포함하지 않으면서(미연결 상태), 본문에 검색된 당소번호를 가지고 있는 출원 검색.
            ids = []
            for m in mastermessage:
                ids.append(m.id)

            searchedappl = Application_Patent_Domestic_Master.objects.filter(
                Q(ourCompanyNumber__in=ourcomnumber_) & ~Q(id__in=ids))
            context['searchedmasternoclassified'] = searchedappl
            context['revenue_invoice'] = revenue_code
    else:
        # 당소번호, 출원번호를 각각 정규식으로 찾는다.
        ourcomnumber, applno = SearchOurCompanyNumberForEmail(email)
        context['searchedourcomnumber'] = ourcomnumber
        context['searchedapplno'] = applno

        # 당소번호리스트 & '-'제거 리스트 합
        ourcomnumber_ = []
        for o in ourcomnumber:
            ourcomnumber_.append(o.replace('-', ''))
        ourcomlist = list(set(ourcomnumber + ourcomnumber_))
        # 출원번호리스트 & '-'제거 리스트 합
        ourappno_ = []
        for o in applno:
            ourappno_.append(o.replace('-', ''))
        ourappnolist = list(set(applno + ourappno_))

        # mastermsghas = email.msg_email.prefetch_related(Prefetch('master_message', queryset=searchedappl, to_attr='master_message_has'))

        # 이메일의 관계 msg_email의 관계 master_message의 id를 포함하지 않으면서(미연결 상태), 본문에 검색된 당소번호를 가지고 있는 출원 검색.

        ids = []
        for m in mastermessage:
            ids.append(m.id)

        searchedappl = Application_Patent_Domestic_Master.objects.filter(
            (Q(ourCompanyNumber__in=ourcomlist) | Q(applicationNumber__in=ourappnolist)) & ~Q(id__in=ids))
        context['searchedmasternoclassified'] = searchedappl

    ## 이메일 메시지 간 연결 필터 찾는 부
    if "emailfilter" in request.session:
        context["emailfilter"] = request.session["emailfilter"]
    else:
        context["emailfilter"] = "none"



    email.msg = msgs.first().id
    email.msgisClassified = msgs.first().isClassified
    context['master'] = mastermessage
    context['emaildetail'] = email
    context['msg'] = msgs.first()
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Case')
    tasklabeldic = get_fielddic(language, 'Task')
    customcodechoice = GetCodeMultpleLanguageForCase(request)
    taskcustomcodechoice = GetCodeMultpleLanguageForTask(request)
    caseform = Custom_CaseForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)
    taskform = TaskModelForm(labeldic=tasklabeldic, customcodechoice=taskcustomcodechoice, request=request, auto_id="task_%s")
    casetype = [CASE_TASK_TYPE.CaseReception, CASE_TASK_TYPE.CaseSubmissionInstruction]
    context['masterfield_multiplelang'] = get_feild_names(language, 'Application_Patent_Domestic_Master', Application_Patent_Domestic_Master)

    #form.fields['case_evidence_file'].choices = evidencefile

    context['form'] = caseform
    context['taskform'] = taskform
    context['casetype'] = casetype
    context['case_task_message_type'] = GetCodeMultpleLanguageForCaseTaskMessage(request)[ALL_CODE_TYPE.Message_CaseTaskType.name]
    context['case_state'] = GetCodeMultpleLanguageForCase(request)[ALL_CODE_TYPE.Case_CaseState.name]
    context['ispop'] = False
    return context
def emaildetail_post_context(self, request):
    context = self.get_context_data()
    dic = self.request.POST.dict()
    if 'isHTML' in dic:
        self.request.session['ishtml'] = dic['isHTML']
    else:
        self.request.session['ishtml'] = "True"
    self.request.session.modified = True
    return context
class EmailContentView(LoginRequiredMixin, HomeMixin, TemplateView):
    login_url = 'People:login'
    template_name = 'emailsystem/email_content.html'

    def get_context_data(self, **kwargs):

        context = super(EmailContentView, self).get_context_data(**kwargs)

        return context

    def get(self, request, pk):
        context = emaildetail_get_context(self, request, pk)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        context = emaildetail_post_context(self, request)
        return self.render_to_response(context)

@method_decorator(xframe_options_sameorigin, name='dispatch')
class EmailContentViewForIFrame(TemplateView):
    template_name = 'emailsystem/email_content_iframe.html'

    def get_context_data(self, **kwargs):

        context = super(EmailContentViewForIFrame, self).get_context_data(**kwargs)

        return context

    def get(self, request, pk):
        context = emaildetail_get_context(self, request, pk)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        context = emaildetail_post_context(self, request)
        return self.render_to_response(context)


def showcidimg(request):
    if request.method == 'GET':
        try:
            numberforsearch = request.GET.get('cid')
            emailid = request.GET.get('emailid')

            file = File.objects.get(cId=numberforsearch.replace('cid:', ''),email_id=emailid)
            image_data = open(file.path.path, "rb").read()
            return HttpResponse(image_data, content_type="image/png")
        except Exception as e:
            print(e)
            return HttpResponse('')




@xframe_options_sameorigin
def EmailTextBodySlice(request, pk):
    if request.method == 'GET':
        email = Email_Detail.objects.get(id=pk)
        return render(request, 'emailsystem/emailtextbody_slice.html', {'email':email})
@xframe_options_sameorigin
def EmailTextBody(request, pk):
    if request.method == 'GET':
        email = Email_Detail.objects.get(id=pk)
        return render(request, 'emailsystem/emailtextbody.html', {'email':email})

class EmailTextBodyView(LoginRequiredMixin, HomeMixin, TemplateView):
    login_url = 'People:login'
    template_name = 'emailsystem/emailtextbody_slice.html'

    def get_context_data(self, **kwargs):

        context = super(EmailTextBodyView, self).get_context_data(**kwargs)
        return context

    def get(self, request, pk):
        context = self.get_context_data()
        email = Email_Detail.objects.get(id=pk)
        context['email'] = email
        return self.render_to_response(context)


@login_required(login_url='People:login')
@csrf_exempt
def savenewemail(request):
    syncemail = request.POST['syncemail']
    isImap = request.POST['isImap']
    password = request.POST['password']
    imapServer = request.POST['imapServer']
    imapPort = request.POST['imapPort']
    popServer = request.POST['popServer']
    popPort = request.POST['popPort']
    isActive = request.POST['isActive']
    syncEmailCount = request.POST['syncEmailCount']
    # isImap = form.cleaned_data['isImap']
    if isImap == 'on':
        isImap = True
    else:
        isImap = False
    if isActive == 'on':
        isActive = True
    else:
        isActive = False
    obj, created = Email_Info.objects.get_or_create(user=request.user, password=password, imapServer=imapServer,
                                                    imapPort=imapPort,
                                                    popServer=popServer, popPort=popPort, isImap=isImap,
                                                    syncEmail=syncemail, isActive=isActive, syncEmailCount=syncEmailCount)
    return redirect('emailsystem:emailsetting')

@login_required(login_url='People:login')
def add_duplicate_child_email(request, pk):
    obj = Email_Detail.objects.filter(emailId=pk)
    if obj.count() > 0:
        eobj, created = obj.get_or_create()
    dobj2 = Email_DuplicateEmail.objects.filter(childemail=eobj.emailId)
    dobj2.delete()
    eobj.confirmAdd = True
    eobj.save()
    return redirect('emailsystem:emailupload')

@login_required(login_url='People:login')
def delete_duplicate_child_email(request, pk):
    fobj = File.objects.filter(email_id=pk)
    if fobj is not None:
        fobjs, created = fobj.get_or_create()
        fobjs.delete()
    mobj = Message.objects.filter(email_id=pk)
    if mobj is not None:
        mobjs, created = mobj.get_or_create()
        mobjs.delete()
    obj = Email_Detail.objects.get(emailId=pk)
    dobj2 = Email_DuplicateEmail.objects.filter(childemail=obj.emailId)
    dobj2.delete()
    obj.delete()

@login_required(login_url='People:login')
def add_duplicate_parent_email(request, pk):
    obj = Email_Detail.objects.filter(emailId=pk)
    if obj.count() > 0:
        eobj, created = obj.get_or_create()
    dobj2 = Email_DuplicateEmail.objects.filter(referenceemail=eobj.emailId)
    dobj2.delete()
    eobj.confirmAdd = True
    eobj.save()
    return redirect('emailsystem:emailupload')

@login_required(login_url='People:login')
def delete_duplicate_parent_email(request, pk):
    fobj = File.objects.filter(email_id=pk)
    if fobj is not None:
        fobjs, created = fobj.get_or_create()
        fobjs.delete()
    mobj = Message.objects.filter(email_id=pk)
    if mobj is not None:
        mobjs, created = mobj.get_or_create()
        mobjs.delete()
    obj = Email_Detail.objects.get(emailId=pk)
    dobj2 = Email_DuplicateEmail.objects.filter(referenceemail=obj.emailId)
    dobj2.delete()
    obj.delete()


    return redirect('emailsystem:emailupload')



@login_required(login_url='People:login')
@csrf_exempt
def EmailFileUpload(request):
    if not request.user.is_authenticated:
        return HttpResponse('<script type="text/javascript">alert("not authenticated");</script>')
    dirc = UPLOAD_FILE_PATH.email.value
    if not os.path.exists(dirc):
        os.makedirs(dirc)

    if request.method == 'POST':
        files = request.FILES.getlist('files[]')

        for f in files:
            savefile = dirc + '/' + f.name
            print('save file : %s' %(f))
            with open(savefile, 'wb') as dest:
                for chunk in f.chunks():
                    dest.write(chunk)

        for f in files:
            msg = None
            savefile = dirc + '/' + f.name
            if savefile.endswith('.msg'):
                try:
                    msg = msg_parse(savefile, request)
                    if msg:
                        msg.close()
                    time.sleep(1)
                    try:

                        os.remove(savefile)
                    except Exception as e:
                        #shutil.rmtree(savefile)
                        print(e)
                        print(traceback.format_exc())
                        continue
                    print('removed %s' % savefile)
                except Exception as e:
                    if msg:
                        msg.close()
                    print(e)
                    print(traceback.format_exc())

            elif savefile.endswith('.eml'):
                try:
                    eml_parse_fromfile(savefile, request)
                except Exception as e:
                    print(e)
                    print(traceback.format_exc())
                    continue


        return redirect('emailsystem:emailupload')

@login_required(login_url='People:login')
def sync_email_file(request):
    if not request.user.is_authenticated:
        return HttpResponse('<script type="text/javascript">alert("not authenticated");</script>')
    file_list = glob.glob(UPLOAD_FILE_PATH.email.value+'/*')
    for file in file_list:
        msg = None
        if file.endswith('.msg'):
            try:
                msg = msg_parse(file, request)
                if msg:
                    msg.close()
                time.sleep(1)
                try:
                    os.remove(file)
                except Exception as e:
                    #shutil.rmtree(os.path.join(os.getcwd(), file))
                    print(e)
                    print(traceback.format_exc())
                    continue
                print('removed %s' % file)
            except Exception as e:
                if msg:
                    msg.close()
                print(e)
                print(traceback.format_exc())

        elif file.endswith('.eml'):
            try:
                eml_parse_fromfile(file, request)

            except Exception as e:
                print(e)
                print(traceback.format_exc())
                continue

    return redirect('emailsystem:emailupload')


@login_required(login_url='People:login')
@csrf_exempt
def AddEmailAttach(request, pk):
    if request.method == 'POST':
        files = request.FILES.getlist('files[]')
        emails = Email_Detail.objects.get(id=pk)
        for f in files:

            custompath = make_attachment_path(request.user.username)

            customfilename = datetime.datetime.today().strftime('%Y%m%d%H%M%S%f') + '.' + f.name.split('.')[1]
            mypath = (settings.MEDIA_ROOT + '/' + custompath + '/' + customfilename).replace('\\', '/')

            with open(mypath, 'wb') as dest:
                for chunk in f.chunks():
                    dest.write(chunk)
            savepath = ('/' + custompath + '/' + customfilename).replace('\\', '/')
            file = File()
            file.email = emails
            file.isActive = True
            file.originFileName = f.name
            file.path = savepath
            file.place = FILETYPE.manual_attach_email.name
            file.save()



    return HttpResponse('')

@login_required(login_url='People:login')
@csrf_exempt
def LinkEmailMsgByOurCompanyNumber(request):
    if request.method == 'POST':
        msgid = request.POST.get('msgid')
        ourcomnumber = request.POST.get('ourcomnumber')
        link = request.POST.get('link')
        msgobj = Message.objects.get(id=int(msgid))
        applobj = Application_Patent_Domestic_Master.objects.get(ourCompanyNumber=ourcomnumber)
        if link == 'true':
            msgobj.master_message.add(applobj)
        else:
            msgobj.master_message.remove(applobj)
        metaget = request.META.get('HTTP_REFERER')
    return HttpResponse('')

@login_required(login_url='People:login')
@csrf_exempt
def LinkEmailMsgByApplNumber(request):
    if request.method == 'POST':
        msgid = request.POST.get('msgid')
        appl = request.POST.get('applno')
        link = request.POST.get('link')
        msgobj = Message.objects.get(id=int(msgid))
        applobj = Application_Patent_Domestic_Master.objects.get(applicationNumber=appl)
        if link == 'true':
            msgobj.master_message.add(applobj)
        else:
            msgobj.master_message.remove(applobj)
        metaget = request.META.get('HTTP_REFERER')
    return HttpResponse('')

@login_required(login_url='People:login')
def syncmail(request):
    sync_email(request)
    return redirect('management:common_mgr')

@csrf_exempt
@login_required(login_url='People:login')
def result_from_email_relation_filter(request):
    if not request.user.is_authenticated:
        return HttpResponse('<script type="text/javascript">alert("not authenticated");</script>')
    if request.is_ajax():
        #클릭하여 이메일 디테일 표시될 때, 무조건 none이 되는 상태.
        filtername = request.POST.get('filtername','none')
        emailid = request.POST.get('emailid')
        request.session["emailfilter"] = filtername
        email = Email_Detail.objects.get(id=emailid)
        mymsg = email.msg_email.first()
        mydic = {}
        if filtername == 'patent':
            mydic = {'master_message__in': mymsg.master_message.all()}
        elif filtername == 'case':
            mydic = {'case__in': mymsg.case.all()}
        elif filtername == 'task':
            mydic = {'task__in': mymsg.task.all()}

        admins = Work_Email.objects.filter(Q(email='jy.park@liboip.com') | Q(email='libo@liboip.com') | Q(email='chang@liboip.com'))

        query = reduce(operator.or_, (Q(email__subject__contains=x) for x in ['RE', 'Re', 're','FW','Fw','fw']))
        try:
            isadmin = False
            for e in email.to_person.all():
                if e.email == 'jy.park@liboip.com' or e.email == 'libo@liboip.com' or e.email == 'chang@liboip.com':
                    isadmin = True

            if isadmin:
                emailquery = Q(email__fromEmail__in=email.to_person.all())
            else:
                emailquery = Q(email__to_person=email.fromEmail, email__fromEmail__in=email.to_person.all())
            re_fw_message = Message.objects.filter(emailquery | Q(query, email__subject__contains=email.subject) & Q(**mydic)).select_related('email').all().values()



            ack_message = Message.objects.filter(email__to_person__in=admins.all(),
                                             email__fromEmail__in=email.to_person.all(), **mydic).select_related('email').all().values()
        except Exception as e:
            print(e)
        """
        refw = serialize('json', re_fw_message)
        ack = serialize('json', ack_message)
        data = {'re_fw_message':refw, 'ack_message':ack}
        """


        for refw in list(re_fw_message):
            email = Email_Detail.objects.get(id=refw['email_id'])
            refw['email_subject'] = email.subject
            try:
                parentchild = ParentMessage_ChildMessage.objects.get(parent_message_id=mymsg.id,child_message_id=refw['id'])
                if not parentchild.type:
                    refw['emailfilter'] = 'none'
                else:
                    refw['emailfilter'] = parentchild.type
            except:
                refw['emailfilter'] = 'none'
        for ack in list(ack_message):
            email = Email_Detail.objects.get(id=ack['email_id'])
            ack['email_subject'] = email.subject
            try:
                parentchild = ParentMessage_ChildMessage.objects.get(parent_message_id=mymsg.id,child_message_id=ack['id'])
                if not parentchild.type:
                    ack['emailfilter'] = 'none'
                else:
                    ack['emailfilter'] = parentchild.type
            except:
                ack['emailfilter'] = 'none'
        data = {'re_fw_message':list(re_fw_message),'ack_message':list(ack_message)}

        #data = serializers.serialize("json", data1)
        return JsonResponse(data)

@csrf_exempt
@login_required(login_url='People:login')
def change_email_relation(request):
    if not request.user.is_authenticated:
        return HttpResponse('<script type="text/javascript">alert("not authenticated");</script>')
    if request.is_ajax():
        try:
            parentmsgid = request.POST.get('parent_msgid', None)
            childmsgid = request.POST.get('child_msgid', None)
            relationtype = request.POST.get('relationtype')
            if not parentmsgid or not childmsgid:
                return JsonResponse({'result':'fail'})
            if relationtype == 'none':
                try:
                    pc = ParentMessage_ChildMessage.objects.get(parent_message_id=parentmsgid, child_message_id=childmsgid)
                    pc.delete()
                except:
                    pass
            else:
                pc, created = ParentMessage_ChildMessage.objects.get_or_create(parent_message_id=parentmsgid, child_message_id=childmsgid)
                pc.type = relationtype
                pc.save()

            return JsonResponse({'result':'success'})
        except:

            return JsonResponse({'result':'fail'})




class EmailSettingView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = "emailsystem/emailsetting.html"
    login_url = 'People:login'


    def get_context_data(self, **kwargs):
        context = super(EmailSettingView, self).get_context_data(**kwargs)
        return context


    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        context = self.get_context_data()
        user = request.user
        try:
            emailinfo = Email_Info.objects.filter(user=user)
            context['emailsetting'] = emailinfo

        except Email_Info.DoesNotExist:
            context['emailsetting'] = ''

        context['allacks'] = Email_ACK_Content.objects.all()
        context['allsignature'] = Email_Signature.objects.all()

        context['allemailinfo'] = Email_Info.objects.all()
        context['ackform'] = EmailACKForm()
        context['signatureform'] = EmailSignatureForm()

        context['email_option'] = Email_Option.objects.all()
        context['user_emailoption_choice'] = Email_Option_User_Choice.objects.filter(user=request.user)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        data = request.POST.dict()
        data.pop('csrfmiddlewaretoken', None)
        temp = []
        ids = []
        for i in data.items():
            if i[0].split('_')[0] == 'id':
                ids.append(i[1])
            else:
                temp.append([i[0].split('_')[0], i[0].split('_')[1], i[1]])
        sortL = []
        for i in ids:
            tempo = {}
            for t in temp:
                if str(i) == t[1]:
                    tempo[t[0]] = t[2]
            sortL.append([i, tempo])
        for sort in sortL:
            obj = Email_Info.objects.get(id=sort[0])
            obj.syncEmail = sort[1]['syncemail']
            obj.password = sort[1]['password']
            obj.imapServer = sort[1]['imapServer']
            obj.imapPort = sort[1]['imapPort']
            obj.smtpServer = sort[1]['smtpServer']
            obj.smtpPort = sort[1]['smtpPort']
            obj.popServer = sort[1]['popServer']
            obj.popPort = sort[1]['popPort']
            obj.syncEmailCount = sort[1]['syncEmailCount']
            if not obj.syncEmailCount:
                obj.syncEmailCount = None
            else:
                semailCount = int(obj.syncEmailCount)
                if semailCount < 0:
                    obj.syncEmailCount = 0
                elif semailCount > 1000:
                    obj.syncEmailCount = 1000
            if 'isImap' in sort[1]:
                if sort[1]['isImap'] == 'on':
                    obj.isImap = True
                else:
                    obj.isImap = False
            else:
                obj.isImap = False

            if 'isActive' in sort[1]:
                if sort[1]['isActive'] == 'on':
                    obj.isActive = True
                else:
                    obj.isActive = False
            else:
                obj.isActive = False

            obj.save()
        """
        syncemail = request.POST.get('sync_email')
        isImap = request.POST.get('isImap')
        password = request.POST['password']
        imapServer = request.POST['imapServer']
        imapPort  = request.POST['imapPort']
        popServer = request.POST['popServer']
        popPort = request.POST['popPort']
        #isImap = form.cleaned_data['isImap']
        if isImap == 'on':
            isImap = True
        else:
            isImap = False
        obj, created = Email_Info.objects.get_or_create(user=self.request.user, password=password, imapServer=imapServer,imapPort=imapPort,
                                         popServer=popServer, popPort=popPort, isImap=isImap, sync_email=syncemail)
        if created == False:
            obj.sync_email = syncemail
            obj.password = password
            obj.imapServer = imapServer
            obj.imapPort = imapPort
            obj.popServer = popServer
            obj.popPort = popPort
            obj.isImap = isImap
            obj.save()
        """

        return redirect('emailsystem:emailsetting')

@csrf_exempt
@login_required(login_url='People:login')
def emailsetting_signature(request, pk=None):
    if request.method == 'POST':
        form = EmailSignatureForm(request.POST)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
        return redirect('emailsystem:emailsetting')

    if request.method == 'GET':
        sig = Email_Signature.objects.get(id=pk)
        Email_Info.objects.filter(signature=sig).update(signature=None)
        sig.delete()
        return redirect('emailsystem:emailsetting')

@csrf_exempt
@login_required(login_url='People:login')
def emailsetting_ack(request, pk=None):
    if request.method == 'POST':
        form = EmailACKForm(request.POST)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
        return redirect('emailsystem:emailsetting')
    if request.method == 'GET':
        ack = Email_ACK_Content.objects.get(id=pk)
        Email_Info.objects.filter(ack_content=ack).update(ack_content=None)
        ack.delete()
        return redirect('emailsystem:emailsetting')


@csrf_exempt
@login_required(login_url='People:login')
def emailsetting_ack_sig_change(request):
    if not request.user.is_authenticated:
        return HttpResponse('<script type="text/javascript">alert("not authenticated");</script>')
    if request.is_ajax():
        try:
            emailinfoid = request.POST.get('emailinfo_id', None)
            ackid = request.POST.get('ack_id', None)
            if not ackid:
                sig_id = request.POST.get('sig_id', None)
            if not ackid and not sig_id:
                return JsonResponse({'result':'fail'})
            if ackid:
                if ackid == 'none':
                    einfo = Email_Info.objects.get(id=emailinfoid)
                    einfo.ack_content_id = None
                    einfo.save()
                else:
                    einfo = Email_Info.objects.get(id=emailinfoid)
                    einfo.ack_content_id = ackid
                    einfo.save()
            elif sig_id:
                if sig_id == 'none':
                    einfo = Email_Info.objects.get(id=emailinfoid)
                    einfo.signature_id = None
                    einfo.save()
                else:
                    einfo = Email_Info.objects.get(id=emailinfoid)
                    einfo.signature_id = sig_id
                    einfo.save()

            return JsonResponse({'result':'success'})
        except:

            return JsonResponse({'result':'fail'})


@csrf_exempt
@login_required(login_url='People:login')
def emailsetting_emailoption_choice(request):
    if not request.user.is_authenticated:
        return HttpResponse('<script type="text/javascript">alert("not authenticated");</script>')
    if request.is_ajax():
        try:
            emailoptioncodeid = request.POST.get('emailoptioncodeid', None)
            emailoptionid = request.POST.get('emailoptionid', None)
            if not emailoptioncodeid or not emailoptionid:
                return JsonResponse({'result':'fail'})
            emailoption = Email_Option.objects.get(id=emailoptionid)
            emailoptioncode = Email_Option_Code.objects.get(id=emailoptioncodeid)
            obj, created = Email_Option_User_Choice.objects.get_or_create(user=request.user, email_option=emailoption)
            obj.email_option_code = emailoptioncode
            obj.save()
            return JsonResponse({'result':'success'})
        except Exception as e:

            return JsonResponse({'result':'fail'})

@csrf_exempt
@login_required(login_url='People:login')
def email_ack_send(request):
    if not request.user.is_authenticated:
        return HttpResponse('<script type="text/javascript">alert("not authenticated");</script>')
    if request.is_ajax():
        try:
            emailId_forReply = request.POST.get('emailid_foreply', None)
            if not emailId_forReply:
                return JsonResponse({'result':'fail : not reply email'})
            email_detail = Email_Detail.objects.get(id=emailId_forReply)
            emailoption = Email_Option.objects.get(optionName='ACK_ACCOUNT_CHOICE')
            code = Email_Option_User_Choice.objects.get(user=request.user, email_option=emailoption).email_option_code.code
            if code == 'SYSTEM':
                emailoption_code = Email_Option_Code.objects.get(email_option__optionName='SYSTEM_EMAIL_ACCOUNT')
                #이메일 ack 보내는 계정을 시스템으로 할경우. Email_Option_Code에 SYSTEM_EMAIL_ACCOUNT의 code가 저장되어 있는데
                # 이건 sytem 계정으로 할 email info의 id 이다.
                emailinfo = Email_Info.objects.get(id=emailoption_code.code)
                result = send_ackemail(email_detail, emailinfo)
                return JsonResponse({'result': result})
            elif code == 'USER':
                for wemail in email_detail.to_person.all():
                    try:
                        emailinfo = Email_Info.objects.get(user=request.user, syncEmail=wemail.email)
                        result = send_ackemail(email_detail, emailinfo)
                        return JsonResponse({'result': result})
                    except Exception as e:
                        return JsonResponse({'result': 'fail :' + e})
        except Exception as e:
            return JsonResponse({'result': 'fail :' + str(e)})




