import datetime
import operator
from functools import reduce

from django import template
from django.db.models import Q

from People.models import Work_Email
from application_patent_domestic.models import Application_Patent_Domestic_Master
from common.codemultilanguage_choice import GetCodeMultpleLanguageForCase
from message.models import Message
from utils.enums import ALL_CODE_TYPE

register = template.Library()


@register.filter(is_safe=True)
def getvariable(val):
    return val


@register.inclusion_tag('forinclude/email_detail/include_email_relation.html', name='include_email_relation', takes_context=True)
def include_email_relation(context, email):

    context['emaildetail'] = email
    return context


@register.simple_tag
def email_relation(email, relationtype, filtername):
    mymsg = email.msg_email.first()
    mydic = {}
    if filtername == 'patent':
        mydic = {'master_message__in': mymsg.master_message.all()}
    elif filtername == 'case':
        mydic = {'case__in': mymsg.case.all()}
    elif filtername == 'task':
        mydic = {'task__in': mymsg.task.all()}
    if relationtype == "ACK":
        admins = Work_Email.objects.filter(Q(email='jy.park@liboip.com')|Q(email='libo@liboip.com'))
        message = Message.objects.filter(email__to_person__in=admins.all(), email__fromEmail__in=email.to_person.all(),
                                         **mydic)
    else:
        query = reduce(operator.and_, (Q(email_subject__contains=x) for x in ['RE','Re','re']))
        message = Message.objects.filter(Q(email__to_person=email.fromEmail, email__fromEmail__in=email.to_person.all(), **mydic) | Q(query, email_subject__contains=email.subject, **mydic))



    return {'REFW':message}

@register.inclusion_tag('forinclude/include_addcase.html', name='include_email_case_form', takes_context=True)
def include_email_case_form(context, linkmsg, form, caseevidencefile):

    context['linkmsg'] = linkmsg
    #context['master'] = master
    msgobj = Message.objects.get(id=linkmsg)
    context['receptionDate'] = msgobj.createdDate.strftime('%Y-%m-%d')
    form.fields['case_evidence_file'].queryset = caseevidencefile
    form.fields['receptionDate'].widget.attrs['readonly'] = True
    form.fields['caseNumber'].widget.attrs['readonly'] = True
    context['form'] = form
    return context

@register.inclusion_tag('forinclude/include_addcase_forpatentdoc.html', name='include_email_case_form_forpatentdoc', takes_context=True)
def include_email_case_form_forpatentdoc(context, linkmsg, form, caseevidencefile):

    context['linkmsg'] = linkmsg
    msgobj = Message.objects.get(id=linkmsg)
    context['receptionDate'] = msgobj.createdDate.strftime('%Y-%m-%d')
    form.fields['case_evidence_file'].queryset = caseevidencefile
    form.fields['receptionDate'].widget.attrs['readonly'] = True
    form.fields['caseNumber'].widget.attrs['readonly'] = True
    context['form'] = form
    return context


@register.inclusion_tag('forinclude/include_addtask.html', name='include_email_task_form', takes_context=True)
def include_email_task_form(context, linkmsg, taskform):

    context['linkmsg'] = linkmsg
    #context['master'] = master
    msgobj = Message.objects.get(id=linkmsg)
    context['internalCompletionDueDate'] = msgobj.createdDate.strftime('%Y-%m-%d')
    context['receptionDate'] = datetime.datetime.now().strftime('%Y-%m-%d')

    taskform.fields['internalCompletionDueDate'].widget.attrs['readonly'] = True
    taskform.fields['receptionDate'].widget.attrs['readonly'] = True
    taskform.fields['taskNumber'].widget.attrs['readonly'] = True
    context['taskform'] = taskform
    return context