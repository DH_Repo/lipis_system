from django import forms

from django.forms import ModelForm
from django_summernote.widgets import SummernoteWidget

from emailsystem.models import Email_Info, Email_ACK_Content, Email_Signature


class EmailSettingForm(forms.Form):
	sync_email = forms.CharField(widget=forms.EmailInput)
	password = forms.CharField(widget=forms.PasswordInput)
	imapServer = forms.CharField(widget=forms.TextInput)
	imapPort = forms.IntegerField(widget=forms.NumberInput)
	popServer = forms.CharField(widget=forms.TextInput)
	popPort = forms.IntegerField(widget=forms.NumberInput)
	isImap = forms.BooleanField(widget=forms.CheckboxInput, required=False)

class EmailFileUploadingForm(forms.Form):
	file = forms.FileField(widget=forms.FileField)

class EmailACKForm(forms.ModelForm):
    class Meta:
        model = Email_ACK_Content
        fields = ['ack_content']
        widgets = {
            'ack_content': SummernoteWidget(attrs={'summernote': {'height': '200px'}}),
        }
class EmailSignatureForm(forms.ModelForm):
    class Meta:
        model = Email_Signature
        fields = ['signature']
        widgets = {
            'signature': SummernoteWidget(attrs={'summernote': {'height': '200px'}}),
        }
