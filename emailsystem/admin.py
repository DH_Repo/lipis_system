from django.contrib import admin

# Register your models here.
#from emailsystem.models import Email_Info
from emailsystem.models import *

admin.site.register(Email_Detail)
admin.site.register(Email_Info)
admin.site.register(Email_Option)
admin.site.register(Email_Option_Code)
admin.site.register(Email_Option_User_Choice)
