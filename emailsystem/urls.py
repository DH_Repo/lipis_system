from django.urls import path

from emailsystem import views, syncemail, tasks, ajax

app_name = 'emailsystem'
urlpatterns = [


    path('newemailsetting', views.savenewemail, name='newemailsetting'),
    path('syncemail', tasks.sync_email, name='syncmail'),
    path('addduplicateparentemail/<pk>', views.add_duplicate_parent_email, name='addduplicateparentemail'),
    path('deleteduplicateparentemail/<pk>', views.delete_duplicate_parent_email, name='deleteduplicateparentemail'),
    path('addduplicatechildemail/<pk>', views.add_duplicate_child_email, name='addduplicatechildemail'),
    path('deleteduplicatechildemail/<pk>', views.delete_duplicate_child_email, name='deleteduplicatechildemail'),
    path('email', views.EmailListView.as_view(), name='emaillist'),
    path('emailcontent/<pk>', views.EmailContentView.as_view(), name='email_content'),
    path('emailcontentiframe/<pk>', views.EmailContentViewForIFrame.as_view(), name='emailcontentiframe'),
    path('email/showcidimg', views.showcidimg, name='email_showcidimg'),

    path('emaildetailfilter', views.SetHTMLFilter, name ='emaildetailfilter'),
    path('emailcontent', views.EmailContentView.as_view(), name='email_content'),
    path('emailtextbodyslice/<pk>', views.EmailTextBodySlice, name='emailtextbodyslice'),
    path('emailtextbody/<pk>', views.EmailTextBody, name='emailtextbody'),

    path('emailupload', views.EmaliUploadView.as_view(), name='emailupload'),
    path('emailuploading', views.EmailFileUpload, name='emailuploading'),
    path('syncemailfile', views.sync_email_file, name='syncemailfile'),
    path('testemail', syncemail.TestEmail, name='testemail'),
    path('searchemail', views.SearchEmailView.as_view(), name='searchemail'),

    path('add_emailattach/<pk>', views.AddEmailAttach, name='add_emailattach'),
    path('linkemailmsgbyourcompanynumber', views.LinkEmailMsgByOurCompanyNumber, name='linkemailmsgbyourcompanynumber'),
    path('linkemailmsgbyapplnumber', views.LinkEmailMsgByApplNumber, name='linkemailmsgbyapplnumber'),

    path('realtimeemail', views.syncmail, name='realtimeemail'),
    path('responseemailfilter', views.result_from_email_relation_filter, name='responseemailfilter'),
    path('email/change_relation', views.change_email_relation, name='email_change_relation'),

    path('emailsetting', views.EmailSettingView.as_view(), name='emailsetting'),
    path('emailsetting/signature',views.emailsetting_signature, name='emailsetting_signature'),
    path('emailsetting/signature/<pk>',views.emailsetting_signature, name='emailsetting_signature'),
    path('emailsetting/ack',views.emailsetting_ack, name='emailsetting_ack'),
    path('emailsetting/ack/<pk>',views.emailsetting_ack, name='emailsetting_ack'),
    path('emailsetting/acksigchange', views.emailsetting_ack_sig_change, name='emailsetting_ack_sig_change'),
    path('emailsetting/emailoption/choice',views.emailsetting_emailoption_choice, name='emailsetting_emailoption_choice'),
    path('email/ack/send', views.email_ack_send, name='email_ack_send'),



]