from django.apps import AppConfig


class EmailsystemConfig(AppConfig):
    name = 'emailsystem'
    verbose_name = 'EmailSystem'