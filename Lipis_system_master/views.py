from time import sleep

from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.views.generic.base import TemplateView
from django.apps import apps
from django.utils import translation
from utils.enums import LANGUAGE_CODE_INSERT


class HomePageView(TemplateView):
    template_name = 'homepage.html'

class HomeMixin(object):

    def get_context_data(self, **kwargs):
        context = super(HomeMixin, self).get_context_data(**kwargs)

        if 'django_language' not in self.request.session:
            self.request.session['django_language'] = 'ko'

        lang = self.request.session['django_language']
        translation.activate(lang)

        context['selectedlang'] = lang
        countrylist = []
        for l in LANGUAGE_CODE_INSERT:
            countrylist.append({'languageCode': l.name, 'languageName': l.value})
        context['countryCodes'] = countrylist
        return context

class HomeView(HomeMixin, TemplateView):
    template_name = 'home.html'


def ChangeLanguage(request):

        language = request.POST.get('lang')

        request.session['django_language'] = language

        translation.activate(language)
        return JsonResponse(['ok'], safe=False)

