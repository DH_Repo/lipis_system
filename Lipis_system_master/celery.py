import os
from datetime import timedelta

from celery import Celery, platforms
from celery._state import _set_current_app
# set the default Django settings module for the 'celery' program.
from celery.bin import base
from celery.bin.celery import CeleryCommand
from celery.schedules import crontab

from Lipis_system_master import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Lipis_system_master.settings')

app = Celery('Lipis_system_master')
#_set_current_app(app)
# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')
#app.control.inspect().active()
# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

#status = CeleryCommand.commands['status']()
#status.app = status.get_app()
delta = timedelta(minutes=5)
app.conf.update(
    CELERY_BROKER_URL = 'redis://localhost:6379/0',
    CELERY_ACCEPT_CONTENT = ['application/json'],
    CELERY_RESULT_BACKEND = 'django-db',
    #CELERY_RESULT_SERIALIZER = 'json',
    CELERY_TASK_SERIALIZER = 'json',
    CELERY_TIMEZONE = 'Asia/Seoul',
    CELERY_IMPORTS = ('emailsystem.tasks'),
    CELERY_ENABLE_UTC=False,
    CELERY_RESULT_EXPIRES = 1000,
    CELERY_BEAT_SCHEDULE={
        'sync_email_task': {
            'task': 'emailsystem.tasks.syncmail_task',
            'schedule': crontab(minute='*/8'),
        },
    },
)
"""
CELERY_RESULT_EXPIRES=delta,
# Application definition
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Asia/Seoul'
CELERY_IMPORTS = ('emailsystem.tasks')

CELERY_BEAT_SCHEDULE = {
    'printHello': {
        'task': 'emailsystem.tasks.debug_task',
        'schedule': 30.0,
    },
}

CELERY_ANNOTATIONS = {'emailsystem.tasks.debug_task': {'rate_limit': '10/s'}}
"""

"""
def checkstatus():
    try:
        status.run()
        return True
    except base.Error as e:
        if e.status == platforms.EX_UNAVAILABLE:
            return False
        raise e

    return status
"""