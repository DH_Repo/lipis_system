"""Lipis_system_master URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from Lipis_system_master import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.HomeView.as_view(), name='home'),
    path('homepage/', views.HomePageView.as_view(), name='homepage'),
    path('application_patent_domestic/', include('application_patent_domestic.urls')),
    path('people/', include('People.urls')),
    path('management/', include('management.urls')),
    path('message/', include('message.urls')),
    path('case/', include('case.urls')),
    path('emailsystem/', include('emailsystem.urls')),
    path('changelang', views.ChangeLanguage, name='changelang'),
    path('summernote/', include('django_summernote.urls')),
    path('patentro/', include('patentro.urls')),

]
