import datetime
import os
import shutil
import sys
import zipfile
from pathlib import Path

from django.contrib.auth.decorators import login_required
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.db import transaction
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt

from application_patent_domestic.models import Application_Patent_Domestic_Applicant, \
    Application_Patent_Domestic_Master_Applicant, Agent, Application_Patent_Domestic_Master_Agent, Priority_Claim, \
    NationResearchProject, \
    ApplicationPatentDomesticMaster_NationResearchProject
from common.codemultilanguage_choice import GetCodeMultpleLanguageForMessage
from management.models import Code_Information
from message.forms import MessageModelForm, MessageForPatentModelForm
from message.models import Message, File, Patent_Document_Attachment
from message.syncpatentdocu import parsing_patent_submit_docu
from utils.db_fieldset import get_fielddic
from utils.dir_mgr import make_abstractFig_attachment_path, make_brandimage_attachment_path
from utils.enums import ALL_CODE_TYPE


@login_required(login_url='People:login')
def MessageCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Message')
    if request.method == 'POST':
        form = MessageModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        nowdate = datetime.datetime.now()
        year = nowdate.strftime('%y')
        mon = nowdate.strftime('%m')
        day = nowdate.strftime('%d')
        srcodename = Code_Information.objects.filter(codeType=ALL_CODE_TYPE.Message_ReceiveSendType.name).first().codeName
        mcodename = Code_Information.objects.filter(codeType=ALL_CODE_TYPE.Message_MessageType.name).first().codeName
        ourlist = []
        f = Message.objects.filter(
            messageCode__regex=r'(^M)(' + srcodename + mcodename + ')(' + year + ')(' + mon + ')(' + day + ')(\d{5})')
        serialnum = ''
        if f:
            for l in f:
                ourlist.append(int(l.messageCode[-5:]))
            serialnum = year + mon + day + str(max(ourlist) + 1).zfill(5)
        else:
            serialnum = year + mon + day + '00001'

        serialnum = 'M' + srcodename + mcodename + serialnum
        customcodechoice = GetCodeMultpleLanguageForMessage(request)
        form = MessageModelForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)

    return render(request, 'message/messagemanagement_form.html', {'form': form, 'serialnum':serialnum})

@login_required(login_url='People:login')
def MessageEdit(request, pk):
    instance = get_object_or_404(Message, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Message')
    if request.method == 'POST':
        form = MessageModelForm(request.POST, request.FILES, instance=instance, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeMultpleLanguageForMessage(request)
        form = MessageModelForm(instance=instance, labeldic=labeldic, customcodechoice=customcodechoice, request=request)
    return render(request, 'message/messagemanagement_form.html', {
        'form': form,
    })

@login_required(login_url='People:login')
def MessageEditForPatent(request, pk):
    instance = get_object_or_404(Message, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Message')
    if request.method == 'POST':
        form = MessageForPatentModelForm(request.POST, request.FILES, instance=instance, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeMultpleLanguageForMessage(request)
        form = MessageForPatentModelForm(instance=instance, labeldic=labeldic, customcodechoice=customcodechoice)
    return render(request, 'message/messagemanagement_form.html', {
        'form': form,
    })

@login_required(login_url='People:login')
@csrf_exempt
def MessageDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Message.objects.get(id=pk).delete()
        return render(request, 'message/messagemanagement.html')

@login_required(login_url='People:login')
def SearchOurComnumForMessage(request):
    numberforsearch = request.GET.get('searchournum')
    ourlist = []
    f = Message.objects.filter(
        messageCode__regex=r'('+numberforsearch+')(\d{5})')
    serialnum = ''
    if f:
        for l in f:
            ourlist.append(int(l.messageCode[5:9]))
        serialnum = numberforsearch + str(max(ourlist) + 1).zfill(5)
    else:
        serialnum = numberforsearch + '00001'
    data = {'serialnum': serialnum}
    return JsonResponse(data)

@login_required(login_url='People:login')
@transaction.atomic
def patent_submit_docu_patent_sync(request):
    if request.method == 'GET':
        pk = request.GET.get('pk')
        msg = Message.objects.get(id=pk)
        file = Patent_Document_Attachment.objects.get(patentoffice_docu=msg.patent_docu).path
        master = msg.master_message.all().first()


    if sys.platform == "linux":
        unzippath = str(Path(file.path.split('.')[0]))
    else:

        unzippath = os.path.abspath(file.path.split('.')[0])
        if not os.path.exists(unzippath):
            os.makedirs(unzippath)
    with zipfile.ZipFile(file, 'r') as zip_ref:
        zip_ref.extractall(unzippath)
    soup = parsing_patent_submit_docu(unzippath)

    # read image unzippath + / + 태그 속성 (file)

    # file save default_storage.save(내가만든 폴더 + / + file, ContentFile(contents))

    # 이미지 필드 저장 시 위의 return path 지정.
    # 출원인
    applicants = soup.find_all('KR_Applicant'.lower())
    for count, applicant in enumerate(applicants):
        applicant_obj = Application_Patent_Domestic_Applicant()
        kr_application_code = applicant.find('KR_ApplicantCode'.lower())
        if kr_application_code:
            applicant_filter = Application_Patent_Domestic_Applicant.objects.filter(code=kr_application_code.text)
            if applicant_filter:
                applicant_obj = applicant_filter.first()
            else:
                applicant_obj.code = kr_application_code.text

        kr_name = applicant.find('KR_Name'.lower())
        if kr_name:
            applicant_obj.korFullName = kr_name.text
            applicant_obj.isCorporation = False
        kr_org_name = applicant.find('KR_ORGName'.lower())
        if kr_org_name:
            applicant_obj.korFullName = kr_org_name.text
            applicant_obj.isCorporation = True

        applicant_obj.save()
        th, created = Application_Patent_Domestic_Master_Applicant.objects.get_or_create(master=master, applicant=applicant_obj)
        th.priority = count
        th.save()

    # 대리인
    agents = soup.find_all('KR_Agent'.lower())
    for count, agent in enumerate(agents):
        agent_obj = Agent()
        kr_agent_code = agent.find('KR_AgentCode'.lower())
        if kr_agent_code:
            agent_filter_obj = Agent.objects.filter(agentCode=kr_agent_code.text)
            if agent_filter_obj:
                agent_obj = agent_filter_obj.first()
            else:
                agent_obj.agentCode = kr_agent_code.text

        kr_name = agent.find('KR_Name'.lower())
        if kr_name:
            agent_obj.korName = kr_name.text
            agent_obj.isCorporation = False
        kr_org_name = agent.find('KR_ORGName'.lower())
        if kr_org_name:
            agent_obj.korName = kr_org_name.text
            agent_obj.isCorporation = True
        agent_obj.save()
        agent_th = Application_Patent_Domestic_Master_Agent.objects.get_or_create(master=master, agent=agent_obj)
        agent_th.priority = count
        agent_th.save()

        # GPOACode 보류
        kr_inclusion_attorney_register_number = agent.find('KR_InclusionAttorneyRegisterNumber'.lower())
        if kr_inclusion_attorney_register_number:
            print(kr_inclusion_attorney_register_number.text)

    # 출원에 발명 이름 입력.
    invention_title = soup.find('KR_InventionTitle'.lower())
    if invention_title:
        master.inventNameKor = invention_title.text
    invention_en_title = soup.find('KR_InventionENTitle'.lower())
    if invention_en_title:
        master.inventNameEng = invention_en_title.text



    # invertor는 주민번호가 없고, 두 개로 pk를 특정하기 애매해서 보류
    """
    inventors = soup.find_all('KR_Inventor'.lower())
    for inventor in inventors:
        kr_name = inventor.find('KR_Name'.lower())
        print(kr_name.text)
        kr_en_name = inventor.find('KR_ENName'.lower())
        print(kr_en_name.text)
        kr_post_code = inventor.find('KR_Postcode'.lower())
        print(kr_post_code.text)
        kr_address = inventor.find('KR_Address'.lower())
        print(kr_address.text)
        kr_en_address = inventor.find('KR_ENAddress'.lower())
        print(kr_en_address.text)
    """
    # 우선권 주장 입력
    priority_claims = soup.find_all('KR_PriorityClaims'.lower())
    for count, priorty_claim in enumerate(priority_claims):
        priority_obj = Priority_Claim()
        kr_application_country = priorty_claim.find('KR_ApplicationCountry'.lower())
        kr_priority_application_number = priorty_claim.find('KR_PriorityApplicationNumber'.lower())
        if kr_application_country and kr_priority_application_number:
            priority_filter_obj = Priority_Claim.objects.filter(countryCode=kr_application_country.text, applNo=kr_priority_application_number.text,
                                                                master=master)
            if priority_filter_obj:
                priority_obj = priority_filter_obj.first()
            priority_obj.countryCode = kr_application_country.text
            priority_obj.applNo = kr_priority_application_number.text
            kr_priority_application_date = priorty_claim.find('KR_PriorityApplicationDate'.lower())
            if kr_priority_application_date:
                priority_date = kr_priority_application_date.text.replace(' ', '').replace('.', '-')
                priority_obj.appDate = priority_date
            priority_obj.master = master
            priority_filter_obj.priority = count
            priority_obj.save()


    projects = soup.find_all('KR_NationResearchProjectAsistOfInvention'.lower())
    for count, project in enumerate(projects):
        n_project_obj = NationResearchProject()
        kr_subject_id = project.find('KR_SubjectID'.lower())
        if kr_subject_id:
            n_project_filter_obj = NationResearchProject.objects.filter(subjectID=kr_subject_id.text)
            if n_project_filter_obj:
                n_project_obj = n_project_filter_obj.first()
            else:
                n_project_obj.subjectID = kr_subject_id.text
        kr_ministry_name = project.find('KR_MinistryName'.lower())
        if kr_ministry_name:
            n_project_obj.ministryName =  kr_ministry_name.text
        kr_research_manage_expert_machin = project.find('KR_ResearchManageExpertMachin'.lower())
        if kr_research_manage_expert_machin:
            n_project_obj.researchManageExpertMachine = kr_research_manage_expert_machin.text
        kr_research_project_name = project.find('KR_ResearchProjectName'.lower())
        if kr_research_project_name:
            n_project_obj.researchProjectName = kr_research_project_name.text
        kr_research_subject_name = project.find('KR_ResearchSubjectName'.lower())
        if kr_research_subject_name:
            n_project_obj.researchSubjectName = kr_research_subject_name.text
        kr_contribution_ration = project.find('KR_ContributionRatio'.lower())
        if kr_contribution_ration:
            n_project_obj.contributionRatio = kr_contribution_ration.text
        kr_management_machinery = project.find('KR_ManagementMachinery'.lower())
        if kr_management_machinery:
            n_project_obj.managementMachinery = kr_management_machinery.text
        kr_research_period = project.find('KR_ResearchPeriod'.lower())
        if kr_research_period:
            period = kr_research_period.text.split('~')
            n_project_obj.researchPeriodStart = period[0].replace(' ', '').replace('.', '-')
            n_project_obj.researchPeriodEnd = period[1].replace(' ', '').replace('.', '-')
        n_project_obj.save()
        project_th = ApplicationPatentDomesticMaster_NationResearchProject.objects.get_or_create(master=master,
                                                                                    project=n_project_obj,
                                                                                    )
        project_th.priority = count
        project_th.save()



    figref = soup.find('figref')
    if figref:
        num = figref.attrs['num']
        drawings = soup.find('drawings')
        figures = drawings.find_all('figures')
        for figure in figures:
            if num == figure.attrs['num']:
                master.abstractFigNum = num
                img = figure.find('img')
                filename = img.attrs['file']
                with open(str(Path(unzippath+'/' + filename)), 'rb') as f:
                    contents = f.read()
                    custompath = make_abstractFig_attachment_path(request.user.username)
                    path = default_storage.save(custompath + '/' + filename, ContentFile(contents))
                    master.abstractFig = path
                break

    # brandimage
    EMI = soup.find('EMI'.lower())
    if EMI:
        brand_image = EMI.attrs['file']
        with open(str(Path(unzippath + '/' + brand_image)), 'rb') as f:
            contents = f.read()
            custompath = make_brandimage_attachment_path(request.user.username)
            path = default_storage.save(custompath + '/' + brand_image, ContentFile(contents))
            master.brandImage = path
    master.save()
    msg.patent_docu.updateDateApp = datetime.datetime.today()
    msg.patent_docu.save()
    ######## 압축 푼 폴더 삭제
    shutil.rmtree(unzippath)
    return redirect('message:patentsubmitdocumsg')