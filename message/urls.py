from django.urls import path
from message import views, crudmodel, requests, ajax

app_name = 'message'
urlpatterns = [




    path('patentdocumsg', views.PatentDocuMessageView.as_view(), name='patentdocumsg'),
    path('patentsubmitdocumsg', views.PatentSubmitDocuMessageView.as_view(), name='patentsubmitdocumsg'),
    path('patentdocuupload', views.PatentDocuUploadView.as_view(), name='patentdocuupload'),
    path('patentdocu/uploading', requests.PatentDocuFileUploading, name='patentdocuuploading'),
    path('patentdocu/sync', requests.sync_patentdocu, name='syncpatentdocu'),

    path('patentsubmitdocuupload', views.PatentSubmitDocuUploadView.as_view(), name='patentdocusubmitupload'),
    path('patentsubmitdocu/uploading', ajax.PatentSubmitDocuFileUploading, name='patentsubmitdocu_uploading'),
    path('patentsubmitdocu/ajax/patent/get', ajax.PatentSubmitDocuGetPatent, name='patentsubmitdocu_patent_get'),
    path('patentsubmitdocu/patent/sync', crudmodel.patent_submit_docu_patent_sync, name='patentsubmitdocu_patent_sync'),


    path('emailmessage', views.EmailMessageView.as_view(), name='emailmessage'),


    path('searchemailmessage', views.SearchEmailMessageView.as_view(), name='searchemailmessage'),

    path('selectfromemailmessage', views.SelectFromEmailMessageView.as_view(), name='selectfromemailmessage'),

    path('searchmasterinemailmessage/<pk>', views.GetMasterInEmailMessage, name='searchmasterinemailmessage'),
    path('searchinmaster/<pk>', views.SearchInMaster, name='searchinmaster'),
    path('linkpatentmastermessage', views.LinkPatentMasterMessage, name='linkpatentmastermessage'),
    path('dellinkpatentmastermessage', views.DelLinkPatentMasterMessage, name='dellinkpatentmastermessage'),
    path('setclassfied', views.SetClassfied, name='setclassfied'),
    path('message/emailiframe/setclassfied', views.EmailIframeSetClassfied, name='emailiframe_setclassfied'),

    path('addattachmentdraganddropview/<pk>', views.AddEmailAttachmentUploadView.as_view(), name='addattachmentdraganddropview'),
    path('addattachment/<pk>', views.AddAttachment, name='addattachment'),
    path('classifyingallmsg', views.ClassfyingAll, name='classifyingallmsg'),

    path('messagemgr', views.MessageManagementView.as_view(), name='messagemgr'),
    path('messagemgr/create', crudmodel.MessageCreate, name='create_messagemgr'),
    path('messagemgr/edit/<pk>', crudmodel.MessageEdit, name='edit_messagemgr'),
    path('messagemgr/delete', crudmodel.MessageDelete, name='delete_messagemgr'),
    path('messagemgr/search/ournumber', crudmodel.SearchOurComnumForMessage, name='searchournumber_messagemgr'),
    path('message/patent/edit/<pk>', crudmodel.MessageEditForPatent, name='message_patent_edit')



]