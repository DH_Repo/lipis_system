from django.forms import ModelForm

from application_patent_domestic.forms import ModelFormBase
from common.codemultilanguage_choice import MakeTagChoiceMultipleLanguage
from message.models import Message, Application_Patent_Domestic_Master_Message
from utils.enums import ALL_CODE_TYPE
from django import forms

class MessageModelForm(ModelFormBase):
	def __init__(self, *args, **kwargs):
		super(MessageModelForm, self).__init__(*args, **kwargs)
		if self.labeldic:
			for i, (k, v) in enumerate(self.labeldic.items()):
				if k != 'createdDate':
					self.fields[k].label = v
		if self.customcodechoice:
			self.fields['send_receiveType'].choices = self.customcodechoice[ALL_CODE_TYPE.Message_ReceiveSendType.name]
			self.fields['type'].choices = self.customcodechoice[ALL_CODE_TYPE.Message_MessageType.name]
			MakeTagChoiceMultipleLanguage(self)
	class Meta:
		model = Message
		fields = '__all__'
		widgets = {
			'receptionDate': forms.DateInput(attrs={'type': 'date'}),
		}

class MessageForPatentModelForm(ModelFormBase):
	def __init__(self, *args, **kwargs):
		super(MessageForPatentModelForm, self).__init__(*args, **kwargs)
		"""
		if self.labeldic:
			for i, (k,v) in enumerate(self.labeldic.items()):
				if k != 'createdDate':
					self.fields[k].label = v
		"""
	class Meta:
		model = Message
		fields = ('master_message',)