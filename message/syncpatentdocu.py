import datetime
import glob
import os
import shutil
import sys
import zipfile
from os import path
from pathlib import Path
from time import localtime

from bs4 import BeautifulSoup
from django.contrib.sites.models import Site
from django.db import transaction
from django.shortcuts import redirect

from Lipis_system_master import settings
from application_patent_domestic.models import Application_Patent_Domestic_Master
from message.models import Patent_Document, Patent_Document_Attachment, Message
from utils.dir_mgr import make_attachment_path
from utils.enums import UPLOAD_FILE_PATH, PATENT_OFFICE_DOCUMENT_FILETYPE, MESSAGE_TYPE

def find_patent_master_with_applno(num):
    for master in Application_Patent_Domestic_Master.objects.all():
        if master.applicationNumber.replace('-', '') == num:
            return master


def parsing_patent_submit_docu(unzippath):

    if sys.platform == "linux":
        filelist = glob.glob(unzippath +'/'+ '*.xml')
    else:
        filelist = glob.glob(unzippath +'\\'+ '*.xml')

    with open(filelist[0], "rt", encoding='UTF8') as file:
        # Read each line in the file, readlines() returns a list of lines
        content = file.readlines()
        # Combine the lines in the list into a string
        content = "".join(content)
        soup = BeautifulSoup(content, "lxml")
        return soup

@transaction.atomic
def insert_submit_patent_docu(request, filename, ourcomno):
    if sys.platform == "linux":
        unzippath = str(Path(filename.split('.')[0]))
        filepath = str(Path(filename))
    else:
        unzippath = os.path.abspath(filename.replace('.gzip', ''))
        filepath = os.path.abspath(filename)
        if not os.path.exists(unzippath):
            os.makedirs(unzippath)
    with zipfile.ZipFile(filename, 'r') as zip_ref:
        zip_ref.extractall(unzippath)
    soup = parsing_patent_submit_docu(unzippath)
    if '-' not in ourcomno:
        ourcomno = ourcomno[:7] + '-' + ourcomno[7:]
    patent = Application_Patent_Domestic_Master.objects.get(
            ourCompanyNumber=ourcomno)

    findpd = Patent_Document.objects.filter(isSubmitDoc=True, receiptSendCode=os.path.basename(filename).split('_')[1])
    if not findpd:
        pd = Patent_Document()
    else:
        pd = findpd.first()

    submitdate = soup.find_all()[2].get('submitdate')
    if submitdate:
        pd.receiptSendDate = submitdate.replace(' ', '').replace('.', '-')

    KR_DocumentCode = soup.find('KR_DocumentCode'.lower())
    if KR_DocumentCode:
        pd.docuCode = KR_DocumentCode.string

    KR_DocumentName = soup.find('KR_DocumentName'.lower())
    if KR_DocumentName:
        pd.sendSubmitDocuName = KR_DocumentName.string

    KR_DocumentCase = soup.find('KR_DocumentCase'.lower())
    if KR_DocumentCase:
        pd.docuCase = KR_DocumentCase.string

    KR_DocumentCaseSub = soup.find('KR_DocumentCaseSub'.lower())
    if KR_DocumentCaseSub:
        pd.docuCaseSub = KR_DocumentCaseSub.string

    KR_ReferenceNumber = soup.find('KR_ReferenceNumber'.lower())
    if KR_ReferenceNumber:
        pd.refNumKIPO = KR_ReferenceNumber.string

    KR_AfterReductionFee = soup.find('KR_AfterReductionFee'.lower())
    if KR_AfterReductionFee:
        pd.officialFee = KR_AfterReductionFee.string
    else:
        KR_ApplicationFee = soup.find('KR_ApplicationFee'.lower())
        if KR_ApplicationFee:
            pd.officialFee = KR_ApplicationFee.string
    pd.isSubmitDoc = True
    pd.receiptSendCode = filename.split('_')[2]
    pd.save()


    ###### 첨부파일 저장
    custompath = make_attachment_path(request.user.username)
    mySavePath = os.path.join(settings.MEDIA_URL, custompath)
    onlyfilename = os.path.basename(filename)
    urlsavepath = str(Path(custompath)) +'/' + onlyfilename
    pda = Patent_Document_Attachment()
    msavePath = os.getcwd() + str(Path(mySavePath))
    # 파일 옮긴다.

    if not os.path.exists(msavePath):
        os.makedirs(msavePath)
    if sys.platform == "linux":
        shutil.copy(filepath, str(Path(msavePath+'/'+onlyfilename)))
    else:
        shutil.copy(filepath, str(Path(msavePath) / onlyfilename))
    # 첨부파일 생성
    pda.fileType = PATENT_OFFICE_DOCUMENT_FILETYPE.SubmitZIP.name
    pda.sendDocuName = pd.sendSubmitDocuName
    pda.sendDocuFileName = onlyfilename
    pda.path = urlsavepath
    pda.patentoffice_docu = pd
    pda.save()
    #####################################

    # 메지지 생성 및 특허청 문서 연결, 출원 연결
    msg = Message()
    msg.patent_docu = pd
    msg.type = MESSAGE_TYPE.KP.name
    msg.save()
    msg.master_message.add(patent)
    msg.user.add(request.user)

    # 기존 파일이 있다면 폴더 전체 삭제
    if os.path.exists(UPLOAD_FILE_PATH.patentsubmitdocu.value):
        file_list = glob.glob(str(Path(UPLOAD_FILE_PATH.patentsubmitdocu.value) / '*'))
        shutil.rmtree(UPLOAD_FILE_PATH.patentsubmitdocu.value)




def insert_patentdocu(request, file):

    if '.zip' in file:
        custompath = make_attachment_path(request.user.username)
        mySavePath = os.path.join(settings.MEDIA_URL, custompath)
        # hdrfile = [i for i in file_list if os.path.basename(file) + '.HDR' == i]
        hdrname = file.replace('.zip', '') + '.HDR'
        fulhdrpath = hdrname
        if not os.path.exists(fulhdrpath):
            print('not exist outerhdr :', fulhdrpath)
            return
        patent_docu = Patent_Document()
        if os.path.exists(fulhdrpath):
            with open(fulhdrpath, 'rt', encoding='utf-8') as f:
                #hdrattach = Patent_Document_Attachment()
                #hdrattach.fileType = PATENT_OFFICE_DOCUMENT_FILETYPE.OuterHDR
                #hdrattach.sendDocuFileName = hdrname
                #shutil.copy(os.path.join(os.getcwd(), fulhdrpath), os.path.join(mySavePath, os.path.basename(fulhdrpath)))
                #hdrattach.path = os.path.join(mySavePath, os.path.basename(fulhdrpath))
                for line in f.readlines():
                    if '[수신일자]' in line:
                        recDate = line.split(' ')[1].strip('\n').strip()

                    if '[수신시간]' in line:
                        recTime = line.split(' ')[1].strip('\n').strip()
                patent_docu.receivedDateTime = datetime.datetime.strptime(recDate + " " + recTime, '%Y%m%d %H%M%S')
            os.remove(fulhdrpath)

        unzippath = Path(os.getcwd()) / Path(file).name.split('.')[0]
        if not os.path.exists(unzippath):
            os.makedirs(unzippath)
        with zipfile.ZipFile(file, 'r') as zip_ref:
            zip_ref.extractall(unzippath)
        foldername = unzippath.name
        msavePath = os.getcwd() + str(Path(mySavePath) / foldername)
        urlsavepath = Path(custompath) / foldername
        if not os.path.exists(msavePath):
            os.makedirs(msavePath)
        shutil.rmtree(msavePath)
        shutil.copytree(unzippath, msavePath)
        os.remove(file)
        shutil.rmtree(unzippath)

        unzip_filelist = glob.glob(str(msavePath) + '/*')
        for ufile in unzip_filelist:
            if ufile.endswith('.HDR'):
                #innerhdr = Patent_Document_Attachment()
                #innerhdr.fileType = PATENT_OFFICE_DOCUMENT_FILETYPE.InnerHDR
                #innerhdr.path = ufile
                #innerhdr.sendDocuFileName = 'HEADER.HDR'
                with open(ufile, 'rt', encoding='cp949') as f:
                    mainpdf = Patent_Document_Attachment()
                    for line in f.readlines():
                        if '[수신인]' in line:
                            receiver = line.replace('[수신인]', '').strip('\n').strip()
                            # print('[수신인] :' + receiver)
                            patent_docu.receiverName = receiver
                        elif '[수신인코드]' in line:
                            recvcode = line.replace('[수신인코드]', '').strip('\n').strip()
                            # print('[수신인코드] :' + recvcode)
                            patent_docu.receiverCode = recvcode
                        elif '[발송번호]' in line:
                            sendcode = line.replace('[발송번호]', '').strip('\n').strip()
                            # print('[발송번호] :' + sendcode)

                            obj = Patent_Document.objects.filter(receiptSendCode=sendcode)
                            if obj.count() != 0:
                                if os.path.exists(file):
                                    os.remove(file)
                                return

                            patent_docu.receiptSendCode = sendcode
                        elif '[발송일자]' in line:
                            senddate = line.replace('[발송일자]', '').strip('\n').strip()
                            # print('[발송일자] :' + senddate)
                            dt = datetime.datetime.strptime(senddate, '%Y%m%d').strftime('%Y-%m-%d')
                            patent_docu.receiptSendDate = dt
                        elif '[문서코드]' in line:
                            docucode = line.replace('[문서코드]', '').strip('\n').strip()
                            # print('[문서코드] :' + docucode)
                            patent_docu.docuCode = docucode
                        elif '[사건번호]' in line:
                            casanum = line.replace('[사건번호]', '').strip('\n').strip()
                            # print('[사건번호] :' + casanum)

                            ## 입력해야 함.

                        elif '[발송일련번호]' in line:
                            sendserial = line.replace('[발송일련번호]', '').strip('\n').strip()
                            # print('[발송일련번호] :' + sendserial)
                            mainpdf.sendDocuSerialNumber = sendserial
                            mainpdf.fileType = PATENT_OFFICE_DOCUMENT_FILETYPE.OriginPDF
                        elif '[발송서류명]' in line:
                            senddocu = line.replace('[발송서류명]', '').strip('\n').strip()
                            # print('[발송서류명] :' + senddocu)
                            mainpdf.sendDocuName = senddocu
                        elif '[발송서류파일명]' in line:
                            senddocuname = line.replace('[발송서류파일명]', '').strip('\n').strip()
                            # print('[발송서류파일명] :' + senddocuname)
                            mainpdf.sendDocuFileName = senddocuname

                            fullpath = str(urlsavepath / senddocuname)
                            mainpdf.path = fullpath
                            # pdfpath =[i for i in f.readlines() if os.path.basename(i) == docuname]
                        elif '[제출마감일자]' in line:
                            dealline = line.replace('[제출마감일자]', '').strip('\n').strip()
                            # print('[제출마감일자] :' + dealline)
                            if dealline:
                                
                                try:
                                    dt = datetime.datetime.strptime(dealline, '%Y%m%d').strftime('%Y-%m-%d')
                                except:
                                    dt = datetime.datetime.strptime(dealline, '%Y.%m.%d').strftime('%Y-%m-%d')
                                mainpdf.deadLineDate = dt
                    try:
                        pdobj = Patent_Document.objects.get(docuCode=patent_docu.docuCode,
                                                            receiptSendCode=patent_docu.receiptSendCode)
                        patent_docu = pdobj
                        print('exist patent document.')
                    except Patent_Document.DoesNotExist:
                        patent_docu.save()
                        print('inserted patent document.')
                    except Exception as e:
                        raise e

                    mainpdf.patentoffice_docu = patent_docu
                    try:
                        obj = Patent_Document_Attachment.objects.get(sendDocuFileName=mainpdf.sendDocuFileName,
                                                                     path=mainpdf.path)
                        mainpdf = obj
                    except Patent_Document_Attachment.DoesNotExist:
                        mainpdf.save()



                with open(ufile, 'rt', encoding='cp949') as f:
                    data = f.read()
                    if '[첨부서류]' in data:
                        attach = data.split('[첨부서류]\n')[1].strip('\n')
                        attline = attach.split('\n')

                        for att in attline:
                            if '[일련번호]' in att:
                                patentattach = Patent_Document_Attachment()
                                patentattach.fileType = PATENT_OFFICE_DOCUMENT_FILETYPE.AttachPDF
                                serial = att.replace('[일련번호]', '').strip('\n').strip()
                                # print('[일련번호] :' + serial)
                                patentattach.serialNumber = serial
                                attindex = attline.index(att)
                                patentattach.sendDocuName = attline[attindex + 1].replace('[서류명]', '').strip('\n').strip()
                                docfilename = attline[attindex + 2].replace('[서류파일명]', '').strip('\n').strip()
                                patentattach.sendDocuFileName = docfilename
                                patentattach.path = str(urlsavepath / docfilename)
                                patentattach.parent = mainpdf
                                patentattach.patentoffice_docu = patent_docu
                                try:
                                    obj = Patent_Document_Attachment.objects.get(
                                        sendDocuFileName=patentattach.sendDocuFileName, path=patentattach.path)
                                except Patent_Document_Attachment.DoesNotExist:
                                    patentattach.save()
                os.remove(ufile)
        msg = Message()
        msg.patent_docu = patent_docu
        msg.type = MESSAGE_TYPE.KP.name
        msg.save()
        msg.user.add(request.user)
        if casanum:
            obj = find_patent_master_with_applno(casanum)
            if obj is not None:
                msg.isClassified = True
                msg.master_message.add(obj)

