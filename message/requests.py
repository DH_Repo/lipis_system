import gzip
import os
import shutil
from pathlib import Path
import glob

from bs4 import BeautifulSoup
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt

from application_patent_domestic.models import Application_Patent_Domestic_Master
from message.syncpatentdocu import insert_patentdocu
from utils.db_fieldset import get_feild_names
from utils.enums import UPLOAD_FILE_PATH

def sync_patentdocu(request):
    file_list = glob.glob(str(Path(UPLOAD_FILE_PATH.patentdocu.value) / '*'))
    for file in file_list:
        insert_patentdocu(request, file)
    return redirect('message:patentdocuupload')


@login_required(login_url='People:login')
@csrf_exempt
def PatentDocuFileUploading(request):
    dirc = UPLOAD_FILE_PATH.patentdocu.value
    if request.method == 'POST':
        files = request.FILES.getlist('files[]')
        if not os.path.exists(dirc):
            os.makedirs(dirc)
        for f in files:
            dest = open(dirc + '/' + f.name, 'wb')
            for chunk in f.chunks():
                dest.write(chunk)
            dest.close()
        for f in files:
            insert_patentdocu(request, dirc + '/' + f.name)


        return redirect('message:patentdocuupload')

