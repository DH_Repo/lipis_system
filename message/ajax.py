import glob
import os
import shutil
import sys
import zipfile
from pathlib import Path
from rest_framework import serializers
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from application_patent_domestic.models import Application_Patent_Domestic_Master, Application_Patent_Domestic_Applicant
from message.syncpatentdocu import parsing_patent_submit_docu
from utils.db_fieldset import get_feild_names
from utils.enums import UPLOAD_FILE_PATH

class ApplicantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application_Patent_Domestic_Applicant
        fields = '__all__'

class PatentMasterSerializer(serializers.ModelSerializer):
    applicants = ApplicantSerializer(many=True)
    class Meta:
        model = Application_Patent_Domestic_Master
        fields = ('state', 'applicants','ourCompanyNumber','applicationNumber','inventNameKor',
                  'applicationDate')




@login_required(login_url='People:login')
@csrf_exempt
def PatentSubmitDocuFileUploading(request):
    if request.method == 'POST':
        dirc = UPLOAD_FILE_PATH.patentsubmitdocu.value
        file = request.FILES.get('file')

        # 기존 파일이 있다면 삭제
        if os.path.exists(dirc):
            file_list = glob.glob(str(Path(UPLOAD_FILE_PATH.patentsubmitdocu.value) / '*'))
            shutil.rmtree(UPLOAD_FILE_PATH.patentsubmitdocu.value)
            os.makedirs(dirc)
        else:
            os.makedirs(dirc)

        dest = open(dirc + '/' + file.name, 'wb+')
        for chunk in file.chunks():
            dest.write(chunk)
        dest.close()

        if sys.platform == "linux":
            unzippath = str(Path(UPLOAD_FILE_PATH.patentsubmitdocu.value + '/' + file.name.split('.')[0]))
        else:
            unzippath = os.path.abspath(UPLOAD_FILE_PATH.patentsubmitdocu.value) + '\\' + file.name.replace('.gzip', '')
            if not os.path.exists(unzippath):
                os.makedirs(unzippath)
        with zipfile.ZipFile(file, 'r') as zip_ref:
            zip_ref.extractall(unzippath)

        soup = parsing_patent_submit_docu(unzippath)
        # 출원번호
        KR_ApplicationNumber = soup.find('KR_ApplicationNumber'.lower())
        # 등록번호
        KR_PatentRegisterNumber = soup.find('KR_ApplicationNumber'.lower())
        # 당소번호
        KR_ReferenceNumber = soup.find('KR_ReferenceNumber'.lower())
        p1obj = ''
        p2obj = ''
        p3obj = ''
        if KR_ApplicationNumber:
            p1obj = Application_Patent_Domestic_Master.objects.filter(
                applicationNumber=KR_ApplicationNumber.string)
        if KR_PatentRegisterNumber and not p1obj:
            p2obj = Application_Patent_Domestic_Master.objects.filter(
                registrationNumber=KR_PatentRegisterNumber.string)
        if KR_ReferenceNumber and not p1obj and not p2obj:
            KR_ReferenceNumber = KR_ReferenceNumber.string[:7] + '-' + KR_ReferenceNumber.string[7:]
            p3obj = Application_Patent_Domestic_Master.objects.filter(
                ourCompanyNumber=KR_ReferenceNumber)
        patent_list = ''
        if p3obj:
            patent_list = p3obj
        elif p2obj:
            patent_list = p2obj
        elif p1obj:
            patent_list = p1obj
        language = request.session['django_language']
        masterfield_multiplelang = get_feild_names(language, 'Application_Patent_Domestic_Master',

                                                   Application_Patent_Domestic_Master)
        if patent_list:
            serializer = PatentMasterSerializer(patent_list, many=True)
            data = {'state': 'success', 'patent_list': serializer.data, 'masterfield_multiplelang': masterfield_multiplelang,
                    'filename': file.name}
        else:
            data = {'state': 'fail', 'msg': 'no patent' }
        return JsonResponse(data)

@login_required(login_url='People:login')
@csrf_exempt
def PatentSubmitDocuGetPatent(request):
    if request.method == 'GET':

        ourcomno = request.GET.get('ourcomno')


        patent_list = Application_Patent_Domestic_Master.objects.filter(
                ourCompanyNumber=ourcomno)
        language = request.session['django_language']
        masterfield_multiplelang = get_feild_names(language, 'Application_Patent_Domestic_Master',

                                                   Application_Patent_Domestic_Master)
        serializer = PatentMasterSerializer(patent_list, many=True)
        data = {'state': 'success', 'patent_list': serializer.data, 'masterfield_multiplelang': masterfield_multiplelang,
                }
        return JsonResponse(data)