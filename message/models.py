import datetime
import os
from uuid import uuid4

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone

from management.models import BaseModel
from utils.enums import FILETYPE, MESSAGE_TYPE, MESSAGE_STATE, PATENT_DOCU_TYPE, PATENT_OFFICE_DOCUMENT_FILETYPE, \
    Email_Child_STATE, PATENT_DOCU_PROCESS_STATUS, MESSAGE_SEND_RECEIVE_TYPE, PARENTMESSAGE_CHILDMESSAGE_RELATION_TYPE


class Message(BaseModel):

    send_receiveType = models.CharField(max_length=10, null=True, blank=True, choices=[(tag.name, tag.value) for tag in MESSAGE_SEND_RECEIVE_TYPE])
    type = models.CharField(max_length=50, null=False, blank=False, choices=[(tag.name, tag.value) for tag in MESSAGE_TYPE])
    user = models.ManyToManyField(
        'auth.User',
        through='Message_User',
        related_name='message_user', blank=True
    )
    messageCode = models.CharField(max_length=30, null=True, blank=True)
    state = models.CharField(max_length=40, null=False, blank=False, choices=[(tag.name, tag.value) for tag in MESSAGE_STATE], default='unspecified')
    createdDate = models.DateTimeField(auto_now_add=True)
    modifiedDate = models.DateTimeField(null=True, blank=True)
    receptionDate = models.DateTimeField(null=True, blank=True)
    master_message = models.ManyToManyField('application_patent_domestic.Application_Patent_Domestic_Master'
                                            ,through='Application_Patent_Domestic_Master_Message'
                                            ,related_name='master_message', blank=True)
    case = models.ManyToManyField('case.Case'
                                          , through='case.Case_Task_Message'
                                          , related_name='message_case', blank=True)
    task = models.ManyToManyField('case.Task'
                                          , through='case.Case_Task_Message'
                                          , related_name='message_task', blank=True)

    email = models.ForeignKey('emailsystem.Email_Detail', on_delete=models.SET_NULL, null=True, blank=True,
                              related_name='msg_email')
    patent_docu = models.ForeignKey('Patent_Document', on_delete=models.SET_NULL, null=True, blank=True,
                              related_name='msg_patentdocu')
    basic_message = models.ForeignKey('Basic_Message', on_delete=models.SET_NULL, null=True, blank=True,
                              related_name='msg_basicmsg')
    isClassified = models.BooleanField(null=False, blank=False, default=False)
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True,
                              related_name='message_parent')
    childType = models.CharField(max_length=20, null=True, blank=True,
                            choices=[(tag.name, tag.value) for tag in Email_Child_STATE])
    def __str__(self):
        return self.state
    class Meta:
        db_table = 'message'

class ParentMessage_ChildMessage(models.Model):
    parent_message = models.ForeignKey('Message', null=True, blank=True, on_delete=models.CASCADE, related_name='parentmessage_childmessage_parentmessage')
    child_message = models.ForeignKey('Message', null=True, blank=True, on_delete=models.CASCADE,
                                related_name='parentmessage_childmessage_childmessage')
    type = models.CharField(max_length=20, blank=True, null=True, choices=[(tag.name, tag.value) for tag in PARENTMESSAGE_CHILDMESSAGE_RELATION_TYPE])
    class Meta:
        db_table = 'message_parentmessage_childmessage'
class Message_User(models.Model):
    user = models.ForeignKey('auth.User', null=True, blank=True, on_delete=models.CASCADE, related_name='message_users')
    message = models.ForeignKey('Message', null=True, blank=True, on_delete=models.CASCADE, related_name='message_users')
    class Meta:
        db_table = 'message_message_user'

class Patent_Document(BaseModel):
    patentdocuType = models.CharField(max_length=50, null=False, blank=False,
                                      choices=[(tag.name, tag.value) for tag in PATENT_DOCU_TYPE],
                                      default='')
    processingState = models.CharField(max_length=100, null=True, blank=True,
                                       choices=[(tag.name, tag.value) for tag in PATENT_DOCU_PROCESS_STATUS])

    receiptSendDate = models.DateField(null=False, blank=False, default=datetime.date.today)
    receiptSendCode = models.CharField(max_length=30, null=False, blank=False, default='')
    docuCode = models.CharField(max_length=30, null=False, blank=False, default='')
    receiverCode = models.CharField(max_length=30, null=False, blank=False, default='')
    receiverName = models.CharField(max_length=20, null=False, blank=False, default='')
    receivedDateTime = models.DateField(null=False, blank=False, default=datetime.date.today)
    createdDate = models.DateTimeField(auto_now_add=True)
    isSubmitDoc = models.BooleanField(null=True, blank=True)
    updateDateApp = models.DateField(null=True, blank=True)
    refNumKIPO = models.CharField(max_length=30, null=True, blank=True)
    receiptState = models.CharField(max_length=30, null=True, blank=True)
    docuCase = models.CharField(max_length=30, null=True, blank=True)
    docuCaseSub = models.CharField(max_length=30, null=True, blank=True)
    officialFee = models.CharField(max_length=50, null=True, blank=True)
    sendSubmitDocuName = models.CharField(max_length=30, null=True, blank=True)
    class Meta:
        db_table = 'patent_document'
        unique_together = (('docuCode', 'receiptSendCode'),)

class Patent_Document_Attachment(BaseModel):
    fileType = models.CharField(max_length=50, null=False, blank=False, choices=[(tag.name, tag.value) for tag in PATENT_OFFICE_DOCUMENT_FILETYPE] ,default=PATENT_OFFICE_DOCUMENT_FILETYPE.OuterHDR)
    sendDocuFileName = models.CharField(max_length=300, null=False, blank=False, default='')
    sendDocuName = models.CharField(max_length=300, null=False, blank=False, default='')
    sendDocuSerialNumber = models.IntegerField(null=False, blank=False, default=0)
    path = models.FileField(max_length=300, default='')
    createdDate = models.DateTimeField(auto_now_add=True)
    patentoffice_docu = models.ForeignKey('Patent_Document', null=False, blank=False, related_name='patentofficedocu_attachment',
                                          on_delete=models.CASCADE)
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)
    deadLineDate = models.DateTimeField(null=True, blank=True)
    def __str__(self):
        return self.sendDocuName
    class Meta:
        db_table = 'patent_document_attachment'
        unique_together = (('sendDocuFileName', 'path'),)

class Basic_Message(models.Model):
    title = models.CharField(max_length=50, null=False, blank=False)
    contents = models.CharField(max_length=50, null=False, blank=False)
    createdDate = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = 'basic_message'

class File(BaseModel):


    def date_upload_to(instance, filename):
        # upload_to="%Y/%m/%d" 처럼 날짜로 세분화
        ymd_path = timezone.now().strftime('%Y/%m/%d')
        # 길이 32 인 uuid 값
        uuid_name = uuid4().hex
        # 확장자 추출
        extension = os.path.splitext(filename)[-1].lower()
        # 결합 후 return
        return '/'.join([
            'images', ymd_path,
            uuid_name + extension,
        ])
    place = models.CharField(max_length=30, null=False, blank=False, choices=[(tag.name, tag.value) for tag in FILETYPE], default='sync_email')
    path = models.FileField(max_length=150, default='')
    patent_docu = models.ForeignKey('Patent_Document', on_delete=models.SET_NULL, null=True, blank=True, related_name='patentdocu_file')
    email = models.ForeignKey('emailsystem.Email_Detail', on_delete=models.SET_NULL, null=True, blank=True, related_name='email_file')
    basic_message = models.ForeignKey('Basic_Message', on_delete=models.SET_NULL, null=True, blank=True, related_name='email_basic_message')
    createdDate = models.DateTimeField(auto_now_add=True)
    originFileName = models.CharField(max_length=200, null=False, blank=False)
    task = models.ForeignKey('case.Task', on_delete=models.SET_NULL, null=True,blank=True, related_name='file_task')
    revenue_invoice = models.ForeignKey('case.Revenue_Invoice', on_delete=models.SET_NULL, null=True, blank=True, related_name='file_revenue_invoice')
    purchase_invoice = models.ForeignKey('case.Purchase_Invoice', on_delete=models.SET_NULL, null=True, blank=True, related_name='file_purchase_invoice')
    isActive = models.BooleanField(null=False, blank=False, default=True)
    cId = models.CharField(max_length=100, null=True, blank=True)
    case_evidence = models.ForeignKey('case.Case', on_delete=models.SET_NULL, null=True, blank=True, related_name='file_case_evidence')
    def __str__(self):
        return self.originFileName
    class Meta:
        db_table = 'file'


class Application_Patent_Domestic_Master_Message(models.Model):
    master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master'
                                                           , on_delete=models.CASCADE, related_name='patent_domestic_master_message')
    message = models.ForeignKey('Message', on_delete=models.CASCADE, related_name='patent_domestic_master_message')
    file = models.ManyToManyField('File', through='Application_Patent_Domestic_Master_Message_File',
                                  related_name='master_message_file', blank=True)
    class Meta:
        db_table = "application_patent_domestic_master_message"

class Application_Patent_Domestic_Master_Message_File(models.Model):
    master_message = models.ForeignKey(
        'Application_Patent_Domestic_Master_Message'
        , on_delete=models.CASCADE, related_name='master_message_file_master_message')
    file = models.ForeignKey('File', on_delete=models.CASCADE, related_name='master_message_file_master_file')
    docuCode = models.CharField(max_length=30, blank=True, null=True)
    class Meta:
        db_table = "application_patent_domestic_master_message_file"
