import glob
import os
import shutil
import sys
import zipfile
from pathlib import Path

from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Q, Prefetch
from django.db.models.functions import Length
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, ListView

from Lipis_system_master.views import HomeMixin
from People.models import Work_Email
from application_patent_domestic.models import Application_Patent_Domestic_Master
from common.tableview_function import get_all_model_context_for_table, get_model_query_context_for_table_search_page, \
    get_model_query_context_for_table_nopage_nosearch
from emailsystem.models import Email_Detail, EmailTo_WorkEmail
from message.models import Message, File, Patent_Document_Attachment, Patent_Document
from message.syncpatentdocu import parsing_patent_submit_docu, insert_submit_patent_docu
from utils.db_fieldset import get_feild_names
from utils.dir_mgr import make_attachment_path

from utils.enums import UPLOAD_FILE_PATH, MESSAGE_TYPE, FILETYPE, EMAILTYPE


class MessageManagementView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = "message/messagemanagement.html"
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(MessageManagementView, self).get_context_data(**kwargs)
        ###출원정보
        #context['messages'] = Message.objects.all()
        #user = auth.authenticate(self.request, username='super', password='super')
        #context['mywork'] = Work.objects.filter(user=user)
        return get_all_model_context_for_table(self, Message, 'Message', 'Message', context)


class SearchEmailMessageView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'message/emailmessagebox.html'
    login_url = 'People:login'

    def get_context_data(self, **kwargs):
        context = super(SearchEmailMessageView, self).get_context_data(**kwargs)

        search = self.request.GET.get('Search')
        search_column = self.request.GET.get('SearchColumn')
        myfilter = 'email__' + search_column + '__contains'
        makecontext = get_model_query_context_for_table_nopage_nosearch(self, Message, 'Message',
                                                                        context,
                                                                        Message.objects.prefetch_related('email').filter(type=MESSAGE_TYPE.EM.name,
                                                                               **{myfilter:search}).order_by('-email__sentDate'))

        return makecontext

class SelectFromEmailMessageView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'message/emailmessages.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(SelectFromEmailMessageView, self).get_context_data(**kwargs)

        email = self.request.GET.get('fromemail')

        if email != 'all':

            msgs = Message.objects.filter(type=MESSAGE_TYPE.EM.name,
                                   isClassified=False,email__to_person__email=email).all()

            makecontext = get_model_query_context_for_table_nopage_nosearch(self, Message, 'Message',
                                                                            context, msgs)
            allobj = makecontext['model_list'][1].all()

            resultquery = (Q(email='libo@liboip.com') | Q(email='chang@liboip.com'))

            for obj in allobj:
                obj.emailfiles = File.objects.filter(email=obj.email)
            #self.request.session['selectedemail'] = emailid
            makecontext['model_list'][1] = allobj
            makecontext['selectedfrom'] = email
            makecontext['fromemail'] = Work_Email.objects.filter(resultquery).all().distinct('email')
        return makecontext

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        email = self.request.GET.get('fromemail')
        if email == 'all':
            return redirect('message:emailmessage')
        else:
            return self.render_to_response(context)



class EmailMessageView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'message/emailmessagebox.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(EmailMessageView, self).get_context_data(**kwargs)
        toemail = self.request.POST.get('toemail')
        fromemail = self.request.POST.get('fromemail')
        classfication = self.request.POST.get('classificationemail')
        taxinvoice = self.request.POST.get('taxinvoice')
        emailtype = self.request.POST.get('emailtype')


        if emailtype is None and 'emailtype' in self.request.session:
            emailtype = self.request.session['emailtype']
        elif emailtype is None and 'emailtype' not in self.request.session:
            emailtype = 'emailtype_all'

        if taxinvoice is None and 'taxinvoice' in self.request.session:
            taxinvoice = self.request.session['taxinvoice']
        elif taxinvoice is None and 'taxinvoice' not in self.request.session:
            taxinvoice = 'taxinvoice_all'

        if classfication is None and 'classfication' in self.request.session:
            classfication = self.request.session['classfication']
        elif classfication is None and 'classfication' not in self.request.session:
            classfication = 'classification_all'


        if toemail and toemail != 'to_all':
            self.request.session['toemail'] = toemail
        elif toemail == 'to_all':
            self.request.session['toemail'] = None

        if fromemail and fromemail != 'from_all':
            self.request.session['fromemail'] = fromemail
        elif fromemail == 'from_all':
            self.request.session['fromemail'] = None

        if not 'toemail' in self.request.session:
            self.request.session['toemail'] = None
        if not 'fromemail' in self.request.session:
            self.request.session['fromemail'] = None

        self.request.session['classfication'] = classfication
        self.request.session['taxinvoice'] = taxinvoice
        self.request.session['emailtype'] = emailtype

        classficationfilter = {}
        relationfilter = {}
        if classfication == 'classification_not':
            classficationfilter = {'isClassified':False}
        elif classfication == 'classification':
            classficationfilter = {'isClassified':True}
            relationfilter = {'master_message__isnull':False}
        elif classfication == 'classification_norelation':
            classficationfilter = {'isClassified': True}
            relationfilter = {'master_message__isnull': True}



        if self.request.session['toemail'] is not None:
            toemail = self.request.session['toemail']

        else:
            toemail = None

        if self.request.session['fromemail'] is not None:
            fromemail = self.request.session['fromemail']

        else:
            fromemail = None


        if toemail and fromemail is None:
            msgobj = Message.objects.filter(type=MESSAGE_TYPE.EM.name,
                                   **classficationfilter,**relationfilter,
                                   email__to_person__email=toemail).order_by('-email__sentDate')
        elif toemail is None and fromemail:
            msgobj = Message.objects.filter(type=MESSAGE_TYPE.EM.name,
                                            **classficationfilter,**relationfilter,
                                            email__fromEmail__email=fromemail).order_by('-email__sentDate')
        elif toemail and fromemail:
            msgobj = Message.objects.filter(type=MESSAGE_TYPE.EM.name,
                                            **classficationfilter,**relationfilter, email__to_person__email=toemail,
                                            email__fromEmail__email=fromemail).order_by('-email__sentDate')
        elif toemail is None and fromemail is None:
            msgobj = Message.objects.filter(type=MESSAGE_TYPE.EM.name,
                                   **classficationfilter,**relationfilter).order_by('-email__sentDate')

        taxinvoicefilter = {}
        taxinvoicefilefilter = {}
        if taxinvoice == 'taxinvoice_hasinvoice':
            taxinvoicefilter = {'email__fromEmail__email__contains':'hometax.go.kr'}
        elif taxinvoice == 'taxinvoice_hasinvoicexml':
            taxinvoicefilter = {'email__fromEmail__email__contains':'hometax.go.kr'}
            taxinvoicefilefilter = {'email__email_file__originFileName__contains':'.xml'}

        emailtypefilter = {}

        if emailtype == 'emailtype_realtime':
            emailtypefilter = {'email__emailType': EMAILTYPE.FromServer.name}
        elif emailtype == 'emailtype_file':
            emailtypefilter = {'email__emailType': EMAILTYPE.FromFile.name}


        msgobj1 = msgobj.filter(**taxinvoicefilter,**taxinvoicefilefilter,**emailtypefilter).order_by('-email__sentDate')
        makecontext = get_model_query_context_for_table_search_page(self, Message, 'Message',
                                                            'Message', context, msgobj1)

        objsize = len(makecontext['model_list'][1].object_list)


        makecontext['classifiedcount'] = str(Message.objects.filter(type=MESSAGE_TYPE.EM.name, isClassified=False).count()) + '/' + str(Message.objects.filter(type=MESSAGE_TYPE.EM.name).count())

        objlist = makecontext['model_list'][1].object_list

        for i in range(0, objsize):
            obj =objlist[i]

            obj.emailfiles = File.objects.filter(email=obj.email)

        # 보낸 사람 전체
        #fromE = Message.objects.all().distinct('email__fromEmail__email').values('email__fromEmail__email')


        makecontext['selectedto'] = self.request.session['toemail']
        if self.request.session['fromemail'] is None:
            makecontext['selectedfrom'] = 'from_all'
        else:
            makecontext['selectedfrom'] = self.request.session['fromemail']
        resultquery = (Q(email='libo@liboip.com') | Q(email='chang@liboip.com'))
        makecontext['toemail'] = Work_Email.objects.filter(resultquery).all().distinct('email')
        makecontext['fromemail'] = Work_Email.objects.filter(resultquery).all().distinct('email')
        makecontext['selectedclassification'] = self.request.session['classfication']
        makecontext['selectedtaxinvoice'] = self.request.session['taxinvoice']
        makecontext['selectedemailtype'] = self.request.session['emailtype']

        return makecontext

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context)

class PatentDocuMessageView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'message/patentdocumessages.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(PatentDocuMessageView, self).get_context_data(**kwargs)
        context = get_model_query_context_for_table_search_page(self, Message, 'Message',
                                                        'Message', context,
                                                                Message.objects.filter(type=MESSAGE_TYPE.KP.name, patent_docu__isSubmitDoc=False).order_by('-patent_docu__receiptSendDate'))

        language = self.request.session['django_language']
        context['Patent_Document_multilang'] = get_feild_names(language, 'Patent_Document', Patent_Document)
        context['Patent_Document_Attachment_multilang'] = get_feild_names(language, 'Patent_Document_Attachment', Patent_Document_Attachment)
        context['masterfield_multiplelang'] = get_feild_names(language, 'Application_Patent_Domestic_Master', Application_Patent_Domestic_Master)




        return context

class PatentSubmitDocuMessageView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'message/patentsubmitdocumessages.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(PatentSubmitDocuMessageView, self).get_context_data(**kwargs)
        context = get_model_query_context_for_table_search_page(self, Message, 'Message',
                                                        'Message', context,
                                                                Message.objects.filter(type=MESSAGE_TYPE.KP.name, patent_docu__isSubmitDoc=True).order_by('-patent_docu__receiptSendDate'))

        language = self.request.session['django_language']
        context['Patent_Document_multilang'] = get_feild_names(language, 'Patent_Document', Patent_Document)
        context['Patent_Document_Attachment_multilang'] = get_feild_names(language, 'Patent_Document_Attachment', Patent_Document_Attachment)
        context['masterfield_multiplelang'] = get_feild_names(language, 'Application_Patent_Domestic_Master', Application_Patent_Domestic_Master)




        return context

class PatentDocuUploadView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'message/patentdocu_upload.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(PatentDocuUploadView, self).get_context_data(**kwargs)
        file_list = glob.glob(str(Path(UPLOAD_FILE_PATH.patentdocu.value) / '*'))
        context['file_list'] = file_list
        return context

class PatentSubmitDocuUploadView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'message/patent_submit_docu_upload.html'
    login_url = 'People:login'
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        dirc = UPLOAD_FILE_PATH.patentsubmitdocu.value

        if sys.platform == "linux":
            filelist = glob.glob(str(Path(dirc)) + '/' + '*.gzip')
        else:
            filelist = glob.glob(dirc + '\\' + '*.gzip')
        if filelist:
            context['file_name'] = filelist[0]
            if sys.platform == "linux":
                unzippath = str(Path(filelist[0].split('.')[0]))
            else:
                unzippath = os.path.abspath(filelist[0].replace('.gzip', ''))
                if not os.path.exists(unzippath):
                    os.makedirs(unzippath)
            with zipfile.ZipFile(filelist[0], 'r') as zip_ref:
                zip_ref.extractall(unzippath)
            soup = parsing_patent_submit_docu(unzippath)
            # 출원번호
            KR_ApplicationNumber = soup.find('KR_ApplicationNumber'.lower())
            # 등록번호
            KR_PatentRegisterNumber = soup.find('KR_ApplicationNumber'.lower())
            # 당소번호
            KR_ReferenceNumber = soup.find('KR_ReferenceNumber'.lower())
            p1obj = ''
            p2obj = ''
            p3obj = ''
            if KR_ApplicationNumber:
                p1obj = Application_Patent_Domestic_Master.objects.filter(
                    applicationNumber=KR_ApplicationNumber.string)
            if KR_PatentRegisterNumber and not p1obj:
                p2obj = Application_Patent_Domestic_Master.objects.filter(
                    registrationNumber=KR_PatentRegisterNumber.string)
            if KR_ReferenceNumber and not p1obj and not p2obj:
                KR_ReferenceNumber = KR_ReferenceNumber.string[:7] + '-' + KR_ReferenceNumber.string[7:]
                p3obj = Application_Patent_Domestic_Master.objects.filter(
                    ourCompanyNumber=KR_ReferenceNumber)
            if p3obj:
                context['patent_list'] = p3obj
            elif p2obj:
                context['patent_list'] = p2obj
            elif p1obj:
                context['patent_list'] = p1obj
            language = request.session['django_language']
            context['masterfield_multiplelang'] = get_feild_names(language, 'Application_Patent_Domestic_Master',

                                                                  Application_Patent_Domestic_Master)
        return self.render_to_response(context)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        ourcomno = request.POST.get('ourcomno')
        dirc = UPLOAD_FILE_PATH.patentsubmitdocu.value
        if sys.platform == "linux":
            filelist = glob.glob(str(Path(dirc)) +'/'+ '*.gzip')
        else:
            filelist = glob.glob(dirc + '\\' + '*.gzip')
        if filelist:
            filename = filelist[0]
        insert_submit_patent_docu(request, filename, ourcomno)
        return self.render_to_response(context)
    def get_context_data(self, **kwargs):
        context = super(PatentSubmitDocuUploadView, self).get_context_data(**kwargs)
        return context




@login_required(login_url='People:login')
def GetMasterInEmailMessage(request, pk):
    #master = get_object_or_404(Application_Patent_Domestic_Master, id=pk)
    if request.method == 'POST':

        return render(request, 'message/searchmaster_form.html')
    else:
        applno = []
        ourcomno = []
        for m in Application_Patent_Domestic_Master.objects.filter(applicationNumber__isnull=False, ourCompanyNumber__isnull=False).all():
            if len(m.applicationNumber) > 3 and len(m.ourCompanyNumber) > 3:
                applno.append(m.applicationNumber.strip())
                ourcomno.append(m.ourCompanyNumber.strip())
        applno.sort()
        ourcomno.sort()
    return render(request, 'message/searchmaster_form.html', {'applno': applno,'ourcomno':ourcomno , 'pid':pk})

@login_required(login_url='People:login')
def SearchInMaster(request, pk):

    term = request.GET.get('term')
    if pk == '0':
        parameter = 'applicationNumber'
        master = Application_Patent_Domestic_Master.objects.filter(applicationNumber__contains=term).values(parameter)
    elif pk == '1':
        parameter = 'ourCompanyNumber'
        master = Application_Patent_Domestic_Master.objects.filter(ourCompanyNumber__contains=term).values(parameter)

    serialmaster = list(master)
    newmaster =[]
    for serial in serialmaster:
        newmaster.append(serial[parameter])
    return JsonResponse(newmaster,  safe=False)

@login_required(login_url='People:login')
@csrf_exempt
def LinkPatentMasterMessage(request):
    if request.method == 'POST':
        searchcontent = request.POST.get('searchContent')
        pk = request.POST.get('pid')
        msg = Message.objects.get(id=pk)
        searchType = request.POST.get('searchType')

        if searchType == 'applicationNumber':
            masters = Application_Patent_Domestic_Master.objects.filter(applicationNumber=searchcontent)
        elif searchType == 'ourCompanyNumber':
            masters = Application_Patent_Domestic_Master.objects.filter(ourCompanyNumber=searchcontent)
        for master in masters:
            if master in msg.master_message.all():
                pass
            else:
                msg.master_message.add(master)
    return HttpResponse('')

@login_required(login_url='People:login')
@csrf_exempt
def DelLinkPatentMasterMessage(request):
    if request.method == 'POST':
        msgid = request.POST.get('msgid')
        masterid = request.POST.get('masterid')
        msg = Message.objects.get(id=msgid)
        master = Application_Patent_Domestic_Master.objects.get(id=masterid)
        msg.master_message.remove(master)
    return HttpResponse('')

@login_required(login_url='People:login')
@csrf_exempt
def SetClassfied(request):
    if request.method == 'POST':
        msgid = request.POST.get('msgid')
        print('classified msg id : {}'.format(msgid))
        msg = Message.objects.get(id=msgid)
        msg.isClassified = not msg.isClassified
        msg.save()
    return redirect('message:emailmessage')

@login_required(login_url='People:login')
@csrf_exempt
def EmailIframeSetClassfied(request):
    if request.method == 'POST':
        msgid = request.POST.get('msgid')
        msg = Message.objects.get(id=msgid)
        msg.isClassified = not msg.isClassified
        msg.save()
    return HttpResponse(
                    '<script type="text/javascript">window.opener.location.reload();</script>')

@login_required(login_url='People:login')
@csrf_exempt
def AddAttachment(request, pk):
    dirc = UPLOAD_FILE_PATH.email.value
    if request.method == 'POST':
        files = request.FILES.getlist('files[]')
        if not os.path.exists(dirc):
            os.makedirs(dirc)
        for f in files:
            dest = open(dirc+'/' + f.name, 'wb')
            for chunk in f.chunks():
                dest.write(chunk)
            dest.close()
            savepath = os.getcwd() + '/' + dirc+'/' + f.name
            msg = Message.objects.get(id=pk)
            attach = File()
            attach.originFileName = f.name
            attach.path = savepath
            attach.place = FILETYPE.manual_attach_email.name
            attach.email = msg.email
            attach.save()


        return HttpResponse('')

class AddEmailAttachmentUploadView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'message/emailattachdropdown.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(AddEmailAttachmentUploadView, self).get_context_data(**kwargs)

        context['pk'] = self.kwargs.get('pk')
        return context

@login_required(login_url='People:login')
@csrf_exempt
def ClassfyingAll(request):
    if request.method == 'GET':
        msgs = Message.objects.filter(master_message__isnull=False, isClassified=False).all()
        for msg in msgs:
            msg.isClassified = True
            msg.save()
    return redirect('message:emailmessage')
    #return HttpResponse('<script type="text/javascript">window.location.reload();</script>')