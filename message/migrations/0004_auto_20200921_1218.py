# Generated by Django 3.1.1 on 2020-09-21 03:18

from django.db import migrations, models
import utils.enums


class Migration(migrations.Migration):

    dependencies = [
        ('message', '0003_patent_document_sendsubmitdocuname'),
    ]

    operations = [
        migrations.AlterField(
            model_name='patent_document_attachment',
            name='fileType',
            field=models.CharField(choices=[('OuterHDR', 'outerhdr'), ('InnerHDR', 'innerhdr'), ('OriginPDF', 'originpdf'), ('AttachPDF', 'attachpdf'), ('SubmitZIP', 'submitzip')], default=utils.enums.PATENT_OFFICE_DOCUMENT_FILETYPE['OuterHDR'], max_length=50),
        ),
    ]
