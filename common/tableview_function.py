from django.core.paginator import Paginator

from common.view_functions import custompagerange
from emailsystem.models import Email_Detail
from utils.search_function import searchpara_save, Search_Parameter, applicants_column_filter, \
    column_filter, column_filter_usingModelobj
from utils.db_fieldset import get_feild_names

def get_model_query_context_for_table_nopage_nosearch(self, model, modelname, context, modelqueryset, contextname = None):

    language = self.request.session['django_language']
    col = get_feild_names(language, modelname, model)
    model_list = modelqueryset
    if contextname:
        context[contextname] = [col, model_list]
    else:
        context['model_list'] = [col, model_list]

    return context

def get_model_query_context_for_table_page_nosearch(self, model, modelname, context, modelqueryset, contextname = None):
    page = self.request.GET.get('page', None)
    language = self.request.session['django_language']
    col = get_feild_names(language, modelname, model)
    model_list = modelqueryset
    paginator = Paginator(model_list, 10)
    page = custompagerange(paginator, page)
    if contextname:
        context[contextname] = [col, paginator.get_page(page)]
    else:
        context['model_list'] = [col, paginator.get_page(page)]


    return context

def get_model_query_context_for_table_search_page(self, model, modelname, searchpara, context, modelqueryset):
    search = self.request.GET.get('Search')
    page = self.request.GET.get('page')
    search_column = self.request.GET.get('SearchColumn')
    language = self.request.session['django_language']
    col = get_feild_names(language, modelname, model)

    if search:
        searchpara_save(searchpara, search_column, search)
        model_list = column_filter_usingModelobj(modelqueryset, search_column, search)
        paginator = Paginator(model_list, 10)
        context['model_list'] = [col, paginator.get_page(page)]
        context['searchcolumn'] = search_column
        context['searchtxt'] = search
    else:
        searchpara = Search_Parameter.objects.get(tableType=searchpara)

        if searchpara.searchname and page:
            model_list = column_filter_usingModelobj(modelqueryset, search_column, search)
            context['searchcolumn'] = searchpara.colname
            context['searchtxt'] = searchpara.searchname
        else:
            model_list = modelqueryset
            context['searchcolumn'] = 'empty'
            context['searchtxt'] = ''
            searchpara_save(searchpara, '', '')

        paginator = Paginator(model_list, 10)
        page = custompagerange(paginator, page)
        context['model_list'] = [col, paginator.get_page(page)]

    return context


def get_all_model_context_for_table(self, model, modelname, searchpara, context, filter=None):
    search = self.request.GET.get('Search')
    search_column = self.request.GET.get('SearchColumn')
    page = self.request.GET.get('page')
    language = self.request.session['django_language']
    col = get_feild_names(language, modelname, model)

    if search:
        searchpara_save(searchpara, search_column, search)
        model_list = column_filter(searchpara, search_column, search)
        paginator = Paginator(model_list, 10)
        context['model_list'] = [col, model_list]
        context['searchcolumn'] = search_column
        context['searchtxt'] = search
    else:
        searchpara = Search_Parameter.objects.get(tableType=searchpara)

        if searchpara.searchname and page:
            model_list = column_filter(searchpara.colname, searchpara.searchname, searchpara)
            context['searchcolumn'] = searchpara.colname
            context['searchtxt'] = searchpara.searchname
        else:
            if model is Email_Detail:
                if filter is not None:
                    model_list = model.objects.filter(**filter).all().order_by('-sentDate')
                else:
                    model_list = model.objects.all().order_by('-sentDate')
            else:
                if filter is not None:
                    model_list = model.objects.filter(**filter).all()
                else:
                    model_list = model.objects.all()
            context['searchcolumn'] = 'empty'
            context['searchtxt'] = ''
            searchpara_save(searchpara, '', '')

        paginator = Paginator(model_list, 10)
        page = custompagerange(paginator, page)

        context['model_list'] = [col, paginator.get_page(page)]

    return context

def get_field_multilang(self, model, modelname):
    language = self.request.session['django_language']
    return get_feild_names(language, modelname, model)



