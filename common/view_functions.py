def custompagerange(paginator,page):
    if page is None:
        page = '1'
    if paginator.num_pages <= 10:
        paginator.customrange = range(1, paginator.num_pages + 1)
    else:
        if int(page) < 6:
            paginator.customrange = range(1, paginator.per_page + 1)
        elif int(page) >= 6:
            if int(page) + 5 >= paginator.num_pages+1:
                paginator.customrange = range(paginator.num_pages-9, paginator.num_pages+1)
            else:
                paginator.customrange = range(int(page) - 5, int(page) + 5)
    return page