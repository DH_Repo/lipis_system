from django.db.models import Prefetch

from People.models import ClientType
from management.models import Code_Multiple_Language, Code_Information, Country_Multiple_Language, CountryInformation, \
    Tag_List
from utils.enums import ALL_CODE_TYPE
from django.db.models import Case, When

def MakeTagChoiceMultipleLanguage(self):
    if 'tag' in self.fields:
        if self.customcodechoice:
            if ALL_CODE_TYPE.Management_TagListType.name in self.customcodechoice:
                self.fields['tag'].choices = self.customcodechoice[ALL_CODE_TYPE.Management_TagListType.name]
            else:
                lang = self.request.session['django_language']
                self.fields['tag'].choices = GetTagCodeInfoMultipleLanguage(lang)
        else:
            lang = self.request.session['django_language']
            self.fields['tag'].choices = GetTagCodeInfoMultipleLanguage(lang)


def GetTagCodeInfoMultipleLanguage(lang):
    return GetCodeInfoMultipleLanguageOnlyForTag(lang, ALL_CODE_TYPE.Management_TagListType.name)
def GetTagCodeNameAndMultipleLanguage(lang):
    return GetCodeNamdAndMultipleLanguageOnlyForTag(lang, ALL_CODE_TYPE.Management_TagListType.name)
def GetCodeMultpleLanguageForPatentMaster(request):
    lang = request.session['django_language']
    plist = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.PatentType.name)
    nlist = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.PatentNationType.name)
    slist = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.ApplicationPatent_Status.name)
    countrychoice = GetCountryCodeInfoMultipleLanguage(lang)
    return {ALL_CODE_TYPE.PatentType.name:plist,
            ALL_CODE_TYPE.PatentNationType.name:nlist,
            ALL_CODE_TYPE.ApplicationPatent_Status.name:slist,
            'countryCode':countrychoice}

def GetCodeMultpleLanguageForCase(request):
    lang = request.session['django_language']
    clist = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.Case_CaseType.name)
    cstate = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.Case_CaseState.name)

    return {ALL_CODE_TYPE.Case_CaseType.name: clist, ALL_CODE_TYPE.Case_CaseState.name:cstate}

def GetCodeMultpleLanguageForTask(request):
    lang = request.session['django_language']
    tlist = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.Task_TaskType.name)
    tstate = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.Task_TaskState.name)
    return {ALL_CODE_TYPE.Task_TaskType.name: tlist,ALL_CODE_TYPE.Task_TaskState.name:tstate}

def GetCodeMultpleLanguageForCaseTaskMessage(request):
    lang = request.session['django_language']
    ctmlist = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.Message_CaseTaskType.name)
    return {ALL_CODE_TYPE.Message_CaseTaskType.name: ctmlist}




def GetCodeMultpleLanguageForMessage(request):
    lang = request.session['django_language']
    srlist = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.Message_ReceiveSendType.name)
    mlist = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.Message_MessageType.name)

    return {ALL_CODE_TYPE.Message_ReceiveSendType.name: srlist,
            ALL_CODE_TYPE.Message_MessageType.name:mlist}




def GetCodeMultpleLanguageForRevenueInvoice(request):
    lang = request.session['django_language']
    plist = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.PatentType.name)
    nlist = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.PatentNationType.name)
    rstate = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.Revenue_Invoice_State.name)

    for p in plist:
        if p[0] == 'I' or p[0] == 'C':
            del p
    return {ALL_CODE_TYPE.PatentType.name: plist,
            ALL_CODE_TYPE.PatentNationType.name: nlist,
            ALL_CODE_TYPE.Revenue_Invoice_State.name: rstate}

def GetCodeMultpleLanguageForPurchaseInvoice(request):
    lang = request.session['django_language']

    pstate = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.Purchase_Invoice_State.name)
    return {ALL_CODE_TYPE.Purchase_Invoice_State.name: pstate}


def GetCodeWithMultpleLanguage(request):
    lang = request.session['django_language']
    countrychoice = GetCountryCodeInfoMultipleLanguage(lang)

    return {'countryCode': countrychoice}

def GetCountryCodeInfoMultipleLanguage(language):
    choices = [('', '--------')]
    qs = Country_Multiple_Language.objects.filter(languageCode=language)
    prefetch1 = Prefetch(
        'country_multiple_language',
        queryset=qs,
        to_attr='cml'
    )
    countryval = ['KR', 'EN', 'JP', 'CN']
    order = Case(*[When(countryCode=ccode, then=pos) for pos, ccode in enumerate(countryval)])
    for ci in CountryInformation.objects.prefetch_related(prefetch1).all().order_by(order):
        if ci.cml:
            cf = ci.cml[0]
            choices.append((ci.countryCode, ci.countryCode + '('+cf.name + ')'))
    return choices

def GetCodeInfoMultipleLanguage(language, codeType):
    choicelist = [('', '--------')]
    qs = Code_Multiple_Language.objects.filter(languageCode=language,
                                               code_info__codeType=codeType)
    prefetch1 = Prefetch(
        'code_multiple_language',
        queryset=qs,
        to_attr='cml'
    )
    for ci in Code_Information.objects.prefetch_related(prefetch1).filter(codeType=codeType):
        cf = ci.cml[0]
        choicelist.append((ci.codeName, ci.codeName + '('+cf.name+')'))
    return choicelist

def GetCodeInfoMultipleLanguage_all(request):
    language = request.session['django_language']
    choicelist = [('', '--------')]
    qs = Code_Multiple_Language.objects.filter(languageCode=language)
    prefetch1 = Prefetch(
        'code_multiple_language',
        queryset=qs,
        to_attr='cml'
    )
    for ci in Code_Information.objects.prefetch_related(prefetch1).all():
        cf = ci.cml[0]
        choicelist.append((ci.codeName, ci.codeName + '('+cf.name+')'))
    return choicelist

def GetCodeMultpleLanguageForClient(request):
    lang = request.session['django_language']
    lists = GetCodeInfoMultipleLanguage(lang, ALL_CODE_TYPE.Client_Type.name)
    return {ALL_CODE_TYPE.Client_Type.name: lists}

def GetCodeInfoMultipleLanguageOnlyForTag(language, codeType):
    choicelist = [('', '--------')]
    qs = Code_Multiple_Language.objects.filter(languageCode=language,
                                               code_info__codeType=codeType)
    prefetch1 = Prefetch(
        'code_multiple_language',
        queryset=qs,
        to_attr='cml'
    )
    for ci in Code_Information.objects.prefetch_related(prefetch1).filter(codeType=codeType):
        cf = ci.cml[0]
        tag = Tag_List.objects.get(code=ci.codeName)
        choicelist.append((tag.id, ci.codeName + '(' + cf.name + ')'))
    return choicelist

def GetCodeNamdAndMultipleLanguageOnlyForTag(language, codeType):
    choicelist = [('', '--------')]
    qs = Code_Multiple_Language.objects.filter(languageCode=language,
                                               code_info__codeType=codeType)
    prefetch1 = Prefetch(
        'code_multiple_language',
        queryset=qs,
        to_attr='cml'
    )
    for ci in Code_Information.objects.prefetch_related(prefetch1).filter(codeType=codeType):
        cf = ci.cml[0]
        tag = Tag_List.objects.get(code=ci.codeName)
        choicelist.append((ci.codeName, ci.codeName + '(' + cf.name + ')'))
    return choicelist

def set_code_to_multilanguage(language, codetype, codename):
    codeinfo = Code_Multiple_Language.objects.get(languageCode=language,
                                               code_info__codeType=codetype,
                                               code_info__codeName=codename)
    return codeinfo.name+"(" + codename + ")"

def set_counrycode_to_multilanguage(language, countrycode):
    qs = Country_Multiple_Language.objects.filter(languageCode=language)
    prefetch1 = Prefetch(
        'country_multiple_language',
        queryset=qs,
        to_attr='cml'
    )
    for ci in CountryInformation.objects.prefetch_related(prefetch1).all():
        if ci.cml:
            cf = ci.cml[0]
            if ci.countryCode == countrycode:
                return cf.name + "(" + ci.countryCode +")"



