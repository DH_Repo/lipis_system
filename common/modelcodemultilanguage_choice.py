from django.db.models import Prefetch

from People.models import ClientType
from management.models import Code_Multiple_Language, Code_Information
from utils.enums import ALL_CODE_TYPE


def GetModelCodeMultpleLanguageForClient(request):
    lang = request.session['django_language']
    lists = GetModelCodeInfoMultipleLanguageForClient(lang, ALL_CODE_TYPE.Client_Type.name)
    return {ALL_CODE_TYPE.Client_Type.name: lists}


def GetModelCodeInfoMultipleLanguageForClient(language, codeType):
    choicelist = [('', '--------')]
    qs = Code_Multiple_Language.objects.filter(languageCode=language,
                                               code_info__codeType=codeType)
    prefetch1 = Prefetch(
        'code_multiple_language',
        queryset=qs,
        to_attr='cml'
    )
    for ci in Code_Information.objects.prefetch_related(prefetch1).filter(codeType=codeType):
        cf = ci.cml[0]
        for cleint in ClientType.objects.all():
            if cleint.typeName == ci.codeName:
                choicelist.append((cleint.id, ci.codeName + '('+cf.name+')'))
    return choicelist