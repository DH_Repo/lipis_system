import glob
import json
import os
from pathlib import Path

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.db.models import Q, Case, When, Prefetch
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from Lipis_system_master.views import HomeMixin
from People.models import Client
from case.models import Task_Template_Ref, Task_Template
from common.codemultilanguage_choice import GetTagCodeNameAndMultipleLanguage, \
    GetCodeMultpleLanguageForPatentMaster, \
    GetCodeMultpleLanguageForCase, GetCodeMultpleLanguageForTask, GetCodeMultpleLanguageForCaseTaskMessage, \
    GetTagCodeInfoMultipleLanguage, set_code_to_multilanguage, set_counrycode_to_multilanguage
from case.forms import TaskTemplateRefForm, CustomTaskTemplateForm
from management.models import DB_Modifying_Field_Name, Code_Information, Code_Multiple_Language, \
    Country_Multiple_Language, CountryInformation, Tag_List, Table_History
from utils.db_fieldset import getModifedFieldName, get_fielddic
from utils.enums import UPLOAD_FILE_PATH, LANGUAGE_CODE_INSERT, ALL_CODE_TYPE, CRUD_TYPE
from django_celery_results.models import TaskResult
#from celery.result import ResultBase

def make_field_data(lists):
    temp = []
    modifying_objs = DB_Modifying_Field_Name.objects.all()
    for langcode in LANGUAGE_CODE_INSERT:
        for model in lists:
            obj = modifying_objs.filter(dbfieldname__tableName=model, languageCode=langcode.name)\
                .values('dbfieldname__originFieldName', 'customFieldName', 'explanation').order_by('id')
            temp.append([langcode.name, model, obj])
    temp.sort(key=lambda test_list: test_list[1])
    return temp

class CommonMgrView(LoginRequiredMixin, HomeMixin, TemplateView):
    login_url = 'People:login'
    template_name = 'management/commoninfo_form.html'
    def get_context_data(self, **kwargs):
        context = super(CommonMgrView, self).get_context_data(**kwargs)
        tobj = TaskResult.objects.first()
        context['taskinfo'] = tobj
        #date_done, task_name, status
        return context


class CustomDBFieldNameView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'management/edit_fieldname_form.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):

        context = super(CustomDBFieldNameView, self).get_context_data(**kwargs)
        temp = []

        fieldtype = self.request.GET.get('fieldtype')
        lists = getModifedFieldName(fieldtype)
        context['fieldnamelist'] = make_field_data(lists)
        context['ttype'] = lists
        return context

class Edit_People_FieldView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'management/edit_fieldname_form.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):

        context = super(Edit_People_FieldView, self).get_context_data(**kwargs)
        temp = []


        lists = getModifedFieldName('people')


        context['fieldnamelist'] = make_field_data(lists)
        return context

class ExcelUploadView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'management/excelfileupload.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(ExcelUploadView, self).get_context_data(**kwargs)
        file_list = glob.glob(str(Path(UPLOAD_FILE_PATH.excelfile.value) / '*'))
        context['file_list'] = file_list
        return context

@login_required(login_url='People:login')
@csrf_exempt
def ExcelFileUpload(request):
    dirc = UPLOAD_FILE_PATH.excelfile.value
    if request.method == 'POST':
        files = request.FILES.getlist('files[]')
        if not os.path.exists(dirc):
            os.makedirs(dirc)
        for f in files:
            with open(dirc +'/'+ f.name, 'wb') as dest:
                for chunk in f.chunks():
                    dest.write(chunk)
        return redirect('management:excelupload')

@login_required(login_url='People:login')
def checktaskstatus(request):

    #driver,path = chrome_driver()
    #soup = BeautifulSoup(open("/root/lipis/lipis_system/media/super/20200529/20200529133509708790.html"), "html.parser")
    #driver.get("http://lipis.iptime.org/media/super/20200529/20200529135739030895.html")

    result = request.get_full_path()
    #driver.close()





    #result = t.AsyncResult(t.request.id).state
    #return HttpResponse(
    #    '<script type="text/javascript">alert('+result+');</script>')

    return render(request, 'management/commoninfo_form.html', {
        'result': result,
    })
@login_required(login_url='People:login')
def SaveDbCustomFieldName(request):
    if request.method == 'POST':
        tabletype = request.POST.get('tabletype').replace('\'','').replace('[','').replace(']','').replace(' ','').split(',')
        tablefilter = {'dbfieldname__tableName__in':tabletype}
        for lcode in LANGUAGE_CODE_INSERT:
            for fieldname in DB_Modifying_Field_Name.objects.filter(**tablefilter).values('dbfieldname__tableName', 'dbfieldname__originFieldName', 'explanation'):
                obj = request.POST.get(lcode.name+'_'+fieldname['dbfieldname__tableName']+'_'+fieldname['dbfieldname__originFieldName'])
                exp = request.POST.get(lcode.name + '_' + fieldname['dbfieldname__tableName'] + '_' + fieldname[
                    'dbfieldname__originFieldName']+'_explanation')
                DB_Modifying_Field_Name.objects.filter(languageCode=lcode.name, dbfieldname__tableName=fieldname['dbfieldname__tableName'], dbfieldname__originFieldName=fieldname['dbfieldname__originFieldName']
                                                             ).update(customFieldName=obj, explanation=exp)
        metaget = request.META.get('HTTP_REFERER')
        return HttpResponse(metaget)

class CodeInformationView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'management/codeinfo.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(CodeInformationView, self).get_context_data(**kwargs)


        qs = Code_Multiple_Language.objects.all().order_by('languageCode')
        prefetch1 = Prefetch(
            'code_multiple_language',
            queryset=qs,
            to_attr='cml'
        )
        query = Code_Information.objects.all().order_by('codeType').prefetch_related(prefetch1)


        context['model_list'] = query
        codedic ={}
        for code in ALL_CODE_TYPE:
            codedic[code.name] = code.value
        context['codetypedic'] = codedic
        return context

@login_required(login_url='People:login')
def CreateCode(request):
    if request.method == 'POST':
        codeType = request.POST.get('codeType')
        codeName = request.POST.get('codeName')
        if codeType == 'Management_TagListType':
            Tag_List.objects.create(code=codeName)
        cinfo = Code_Information.objects.create(codeType=codeType, codeName=codeName)
        for lang in LANGUAGE_CODE_INSERT:
            Code_Multiple_Language.objects.create(languageCode=lang.name, code_info=cinfo)
        return HttpResponse('')

@login_required(login_url='People:login')
def DeleteCode(request):
    if request.method == 'POST':
        id = request.POST.get('pk')
        ci = Code_Information.objects.get(id=id)
        if ci.codeType == 'Management_TagListType':
            Tag_List.objects.get(code=ci.codeName).delete()
        ci.delete()
        return HttpResponse('')

@transaction.atomic
@login_required(login_url='People:login')
@csrf_exempt
def UpdateCodeMultipleLang(request):
    if request.method == 'POST':
        for index, key in enumerate(request.POST):
            value = request.POST[key]
            Code_Multiple_Language.objects.filter(id=key).update(name=value)
        return HttpResponse('')


class CountryCodeInformationView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'management/countrycodeinfo.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(CountryCodeInformationView, self).get_context_data(**kwargs)

        qs = Country_Multiple_Language.objects.all().order_by('languageCode')
        prefetch1 = Prefetch(
            'country_multiple_language',
            queryset=qs,
            to_attr='cml'
        )
        query = CountryInformation.objects.all().prefetch_related(prefetch1)
        context['model_list'] = query
        return context

@login_required(login_url='People:login')
@csrf_exempt
def UpdateLanguageCodeMultipleLang(request):
    if request.method == 'POST':
        multiplelanguagej = request.POST['multiplelanguage']
        multiplelanguage = json.loads(multiplelanguagej)
        for index, key in enumerate(multiplelanguage):
            multipledic = json.loads(key)
            for mdic in multipledic.items():
                Country_Multiple_Language.objects.filter(id=mdic[0]).update(name=mdic[1])
        return HttpResponse('')


class TagsView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'management/taginfo.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(TagsView, self).get_context_data(**kwargs)
        return context
    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        query = Tag_List.objects.all().order_by('id')
        all_id = Tag_List.objects.all().values('id').order_by('id')
        context['tag_lang'] = GetTagCodeNameAndMultipleLanguage('ko')
        context['model_list'] = query
        context['all_id'] = all_id
        return self.render_to_response(context)



class TaskTemplateIframeView(TemplateView):
    template_name = 'management/task_template_iframe.html'
    def get_context_data(self, **kwargs):
        context = super(TaskTemplateIframeView, self).get_context_data(**kwargs)
        return context
    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        language = request.session['django_language']
        pk = request.GET.get('pk')
        labeldic3 = get_fielddic(language, 'Task_Template')
        customcodechoice = GetCodeMultpleLanguageForPatentMaster(request)
        customcodechoice1 = GetCodeMultpleLanguageForCase(request)
        customcodechoice2 = GetCodeMultpleLanguageForTask(request)
        customcodechoice3 = GetCodeMultpleLanguageForCaseTaskMessage(request)
        customcodechoice.update(customcodechoice1)
        customcodechoice.update(customcodechoice2)
        ttr = Task_Template_Ref.objects.filter(id=pk).first()
        context['tem_ref'] = ttr
        context['Task_Template_label'] = labeldic3
        context['Case_CaseState'] = customcodechoice1[ALL_CODE_TYPE.Case_CaseState.name]
        context['Message_CaseTaskType'] = customcodechoice3[ALL_CODE_TYPE.Message_CaseTaskType.name]
        context['Task_TaskState'] = customcodechoice2[ALL_CODE_TYPE.Task_TaskState.name]
        context['taskmultiplecode'] = customcodechoice2[ALL_CODE_TYPE.Task_TaskType.name]
        context['Tag_list'] = GetTagCodeInfoMultipleLanguage(language)


        caseType = set_code_to_multilanguage(language, ALL_CODE_TYPE.Case_CaseType.name, ttr.caseType)
        patentType = set_code_to_multilanguage(language, ALL_CODE_TYPE.PatentType.name, ttr.patentType)
        patentNationType = set_code_to_multilanguage(language, ALL_CODE_TYPE.PatentNationType.name, ttr.patentNationType)
        countryCode = set_counrycode_to_multilanguage(language, ttr.countryCode)
        if ttr.isGeneralClient:
            client = '일반고객'
        else:
            client = ttr.cleint.korCorporationName
        specialtempname= ''
        if not ttr.isGeneralTemplate:
            specialtempname = ttr.specialTemplateName

        if patentNationType == 'O':
            context['custom_tem_ref_name'] = client+'_'+patentType+'_'+patentNationType+'_'+countryCode+ '_'+caseType +'_'+specialtempname
        else:
            context['custom_tem_ref_name'] = client + '_' + patentType + '_' + patentNationType + '_' + caseType + '_' + specialtempname


        return self.render_to_response(context)

class TasksTemplateView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'management/task_template.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(TasksTemplateView, self).get_context_data(**kwargs)
        return context
    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        language = request.session['django_language']
        labeldic = get_fielddic(language, 'Application_Patent_Domestic_Master')
        labeldic1 = get_fielddic(language, 'Case')
        labeldic2 = get_fielddic(language, 'Task')
        labeldic.update(labeldic1)
        labeldic.update(labeldic2)
        customcodechoice = GetCodeMultpleLanguageForPatentMaster(request)
        customcodechoice1 = GetCodeMultpleLanguageForCase(request)
        customcodechoice2 = GetCodeMultpleLanguageForTask(request)
        customcodechoice3 = GetCodeMultpleLanguageForCaseTaskMessage(request)
        customcodechoice.update(customcodechoice1)
        customcodechoice.update(customcodechoice2)
        form = TaskTemplateRefForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)
        context['ref_form'] = form
        context['template_ref'] = Task_Template_Ref.objects.all()

        context['Message_CaseTaskType'] = customcodechoice3[ALL_CODE_TYPE.Message_CaseTaskType.name]

        context['Task_TaskState'] = customcodechoice2[ALL_CODE_TYPE.Task_TaskState.name]
        context['taskmultiplecode'] = customcodechoice2[ALL_CODE_TYPE.Task_TaskType.name]
        context['Tag_list'] = GetTagCodeInfoMultipleLanguage(language)
        context['client_list'] = Client.objects.filter(client_clienttype__typeName='Client')
        return self.render_to_response(context)

