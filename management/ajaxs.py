from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from case.models import Task_Template, Task_Template_Ref, Task_Template_Ref_MessageType, Task_Template_Ref_CaseState, \
    Task_Template_TaskState, Task_Template_TaskRequestCaseMessage, Task_Template_TaskReceiptCaseMessage, \
    Task_Template_Ref_CaseStateChangeCondition
from common.codemultilanguage_choice import set_code_to_multilanguage, set_counrycode_to_multilanguage
from management.models import Tag_List, Linked_Tags, Table_History
from rest_framework import serializers

from utils.enums import ALL_CODE_TYPE, CRUD_TYPE


@login_required(login_url='People:login')
@csrf_exempt
def UpdateTagParent(request):
    if request.method == 'POST':
        parentid = request.POST['parentid']
        tagid = request.POST['tagid']
        tags = Tag_List.objects.filter(id=tagid)
        if tags:
            tag = tags.first()
            if parentid == 'none':
                tag.parent = None
                tag.save()
            else:
                tag.parent = Tag_List.objects.get(id=parentid)
                tag.save()
        return HttpResponse('')


@login_required(login_url='People:login')
@csrf_exempt
def UpdateTemplateChild(request):
    if request.method == 'GET':
        tempid = request.GET['tempid']
        childid = request.GET['childid']


        if childid:
            Task_Template.objects.filter(id=tempid).update(child_id=childid)
        data = {'state': 'success'}
        return JsonResponse(data)

@login_required(login_url='People:login')
@csrf_exempt
def UpdateTaskTemplateState(request):
    if request.method == 'GET':
        tempid = request.GET['tempid']
        taskstate = request.GET.getlist('taskstate[]')

        if tempid and '' in taskstate and len(taskstate) == 1:
            Task_Template_TaskState.objects.filter(tasktemplate_id=tempid).delete()
        else:
            tt = Task_Template.objects.get(id=tempid)
            samelist = []
            for ctm in tt.tasktemplatetaskstate_tasktemplate.all():
                if ctm.taskState not in taskstate:
                    ctm.delete()
                else:
                    samelist.append(ctm.taskState)
            for code in taskstate:
                if code not in samelist:
                    Task_Template_TaskState.objects.get_or_create(taskState=code,
                                                                        tasktemplate_id=tempid)

        data = {'state': 'success'}
        return JsonResponse(data)


@login_required(login_url='People:login')
@csrf_exempt
def UpdateTaskTemplate_TaskRequestCaseMessage(request):
    if request.method == 'GET':
        tempid = request.GET['tempid']
        code = request.GET['code']
        if tempid and not code:
            Task_Template_TaskRequestCaseMessage.objects.filter(tasktemplate_id=tempid).delete()
        else:
            Task_Template_TaskRequestCaseMessage.objects.get_or_create(taskRequestCaseMessage=code,
                                                                       tasktemplate_id=tempid)

        data = {'state': 'success'}
        return JsonResponse(data)

@csrf_exempt
def UpdateTaskTemplate_TaskReceiptCaseMessage(request):
    if request.method == 'GET':
        tempid = request.GET['tempid']
        code = request.GET['code']
        if tempid and not code:
            Task_Template_TaskReceiptCaseMessage.objects.filter(tasktemplate_id=tempid).delete()
        else:
            Task_Template_TaskReceiptCaseMessage.objects.get_or_create(taskReceiptCaseMessage=code,
                                                                       tasktemplate_id=tempid)

        data = {'state': 'success'}
        return JsonResponse(data)

@csrf_exempt
def UpdateTaskTemplate_Tag(request):
    if request.method == 'GET':
        tempid = request.GET['tempid']
        tag = request.GET.getlist('tag[]')
        if tempid and '' in tag and len(tag) == 1:
            Linked_Tags.objects.filter(case_task_template_id=tempid).delete()
        else:
            for t in tag:
                Linked_Tags.objects.get_or_create(tag_list_id=t, case_task_template_id=tempid)
        data = {'state': 'success'}
        return JsonResponse(data)



@login_required(login_url='People:login')
@csrf_exempt
def DeleteTemplate(request):
    if request.method == 'GET':
        tempid = request.GET['tempid']
        Task_Template.objects.filter(id=tempid).delete()
        data = {'state': 'success'}
        return JsonResponse(data)


@login_required(login_url='People:login')
@csrf_exempt
def UpdateTemplateIndex(request):
    if request.method == 'GET':
        tempid = request.GET['tempid']
        index = request.GET['index']
        Task_Template.objects.filter(id=tempid).update(index=index)
        data = {'state': 'success'}
        return JsonResponse(data)

@login_required(login_url='People:login')
@csrf_exempt
def UpdateTemplateCaseMessageType(request):
    if request.method == 'GET':
        temprefid = request.GET['temprefid']
        messageType = request.GET.getlist('messageType[]')
        tr = Task_Template_Ref.objects.get(id=temprefid)
        if temprefid and '' in messageType and len(messageType) == 1:
            Task_Template_Ref_MessageType.objects.filter(tasktemplate_ref_id=temprefid).delete()
        else:
            samelist = []
            for ctm in tr.tasktemplaterefmessagetype_tasktemplateref.all():
                if ctm.messageType not in messageType:
                    ctm.delete()
                else:
                    samelist.append(ctm.messageType)
            for code in messageType:
                if code not in samelist:
                    Task_Template_Ref_MessageType.objects.get_or_create(messageType=code,
                                                                    tasktemplate_ref_id=temprefid)
        data = {'state': 'success'}
        return JsonResponse(data)


@login_required(login_url='People:login')
@csrf_exempt
def UpdateTemplateCaseState(request):
    if request.method == 'GET':
        temprefid = request.GET['temprefid']
        casetype = request.GET.getlist('caseState[]')
        tr = Task_Template_Ref.objects.get(id=temprefid)
        if temprefid and '' in casetype and len(casetype) == 1:
            Task_Template_Ref_CaseState.objects.filter(tasktemplate_ref_id=temprefid).delete()
        else:
            samelist = []
            for ctm in tr.tasktemplaterefcasestate_tasktemplateref.all():
                if ctm.caseState not in casetype:
                    ctm.delete()
                else:
                    samelist.append(ctm.caseState)
            for code in casetype:
                if code not in samelist:
                    Task_Template_Ref_CaseState.objects.get_or_create(caseState=code,
                                                                      tasktemplate_ref_id=temprefid)
        data = {'state': 'success'}
        return JsonResponse(data)

@login_required(login_url='People:login')
@csrf_exempt
def UpdateTemplateCaseStateChangeCondition(request):
    if request.method == 'GET':
        temprefid = request.GET['temprefid']
        casestate = request.GET.getlist('caseState[]')
        messageCaseType = request.GET['messageCaseType']
        if messageCaseType and '' in casestate and len(casestate) == 1:
            Task_Template_Ref_CaseStateChangeCondition.objects.filter(tasktemplate_ref_id=temprefid, messageType=messageCaseType).delete()
        else:
            for cscc in Task_Template_Ref.objects.get(id=temprefid).tasktemplaterefcasestatechangecondition_tasktemplateref.all():
                if messageCaseType == cscc.messageType and cscc.caseState not in casestate:
                    cscc.delete()
            for code in casestate:
                Task_Template_Ref_CaseStateChangeCondition.objects.get_or_create(messageType=messageCaseType, caseState=code
                                                                                 , tasktemplate_ref_id=temprefid)
        data = {'state': 'success'}
        return JsonResponse(data)

class TaskTemplateRefSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task_Template_Ref
        fields = '__all__'

@csrf_exempt
def GetTemplateRef(request):
    if request.method == 'GET':
        clientid = request.GET['clientid']
        patenttype = request.GET['patenttype']
        nationtype = request.GET['nationtype']
        countrycode = request.GET['countrycode']
        if nationtype == "O":
            if clientid == 'na':
                ttr = Task_Template_Ref.objects.filter(isGeneralClient=True, patentType=patenttype,
                                                     patentNationType=nationtype,
                                                     countryCode=countrycode)
            else:
                ttr = Task_Template_Ref.objects.filter(client_id=clientid, patentType=patenttype, patentNationType=nationtype,
                                                     countryCode=countrycode)
        else:
            if clientid == 'na':
                ttr = Task_Template_Ref.objects.filter(isGeneralClient=True, patentType=patenttype,
                                                     patentNationType=nationtype)
            else:
                ttr = Task_Template_Ref.objects.filter(client_id=clientid, patentType=patenttype, patentNationType=nationtype)

        lang = request.session['django_language']
        for t in ttr:
            if t.isGeneralClient:
                tc = '일반 고객'
            else:
                tc = t.client.korCorporationName
            t.caseType = set_code_to_multilanguage(lang, ALL_CODE_TYPE.Case_CaseType.name, t.caseType)
            t.patentType = tc + '_' + set_code_to_multilanguage(lang, ALL_CODE_TYPE.PatentType.name, t.patentType)
            t.patentNationType = set_code_to_multilanguage(lang, ALL_CODE_TYPE.PatentNationType.name, t.patentNationType)
            if not t.isGeneralTemplate:
                t.caseType = t.caseType + '_' + t.specialTemplateName
            t.countryCode = set_counrycode_to_multilanguage(lang, t.countryCode)
        serializer = TaskTemplateRefSerializer(ttr, many=True)
        data = {'state': 'success','result': serializer.data}
        return JsonResponse(data)


@login_required(login_url='People:login')
@csrf_exempt
def CreateTaskTemplate(request):
    if request.method == 'GET':
        taskCode = request.GET.get('taskCode')
        tr = request.GET.get('tasktemplaterefid')
        tt, created = Task_Template.objects.get_or_create(taskCode=taskCode, tasktemplate_ref_id=tr)
        if created:
            Table_History.objects.create(crudType=CRUD_TYPE.C.name, tableName='Task_Template', primaryKey=tt.id,
                                  user=request.user)
    data = {'state': 'success', 'result': ''}
    return JsonResponse(data)


@login_required(login_url='People:login')

def CreateTaskTemplateRef(request):
    if request.method == 'GET':
        client = request.GET.get('client')
        patentType = request.GET.get('patentType')
        nationType = request.GET.get('nationType')
        countryCode = request.GET.get('countryCode')
        caseType = request.GET.get('caseType')
        isSpecialTemp = request.GET.get('isSpecialTemp')
        specialTemp = request.GET.get('specialTemp')

        if client == 'na':
            para = {'isGeneralClient': True}
        else:
            para = {'isGeneralClient': False, 'client_id': client}

        if isSpecialTemp == 'spc':
            para['isGeneralTemplate'] = False
            para['specialTemplateName'] = specialTemp
        else:
            para['isGeneralTemplate'] = True

        if nationType != 'O':
            if not patentType or not nationType or not caseType:
                data = {'state': 'fail', 'result': 'parameter is missing'}
                return JsonResponse(data)
            countryCode = ""
        else:
            if not patentType or not nationType or not countryCode or not caseType:
                data = {'state': 'fail', 'result': 'parameter is missing'}
                return JsonResponse(data)

        tm, created = Task_Template_Ref.objects.get_or_create(patentType=patentType, patentNationType=nationType,
                                              countryCode=countryCode, caseType=caseType, **para)

        if created:
            Table_History.objects.create(crudType=CRUD_TYPE.C.name, tableName='Task_Template_Ref', primaryKey=tm.id,
                                         user=request.user)
        lang = request.session['django_language']

        if client == 'na':
            para = {'isGeneralClient': True}
        else:
            para = {'isGeneralClient': False, 'client_id': client}

        if nationType == 'O':
            tr = Task_Template_Ref.objects.filter(patentType=patentType, patentNationType=nationType,
                                              countryCode=countryCode,**para)
        else:
            tr = Task_Template_Ref.objects.filter(patentType=patentType, patentNationType=nationType,**para)
        for t in tr:
            if t.isGeneralClient:
                tc = '일반 고객'
            else:
                tc = t.client.korCorporationName
            t.caseType = set_code_to_multilanguage(lang, ALL_CODE_TYPE.Case_CaseType.name, t.caseType)
            t.patentType = tc + '_' + set_code_to_multilanguage(lang, ALL_CODE_TYPE.PatentType.name, t.patentType)
            t.patentNationType = set_code_to_multilanguage(lang, ALL_CODE_TYPE.PatentNationType.name,
                                                           t.patentNationType)
            if not t.isGeneralTemplate:
                t.caseType = t.caseType + '_' + t.specialTemplateName
            t.countryCode = set_counrycode_to_multilanguage(lang, t.countryCode)
        serializer = TaskTemplateRefSerializer(tr, many=True)
        data = {'state': 'success','result': serializer.data}
        return JsonResponse(data)