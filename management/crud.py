import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from case.models import Task_Template_Ref, Task_Template, Task_Template_Ref_MessageType
from management.forms import TagModelForm
from management.models import Tag_List, Table_History
from utils.db_fieldset import get_fielddic
from utils.enums import LANGUAGE_CODE_INSERT, CRUD_TYPE


@login_required(login_url='People:login')
def TagCreate(request):
    if request.method == 'POST':
        code = request.POST.get('code', None)
        if code:
            tag = Tag_List.objects.create(code=code)
            for l in LANGUAGE_CODE_INSERT:
                Tag_List_Multiple_Language.objects.create(languageCode=l.name, tag_list=tag)
            return redirect('management:tags')


@login_required(login_url='People:login')
@csrf_exempt
def TagUpdate(request):
    if request.method == 'POST':
        multiplelanguagej = request.POST['multiplelanguage']
        multiplelanguage = json.loads(multiplelanguagej)
        for index, key in enumerate(multiplelanguage):
            multipledic = json.loads(key)
            for mdic in multipledic.items():
                Tag_List_Multiple_Language.objects.filter(id=mdic[0]).update(name=mdic[1])
        return HttpResponse('')

@login_required(login_url='People:login')
@csrf_exempt
def CreateTaskTemplateRef(request):
    if request.method == 'POST':
        patentType = request.POST.get('patentType')
        nationType = request.POST.get('nationType')
        countryCode = request.POST.get('countryCode')
        caseType = request.POST.get('caseType')


        if nationType != 'O':
            if not patentType or not nationType or not caseType:
                return redirect('management:task_template')
            countryCode = ""
        else:
            if not patentType or not nationType or not countryCode or not caseType:
                return redirect('management:task_template')
        # child = request.POST.get('child')
        tr, created = Task_Template_Ref.objects.get_or_create(patentType=patentType, patentNationType=nationType,
                                              countryCode=countryCode, caseType=caseType)

        if created:
            Table_History.objects.create(crudType=CRUD_TYPE.C.name, tableName='Task_Template_Ref', primaryKey=tr.id,
                                         user=request.user)


    return redirect('management:task_template')


@login_required(login_url='People:login')
@csrf_exempt
def CreateTaskTemplate(request):
    if request.method == 'POST':
        taskCode = request.POST.get('taskCode')
        tr = request.POST.get('tasktemplaterefid')
        tt, created = Task_Template.objects.get_or_create(taskCode=taskCode, tasktemplate_ref_id=tr)
        if created:
            Table_History.objects.create(crudType=CRUD_TYPE.C.name, tableName='Task_Template', primaryKey=tt.id,
                                  user=request.user)

    return redirect('management:task_template')



@login_required(login_url='People:login')
@csrf_exempt
def TaskTemplateCreate(request):
    if request.method == 'POST':
        taskCode = request.POST.get('taskCode')
        tr = Task_Template.objects.create(taskCode=taskCode)
        Table_History.objects.create(crudType=CRUD_TYPE.C.name, tableName='Task_Template', primaryKey=tr.id,
                                     user=request.user)
        redirect('management:task_template')

