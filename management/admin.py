from django.contrib import admin

# Register your models here.
from management.models import DB_Modifying_Field_Name, CountryInformation

admin.site.register(DB_Modifying_Field_Name)
admin.site.register(CountryInformation)

