import glob

import xlrd
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import redirect

from management.models import DB_Modifying_Field_Name, CountryInformation, Code_Information, Code_Multiple_Language, \
    Country_Multiple_Language
from utils.db_fieldset import Insert_DB_Field_Name
from utils.enums import LANGUAGE_CODE_INSERT, MakeWIPO_ST3_Enum, MakeWIPO_ST3_Dic, UPLOAD_FILE_PATH


@login_required(login_url='People:login')
@transaction.atomic
def InitDbCustomFieldName(request):

    Insert_DB_Field_Name()

    #next = request.POST.get('next', '/')
    metaget = request.META.get('HTTP_REFERER')
    return redirect('/management/common_mgr')

@login_required(login_url='People:login')
@transaction.atomic
def InitDBCountryCode(request):
    if request.method == 'GET':
        file_list = glob.glob(UPLOAD_FILE_PATH.excelfile.value + '/*')
        for f in file_list:
            wb = xlrd.open_workbook(f)
            sheet = wb.sheet_by_index(0)
            num_cols = sheet.ncols  # Number of columns
            all_val = []
            cols = sheet.ncols
            rows = sheet.nrows
            for row in range(rows):
                row_val = []
                for col in range(cols):
                    row_val.append(sheet.cell_value(row, col))
                all_val.append(row_val)
            for onerow in all_val:
                try:
                    ci = CountryInformation.objects.get(countryCode=onerow[0])
                except Exception as e:
                    print(e)

                for l in LANGUAGE_CODE_INSERT:
                    if l.name == 'ko':
                        langname = onerow[2]
                    elif l.name == 'en':
                        langname = onerow[1]
                    elif l.name == 'ja':
                        langname = onerow[3]
                    elif l.name == 'zh_Hant':
                        langname = onerow[4]
                    elif l.name == 'zh_Hans':
                        langname = onerow[5]
                    Country_Multiple_Language.objects.get_or_create(country_info=ci, languageCode=l.name, name=langname)

    return redirect('/management/common_mgr')

@login_required(login_url='People:login')
@transaction.atomic
def InitDBCodeInfoMultipleLanguage(request):
    if request.method == 'GET':
        for code in Code_Information.objects.all():
            for lang in LANGUAGE_CODE_INSERT:
                Code_Multiple_Language.objects.get_or_create(languageCode=lang.name, code_info=code)

    return redirect('/management/common_mgr')