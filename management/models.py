from django.contrib import auth
from django.db import models

# Create your models here.
from utils.enums import MakeWIPO_ST3_Enum, LANGUAGE_CODE_INSERT, ALL_CODE_TYPE, CRUD_TYPE, TAG_TYPE, TAG_COMMENT_TYPE


class BaseModel(models.Model):
    tag = models.ManyToManyField(
        'management.Tag_List',
        through='management.Linked_Tags', blank=True
    )
    class Meta:
        abstract = True
class CountryInformation(models.Model):
    countryCode = models.CharField(max_length=100, blank=False, null=False,
                                   choices=[(tag.name, tag.value) for tag in MakeWIPO_ST3_Enum()], default='EN',
                                   unique=True)

    def __str__(self):
        return self.countryCode
    class Meta:
        db_table = 'country_information'

class Country_Multiple_Language(models.Model):
    languageCode = models.CharField(max_length=20, blank=True, null=True,
                                    choices=[(tag.name, tag.value) for tag in LANGUAGE_CODE_INSERT])
    country_info = models.ForeignKey('CountryInformation', null=True, blank=True
                                  , related_name='country_multiple_language', on_delete=models.CASCADE)
    name = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        db_table = 'country_multiple_language'

class DB_Field_Name(models.Model):
    tableName = models.CharField(max_length=60, blank=False, null=False)
    originFieldName = models.CharField(max_length=60, blank=False, null=False)
    fieldCode = models.CharField(max_length=30, blank=True, null=True)
    class Meta:
        db_table = 'db_field_name'
        unique_together = (('tableName', 'originFieldName'),)

class DB_Modifying_Field_Name(models.Model):

    dbfieldname = models.ForeignKey('DB_Field_Name', blank=False, null=False, related_name='field_modifyingfield', on_delete=models.CASCADE, default='')
    customFieldName = models.CharField(max_length=60, blank=True, null=True)
    explanation = models.CharField(max_length=100, blank=True, null=True)
    languageCode = models.CharField(max_length=20, blank=True, null=True, choices=[(tag.name, tag.value) for tag in LANGUAGE_CODE_INSERT])
    class Meta:
        db_table = 'db_modifying_field_name'
        unique_together = (('dbfieldname', 'languageCode'),)

class Code_Information(models.Model):
    codeType = models.CharField(max_length=100, blank=True, null=True, choices=[(tag.name, tag.value) for tag in ALL_CODE_TYPE])
    codeName = models.CharField(max_length=100, blank=True, null=True)
    def __str__(self):
        return self.codeName
    class Meta:
        db_table = 'code_information'
        unique_together = (('codeType', 'codeName'),)

class Code_Multiple_Language(models.Model):
    languageCode = models.CharField(max_length=20, blank=True, null=True, choices=[(tag.name, tag.value) for tag in LANGUAGE_CODE_INSERT])
    code_info = models.ForeignKey('Code_Information', null=True, blank=True
                                  , related_name='code_multiple_language', on_delete=models.CASCADE)
    name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'code_multiple_language'

class Table_History(BaseModel):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=False, default='')
    crudType = models.CharField(max_length=20, blank=True, null=True, choices=[(tag.name, tag.value) for tag in CRUD_TYPE])
    tableName = models.CharField(max_length=200, blank=True, null=True)
    primaryKey = models.IntegerField(blank=False, null=False)
    fieldName = models.CharField(max_length=200, blank=True, null=True)
    initialFieldValue = models.CharField(max_length=200, blank=True, null=True)
    modifiedFieldValue = models.CharField(max_length=300, blank=True, null=True)
    createdDateTime = models.DateTimeField(auto_now_add=True)
    deleteDetail = models.CharField(max_length=1000, blank=True, null=True)
    def __str__(self):
        return self.crudType + ' ' + self.tableName
    class Meta:
        db_table = 'table_history'

class Tag_List(models.Model):
    code = models.CharField(max_length=30, blank=True, null=True, choices=[(tag.name, tag.value) for tag in TAG_TYPE])
    parent = models.ForeignKey('self', null=True, related_name='tag_parent', on_delete=models.SET_NULL)
    def __str__(self):
        return self.code
    class Meta:
        db_table = 'tag_list'


class Tables_Foreign(models.Model):
    app_patent_domestic_master = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Master', null=True, on_delete=models.SET_NULL, related_name='all_table_foreign_app_patent_domestic_master')
    app_patent_domestic_applicant = models.ForeignKey('application_patent_domestic.Application_Patent_Domestic_Applicant',
                                                   null=True, on_delete=models.SET_NULL,
                                                   related_name='all_table_foreign_app_patent_domestic_applicant')
    app_patent_domestic_maininventor = models.ForeignKey(
        'application_patent_domestic.Main_Inventor',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_app_patent_domestic_maininventor')
    app_patent_domestic_agent = models.ForeignKey(
        'application_patent_domestic.Agent',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_app_patent_domestic_agent')
    case_case = models.ForeignKey(
        'case.Case',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_case_case')
    case_task = models.ForeignKey(
        'case.Task',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_case_task')
    case_revenue_invoice = models.ForeignKey(
        'case.Revenue_Invoice',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_case_revenue_invoice')
    case_purchase_invoice = models.ForeignKey(
        'case.Purchase_Invoice',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_case_purchase_invoice')
    case_task_template_ref = models.ForeignKey(
        'case.Task_Template_Ref',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_case_task_template_ref')
    case_task_template = models.ForeignKey(
        'case.Task_Template',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_case_task_template')

    email_emaildetail = models.ForeignKey(
        'emailsystem.Email_Detail',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_email_emaildetail')
    email_emailinfo = models.ForeignKey(
        'emailsystem.Email_Info',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_email_emailinfo')
    management_tablehistory = models.ForeignKey(
        'Table_History',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_management_tablehistory')
    message_message = models.ForeignKey(
        'message.Message',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_message_message')
    message_patentdocument = models.ForeignKey(
        'message.Patent_Document',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_message_patentdocument')
    message_patentdocument_attachment = models.ForeignKey(
        'message.Patent_Document_Attachment',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_message_patentdocument_attachment')
    message_file = models.ForeignKey(
        'message.File',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_message_file')
    people_client = models.ForeignKey(
        'People.Client',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_people_client')
    people_person = models.ForeignKey(
        'People.Person',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_people_person')
    people_workemail = models.ForeignKey(
        'People.Work_Email',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_people_workemail')
    people_applicantinformation = models.ForeignKey(
        'People.ApplicantInformation',
        null=True, on_delete=models.SET_NULL,
        related_name='all_table_foreign_people_applicantinformation')
    class Meta:
        abstract = True


class Linked_Tags(Tables_Foreign):
    type = models.CharField(max_length=20, null=True, blank=True, choices=[(tag.name, tag.value) for tag in TAG_COMMENT_TYPE])
    code = models.CharField(max_length=30, blank=True, null=True)
    description = models.CharField(max_length=300, blank=True, null=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    updatedDate = models.DateTimeField(auto_now=True)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=True, default='')
    tag_list = models.ForeignKey('Tag_List', null=True, on_delete=models.CASCADE, related_name='linked_tag_tag_list')
    comment = models.CharField(max_length=500, null=True, blank=True)


    def __str__(self):
        return self.code
    class Meta:
        db_table = 'linked_tags'