import datetime
import glob
import os
from numbers import Number

from django.db import transaction
from langdetect import detect
import xlrd
from django.shortcuts import redirect

from People.models import Person, Client, ClientType
from application_patent_domestic.models import Application_Patent_Domestic_Master_Applicant, \
    Application_Patent_Domestic_Applicant, Main_Inventor, Agent, Application_Patent_Domestic_Master, \
    Application_Patent_Domestic_Master_SupportingOrganization, Application_Patent_Domestic_Master_Case
from case.models import Revenue_Invoice, Transaction_History, BankAccount_Information, Client_Receiver_AccountInfo
from utils.enums import UPLOAD_FILE_PATH, PATENT_STATE_CHOICE, CLIENT_TYPE, MakeCurrencyEnum, \
    PATENT_TYPE_FOR_OURCOM, NATION_TYPE_FOR_OURCOM
from django.db.models import Q, Max
from dateutil import tz

def sync_excel_files(request):
    file_list = glob.glob(UPLOAD_FILE_PATH.excelfile.value + '/*')

    wb = xlrd.open_workbook(file_list[0])
    """
    Insert_applicant(wb)
    Insert_maininventor(wb)
    Insert_agent(wb)
    #Insert_domestic_patent(wb)

    Insert_nativeagent(wb)
    Insert_agentManager(wb)

    Insert_oversea_patent(wb)
    Insert_domestic_brand(wb)
    Insert_oversea_brand(wb)
    """

    return redirect('management:excelupload')

@transaction.atomic
def sync_excel_files_other(request):
    file_list = glob.glob(UPLOAD_FILE_PATH.excelfile.value + '/*')
    wb = xlrd.open_workbook(file_list[0])
    Insert_revenueInvoice(wb)

    return redirect('management:excelupload')

@transaction.atomic
def Insert_BankAccountInformation():
    bobj, created = BankAccount_Information.objects.get_or_create(depositorName='옥창안',
                                                                  bankName='하나은행',
                                                                  accountNumber='905-910004-75804',
                                                                  isClientInfo=False)


@transaction.atomic
def Insert_transactionHistory(wb):
    sheet = wb.sheet_by_index(0)
    num_cols = sheet.ncols  # Number of columns
    all_val = []
    cols = sheet.ncols
    rows = sheet.nrows

    try:
        with transaction.atomic():
            for row in range(7, rows-1):
                row_val = []
                for col in range(cols):
                    if col == 1:
                        a = sheet.cell_value(row, col)
                        try:
                            kort = datetime.datetime.strptime(sheet.cell_value(row, col) + ' +0900', '%Y-%m-%d %H:%M %z')
                        except:
                            kort = datetime.datetime.strptime(sheet.cell_value(row, col) + ' +0900',
                                                              '%Y-%m-%d %H:%M:%S %z')
                        utczone = tz.gettz('UTC')
                        utime = kort.astimezone(utczone)
                        row_val.append(utime)

                    elif col == 4:
                        row_val.append('Won')
                    elif col == 5 or col == 6 or col == 7:
                        if sheet.cell_value(row, col):
                            money = round(sheet.cell_value(row, col))
                        else:
                            money = 0


                        row_val.append(money)

                    else:
                        row_val.append(sheet.cell_value(row, col))
                all_val.append(row_val)
            for val in all_val:
                #max_rating = App.objects.aggregate(Max('rating'))['rating__max']
                #return App.objects.get(rating=max_rating)
                if Transaction_History.objects.count() == 0:
                    tmax = 0
                else:
                    tmax = Transaction_History.objects.aggregate(Max('order'))['order__max']
                obj, create = Transaction_History.objects.get_or_create(order=tmax+1,
                                                                        transactionDate=val[1],
                                                                        breifs=val[2],
                                                                        recipient=val[3],
                                                                        currency=val[4],
                                                                        depositAmount=val[5],
                                                                        withdrawalAmount=val[6],
                                                                        balance=val[7],
                                                                        type=val[8],
                                                                        transactionOffice=val[9])


                bi = BankAccount_Information.objects.get(depositorName='옥창안')
                ca = Client_Receiver_AccountInfo(accountinfo=bi, transactionhistory=obj, isReceiver=False)
                ca.save()
    except Exception as e:
        print(e)


def Insert_revenueInvoice(wb):
    sheet = wb.sheet_by_index(0)
    rows = sheet.nrows
    cols = sheet.ncols
    all_val = []
    for row in range(rows):
        if row == 0:
            continue
        for col in range(cols):
            print(sheet.cell_value(row, col))
    for row in range(rows):
        row_val = []
        if row == 0:
            continue
        for col in range(cols):
            if col == 5 or col == 8 or col == 10:
                try:
                    row_val.append(
                        datetime.datetime(*xlrd.xldate_as_tuple(sheet.cell_value(row, col), wb.datemode)).strftime(
                            '%Y-%m-%d'))
                except:
                    row_val.append(None)
            elif col == 6 or col == 7 or col == 9 or col == 11:
                try:
                    if type(sheet.cell_value(row, col)) == str:
                        mstr = sheet.cell_value(row, col)
                        try:
                            row_val.append(int(mstr))
                        except:
                            row_val.append(0)
                    else:
                        money = round(sheet.cell_value(row, col))
                        try:
                            if isinstance(money, Number):
                                row_val.append(money)
                            else:
                                row_val.append(0)
                        except:
                            row_val.append(0)
                except Exception as e:
                    print(e)

            else:
                row_val.append(sheet.cell_value(row, col))
            all_val.append(row_val)
    moneyenum = MakeCurrencyEnum()
    wonname = moneyenum.Won.name
    for row in all_val:
        cdate = {}
        taxdate = {}
        ddate = {}
        if row[5]:
            cdate = {'claimDate':row[5]}
        if row[8]:
            taxdate = {'taxInvoiceIssueDate':row[8]}
        if row[10]:
            ddate = {'depositDate':row[10]}
        obj, create = Revenue_Invoice.objects.get_or_create(ourCompanyNumber=row[0],
                                                           ourCompanyClaimNumber=row[4],
                                                           **cdate,
                                                           ourCompanyCharges=row[6],
                                                           vat=row[7],
                                                           **taxdate,
                                                           totalCharges=row[9],
                                                           **ddate,
                                                           totalDepositAmount=row[11],
                                                           chargesCurrency=wonname,
                                                           totalDepositAmountCurrency=wonname)



def Insert_consulting(wb):
    sheet = wb.sheet_by_index(2)
    rows = sheet.nrows
    cols = sheet.ncols
    all_val = []
    for row in range(rows):
        if row == 0:
            continue
        for col in range(cols):
            print(sheet.cell_value(row, col))
    for row in range(rows):
        row_val = []
        if row == 0:
            continue
        for col in range(cols):
            if col == 11 or col == 12 or col == 13 or col == 14:
                try:
                    row_val.append(
                        datetime.datetime(*xlrd.xldate_as_tuple(sheet.cell_value(row, col), wb.datemode)).strftime(
                            '%Y-%m-%d'))
                except:
                    row_val.append(None)
            else:
                row_val.append(sheet.cell_value(row, col))
            all_val.append(row_val)
    for onerow in all_val:
        for en in PATENT_STATE_CHOICE:
            if en.value == onerow[0]:
                mstate = en
                break
        if not mstate:
            mstate = PATENT_STATE_CHOICE.EMPTY
        cobj = ''
        cmobj = ''
        su_obj = ''
        sum_obj = ''
        if onerow[1]:
            cobj = Client.objects.filter(name_company__korCorporationName=onerow[1], name_company__isCompany=True).last()
        if onerow[6]:
            if 'k' in onerow[6]:
                cmobj = Person.objects.filter(name_company__engFullName=onerow[6], name_company__isCompany=False).last()
            else:
                cmobj = Person.objects.filter(name_company__korName=onerow[6], name_company__isCompany=False).last()
        if onerow[8] and len(onerow[8]) > 2:
            su_obj = Client.objects.filter(name_company__korCorporationName=onerow[8], name_company__isCompany=True).last()
        if onerow[9]:
            sum_obj = Person.objects.filter(name_company__korName=onerow[9], name_company__isCompany=False).last()

        d1 = {}
        d2 = {}
        d3 = {}
        d4 = {}
        if onerow[11]:
            d1 = {'receptionDate':onerow[11]}
        if onerow[12]:
            d2 = {'applicationRequestFixedDate':onerow[12]}
        if onerow[13]:
            d3 = {'draftFixedDate':onerow[13]}
        if onerow[14]:
            d4 = {'draftSendDate':onerow[14]}
        obj, create = Application_Patent_Domestic_Master.objects.get_or_create(patentType=PATENT_TYPE_FOR_OURCOM.R.name,
                                                                              nationType=NATION_TYPE_FOR_OURCOM.D.name,
                                                                              state=mstate.name,
                                                                              ourCompanyNumber=onerow[3],
                                                                              inventNameKor=onerow[5],
                                                                              **d1,**d2,**d3,**d4
                                                                                 )
        if cobj:
            obj.client.add(cobj)
        if cmobj:
            obj.managerOfApplicant.add(cmobj)
        if su_obj:
            aso = obj.supporting_organization.through()
            aso.supporting_organization = su_obj
            if onerow[10]:
                aso.managementCode = onerow[10]
            aso.master = obj
            aso.save()
        if sum_obj:
            obj.supporting_organization_management.add(sum_obj)
        obj.save()
"""
def Insert_client(wb):
    sheet = wb.sheet_by_index(2)
    cols = sheet.ncols
    rows = sheet.nrows

    for row in range(rows):

        if row == 0:
            continue
        if sheet.cell_value(row, 1):
            obj = Name_Company.objects.filter(isCompany=True,
                                               korCorporationName=sheet.cell_value(row,1)).last()
            if not obj:
                obj, created = Name_Company.objects.get_or_create(isCompany=True,
                                            korCorporationName=sheet.cell_value(row, 1))
            Client.objects.get_or_create(name_company=obj)
        if len(sheet.cell_value(row, 8)) > 2:
            obj = Name_Company.objects.filter(isCompany=True,
                                                              korCorporationName=sheet.cell_value(row, 8)).last()
            if not obj:
                obj, created = Name_Company.objects.get_or_create(isCompany=True,
                                            korCorporationName=sheet.cell_value(row, 8))
            Client.objects.get_or_create(name_company=obj)
"""

"""
def insert_person(wb):
    sheet = wb.sheet_by_index(2)
    cols = sheet.ncols
    rows = sheet.nrows

    for row in range(rows):

        if row == 0:
            continue
        if sheet.cell_value(row, 6):
            if 'k' in sheet.cell_value(row, 6):
                obj, created = Name_Company.objects.get_or_create(isCompany=False,
                                                                  engFullName=sheet.cell_value(row, 6))
            else:
                obj, created = Name_Company.objects.get_or_create(isCompany=False,
                                                                  korName=sheet.cell_value(row, 6))
            Person.objects.get_or_create(name_company=obj)
        if sheet.cell_value(row, 9):
            obj, created = Name_Company.objects.get_or_create(isCompany=False,
                                                              korName=sheet.cell_value(row, 9))
            Person.objects.get_or_create(name_company=obj)

"""

def Insert_applicant(wb):

    # 출원인은 출원인 코드가 기준인데, 엑셀에 출원인 정보에 없을 경우 일단 누락해야함.
    sheet = wb.sheet_by_index(14)
    cols = sheet.ncols
    rows = sheet.nrows

    for row in range(rows):


        if row == 0:
            continue
        if len(sheet.cell_value(row, 1)) == 3:
            Application_Patent_Domestic_Applicant.objects.get_or_create(isCompany=False,isCorporation=False,korFullName=sheet.cell_value(row, 1),
                                                                        code=sheet.cell_value(row, 2),
                                                                        engFullName=sheet.cell_value(row, 4),korAddress=sheet.cell_value(row, 5), engAddress=sheet.cell_value(row, 6))

        else:
            Application_Patent_Domestic_Applicant.objects.get_or_create(isCompany=True, isCorporation=True,
                                                                        code=sheet.cell_value(row, 2),
                                                                        korFullName=sheet.cell_value(row, 1),
                                                                        engFullName=sheet.cell_value(row, 4),
                                                                        korAddress=sheet.cell_value(row, 5),
                                                                        engAddress=sheet.cell_value(row, 6))



            """
            obj = Name_Company()
            obj.korCorporationName = sheet.cell_value(row, 1)
            obj. isCompany = True
            obj.engCorporationName = sheet.cell_value(row, 4)
            obj.korAddress = sheet.cell_value(row, 5)
            obj.engAddress = sheet.cell_value(row, 6)
            """

def Insert_maininventor(wb):


    sheet = wb.sheet_by_index(13)
    cols = sheet.ncols
    rows = sheet.nrows
    for row in range(rows):
        if row == 0:
            continue
        obj,create = Main_Inventor.objects.get_or_create(korName=sheet.cell_value(row, 4), nationality=sheet.cell_value(row, 3),
                                            engName=sheet.cell_value(row, 5),korId=sheet.cell_value(row, 6),
                                            korAddress=sheet.cell_value(row, 7),zipCode=sheet.cell_value(row, 8),
                                            engAddress=sheet.cell_value(row, 9))

def Insert_agent(wb):
    sheet = wb.sheet_by_index(12)
    cols = sheet.ncols
    rows = sheet.nrows
    for row in range(rows):
        if row == 0:
            continue
        obj,create = Agent.objects.get_or_create(agentCode=sheet.cell_value(row, 1), korName=sheet.cell_value(row, 0),
                                    engName=sheet.cell_value(row, 2),korAddress=sheet.cell_value(row, 3),
                                    engAddress=sheet.cell_value(row, 4))


"""
def Insert_nativeagent(wb):
    sheet = wb.sheet_by_index(4)
    rows = sheet.nrows
    for row in range(rows):
        if row == 0:
            continue
        if len(sheet.cell_value(row, 9)) > 2:
            nc, created = Name_Company.objects.get_or_create(engCorporationName=sheet.cell_value(row, 9))
            cobj, created = Client.objects.get_or_create(name_company=nc)
            ctypeobj, created = ClientType.objects.get_or_create(typeName=CLIENT_TYPE.OverseaAgent)
            cobj.client_clienttype.add(ctypeobj)
            cobj.save()

    sheet = wb.sheet_by_index(9)
    rows = sheet.nrows
    for row in range(rows):
        if row == 0:
            continue
        if len(sheet.cell_value(row, 11)) > 0:
            nc, created = Name_Company.objects.get_or_create(engCorporationName=sheet.cell_value(row, 11))
            cobj, created = Client.objects.get_or_create(name_company=nc)
            ctypeobj, created = ClientType.objects.get_or_create(typeName=CLIENT_TYPE.OverseaAgent)
            cobj.client_clienttype.add(ctypeobj)
            cobj.save()

def Insert_agentManager(wb):
    sheet = wb.sheet_by_index(4)
    rows = sheet.nrows
    nc = ''
    for row in range(rows):
        if row == 0:
            continue

        txt = sheet.cell_value(row, 10)
        if not txt:
            continue
        try:
            lang = detect(txt)
        except:
            continue
        if lang == "kr":
            nc, created = Name_Company.objects.get_or_create(korName=sheet.cell_value(row, 10), isCompany=False)
        elif lang == "en":
            nc, created = Name_Company.objects.get_or_create(engFullName=sheet.cell_value(row, 10), isCompany=False)
        if nc:
            Person.objects.get_or_create(name_company=nc)

    sheet = wb.sheet_by_index(9)
    rows = sheet.nrows
    nc =''
    for row in range(rows):
        if row == 0:
            continue
        txt = sheet.cell_value(row, 10)
        if not txt:
            continue
        try:
            lang = detect(txt)
        except:
            continue

        if lang == "kr":
            nc, created = Name_Company.objects.get_or_create(korName=sheet.cell_value(row, 13),isCompany=False)
        elif lang == "en":
            nc, created = Name_Company.objects.get_or_create(engFullName=sheet.cell_value(row, 13),isCompany=False)
        if nc:
            Person.objects.get_or_create(name_company=nc)
"""


"""
def Insert_domestic_patent(wb):
    sheet = wb.sheet_by_index(3)
    num_cols = sheet.ncols  # Number of columns
    all_val = []
    cols = sheet.ncols
    rows = sheet.nrows
    for row in range(rows):
        row_val = []
        if row == 0:
            continue
        for col in range(cols):
            if col == 7 or col == 9 or col == 10 or col == 11 or col == 12 or col == 15:
                try:
                    row_val.append(
                        datetime.datetime(*xlrd.xldate_as_tuple(sheet.cell_value(row, col), wb.datemode)).strftime(
                            '%Y-%m-%d'))
                except:
                    row_val.append(datetime.datetime.now().strftime('%Y-%m-%d'))


            else:
                row_val.append(sheet.cell_value(row, col))
        all_val.append(row_val)
    for onerow in all_val:
        for en in PATENT_STATE_CHOICE:
            if en.value == onerow[0]:
                mstate = en
                break
        if not mstate:
            mstate = PATENT_STATE_CHOICE.EMPTY
        obj, create = Application_Patent_Domestic_Master.objects.get_or_create(patentType=PATENT_TYPE_FOR_OURCOM.P.name,nationType=NATION_TYPE_FOR_OURCOM.D.name,state=mstate.name,ourCompanyNumber=onerow[2], inventNameKor=onerow[3],
                                                                 receptionDate=onerow[7], priorityDate=onerow[9],
                                                                 applicationRequestFixedDate=onerow[10], draftFixedDate=onerow[11],
                                                                 draftSendDate=onerow[12], currentProgress=onerow[13], applicationNumber=onerow[14],
                                                                 applicationDate=onerow[15], remarks=onerow[16]
                                                                 )

        # 당소 담당 정보가 없으면 만들어서 연결.
        if onerow[6]:
            nc, created = Name_Company.objects.get_or_create(korName=onerow[6], isCompany=False)
            applmanagement, created = Person.objects.get_or_create(name_company=nc)
            obj.managerOfOurCompany.add(applmanagement)
        # 출원인 담당 정보가 없으면 만들어서 연결.
        if onerow[5]:
            nc, created = Name_Company.objects.get_or_create(korName=onerow[5], isCompany=False)
            applmanagement, created = Person.objects.get_or_create(name_company=nc)
            obj.managerOfApplicant.add(applmanagement)

        # 발명자 정보가 없으면 만들어서 연결.
        if onerow[4]:
            miobj, created = Main_Inventor.objects.get_or_create(korName=onerow[4])
            obj.mainInventor.add(miobj)


        # 출원인 연결
        try:
            if len(str(onerow[1])) > 2:
                if onerow[1] == '박청종/송승준':
                    names = onerow[1].split('/')
                    for name in names:
                        appobj, created = Application_Patent_Domestic_Applicant.objects.get_or_create(korFullName=name, isCompany=False)
                        obj.applicants.add(appobj)
                else:
                    query = {}
                    if len(onerow[1]) == 3 or len(onerow[1]) == 4:
                        query = {'isCompany': False}
                    else:
                        query = {'isCompany': True}
                    ncobj, created = Application_Patent_Domestic_Applicant.objects.get_or_create(korFullName=onerow[1],**query)

                    if ncobj:
                        obj.applicants.add(ncobj)
        except Exception as e:
            print(e)
"""

"""
def Insert_oversea_patent(wb):
    sheet = wb.sheet_by_index(4)
    num_cols = sheet.ncols  # Number of columns
    all_val = []
    cols = sheet.ncols
    rows = sheet.nrows
    for row in range(rows):
        row_val = []
        if row == 0:
            continue
        for col in range(cols):
            if col == 12 or col == 14 or col == 17 or col == 18 or col == 21:
                try:
                    row_val.append(
                        datetime.datetime(*xlrd.xldate_as_tuple(sheet.cell_value(row, col), wb.datemode)).strftime(
                            '%Y-%m-%d'))
                except:
                    row_val.append(datetime.datetime.now().strftime('%Y-%m-%d'))


            else:
                row_val.append(sheet.cell_value(row, col))
        all_val.append(row_val)
    for onerow in all_val:
        for en in PATENT_STATE_CHOICE:
            if en.value == onerow[0]:
                mstate = en
                break
        if not mstate:
            mstate = PATENT_STATE_CHOICE.EMPTY
        obj,create = Application_Patent_Domestic_Master.objects.get_or_create(patentType=PATENT_TYPE_FOR_OURCOM.P.name,nationType=NATION_TYPE_FOR_OURCOM.O.name,state=mstate.name,applicationCountryCode=onerow[1], ourCompanyNumber=onerow[2], inventNameOrigin=onerow[3],
                                                                              inventNameKor=onerow[4], receptionDate=onerow[12], priorityDate=onerow[14],pctBaseApplication=onerow[16],
                                                                              draftFixedDate=onerow[17],draftSendDate=onerow[18], currentProgress=onerow[19], applicationNumber=onerow[20],applicationDate=onerow[21], remarks=onerow[22])

        # 당소 담당 정보가 없으면 만들어서 연결.
        if onerow[11]:
            nc, created = Name_Company.objects.get_or_create(korName=onerow[11], isCompany=False)
            applmanagement, created = Person.objects.get_or_create(name_company=nc)
            obj.managerOfOurCompany.add(applmanagement)
        # 출원인 담당 정보가 없으면 만들어서 연결.
        if onerow[7]:
            nc, created = Name_Company.objects.get_or_create(korName=onerow[7],isCompany=False)
            applmanagement, created = Person.objects.get_or_create(name_company=nc)
            obj.managerOfApplicant.add(applmanagement)

        # 발명자 정보가 없으면 만들어서 연결.
        if onerow[6]:
            miobj, created = Main_Inventor.objects.get_or_create(korName=onerow[6])
            obj.mainInventor.add(miobj)
        pobj = ''
        # 현지 대리인 담당 연결
        if len(onerow[10]) > 2:
            try:
                lang = detect(onerow[10])
                if lang == "en":
                    oagent, created = Name_Company.objects.get_or_create(engFullName=onerow[10], isCompany=False)
                    pobj, created = Person.objects.get_or_create(name_company=oagent)
                elif lang == "kr":
                    oagent, created = Name_Company.objects.get_or_create(korName=onerow[10],isCompany=False)
                    pobj, created = Person.objects.get_or_create(name_company=oagent)
            except:
                print(onerow[10])
                pass


        # 현지 대리인 연결
        if len(onerow[9]) > 1:
            oagent, created = Name_Company.objects.get_or_create(engCorporationName=onerow[9])
            client, created = Client.objects.get_or_create(name_company=oagent)
            throughmodel = obj.oversea_agent.through()
            throughmodel.oversea_agent = client
            throughmodel.master = obj
            if pobj:
                throughmodel.oversea_management = pobj
            if len(onerow[8]) > 1:
                throughmodel.agentCode = onerow[8]
            throughmodel.save()



        # 출원인 연결
        if len(str(onerow[5])) > 2:
            ncobj = Application_Patent_Domestic_Applicant.objects.filter(korFullName=onerow[5]).last()
            if not ncobj:
                query = {}
                if len(onerow[1]) == 3 or len(onerow[1]) == 4:
                    query = {'isCompany': False}
                else:
                    query = {'isCompany': True}
                ncobj, created = Application_Patent_Domestic_Applicant.objects.get_or_create(korFullName=onerow[5],**query)

            if ncobj:
                obj.applicants.add(ncobj)
"""

"""
def imagetest(wb):
    sheet = wb.sheet_by_index(8)
    all_val = []
    cols = sheet.ncols
    rows = sheet.nrows
    for row in range(rows):
        row_val = []
        if row == 0:
            continue
        for col in range(cols):
            if col == 10 or col == 11 or col == 14:
                try:
                    row_val.append(
                        datetime.datetime(*xlrd.xldate_as_tuple(sheet.cell_value(row, col), wb.datemode)).strftime(
                            '%Y-%m-%d'))
                except:
                    row_val.append(datetime.datetime.now().strftime('%Y-%m-%d'))


            else:
                row_val.append(sheet.cell_value(row, col))
        all_val.append(row_val)

    for onerow in all_val:
        test = onerow[4]
        print(test)
"""

"""
def Insert_domestic_brand(wb):
    sheet = wb.sheet_by_index(8)
    num_cols = sheet.ncols  # Number of columns
    all_val = []
    cols = sheet.ncols
    rows = sheet.nrows
    for row in range(rows):
        row_val = []
        if row == 0:
            continue
        for col in range(cols):
            if col == 10 or col == 11 or col == 14:
                try:
                    row_val.append(
                        datetime.datetime(*xlrd.xldate_as_tuple(sheet.cell_value(row, col), wb.datemode)).strftime(
                            '%Y-%m-%d'))
                except:
                    row_val.append(datetime.datetime.now().strftime('%Y-%m-%d'))


            else:
                row_val.append(sheet.cell_value(row, col))
        all_val.append(row_val)
    for onerow in all_val:
        for en in PATENT_STATE_CHOICE:
            if en.value == onerow[0]:
                mstate = en
                break
        if not mstate:
            mstate = PATENT_STATE_CHOICE.EMPTY
        obj, create = Application_Patent_Domestic_Master.objects.get_or_create(patentType=PATENT_TYPE_FOR_OURCOM.T.name,nationType=NATION_TYPE_FOR_OURCOM.D.name,state=mstate.name, ourCompanyNumber=onerow[3], brandName=onerow[5],
                                                                              productClassification=onerow[6],designatedProductKor=onerow[7], receptionDate=onerow[10], applicationRequestFixedDate=onerow[11],
                                                                              currentProgress=onerow[12], applicationNumber=onerow[13],applicationDate=onerow[14], remarks=onerow[15])

        # 당소 담당 정보가 없으면 만들어서 연결.
        if onerow[9]:
            nc, created = Name_Company.objects.get_or_create(korName=onerow[9], isCompany=False)
            applmanagement, created = Person.objects.get_or_create(name_company=nc)
            obj.managerOfOurCompany.add(applmanagement)
        # 출원인 담당 정보가 없으면 만들어서 연결.
        if onerow[8]:
            nc, created = Name_Company.objects.get_or_create(korName=onerow[8], isCompany=False)
            applmanagement, created = Person.objects.get_or_create(name_company=nc)
            obj.managerOfApplicant.add(applmanagement)

        # 출원인 연결
        if len(str(onerow[1])) > 2:
            ncobj = Application_Patent_Domestic_Applicant.objects.filter(korFullName=onerow[1]).last()
            if not ncobj:
                query = {}
                if len(str(onerow[1])) == 3 or len(str(onerow[1])) == 4:
                    query = {'isCompany':False}
                else:
                    query = {'isCompany': True}
                ncobj, created = Application_Patent_Domestic_Applicant.objects.get_or_create(korFullName=onerow[1],**query)
            if ncobj:
                obj.applicants.add(ncobj)
"""

"""
def Insert_oversea_brand(wb):
    sheet = wb.sheet_by_index(9)
    num_cols = sheet.ncols  # Number of columns
    all_val = []
    cols = sheet.ncols
    rows = sheet.nrows
    for row in range(rows):
        row_val = []
        if row == 0:
            continue
        for col in range(cols):
            if col == 15 or col == 17 or col == 21:
                try:
                    row_val.append(
                        datetime.datetime(*xlrd.xldate_as_tuple(sheet.cell_value(row, col), wb.datemode)).strftime(
                            '%Y-%m-%d'))
                except:
                    row_val.append(datetime.datetime.now().strftime('%Y-%m-%d'))


            else:
                row_val.append(sheet.cell_value(row, col))
        all_val.append(row_val)
    mstate = ''
    for onerow in all_val:
        for en in PATENT_STATE_CHOICE:
            if en.value == onerow[0]:
                mstate = en
                break
        if not mstate:
            mstate = PATENT_STATE_CHOICE.EMPTY
        obj, create = Application_Patent_Domestic_Master.objects.get_or_create(patentType=PATENT_TYPE_FOR_OURCOM.T.name,nationType=NATION_TYPE_FOR_OURCOM.O.name,state=mstate.name,applicationCountryCode=onerow[1], ourCompanyNumber=onerow[2], brandName=onerow[4],
                                                                              productClassification=onerow[5],designatedProductOrigin=onerow[6],designatedProductKor=onerow[7], receptionDate=onerow[15],
                                                                              priorityDate=onerow[17], currentProgress=onerow[19], applicationNumber=onerow[20], applicationDate=onerow[21], remarks=onerow[22])

        # 당소 담당 정보가 없으면 만들어서 연결.
        if onerow[14]:
            nc, created = Name_Company.objects.get_or_create(korName=onerow[14], isCompany=False)
            applmanagement, created = Person.objects.get_or_create(name_company=nc)
            obj.managerOfOurCompany.add(applmanagement)

        # 현지 대리인 담당 연결
        if len(onerow[13]) > 1:
            lang = detect(onerow[13])
            if lang == "en":
                oagent, created = Name_Company.objects.get_or_create(engFullName=onerow[13],isCompany=False)
                pobj, created = Person.objects.get_or_create(name_company=oagent)
            elif lang == "kr":
                oagent, created = Name_Company.objects.get_or_create(korName=onerow[13],isCompany=False)
                pobj, created = Person.objects.get_or_create(name_company=oagent)
        pobj = ''
        # 현지 대리인 연결
        if len(onerow[11]) > 1:
            oagent, created = Name_Company.objects.get_or_create(engCorporationName=onerow[11])
            client, created = Client.objects.get_or_create(name_company=oagent)
            throughmodel = obj.oversea_agent.through()
            throughmodel.oversea_agent = client
            throughmodel.master = obj
            if pobj:
                throughmodel.oversea_management = pobj
            if len(onerow[10]) > 1:
                throughmodel.agentCode = onerow[10]
            throughmodel.save()

        # 출원인 연결
        if len(str(onerow[9])) > 2:
            query = {}
            if len(onerow[9]) == 3 or len(onerow[9]) == 4:
                query = {'isCompany':False}
            else:
                query = {'isCompany':True}
            ncobj, created = Application_Patent_Domestic_Applicant.objects.get_or_create(korFullName=onerow[9], originFullName=onerow[8], **query)

            if ncobj:
                obj.applicants.add(ncobj)
"""