import ast
import os
import shutil
import sys
import zipfile
from pathlib import Path

from bs4 import BeautifulSoup
from django.core.files.storage import default_storage
from django.http import JsonResponse, HttpResponse
from django.test import TestCase
import json
# Create your tests here.
from People.models import Client, Person
from application_patent_domestic.models import Application_Patent_Domestic_Master
from emailsystem.models import Email_Detail
from message.models import File
from django.core.files.base import ContentFile

from message.syncpatentdocu import parsing_patent_submit_docu
from utils.dir_mgr import make_abstractFig_attachment_path


def managementtest(request):
    """
    with open('upload_file/image-5.png', 'rb') as f:
        contents = f.read()
        path = default_storage.save('upload_file/image.png', ContentFile(contents))
    """
    with open('upload_file/MP_212020007175894_220200000024437.xml', "rt", encoding='UTF8') as file:
        # Read each line in the file, readlines() returns a list of lines
        content = file.readlines()
        # Combine the lines in the list into a string
        content = "".join(content)
        soup = BeautifulSoup(content, "lxml")
        for kp in soup.find_all('KR_PatentRegisterNumber'.lower()):
            print(kp.text)



    return HttpResponse('')

def managementtest_imagedata(request):
    numberforsearch = request.GET.get('cid')

    file = File.objects.get(cId=numberforsearch.replace('cid:',''))
    image_data = open(file.path.path, "rb").read()
    return HttpResponse(image_data, content_type="image/jpg")
