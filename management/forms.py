from django.forms import ModelForm
from django import forms

from application_patent_domestic.forms import ModelFormBase, BasicFormBase
from case.models import Task_Template
from management.models import DB_Modifying_Field_Name, Tag_List, Code_Information, CountryInformation
from utils.enums import ALL_CODE_TYPE


class ChangeFieldForm(ModelForm):
    class Meta:
        model = DB_Modifying_Field_Name
        fields = ['customFieldName', ]


class TagModelForm(ModelFormBase):
    def __init__(self, *args, **kwargs):
        super(TagModelForm, self).__init__(*args, **kwargs)
        if self.labeldic:
            for i, (k, v) in enumerate(self.labeldic.items()):
                self.fields[k].label = v
        if self.customcodechoice:
            self.fields['code'].choices = self.customcodechoice[ALL_CODE_TYPE.Management_TagListType.name]

    class Meta:
        model = Tag_List
        fields = '__all__'





