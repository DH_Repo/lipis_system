from django.urls import path

import case
import emailsystem

from emailsystem.tests import syncemail
from management import InitDB, sync_excelfile, tests, crud, ajaxs
from management import views



app_name = 'management'
urlpatterns = [
    path('customdbfilename', views.CustomDBFieldNameView.as_view(), name='custom_dbfieldname'),
    path('edit_people_fieldname', views.Edit_People_FieldView.as_view(), name='edit_people_fieldname'),
    path('test', emailsystem.tests.sendemail, name='test'),
    path('dbfieldname/init', InitDB.InitDbCustomFieldName, name='initdb_customfieldName'),
    path('dbfieldname/save', views.SaveDbCustomFieldName, name='savedb_customfieldName'),
    path('commonmgr', views.CommonMgrView.as_view(), name='common_mgr'),
    path('countrycode/save', InitDB.InitDBCountryCode, name='initdb_countrycode'),
    path('codeinfo/multiplelanguage/save', InitDB.InitDBCodeInfoMultipleLanguage, name='initdb_codeinfoformultiplelanguage'),
    path('excelupload', views.ExcelUploadView.as_view(), name='excelupload'),
    path('exceluploading', views.ExcelFileUpload, name='exceluploading'),
    path('syncexcel', sync_excelfile.sync_excel_files, name='syncexcel'),
    path('syncexcelother', sync_excelfile.sync_excel_files_other, name='syncexcel_other'),
    path('managementtest', tests.managementtest, name='managementtest'),

    path('codeinfo', views.CodeInformationView.as_view(), name='codeinfo'),
    path('codeinfo/create', views.CreateCode, name='create_codeinfo'),
    path('codeinfo/delete', views.DeleteCode, name='delete_codeinfo'),

    path('mulitplelangcode/update', views.UpdateCodeMultipleLang, name='update_multiplelanguagecode'),

    path('countrycode', views.CountryCodeInformationView.as_view(), name='countrycodeinfo'),
    path('mulitplelangcountrycode/update', views.UpdateLanguageCodeMultipleLang, name='update_multiplelanguagecountrycode'),
    path('tags', views.TagsView.as_view(), name='tags'),
    path('tags/parent/update', ajaxs.UpdateTagParent, name='tags_parent_update'),
    path('tasktemplate', views.TasksTemplateView.as_view(), name='task_template'),
    path('tasktemplateIframe', views.TaskTemplateIframeView.as_view(), name='task_template_iframe'),

    path('tasktemplate_ref/create', ajaxs.CreateTaskTemplateRef, name='create_task_template_ref'),

    path('tasktemplate/child/ajax/update', ajaxs.UpdateTemplateChild, name='update_child_task_template'),
    path('tasktemplate/state/ajax/update', ajaxs.UpdateTaskTemplateState, name='update_task_template_state'),
    path('tasktemplate/requestcasemessage/ajax/update', ajaxs.UpdateTaskTemplate_TaskRequestCaseMessage, name='update_task_template_requestcasemessage'),
    path('tasktemplate/requestreceiptmessage/ajax/update', ajaxs.UpdateTaskTemplate_TaskReceiptCaseMessage, name='update_task_template_receiptcasemessage'),
    path('tasktemplate/tag/ajax/update', ajaxs.UpdateTaskTemplate_Tag, name='update_task_template_tag'),


    path('tasktemplate/ajax/delete', ajaxs.DeleteTemplate, name='delete_task_template'),
    path('tasktemplate/ajax/create', ajaxs.CreateTaskTemplate, name='create_task_template'),
    path('tasktemplate_ref/messagetype/ajax/update', ajaxs.UpdateTemplateCaseMessageType, name='update_messagetype_tasktemplateref'),
    path('tasktemplate_ref/casestate/ajax/update', ajaxs.UpdateTemplateCaseState, name='update_casestate_tasktemplateref'),
    path('tasktemplate_ref/casestatechangecondition/ajax/update', ajaxs.UpdateTemplateCaseStateChangeCondition, name='update_casestatechangecondition_tasktemplateref'),
    path('tasktemplate_ref/ajax/get', ajaxs.GetTemplateRef, name='gettemplateref'),









]

