from django.urls import path

from People import views, crudmodel, account, ajax

app_name = 'People'
urlpatterns = [

    path('client', views.ClientView.as_view(), name='client'),
    path('client/create', crudmodel.ClientCreate, name='client_create'),
    path('client/edit/<pk>', crudmodel.ClientEdit, name='client_edit'),
    path('client/delete', crudmodel.ClientDelete, name='client_delete'),

    path('applicantinformation', views.ApplicantInformationView.as_view(), name='applicantinformation'),
    path('applicantinformation/create', crudmodel.ApplicantInformationCreate, name='create_applicantinformation'),
    path('applicantinformation/edit/<pk>', crudmodel.ApplicantInformationEdit, name='edit_applicantinformation'),
    path('applicantinformation/delete', crudmodel.ApplicantInformationDelete, name='delete_applicantinformation'),

    path('person/<int:pk>', views.PersonDetailView.as_view(), name='person_detail'),
    path('person', views.PersonView.as_view(), name='person'),
    path('person/workemail/search', crudmodel.SearchEmail, name='person_workemail_search'),
    path('person/workemail/link', ajax.person_email_link, name='person_workemail_link'),


    path('person/create', crudmodel.PersonCreate, name='person_create'),
    path('person/edit/<pk>', crudmodel.PersonEdit, name='person_edit'),
    path('person/delete', crudmodel.PersonDelete, name='person_delete'),

    path('emailcontact', views.EmailContactView.as_view(), name='emailcontact'),
    path('emailcontact/create', crudmodel.WorkEmailCreate, name='emailcontact_create'),
    path('emailcontact/edit/<pk>', crudmodel.WorkEmailEdit, name='emailcontact_edit'),
    path('emailcontact/delete', crudmodel.WorkEmailDelete, name='emailcontact_delete'),

    path('workemail/save/person/link', crudmodel.save_workemail_person_link, name='workemail_save_peson_link'),
    path('workemail/person/unlink', ajax.workemail_person_unlink, name='workemail_peson_unlink'),

    path('login', views.LoginView.as_view(), name='login'),
    path('logout', views.Logout, name='logout'),



]