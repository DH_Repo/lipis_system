import ast

from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt

from People.forms import ClientModelForm, PersonModelForm, WorkEmailModelForm, ApplicantInformationModelForm, \
    CLIENT_TYPE
from People.models import Client, Person, Work_Email, ApplicantInformation, ClientType
from common.modelcodemultilanguage_choice import GetModelCodeMultpleLanguageForClient
from utils.db_fieldset import get_fielddic

@login_required(login_url='People:login')
def ClientCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Client')
    if request.method == 'POST':
        client_type = request.POST.get('client_type')
        form = ClientModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            if client_type == 'Client':
                client_t = ClientType.objects.get(typeName=CLIENT_TYPE.Client.name)
                myform.client_clienttype.add(client_t)
            elif client_type == 'OverseaAgent':
                client_t = ClientType.objects.get(typeName=CLIENT_TYPE.OverseaAgent.name)
                myform.client_clienttype.add(client_t)
            return HttpResponse('<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        client_type = request.GET.get('client_type')
        customcodechoice = GetModelCodeMultpleLanguageForClient(request)
        form = ClientModelForm(labeldic=labeldic, request=request, customcodechoice=customcodechoice)


    return render(request, 'People/client_form.html', {'form': form, 'client_type':client_type})

@login_required(login_url='People:login')
def ClientEdit(request, pk):
    clients = get_object_or_404(Client, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Client')
    if request.method == 'POST':

        form = ClientModelForm(request.POST, request.FILES, instance=clients, crud='u', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()

            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:

        customcodechoice = GetModelCodeMultpleLanguageForClient(request)
        form = ClientModelForm(instance=clients, labeldic=labeldic, request=request, customcodechoice=customcodechoice)
        form.fields['manager'].queryset = clients.member.all()
        form.fields['accounting_manager'].queryset = clients.member.all()
    return render(request, 'People/client_form.html', {
        'form': form
    })

@login_required(login_url='People:login')
@csrf_exempt
def ClientDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Client.objects.get(id=pk).delete()
        return render(request, 'People/client.html', {})

@login_required(login_url='People:login')
def PersonCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Person')
    if request.method == 'POST':
        form = PersonModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            email = request.POST.getlist('email', None)
            myform.save()
            if email:
                for em in email:
                    #Work_Email.objects.filter(person_id=myform.id).update(person_id=None)
                    Work_Email.objects.filter(id=em).update(person=myform)

            form.save_m2m()
            form.save_create()
            return HttpResponse('<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = PersonModelForm(labeldic=labeldic, request=request)

    return render(request, 'People/person_form.html', {'form': form})

@login_required(login_url='People:login')
def PersonEdit(request, pk):
    clients = get_object_or_404(Person, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Person')

    if request.method == 'POST':
        form = PersonModelForm(request.POST, request.FILES, instance=clients, crud='u', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            email = request.POST.getlist('email', None)
            if email:
                for em in email:
                    Work_Email.objects.filter(person_id=myform.id).update(person_id=None)
                    Work_Email.objects.filter(id=em).update(person=myform)
            myform.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = PersonModelForm(instance=clients, labeldic=labeldic, request=request,  initial={'email': Work_Email.objects.filter(person_id=clients.id)})
    return render(request, 'People/person_form.html', {
        'form': form,
    })

@login_required(login_url='People:login')
@csrf_exempt
def PersonDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Person.objects.get(id=pk).delete()
        return render(request, 'People/person.html', {})


@login_required(login_url='People:login')
def WorkEmailCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Work_Email')
    if request.method == 'POST':
        form = WorkEmailModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse('<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = WorkEmailModelForm(labeldic=labeldic, request=request)

    return render(request, 'People/emailcontact_form.html', {'form': form})

@login_required(login_url='People:login')
def WorkEmailEdit(request, pk):
    clients = get_object_or_404(Work_Email, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Work_Email')
    if request.method == 'POST':
        form = WorkEmailModelForm(request.POST, request.FILES, instance=clients, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit = False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = WorkEmailModelForm(instance=clients, labeldic=labeldic, request=request)
    return render(request, 'People/emailcontact_form.html', {
        'form': form,
    })

@login_required(login_url='People:login')
@csrf_exempt
def WorkEmailDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Work_Email.objects.get(id=pk).delete()
        return render(request, 'People/emailcontact.html', {})


@login_required(login_url='People:login')
def ApplicantInformationCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'ApplicantInformation')
    if request.method == 'POST':
        form = ApplicantInformationModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse('<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = ApplicantInformationModelForm(labeldic=labeldic, request=request)

    return render(request, 'People/applicantinformation_form.html', {'form': form})

@login_required(login_url='People:login')
def ApplicantInformationEdit(request, pk):
    instance = get_object_or_404(ApplicantInformation, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'ApplicantInformation')
    if request.method == 'POST':
        form = ApplicantInformationModelForm(request.POST, request.FILES, instance=instance, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit = False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = ApplicantInformationModelForm(instance=instance, labeldic=labeldic, request=request)
    return render(request, 'People/applicantinformation_form.html', {
        'form': form,
    })

@login_required(login_url='People:login')
@csrf_exempt
def ApplicantInformationDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        ApplicantInformation.objects.get(id=pk).delete()
        return render(request, 'People/applicantinformations.html', {})




@login_required(login_url='People:login')
def SearchEmail(request):
    term = request.GET.get('term')
    obj = Work_Email.objects.filter(email__contains=term).values('email', 'emailName')
    serials = list(obj)
    newobj =[]
    for serial in serials:
        newobj.append(serial['email']+' / '+serial['emailName'])
    return JsonResponse(newobj, safe=False)

@login_required(login_url='People:login')
@csrf_exempt
@transaction.atomic
def save_workemail_person_link(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        email = request.POST.get('email')
        name = request.POST.get('name')
        Work_Email.objects.create(email=email, emailName=name, person_id=pk)
        return redirect('People:person')