from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.messages import error
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse

from People.forms import LoginForm
from People.views import LoginView


