# Generated by Django 3.1.1 on 2020-09-21 06:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('People', '0005_auto_20200909_1803'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='cnSimplifiedAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='cnSimplifiedCorporationName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='client',
            name='cnSimplifiedFirstName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='cnSimplifiedFullName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='cnSimplifiedLastName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='cnTraditionalAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='cnTraditionalCorporationName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='client',
            name='cnTraditionalFirstName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='cnTraditionalFullName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='cnTraditionalLastName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='countrycode',
            field=models.CharField(blank=True, choices=[('AF', 'AF'), ('AX', 'AX'), ('AL', 'AL'), ('DZ', 'DZ'), ('AS', 'AS'), ('AD', 'AD'), ('AO', 'AO'), ('AI', 'AI'), ('AQ', 'AQ'), ('AG', 'AG'), ('AR', 'AR'), ('AM', 'AM'), ('AW', 'AW'), ('AU', 'AU'), ('AT', 'AT'), ('AZ', 'AZ'), ('BS', 'BS'), ('BH', 'BH'), ('BD', 'BD'), ('BB', 'BB'), ('BY', 'BY'), ('BE', 'BE'), ('BZ', 'BZ'), ('BJ', 'BJ'), ('BM', 'BM'), ('BT', 'BT'), ('BO', 'BO'), ('BQ', 'BQ'), ('BA', 'BA'), ('BW', 'BW'), ('BV', 'BV'), ('BR', 'BR'), ('IO', 'IO'), ('BN', 'BN'), ('BG', 'BG'), ('BF', 'BF'), ('BI', 'BI'), ('KH', 'KH'), ('CM', 'CM'), ('CA', 'CA'), ('CV', 'CV'), ('KY', 'KY'), ('CF', 'CF'), ('TD', 'TD'), ('CL', 'CL'), ('CN', 'CN'), ('CX', 'CX'), ('CC', 'CC'), ('CO', 'CO'), ('KM', 'KM'), ('CG', 'CG'), ('CD', 'CD'), ('CK', 'CK'), ('CR', 'CR'), ('CI', 'CI'), ('HR', 'HR'), ('CU', 'CU'), ('CW', 'CW'), ('CY', 'CY'), ('CZ', 'CZ'), ('DK', 'DK'), ('DJ', 'DJ'), ('DM', 'DM'), ('DO', 'DO'), ('EC', 'EC'), ('EG', 'EG'), ('SV', 'SV'), ('GQ', 'GQ'), ('ER', 'ER'), ('EE', 'EE'), ('ET', 'ET'), ('FK', 'FK'), ('FO', 'FO'), ('FJ', 'FJ'), ('FI', 'FI'), ('FR', 'FR'), ('GF', 'GF'), ('PF', 'PF'), ('TF', 'TF'), ('GA', 'GA'), ('GM', 'GM'), ('GE', 'GE'), ('DE', 'DE'), ('GH', 'GH'), ('GI', 'GI'), ('GR', 'GR'), ('GL', 'GL'), ('GD', 'GD'), ('GP', 'GP'), ('GU', 'GU'), ('GT', 'GT'), ('GG', 'GG'), ('GN', 'GN'), ('GW', 'GW'), ('GY', 'GY'), ('HT', 'HT'), ('HM', 'HM'), ('VA', 'VA'), ('HN', 'HN'), ('HK', 'HK'), ('HU', 'HU'), ('IS', 'IS'), ('IN', 'IN'), ('ID', 'ID'), ('IR', 'IR'), ('IQ', 'IQ'), ('IE', 'IE'), ('IM', 'IM'), ('IL', 'IL'), ('IT', 'IT'), ('JM', 'JM'), ('JP', 'JP'), ('JE', 'JE'), ('JO', 'JO'), ('KZ', 'KZ'), ('KE', 'KE'), ('KI', 'KI'), ('KP', 'KP'), ('KR', 'KR'), ('KW', 'KW'), ('KG', 'KG'), ('LA', 'LA'), ('LV', 'LV'), ('LB', 'LB'), ('LS', 'LS'), ('LR', 'LR'), ('LY', 'LY'), ('LI', 'LI'), ('LT', 'LT'), ('LU', 'LU'), ('MO', 'MO'), ('MK', 'MK'), ('MG', 'MG'), ('MW', 'MW'), ('MY', 'MY'), ('MV', 'MV'), ('ML', 'ML'), ('MT', 'MT'), ('MH', 'MH'), ('MQ', 'MQ'), ('MR', 'MR'), ('MU', 'MU'), ('YT', 'YT'), ('MX', 'MX'), ('FM', 'FM'), ('MD', 'MD'), ('MC', 'MC'), ('MN', 'MN'), ('ME', 'ME'), ('MS', 'MS'), ('MA', 'MA'), ('MZ', 'MZ'), ('MM', 'MM'), ('NA', 'NA'), ('NR', 'NR'), ('NP', 'NP'), ('NL', 'NL'), ('NC', 'NC'), ('NZ', 'NZ'), ('NI', 'NI'), ('NE', 'NE'), ('NG', 'NG'), ('NU', 'NU'), ('NF', 'NF'), ('MP', 'MP'), ('NO', 'NO'), ('OM', 'OM'), ('PK', 'PK'), ('PW', 'PW'), ('PS', 'PS'), ('PA', 'PA'), ('PG', 'PG'), ('PY', 'PY'), ('PE', 'PE'), ('PH', 'PH'), ('PN', 'PN'), ('PL', 'PL'), ('PT', 'PT'), ('PR', 'PR'), ('QA', 'QA'), ('RE', 'RE'), ('RO', 'RO'), ('RU', 'RU'), ('RW', 'RW'), ('BL', 'BL'), ('SH', 'SH'), ('KN', 'KN'), ('LC', 'LC'), ('MF', 'MF'), ('PM', 'PM'), ('VC', 'VC'), ('WS', 'WS'), ('SM', 'SM'), ('ST', 'ST'), ('SA', 'SA'), ('SN', 'SN'), ('RS', 'RS'), ('SC', 'SC'), ('SL', 'SL'), ('SG', 'SG'), ('SX', 'SX'), ('SK', 'SK'), ('SI', 'SI'), ('SB', 'SB'), ('SO', 'SO'), ('ZA', 'ZA'), ('GS', 'GS'), ('SS', 'SS'), ('ES', 'ES'), ('LK', 'LK'), ('SD', 'SD'), ('SR', 'SR'), ('SJ', 'SJ'), ('SZ', 'SZ'), ('SE', 'SE'), ('CH', 'CH'), ('SY', 'SY'), ('TW', 'TW'), ('TJ', 'TJ'), ('TZ', 'TZ'), ('TH', 'TH'), ('TL', 'TL'), ('TG', 'TG'), ('TK', 'TK'), ('TO', 'TO'), ('TT', 'TT'), ('TN', 'TN'), ('TR', 'TR'), ('TM', 'TM'), ('TC', 'TC'), ('TV', 'TV'), ('UG', 'UG'), ('UA', 'UA'), ('AE', 'AE'), ('GB', 'GB'), ('US', 'US'), ('UM', 'UM'), ('UY', 'UY'), ('UZ', 'UZ'), ('VU', 'VU'), ('VE', 'VE'), ('VN', 'VN'), ('VG', 'VG'), ('VI', 'VI'), ('WF', 'WF'), ('EH', 'EH'), ('YE', 'YE'), ('ZM', 'ZM'), ('ZW', 'ZW'), ('OA', 'OA'), ('AP', 'AP'), ('BX', 'BX'), ('QZ', 'QZ'), ('EA', 'EA'), ('EP', 'EP'), ('IB', 'IB'), ('WO', 'WO'), ('XU', 'XU'), ('XN', 'XN'), ('EM', 'EM'), ('GC', 'GC'), ('XV', 'XV'), ('XX', 'XX')], max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='engAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='engCorporationName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='client',
            name='engFirstName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='engFullName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='engLastName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='isCompany',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='client',
            name='isCorporation',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='jpAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='jpCorporationName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='client',
            name='jpFirstName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='jpFullName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='jpLastName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='korAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='korCorporationName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='client',
            name='korName',
            field=models.CharField(blank=True, max_length=5, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='originAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='originCorporationName',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='originFirstName',
            field=models.CharField(blank=True, max_length=20),
        ),
        migrations.AddField(
            model_name='client',
            name='originFullName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='client',
            name='originLastName',
            field=models.CharField(blank=True, max_length=20),
        ),
        migrations.AddField(
            model_name='person',
            name='cnSimplifiedAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='cnSimplifiedCorporationName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='person',
            name='cnSimplifiedFirstName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='cnSimplifiedFullName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='cnSimplifiedLastName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='cnTraditionalAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='cnTraditionalCorporationName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='person',
            name='cnTraditionalFirstName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='cnTraditionalFullName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='cnTraditionalLastName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='countrycode',
            field=models.CharField(blank=True, choices=[('AF', 'AF'), ('AX', 'AX'), ('AL', 'AL'), ('DZ', 'DZ'), ('AS', 'AS'), ('AD', 'AD'), ('AO', 'AO'), ('AI', 'AI'), ('AQ', 'AQ'), ('AG', 'AG'), ('AR', 'AR'), ('AM', 'AM'), ('AW', 'AW'), ('AU', 'AU'), ('AT', 'AT'), ('AZ', 'AZ'), ('BS', 'BS'), ('BH', 'BH'), ('BD', 'BD'), ('BB', 'BB'), ('BY', 'BY'), ('BE', 'BE'), ('BZ', 'BZ'), ('BJ', 'BJ'), ('BM', 'BM'), ('BT', 'BT'), ('BO', 'BO'), ('BQ', 'BQ'), ('BA', 'BA'), ('BW', 'BW'), ('BV', 'BV'), ('BR', 'BR'), ('IO', 'IO'), ('BN', 'BN'), ('BG', 'BG'), ('BF', 'BF'), ('BI', 'BI'), ('KH', 'KH'), ('CM', 'CM'), ('CA', 'CA'), ('CV', 'CV'), ('KY', 'KY'), ('CF', 'CF'), ('TD', 'TD'), ('CL', 'CL'), ('CN', 'CN'), ('CX', 'CX'), ('CC', 'CC'), ('CO', 'CO'), ('KM', 'KM'), ('CG', 'CG'), ('CD', 'CD'), ('CK', 'CK'), ('CR', 'CR'), ('CI', 'CI'), ('HR', 'HR'), ('CU', 'CU'), ('CW', 'CW'), ('CY', 'CY'), ('CZ', 'CZ'), ('DK', 'DK'), ('DJ', 'DJ'), ('DM', 'DM'), ('DO', 'DO'), ('EC', 'EC'), ('EG', 'EG'), ('SV', 'SV'), ('GQ', 'GQ'), ('ER', 'ER'), ('EE', 'EE'), ('ET', 'ET'), ('FK', 'FK'), ('FO', 'FO'), ('FJ', 'FJ'), ('FI', 'FI'), ('FR', 'FR'), ('GF', 'GF'), ('PF', 'PF'), ('TF', 'TF'), ('GA', 'GA'), ('GM', 'GM'), ('GE', 'GE'), ('DE', 'DE'), ('GH', 'GH'), ('GI', 'GI'), ('GR', 'GR'), ('GL', 'GL'), ('GD', 'GD'), ('GP', 'GP'), ('GU', 'GU'), ('GT', 'GT'), ('GG', 'GG'), ('GN', 'GN'), ('GW', 'GW'), ('GY', 'GY'), ('HT', 'HT'), ('HM', 'HM'), ('VA', 'VA'), ('HN', 'HN'), ('HK', 'HK'), ('HU', 'HU'), ('IS', 'IS'), ('IN', 'IN'), ('ID', 'ID'), ('IR', 'IR'), ('IQ', 'IQ'), ('IE', 'IE'), ('IM', 'IM'), ('IL', 'IL'), ('IT', 'IT'), ('JM', 'JM'), ('JP', 'JP'), ('JE', 'JE'), ('JO', 'JO'), ('KZ', 'KZ'), ('KE', 'KE'), ('KI', 'KI'), ('KP', 'KP'), ('KR', 'KR'), ('KW', 'KW'), ('KG', 'KG'), ('LA', 'LA'), ('LV', 'LV'), ('LB', 'LB'), ('LS', 'LS'), ('LR', 'LR'), ('LY', 'LY'), ('LI', 'LI'), ('LT', 'LT'), ('LU', 'LU'), ('MO', 'MO'), ('MK', 'MK'), ('MG', 'MG'), ('MW', 'MW'), ('MY', 'MY'), ('MV', 'MV'), ('ML', 'ML'), ('MT', 'MT'), ('MH', 'MH'), ('MQ', 'MQ'), ('MR', 'MR'), ('MU', 'MU'), ('YT', 'YT'), ('MX', 'MX'), ('FM', 'FM'), ('MD', 'MD'), ('MC', 'MC'), ('MN', 'MN'), ('ME', 'ME'), ('MS', 'MS'), ('MA', 'MA'), ('MZ', 'MZ'), ('MM', 'MM'), ('NA', 'NA'), ('NR', 'NR'), ('NP', 'NP'), ('NL', 'NL'), ('NC', 'NC'), ('NZ', 'NZ'), ('NI', 'NI'), ('NE', 'NE'), ('NG', 'NG'), ('NU', 'NU'), ('NF', 'NF'), ('MP', 'MP'), ('NO', 'NO'), ('OM', 'OM'), ('PK', 'PK'), ('PW', 'PW'), ('PS', 'PS'), ('PA', 'PA'), ('PG', 'PG'), ('PY', 'PY'), ('PE', 'PE'), ('PH', 'PH'), ('PN', 'PN'), ('PL', 'PL'), ('PT', 'PT'), ('PR', 'PR'), ('QA', 'QA'), ('RE', 'RE'), ('RO', 'RO'), ('RU', 'RU'), ('RW', 'RW'), ('BL', 'BL'), ('SH', 'SH'), ('KN', 'KN'), ('LC', 'LC'), ('MF', 'MF'), ('PM', 'PM'), ('VC', 'VC'), ('WS', 'WS'), ('SM', 'SM'), ('ST', 'ST'), ('SA', 'SA'), ('SN', 'SN'), ('RS', 'RS'), ('SC', 'SC'), ('SL', 'SL'), ('SG', 'SG'), ('SX', 'SX'), ('SK', 'SK'), ('SI', 'SI'), ('SB', 'SB'), ('SO', 'SO'), ('ZA', 'ZA'), ('GS', 'GS'), ('SS', 'SS'), ('ES', 'ES'), ('LK', 'LK'), ('SD', 'SD'), ('SR', 'SR'), ('SJ', 'SJ'), ('SZ', 'SZ'), ('SE', 'SE'), ('CH', 'CH'), ('SY', 'SY'), ('TW', 'TW'), ('TJ', 'TJ'), ('TZ', 'TZ'), ('TH', 'TH'), ('TL', 'TL'), ('TG', 'TG'), ('TK', 'TK'), ('TO', 'TO'), ('TT', 'TT'), ('TN', 'TN'), ('TR', 'TR'), ('TM', 'TM'), ('TC', 'TC'), ('TV', 'TV'), ('UG', 'UG'), ('UA', 'UA'), ('AE', 'AE'), ('GB', 'GB'), ('US', 'US'), ('UM', 'UM'), ('UY', 'UY'), ('UZ', 'UZ'), ('VU', 'VU'), ('VE', 'VE'), ('VN', 'VN'), ('VG', 'VG'), ('VI', 'VI'), ('WF', 'WF'), ('EH', 'EH'), ('YE', 'YE'), ('ZM', 'ZM'), ('ZW', 'ZW'), ('OA', 'OA'), ('AP', 'AP'), ('BX', 'BX'), ('QZ', 'QZ'), ('EA', 'EA'), ('EP', 'EP'), ('IB', 'IB'), ('WO', 'WO'), ('XU', 'XU'), ('XN', 'XN'), ('EM', 'EM'), ('GC', 'GC'), ('XV', 'XV'), ('XX', 'XX')], max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='engAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='engCorporationName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='person',
            name='engFirstName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='engFullName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='engLastName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='isCompany',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='person',
            name='isCorporation',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='jpAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='jpCorporationName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='person',
            name='jpFirstName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='jpFullName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='jpLastName',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='korAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='korCorporationName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='person',
            name='korName',
            field=models.CharField(blank=True, max_length=5, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='originAddress',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='originCorporationName',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='originFirstName',
            field=models.CharField(blank=True, max_length=20),
        ),
        migrations.AddField(
            model_name='person',
            name='originFullName',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='person',
            name='originLastName',
            field=models.CharField(blank=True, max_length=20),
        ),
    ]
