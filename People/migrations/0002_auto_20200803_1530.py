# Generated by Django 3.0.6 on 2020-08-03 06:30

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('case', '0001_initial'),
        ('management', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('emailsystem', '0001_initial'),
        ('People', '0001_initial'),
        ('application_patent_domestic', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='work_email',
            name='tag',
            field=models.ManyToManyField(blank=True, through='management.Linked_Tags', to='management.Tag_List'),
        ),
        migrations.AddField(
            model_name='person',
            name='management_oversea_agent',
            field=models.ManyToManyField(blank=True, related_name='person_management_oversea_agent', through='application_patent_domestic.Oversea_Agent_Management', to='application_patent_domestic.Application_Patent_Domestic_Master_OverseaAgent'),
        ),
        migrations.AddField(
            model_name='person',
            name='name_company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='person_name_company', to='People.Name_Company'),
        ),
        migrations.AddField(
            model_name='person',
            name='tag',
            field=models.ManyToManyField(blank=True, through='management.Linked_Tags', to='management.Tag_List'),
        ),
        migrations.AddField(
            model_name='person',
            name='user',
            field=models.OneToOneField(blank=True, default='', null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='name_company',
            name='tag',
            field=models.ManyToManyField(blank=True, through='management.Linked_Tags', to='management.Tag_List'),
        ),
        migrations.AddField(
            model_name='client_manager',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='clients_manager', to='People.Client'),
        ),
        migrations.AddField(
            model_name='client_manager',
            name='manager',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='clients_manager', to='People.Person'),
        ),
        migrations.AddField(
            model_name='client_clienttype',
            name='client',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='client_clienttype_client', to='People.Client'),
        ),
        migrations.AddField(
            model_name='client_clienttype',
            name='client_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='client_clienttype_clienttype', to='People.ClientType'),
        ),
        migrations.AddField(
            model_name='client_applicantinformation',
            name='applicant',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='client_applicantinformation_applicantinformation', to='People.ApplicantInformation'),
        ),
        migrations.AddField(
            model_name='client_applicantinformation',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='client_applicantinformation_client', to='People.Client'),
        ),
        migrations.AddField(
            model_name='client_accountingmanager',
            name='accounting_manager',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='clients_accountingmanager', to='People.Person'),
        ),
        migrations.AddField(
            model_name='client_accountingmanager',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='clients_accountingmanager', to='People.Client'),
        ),
        migrations.AddField(
            model_name='client',
            name='accounting_manager',
            field=models.ManyToManyField(blank=True, related_name='client_accountingmanager', through='People.Client_AccountingManager', to='People.Person'),
        ),
        migrations.AddField(
            model_name='client',
            name='case',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='client_case', to='case.Case'),
        ),
        migrations.AddField(
            model_name='client',
            name='client_clienttype',
            field=models.ManyToManyField(blank=True, related_name='client_clienttype', through='People.Client_ClientType', to='People.ClientType'),
        ),
        migrations.AddField(
            model_name='client',
            name='emaildetail',
            field=models.ManyToManyField(blank=True, related_name='client_emaildetail', through='emailsystem.EmailDetail_Client', to='emailsystem.Email_Detail'),
        ),
        migrations.AddField(
            model_name='client',
            name='manager',
            field=models.ManyToManyField(blank=True, related_name='client_manager', through='People.Client_Manager', to='People.Person'),
        ),
        migrations.AddField(
            model_name='client',
            name='name_company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='client_name_company', to='People.Name_Company'),
        ),
        migrations.AddField(
            model_name='client',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='People.Client'),
        ),
        migrations.AddField(
            model_name='client',
            name='personal_client',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='client_personal', to='People.Person'),
        ),
        migrations.AddField(
            model_name='client',
            name='tag',
            field=models.ManyToManyField(blank=True, through='management.Linked_Tags', to='management.Tag_List'),
        ),
        migrations.AddField(
            model_name='applicantinformation',
            name='past_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='People.ApplicantInformation'),
        ),
        migrations.AddField(
            model_name='applicantinformation',
            name='tag',
            field=models.ManyToManyField(blank=True, through='management.Linked_Tags', to='management.Tag_List'),
        ),
        migrations.AlterUniqueTogether(
            name='work_email',
            unique_together={('email', 'emailName')},
        ),
    ]
