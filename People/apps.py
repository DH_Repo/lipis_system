from django.apps import AppConfig


class PeoplesConfig(AppConfig):
    name = 'People'
    verbose_name = 'People'
