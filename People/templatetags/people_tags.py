import datetime
import operator
from functools import reduce

from django import template
from django.db.models import Q

from People.models import Work_Email
from application_patent_domestic.models import Application_Patent_Domestic_Master
from common.codemultilanguage_choice import GetCodeMultpleLanguageForCase
from common.tableview_function import get_model_query_context_for_table_nopage_nosearch
from message.models import Message
from utils.enums import ALL_CODE_TYPE

register = template.Library()




@register.simple_tag
def get_count(patentall, ptype, ntype):
    count = 0
    for patent in patentall:
        if ntype:
            if patent.patentType == ptype and patent.nationType == ntype:
                count += 1
        else:
            if patent.patentType == ptype:
                count += 1
    return count


