from django.contrib import auth
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import redirect, render

# Create your views here.
from django.views.generic import TemplateView, FormView
from django.core.paginator import Paginator

from Lipis_system_master.views import HomeMixin
from People.forms import LoginForm
from People.models import *
from common.codemultilanguage_choice import GetCodeMultpleLanguageForClient
from common.tableview_function import get_all_model_context_for_table, \
    get_model_query_context_for_table_nopage_nosearch, get_field_multilang, \
    get_model_query_context_for_table_page_nosearch
from emailsystem.models import Email_Detail
from message.models import Message
from utils.enums import ALL_CODE_TYPE
from utils.search_function import searchpara_save, client_column_filter, Search_Parameter
from utils.db_fieldset import get_feild_names
from django.contrib.auth import logout

class ClientView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'People/client.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):

        context = super(ClientView, self).get_context_data(**kwargs)

        search = self.request.GET.get('Search')
        client_type = self.request.GET.get('ClientType')
        search_column = self.request.GET.get('SearchColumn')
        page = self.request.GET.get('page')
        language = self.request.session['django_language']
        col = get_feild_names(language, "Client", Client)

        if search:
            searchpara_save('Client', search_column, search)
            client_list = client_column_filter(search_column, search)
            client_list = client_list.filter(client_clienttype__typeName=client_type)
            paginator = Paginator(client_list, 10)
            context['client_list'] = [col, paginator.get_page(page)]
            context['searchcolumn'] = search_column
            context['searchtxt'] = search
        else:
            searchpara = Search_Parameter.objects.get(tableType='Client')

            if searchpara.searchname and page:
                client_list = client_column_filter(searchpara.colname, searchpara.searchname)
                client_list = client_list.filter(client_clienttype__typeName=client_type)
                context['searchcolumn'] = searchpara.colname
                context['searchtxt'] = searchpara.searchname
            else:
                client_list = Client.objects.all()
                client_list = client_list.filter(client_clienttype__typeName=client_type)
                context['searchcolumn'] = 'empty'
                context['searchtxt'] = ''
                searchpara_save('Master', '', '')

            paginator = Paginator(client_list, 10)
            context['model_list'] = [col, paginator.get_page(page)]
        context['current_client_type'] = client_type
        context['clienttype_code_multilanguage'] = GetCodeMultpleLanguageForClient(self.request)[ALL_CODE_TYPE.Client_Type.name]



        # And so on for more models
        return context
    
class PersonView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'People/person.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):

        context = super(PersonView, self).get_context_data(**kwargs)

        search = self.request.GET.get('Search')
        search_column = self.request.GET.get('SearchColumn')
        page = self.request.GET.get('page')
        language = self.request.session['django_language']
        col = get_feild_names(language, "Person", Person)

        if search:
            searchpara_save('Person', search_column, search)
            person_list = client_column_filter(search_column, search)
            paginator = Paginator(person_list, 10)
            context['person_list'] = [col, paginator.get_page(page)]
            context['searchcolumn'] = search_column
            context['searchtxt'] = search
        else:
            searchpara = Search_Parameter.objects.get(tableType='Person')

            if searchpara.searchname and page:
                person_list = client_column_filter(searchpara.colname, searchpara.searchname)
                context['searchcolumn'] = searchpara.colname
                context['searchtxt'] = searchpara.searchname
            else:
                person_list = Person.objects.all()
                context['searchcolumn'] = 'empty'
                context['searchtxt'] = ''
                searchpara_save('Master', '', '')

            paginator = Paginator(person_list, 10)
            model = paginator.get_page(page)
            context['model_list'] = [col, model]



        # And so on for more models
        return context

class PersonDetailView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'People/person_detail.html'
    login_url = 'People:login'


    def get_context_data(self, **kwargs):
        return super(PersonDetailView, self).get_context_data(**kwargs)


    def get(self, request, pk, *args, **kwargs):
        context = self.get_context_data()
        persons = Person.objects.get(id=pk)

        get_model_query_context_for_table_nopage_nosearch(self, Person, 'Person', context, Person.objects.filter(id=pk))
        workemail = persons.workemail_person.all()
        msgs = Message.objects.filter(Q(email__fromEmail__in=workemail) | Q(email__to_person__in=workemail) |
                               Q(email__cc_person__in=workemail)).order_by('-email__sentDate')
        get_model_query_context_for_table_page_nosearch(self, Message, 'Message', context, msgs, 'message_list')
        context['email_multilanguage'] = get_field_multilang(self, Email_Detail, 'Email_Detail')
        return self.render_to_response(context)

class EmailContactView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'People/emailcontact.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):

        context = super(EmailContactView, self).get_context_data(**kwargs)

        return get_all_model_context_for_table(self, Work_Email, 'Work_Email', 'Work_Email', context)

class ApplicantInformationView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'People/applicantinformations.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):

        context = super(ApplicantInformationView, self).get_context_data(**kwargs)

        return get_all_model_context_for_table(self, ApplicantInformation, 'ApplicantInformation', 'ApplicantInformation', context)

class LoginView(HomeMixin, FormView):
    template_name = 'People/login.html'
    form_class = LoginForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #context['form'] = LoginForm()
        #context['errors'] = self.request.session.get('errors', '')
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def form_valid(self, form):
        email = form.cleaned_data['emailId']
        password = form.cleaned_data['password']
        try:
            getuser = User.objects.get(email=email)
        except User.DoesNotExist:
            getuser = None
        if getuser is None:
            context = self.get_context_data()
            context['errors'] = 'wrong ID or Password.'
            return self.render_to_response(context)

        user = auth.authenticate(self.request, username=getuser.username, password=password)

        if user is not None:
            auth.login(self.request, user)
            return redirect('home')
        else:
            context = self.get_context_data()
            context['errors'] = 'wrong ID or Password.'
        return self.render_to_response(context)

def Logout(request):

    logout(request)

    return render(request, 'People/login.html')
    """
    def post(self, request, *args, **kwargs):
        
        form = LoginForm(request.POST)

        if form.is_valid():
            emailsystem = request.POST['emailId']
            password = request.POST['password']
            try:
                getuser = User.objects.get(emailsystem=emailsystem)
            except User.DoesNotExist:
                getuser = None
            if getuser is None:
                context = self.get_context_data(**kwargs)
                context['errors'] = 'wrong ID or Password.'
                return self.render_to_response(context)

            user = auth.authenticate(request, username=getuser.username, password=password)

            if user is not None:
                auth.login(request, user)
                return redirect('home')
            else:
                context = self.get_context_data(**kwargs)
                context['errors'] = 'wrong ID or Password.'
            return self.render_to_response(context)
        """