from django.contrib import admin

# Register your models here.
from django.urls import reverse
from django.utils.html import format_html

from People.models import *

"""
class client_person(admin.TabularInline):
    model = Client_Manager
    extra = 2

class ClientAdmin(admin.ModelAdmin):
    list_display = ['isCorporation', 'engCorporationName', 'creationDate', 'child', 'isClientCompany', 'person_link']
    inlines = [client_person]

    def person_link(self, obj):

        clientObj = Client.objects.get(id=obj.id)

        personlist = list(clientObj.person_Id.all())
        str = ''
        for li in personlist:
            url = reverse('admin:%s_%s_change' % (obj._meta.app_label,  'person'),  args=[li.id])
            str += '<a href="%s">%s</a> ' % (url, li.name)
        return format_html(str)

    person_link.allow_tags = True
"""
class PersonAdmin(admin.ModelAdmin):
    list_display = ['name', 'cid_link']

    def cid_link(self, obj):
        #personObj = Person.objects.get(name =obj.name)
        return format_html('<a href="%s">%s</a>' % (obj.name, obj.name))
    cid_link.allow_tags = True



#admin.site.register(Client, ClientAdmin)
#admin.site.register(Person, PersonAdmin)
admin.site.register(Client)
admin.site.register(Person)
admin.site.register(Work_Email)
admin.site.register(ClientType)