from django.db import models

# Create your models here.
from phone_field import PhoneField

from management.models import BaseModel
from utils.enums import CLIENT_TYPE, MakeWIPO_ST3_Enum


class Name_Information(models.Model):
    countrycode = models.CharField(max_length=20, null=True, blank=True,
                                   choices=[(tag.name, tag.value) for tag in MakeWIPO_ST3_Enum()])
    isCorporation = models.BooleanField(null=True, blank=True)
    isCompany = models.BooleanField(null=False, default=True, blank=False)
    originLastName = models.CharField(max_length=20, blank=True)
    originFirstName = models.CharField(max_length=20, blank=True)
    originFullName = models.CharField(max_length=50, blank=True)
    originAddress = models.CharField(max_length=100, blank=True, null=True)
    originCorporationName = models.CharField(max_length=50, blank=True, null=True)
    engLastName = models.CharField(max_length=20, blank=True, null=True)
    engFirstName = models.CharField(max_length=20, blank=True, null=True)
    engFullName = models.CharField(max_length=20, blank=True, null=True)
    engAddress = models.CharField(max_length=100, blank=True, null=True)
    engCorporationName = models.CharField(max_length=50, blank=True)
    korName = models.CharField(max_length=5, blank=True, null=True)
    korAddress = models.CharField(max_length=100, blank=True, null=True)
    korCorporationName = models.CharField(max_length=50, blank=True)
    jpLastName = models.CharField(max_length=20, blank=True, null=True)
    jpFirstName = models.CharField(max_length=20, blank=True, null=True)
    jpFullName = models.CharField(max_length=20, blank=True, null=True)
    jpAddress = models.CharField(max_length=100, blank=True, null=True)
    jpCorporationName = models.CharField(max_length=50, blank=True)
    cnTraditionalLastName = models.CharField(max_length=20, blank=True, null=True)
    cnTraditionalFirstName = models.CharField(max_length=20, blank=True, null=True)
    cnTraditionalFullName = models.CharField(max_length=20, blank=True, null=True)
    cnTraditionalAddress = models.CharField(max_length=100, blank=True, null=True)
    cnTraditionalCorporationName = models.CharField(max_length=50, blank=True)
    cnSimplifiedLastName = models.CharField(max_length=20, blank=True, null=True)
    cnSimplifiedFirstName = models.CharField(max_length=20, blank=True, null=True)
    cnSimplifiedFullName = models.CharField(max_length=20, blank=True, null=True)
    cnSimplifiedAddress = models.CharField(max_length=100, blank=True, null=True)
    cnSimplifiedCorporationName = models.CharField(max_length=50, blank=True)



    class Meta:
        abstract = True

class Client(Name_Information, BaseModel):

    createdDate = models.DateTimeField(auto_now_add=True)
    manager = models.ManyToManyField('People.Person', through='People.Client_Manager', blank=True,
                                     related_name='client_manager')

    accounting_manager = models.ManyToManyField('People.Person', through='People.Client_AccountingManager', blank=True,
                                                related_name='client_accountingmanager')

    parent = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)

    case = models.ForeignKey('case.Case', blank=True, null=True,
                             related_name='client_case'
                             , on_delete=models.SET_NULL)
    personal_client = models.OneToOneField('Person', on_delete=models.SET_NULL, null=True, blank=True, related_name='client_personal')

    client_clienttype = models.ManyToManyField(
        'ClientType',
        through='Client_ClientType',
        related_name='client_clienttype', blank=True
    )
    emaildetail = models.ManyToManyField(
        'emailsystem.Email_Detail',
        through='emailsystem.EmailDetail_Client',
        related_name='client_emaildetail', blank=True
    )
    patent = models.ManyToManyField(
        'application_patent_domestic.Application_Patent_Domestic_Master',
        through='application_patent_domestic.Application_Patent_Domestic_Master_Client',
        related_name='client_patent_master', blank=True
    )
    member = models.ManyToManyField(
        'Person',
        through='Client_Member',
        related_name='client_member', blank=True
    )
    oversea_agent = models.ManyToManyField(
        'application_patent_domestic.Application_Patent_Domestic_Master',
        through='application_patent_domestic.Application_Patent_Domestic_Master_OverseaAgent',
        related_name='client_oversea_agent', blank=True
    )

    def __str__(self):
        result = ''
        if not self.isCompany:
            if self.korName:
                result += self.korName
            if self.engFullName:
                result += '(' + self.engFullName + ')'
            if self.originFullName:
                result += '(' + self.originFullName + ')'

        else:
            if self.korCorporationName:
                result += self.korCorporationName
            if self.engCorporationName:
                result += '(' + self.engCorporationName + ')'
            if self.originCorporationName:
                result += '(' + self.originCorporationName + ')'

        return result
    class Meta:
        db_table = 'client'

class ClientType(models.Model):
    typeName = models.CharField(max_length=50, blank=False, null=False, choices=[(tag.name, tag.value) for tag in CLIENT_TYPE],
                                default=CLIENT_TYPE.Client.name)
    client = models.ForeignKey('Client', blank=True, null=True, on_delete=models.CASCADE, related_name='clienttype_client')
    def __str__(self):
        return self.typeName
    class Meta:
        db_table = 'client_type'

class Client_ClientType(models.Model):
    client = models.ForeignKey('Client', blank=False, null=False, on_delete=models.CASCADE, related_name='client_clienttype_client')
    client_type = models.ForeignKey('ClientType', blank=False, null=False, on_delete=models.CASCADE,
                               related_name='client_clienttype_clienttype')
    class Meta:
        db_table = 'client_clienttype'

class Person(Name_Information, BaseModel):

    companyName = models.CharField(max_length=30, blank=True)
    affiliation = models.CharField(max_length=30, blank=True)
    department = models.CharField(max_length=30, blank=True)
    position = models.CharField(max_length=30, blank=True)
    phoneNumber = PhoneField(blank=True, help_text='Person phone number', null=True)
    companyPhoneNumber = PhoneField(blank=True, help_text='Company phone number', null=True)
    user = models.OneToOneField('auth.User', on_delete=models.SET_NULL, null=True, blank=True, default='')
    management_oversea_agent = models.ManyToManyField(
        'application_patent_domestic.Application_Patent_Domestic_Master_OverseaAgent',
        through='application_patent_domestic.Oversea_Agent_Management',
        related_name='person_management_oversea_agent', blank=True
    )
    company = models.ManyToManyField(
        'Client',
        through='Client_Member',
        related_name='person_company', blank=True
    )
    def __str__(self):
        result = ''
        if not self.isCompany:
            if self.korName:
                result += self.korName
            if self.engFullName:
                result += '(' + self.engFullName + ')'
            if self.originFullName:
                result += '(' + self.originFullName + ')'

        else:
            if self.korCorporationName:
                result += self.korCorporationName
            if self.engCorporationName:
                result += '(' + self.engCorporationName + ')'
            if self.originCorporationName:
                result += '(' + self.originCorporationName + ')'

        return result
    class Meta:
        db_table = 'person'

class Work_Email(BaseModel):
    email = models.EmailField(max_length=200, null=False, blank=False, default='')
    emailName = models.CharField(max_length=200, null=True, blank=True)
    isActive = models.EmailField(null=False, blank=False, default=True)
    isAccountEmail = models.EmailField(null=False, blank=False, default=True)
    person = models.ForeignKey('Person', blank=True, null=True,
                                     related_name='workemail_person',
                                     on_delete=models.SET_NULL)
    def __str__(self):
        return self.email+'('+self.emailName+')'

    class Meta:
        db_table = 'work_email'
        unique_together = ('email', 'emailName')

class Client_Manager(models.Model):
    client = models.ForeignKey('Client', blank=True, null=True, related_name='clients_manager', on_delete=models.SET_NULL)
    manager = models.ForeignKey('Person', blank=True, null=True, related_name='clients_manager', on_delete=models.SET_NULL)

    class Meta:
        db_table = 'client_manager'

class Client_AccountingManager(models.Model):
    client = models.ForeignKey('Client', blank=True, null=True, related_name='clients_accountingmanager', on_delete=models.SET_NULL)
    accounting_manager = models.ForeignKey('Person', blank=True, null=True, related_name='clients_accountingmanager', on_delete=models.SET_NULL)

    class Meta:
        db_table = 'client_accountingmanager'

class Client_Member(models.Model):
    client = models.ForeignKey('Client', blank=True, null=True, related_name='client_member_client', on_delete=models.CASCADE)
    member = models.ForeignKey('Person', blank=True, null=True, related_name='client_member_member', on_delete=models.CASCADE)

    class Meta:
        db_table = 'client_member'






class Client_ApplicantInformation(models.Model):
    client = models.ForeignKey('Client', blank=True, null=True, related_name='client_applicantinformation_client', on_delete=models.CASCADE)
    applicant = models.ForeignKey('ApplicantInformation', blank=True, null=True, related_name='client_applicantinformation_applicantinformation', on_delete=models.CASCADE)
    class Meta:
        db_table = 'client_applicantinformation'


class ApplicantInformation(BaseModel):
    code = models.CharField(max_length=50, blank=True)
    past_id = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)
    patentOfficeCode = models.CharField(max_length=10, blank=True, choices=[(tag.name, tag.value) for tag in MakeWIPO_ST3_Enum()])
    creationDate = models.DateTimeField(auto_now_add=True)
    isCorporation = models.BooleanField(null=True, blank=True)
    isCompany = models.BooleanField(null=False, default=True, blank=False)
    originLastName = models.CharField(max_length=20, blank=True)
    originFirstName = models.CharField(max_length=20, blank=True)
    originFullName = models.CharField(max_length=50, blank=True)
    originAddress = models.CharField(max_length=100, blank=True, null=True)
    engLastName = models.CharField(max_length=20, blank=True, null=True)
    engFirstName = models.CharField(max_length=20, blank=True, null=True)
    engFullName = models.CharField(max_length=20, blank=True, null=True)
    engAddress = models.CharField(max_length=100, blank=True, null=True)
    korLastName = models.CharField(max_length=20, blank=True, null=True)
    korFirstName = models.CharField(max_length=20, blank=True, null=True)
    korFullName = models.CharField(max_length=20, blank=True, null=True)
    korAddress = models.CharField(max_length=100, blank=True, null=True)


    def __str__(self):
        if self.originFullName is not None:
            orgin = self.originFullName
        else:
            orgin = 'None'
        if self.korFullName is not None:
            kor = self.korFullName
        else:
            kor = 'None'
        return orgin +'('+ kor+')'

        #return self.korCorporationName

    class Meta:
        db_table = 'applicant_information'



