from django.forms import ModelForm

from People.models import *
from django import forms

from application_patent_domestic.forms import ModelFormBase
from common.codemultilanguage_choice import MakeTagChoiceMultipleLanguage
from utils.enums import ALL_CODE_TYPE


class ClientModelForm(ModelFormBase):
	def __init__(self, *args, **kwargs):
		super(ClientModelForm, self).__init__(*args, **kwargs)
		if self.labeldic:
			for i, (k, v) in enumerate(self.labeldic.items()):
				if k != 'createdDate':
					self.fields[k].label = v
		if self.customcodechoice:
			self.fields['client_clienttype'].choices = self.customcodechoice[ALL_CODE_TYPE.Client_Type.name]
			MakeTagChoiceMultipleLanguage(self)


	class Meta:
		model = Client
		fields = '__all__'

class PersonModelForm(ModelFormBase):
	def __init__(self, *args, **kwargs):
		self.after_create_history = True
		super(PersonModelForm, self).__init__(*args, **kwargs)
		if self.labeldic:
			for i, (k, v) in enumerate(self.labeldic.items()):

				self.fields[k].label = v
		self.fields['company'].queryset = Client.objects.all()
		ModelFormBase.arrange_and_createtablehistory(self)


	class Meta:
		model = Person
		fields = '__all__'

class WorkEmailModelForm(ModelFormBase):
	def __init__(self, *args, **kwargs):
		super(WorkEmailModelForm, self).__init__(*args, **kwargs)
		if self.labeldic:
			for i, (k,v) in enumerate(self.labeldic.items()):
				self.fields[k].label = v

	class Meta:
		model = Work_Email
		fields = '__all__'

class ApplicantInformationModelForm(ModelFormBase):
	def __init__(self, *args, **kwargs):
		super(ApplicantInformationModelForm, self).__init__(*args, **kwargs)
		if self.labeldic:
			for i, (k,v) in enumerate(self.labeldic.items()):
				if k != 'creationDate':
					self.fields[k].label = v


	class Meta:
		model = ApplicantInformation
		fields = '__all__'




class LoginForm(forms.Form):
	emailId = forms.CharField(widget=forms.EmailInput)
	password = forms.CharField(widget=forms.PasswordInput)