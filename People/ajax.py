from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import JsonResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt

from People.models import Work_Email




@login_required(login_url='People:login')
@csrf_exempt
@transaction.atomic
def workemail_person_unlink(request):
    if request.method == 'GET':
        pk = request.GET.get('pk')
        wobj = Work_Email.objects.get(id=pk)
        wobj.person = None
        wobj.save()
        return JsonResponse({'state':'success'}, safe=False)

@login_required(login_url='People:login')
def person_email_link(request):
    if request.method == 'GET':
        workemail = request.GET.get('workemail')
        pk = request.GET.get('pk')
        splitemail = workemail.split('/')
        email = splitemail[0].strip()
        email_name = splitemail[1].strip()
        Work_Email.objects.filter(email=email, emailName=email_name).update(person_id=pk)
        return JsonResponse({'state': 'success'}, safe=False)