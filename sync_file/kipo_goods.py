import glob

import xlrd
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from application_patent_domestic.models import KIPOGoodsName
from utils.enums import UPLOAD_FILE_PATH

@csrf_exempt
def sync_kipogoods(request):
    if request.method == 'POST':
        noticeNumber = request.POST.get('noticeNumber', None)
        effectiveDate = request.POST.get('effectiveDate', None)
        kipogoodsfile = request.POST.get('kipogoodsfile', None)
        sheetIndex = request.POST.get('sheetIndex', None)
        rangeIndex = request.POST.get('rangeIndex', None)
        file = glob.glob(kipogoodsfile)
        wb = xlrd.open_workbook(file[0])
        save_kipogoods(wb, noticeNumber, effectiveDate, sheetIndex, rangeIndex)
        return HttpResponse('')
def save_kipogoods(wb, noticeNumber, effectiveDate, sheetIndex, rangeIndex):
    sheetnames = wb.sheet_names()
    sheet = wb.sheet_by_name(sheetnames[int(sheetIndex)])
    rows = sheet.nrows
    cols = sheet.ncols
    all_val = []
    start = (int(rangeIndex) - 1) * 10000 + 1
    end = start + 10000
    if end > rows:
        end = rows
    for row in range(start, end):
        row_val = []
        for col in range(cols):
            row_val.append(sheet.cell_value(row, col))
        all_val.append(row_val)
    serviceC = {}
    if int(sheetIndex) != 0:
        serviceC = {'serviceClass': sheetnames[int(sheetIndex)]}
    for row in all_val:
        dic = {}
        if len(row) > 4:
            dic = {'sourcePatentOffice': row[4]}

        obj, create = KIPOGoodsName.objects.get_or_create(noticeNumber=noticeNumber,
                                                        effectiveDate=effectiveDate,
                                                        goodsNameKor=row[0],
                                                        classification=row[1],
                                                        similarGroupCode=row[2],
                                                        goodNameEng=row[3],
                                                        **dic,
                                                        **serviceC)
