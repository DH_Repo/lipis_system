import os
from os import path

import django

from Lipis_system_master import settings


def make_attachment_path(username):

    # upload_to="%Y/%m/%d" 처럼 날짜로 세분화
    ymd_path = django.utils.timezone.now().strftime('%Y%m%d')

    custompath = os.path.join(username, ymd_path)
    if not path.exists(settings.MEDIA_ROOT + '/' + custompath):
        os.makedirs(settings.MEDIA_ROOT + '/' + custompath)
    return custompath

def make_abstractFig_attachment_path(username):
    # upload_to="%Y/%m/%d" 처럼 날짜로 세분화
    ymd_path = django.utils.timezone.now().strftime('%Y%m%d')

    custompath = os.path.join(username, ymd_path)
    if not path.exists(settings.MEDIA_ROOT + '/' + 'abstractFig' + '/' + custompath):
        os.makedirs(settings.MEDIA_ROOT + '/' + 'abstractFig' + '/' + custompath)
    return ('abstractFig' + '/' + custompath).replace('\\', '/')

def make_brandimage_attachment_path(username):
    # upload_to="%Y/%m/%d" 처럼 날짜로 세분화
    ymd_path = django.utils.timezone.now().strftime('%Y%m%d')

    custompath = os.path.join(username, ymd_path)
    if not path.exists(settings.MEDIA_ROOT + '/'+'brandimage'+ '/' + custompath):
        os.makedirs(settings.MEDIA_ROOT + '/'+'brandimage'+ '/' + custompath)
    return ('brandimage'+'/'+custompath).replace('\\', '/')

def absoluteFilePaths(directory):
   for dirpath,_,filenames in os.walk(directory):
       for f in filenames:
           yield os.path.abspath(os.path.join(dirpath, f))