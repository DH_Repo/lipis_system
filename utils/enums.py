
from enum import Enum

import pycountry
from iso3166 import Country
from iso4217 import Currency

class ALL_CODE_TYPE(Enum):
    PatentType = '출원종류'
    PatentNationType = '출원국가종류'
    ApplicationPatent_Status = '출원_상태'
    Case_CaseType = '사건_사건종류'
    Case_CaseState = '사건_사건상태'
    Task_TaskType = '업무_업무종류'
    Task_TaskState = '업무_업무상태'
    Revenue_Invoice_State = '매출청구서_상태'
    Purchase_Invoice_State = '매입청구서_상태'
    Message_ReceiveSendType = '메시지_수발종류'
    Message_MessageType = '메시지_메시지종류'
    File_FileType = "파일_파일종류"
    ParentMessage_ChildMessage_Relation_Type = "부모메시지_자식메시지_관계종류"
    Management_TagListType = "관리_태그종류"
    Message_CaseTaskType = "메시지_사건업무종류"
    Client_Type = "고객타입"



class PATENT_TYPE_FOR_OURCOM(Enum):
    P = '특허'
    T = '상표'
    D = '디자인'
    R = '조사/분석/컨설팅'
    I = '반도체배치설계'
    C = '저작권'

class NATION_TYPE_FOR_OURCOM(Enum):
    D = '국내'
    O = '해외'
    I = '국내/해외고객'
    S = 'S'

class PATENT_STATE_CHOICE(Enum):   # A subclass of Enum
    EMPTY = "미지정"
    CANCEL = "취하"
    COMPLETE = "완료"
    CLOSING = "종결"
    COMINGOUT = "취하예정"
    PROCEEDING = "진행중"
    RECEIPT = "접수"

class CASE_TYPE(Enum):
    APP = '출원'
    ROA = '의견제출통지 Office Action'
    FOA = '거절결정, Final Office Action'
    APL = '불복심판'
    EXM = '심사청구'
    REG = '등록'
    REW = '갱신, 연차료, Renewal'
    TRI = '당사자계 심판'

class CASE_STATE(Enum):
    REP = '접수'


class TASK_TYPE(Enum):
    REP = '문의 답변'
    SOA = '의견제출통지 Office Action'
    FOA = '거절결정, Final Office Action'
    APL = '불복심판'
    EXM = '심사청구'
    REG = '등록'
    REW = '갱신, 연차료, Renewal'
    TRI = '당사자계 심판'

class TASK_STATE(Enum):
    REP = '접수'

class REVENUE_INVOICE_STATE(Enum):
    ISU = '발행'

class PURCHASE_INVOICE_STATE(Enum):
    ISU = '발행'

class MESSAGE_TYPE(Enum):
    EM = '이메일'
    PO = '우편'
    FX = '팩스'
    DM = '직접전달'
    KP = '한국특허청문서'

class MESSAGE_SEND_RECEIVE_TYPE(Enum):
    S = '발송'
    R = '수신'
    I = '내부메세지'

class PARENTMESSAGE_CHILDMESSAGE_RELATION_TYPE(Enum):
    RE = '답장'
    FW = '전달'
    AK = 'Ack'

class TAG_TYPE(Enum):
    TT = "TT"

class MESSAGE_CASETASK_TYPE(Enum):
    CRE = "사건접수"
    CSI = "사건제출지시"
    TRE = "업무접수"
    TRP = "업무보고"
########################################

"""
class PATENT_TYPE(Enum):
    PATENT = '특허'
    TRADEMARK = '상표'
    DESIGN = '디자인'
    RESEARCH = '조사/분석/컨설팅'
    INTEGRATED_CIRCUIT_DESIGN = '반도체배치설계'
    COPYRIGHT = '저작권'

class NATION_TYPE(Enum):
    DOMESTIC = '국내'
    OUTGOING = '해외'
    INCOMING = '국내/해외고객'
"""

class TABLE_TYPE(Enum):
    Master = 'Master'
    Applicant = 'Applicant'
    Client = 'Client'
    Person = 'Person'
    Email = 'Email'
    Message = 'Message'
    File = 'File'
    MainInventor = 'MainInventor'
    Agent = 'Agent'
    Case = 'Case'
    Task = 'Task'
    Work_Email = 'Work_Email'
    Revenue_Invoice = 'Revenue_Invoice'
    Purchase_Invoice = 'Purchase_Invoice'
    Transaction_History = 'Transaction_History'
    BankAccount_Information = 'BankAccount_Information'
    ApplicantInformation = 'ApplicantInformation'
    Task_Template = 'Task_Template'
    Patent_Document = 'Patent_Document'
    Patent_Document_Attachment = 'Patent_Document_Attachment'


    @staticmethod
    def list():
        return list(map(lambda c: c.value, TABLE_TYPE))

class FILETYPE(Enum):
    sync_email = 'SYNCEMAIL'
    file_email = 'FILEEMAIL'
    manual_attach_email = 'MANUAL_ATTACH_EMAIL'
    patent_docu = 'PATENT_DOCU'
    post = 'POST'
    fax = 'FAX'
    electronic_doc = 'ELECTRONIC_DOC'
    call = 'CALL'
    system = 'SYSTEM'
    externalServiceResults = '외부용역결과물'
    revenue_invoice = '매출청구서'
    revenue_tax_invoice = '매출세금계산서'
    purchase_tax_invoice = '매입세금계산서'


class EMAILTYPE(Enum):
    FromFile = '파일이메일'
    FromServer = '서버이메일'

class MESSAGE_STATE(Enum):
    unspecified = 'UNSPECIFIED'
    new = 'NEW'
    proceeding = 'PROCEEDING'
    finished ='FINISHED'

class PATENT_DOCU_TYPE(Enum):
    PatentApplication = '특허출원서'
    RequestforAmendment = '보정요구서'
    NoticeforDispositionofInvalidation = '무효처분통지서'
    RequestforPriorArtSearch = '선행기술조사의뢰서'
    ReportofPriorArtSearch = '선행기술조사보고서'

class PATENT_DOCU_PROCESS_STATUS(Enum):
    RECEIVED = '수리'
    SEND_PROCESSING_COMPLETE = '발송처리완료'
    INVALIDITY = '무효'

class UPLOAD_FILE_PATH(Enum):
    email = 'upload_file/emailfiles'
    patentdocu = 'upload_file/patentdocu'
    patentsubmitdocu = 'upload_file/patentsubmitdocu'
    excelfile = 'upload_file/excelfiles'
    kipogoods = 'upload_file/kipogoods'

class LANGUAGE_CODE_INSERT(Enum):
    ko = '한국어'
    en = 'English'
    ja = '日本語'
    zh_Hant = '繁體中文' #번체
    zh_Hans = '简体中文' #간체

class PATENT_OFFICE_DOCUMENT_FILETYPE(Enum):
    OuterHDR = 'outerhdr'
    InnerHDR = 'innerhdr'
    OriginPDF = 'originpdf'
    AttachPDF = 'attachpdf'
    SubmitZIP = 'submitzip'

class REGEX_APPLCODE_REFCODE(Enum):
    applNo = '(10-?[0-9]{4}-?\d{7})'
    #applNowithBar = '(10-)(?P<year>[0-9]{4})(-)(?P<num>\d{7})'
    #PCT = '(L[PTDRIC]O)(?P<num>\d{8})(PCT)'
    #WO = '(L[PTDRIC]O)(?P<num>\d{8})(WO)'
    #RefOversea = '(L[PTDRIC]O)(?P<num>\d{8})(?P<countrycode>\d{3})'
    #RefDomestic = '(L[PTDRIC][DI])(?P<num>\d{8})'
    RefOurCom = '(L[PTDRIC][DOI]\d{4}-?\d{4}([A-Z]{3}|[A-Z]{2})?)'
    RevenueInvoice_OurComNumber = '([DOI]\d{8}[PTDR])'
class CLIENT_TYPE(Enum):
    Client = '고객'
    Partner = '협력사'
    OverseaAgent = '해외대리인'

class TASK_PROCEDURE_TYPE(Enum):
    TEMPLATE = '템플릿'
    WORK = '작업'


class INVOICE_TYPE(Enum):
    RevenuePaymentCompletionNotification = '매출결제완료통지메시지'
    RevenueClaim = '매출청구메시지'
    PurchaseTaxBill = '매입세금계산서'
    PurchaseClaim = '매입청구메세지'
    PurchasePaymentCompletionNotification = '매입결제완료통지메시지'

class CASE_TASK_TYPE(Enum):
    CaseReception = '사건접수'
    CaseSubmissionInstruction = '사건제출지시'
    TaskReception = '업무접수'
    TaskReport = '업무보고'

class EXTERNALSERVICE_MESSAGE_TYPE(Enum):
    CaseReception = '외부용역의뢰메시지'
    CaseSubmissionInstruction = '외부용역결과메시지'

def MakeWIPO_ST3_Enum():
    countryDic = {}
    for c in Country:
        countryDic[c.alpha2] = c.alpha2
    countryDic['OA'] = 'OA'
    countryDic['AP'] = 'AP'
    countryDic['BX'] = 'BX'
    countryDic['QZ'] = 'QZ'
    countryDic['EA'] = 'EA'
    countryDic['EP'] = 'EP'
    countryDic['IB'] = 'IB'
    countryDic['WO'] = 'WO'
    countryDic['XU'] = 'XU'
    countryDic['XN'] = 'XN'
    countryDic['EM'] = 'EM'
    countryDic['GC'] = 'GC'
    countryDic['XV'] = 'XV'
    countryDic['XX'] = 'XX'
    return Enum('DynamicEnum', countryDic)

def MakeWIPO_ST3_Dic():
    countrydic = {}
    for c in Country:
        countrydic[c.alpha2] = c.english_short_name
    countrydic['OA'] = 'African Intellectual Property Organization'
    countrydic['AP'] = 'African Regional Intellectual Property Organization'
    countrydic['BX'] = 'Benelux Office for Intellectual Property'
    countrydic['QZ'] = 'Community Plant Variety Office of the European Union'
    countrydic['EA'] = 'Eurasian Patent Organization'
    countrydic['EP'] = 'European Patent Office'
    countrydic['IB'] = 'International Bureau of the World Intellectual Property Organization(IB)'
    countrydic['WO'] = 'International Bureau of the World Intellectual Property Organization(WO)'
    countrydic['XU'] = 'International Union for the Protection of New Varieties of Plants'
    countrydic['XN'] = 'Nordic Patent Institute'
    countrydic['EM'] = 'European Union Intellectual Property Office'
    countrydic['GC'] = 'Patent Office of the Cooperation Council for the Arab States of the Gulf'
    countrydic['XV'] = 'Visegrad Patent Institute'
    countrydic['XX'] = 'Unknown states, other entities or organizations'
    return countrydic

def MakeCurrencyEnum():
    currencyDic = {}
    for c in Currency:
        currencyDic[c.currency_name] = c.code
    return Enum('CurrencyEnum', currencyDic)

def MakeLanguageCodeEnum():
    languageDic = {}
    lang = pycountry.languages
    for l in lang:
        try:
            if l.alpha_2:
                #langinfo = Language.get(l.alpha_2).language_name(l.alpha_2)
                languageDic[l.alpha_2] = l.name
        except:
            continue

    return Enum('LanguageCodeEnum', languageDic)

class Email_Child_STATE(Enum):   # A subclass of Enum
    ACK = 'ACK'
    REPLY = 'REPLY'
    FORWARD = 'FORWARD'

class CRUD_TYPE(Enum):
    C = 'CREATE'
    R = 'READ'
    U = 'UPDATE'
    D = 'DELETE'


class TAG_COMMENT_TYPE(Enum):
    tag = "tag"
    comment = "comment"

class PATENT_FILTER_TYPE(Enum):
    patent_client_type = 'client__client_clienttype__typeName'