import os
from datetime import datetime
from os import path

from fake_useragent import UserAgent
from selenium import webdriver

from Lipis_system_master import settings


def chrome_driver():
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    #options.add_argument('window-size=1920x1080')
    #display = Display(visible=0,size=())
    #display.start()
    options.add_argument("disable-gpu")
    options.add_argument('--no-sandbox')
    returpath = '/hometax/' + datetime.today().strftime('%Y%m%d')
    custompath = settings.MEDIA_ROOT + returpath
    options.add_argument('--disable-dev-shm-usage')
    if not path.exists(custompath):
        os.makedirs(custompath)

    preferences = {"download.default_directory": custompath, "safebrowsing.enabled": "false"}

    options.add_experimental_option("prefs", preferences)

    user_agent = UserAgent()
    options.add_argument(
        "user-agent=%s" % user_agent.safari)
    options.add_argument("lang=ko_KR")
    driver = webdriver.Chrome(executable_path='/usr/local/bin/chromedriver', options=options)
    driver.get('about:blank')
    driver.execute_script("Object.defineProperty(navigator, 'plugins', {get: function() {return[1, 2, 3, 4, 5];},});")
    driver.execute_script("Object.defineProperty(navigator, 'languages', {get: function() {return ['ko-KR', 'ko']}})")
    driver.execute_script(
        "const getParameter = WebGLRenderingContext.getParameter;WebGLRenderingContext.prototype.getParameter = function(parameter) {if (parameter === 37445) {return 'NVIDIA Corporation'} if (parameter === 37446) {return 'NVIDIA GeForce GTX 980 Ti OpenGL Engine';}return getParameter(parameter);};")
    print("chrome 활성화.")
    return driver, returpath