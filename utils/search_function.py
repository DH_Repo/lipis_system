from django.db.models import ExpressionWrapper, Value, CharField

from People.models import Client, Person
from application_patent_domestic.models import *
from case.models import Case, Task
from emailsystem.models import Email_Detail
from message.models import Message, File

def column_filter_usingModelobj(modelobj, str, str1):
    myfilter = str + '__' + 'contains'
    return modelobj.filter(**{myfilter:str1})

def column_filter(table, field, search):

    myfilter = field + '__icontains'

    if table == 'Master':
        return Application_Patent_Domestic_Master.objects.filter(**{myfilter: search})
    elif table == 'Applicant':
        return Application_Patent_Domestic_Applicant.objects.filter(**{myfilter: search})
    elif table == 'Client':
        return Client.objects.filter(**{myfilter: search})
    elif table == 'Person':
        return Person.objects.filter(**{myfilter: search})
    elif table == 'Email_Detail':
        return Email_Detail.objects.filter(**{myfilter: search}).order_by('-sentDate')
    elif table == 'Message':
        return Message.objects.filter(**{myfilter: search})
    elif table == 'File':
        return File.objects.filter(**{myfilter: search})
    elif table == 'MainInventor':
        return Main_Inventor.objects.filter(**{myfilter: search})
    elif table == 'Agent':
        return Agent.objects.filter(**{myfilter: search})
    elif table == 'Case':
        return Case.objects.filter(**{myfilter: search})
    elif table == 'Task':
        return Task.objects.filter(**{myfilter: search})


def applicants_column_filter(str, str1):
    mylist = []

    myfilter = str + '__' + 'contains'
    mylist = Application_Patent_Domestic_Applicant.objects.filter(**{myfilter: str1})
    return mylist

def client_column_filter(str, str1):
    mylist = []

    myfilter = str + '__' + 'contains'
    mylist = Client.objects.filter(**{myfilter: str1})
    return mylist

def person_column_filter(str, str1):
    mylist = []

    myfilter = str + '__' + 'contains'
    mylist = Person.objects.filter(**{myfilter: str1})
    return mylist

def searchpara_save(para1, para2, para3):
    searchpara = Search_Parameter(tableType=para1)
    searchpara.colname = para2
    searchpara.searchname = para3
    searchpara.save()
