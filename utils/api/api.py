import requests

from patentro.models import PatentroWebapiAuthenticationInformation


def callapi_readAppHistInfo(request, applnumber):



    if request.method == 'POST':
        r = requests.post('https://www.somedomain.com/some/url/save', params=request.POST)
    else:
        applnumber = applnumber.strip()
        pobj = PatentroWebapiAuthenticationInformation.objects.all().first()
        para = 'ctfctKey=' + pobj.authKey + '&apagtCd=' + pobj.apagtCd
        r = requests.get('https://www.patent.go.kr/smart/webservice/apl/readApplHistInfo.do?' + para + '&applNo=' + applnumber, params=request.GET)
    return r.text
