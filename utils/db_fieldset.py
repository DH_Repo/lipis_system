from application_patent_domestic.models import Search_Parameter
from management.models import DB_Modifying_Field_Name, DB_Field_Name
from django.apps import apps


from utils.enums import TABLE_TYPE, LANGUAGE_CODE_INSERT


def specific_field_name(language,tablename):
    return DB_Modifying_Field_Name.objects.filter(languagecode=language, dbfieldname__tableName=tablename).values \
        ("dbfieldname__originFieldName", "customFieldName").order_by('languageCode')

def get_fielddic(language, tablename):
    filterdic = {}
    for fieldname in DB_Modifying_Field_Name.objects.filter(
            dbfieldname__tableName=tablename, languageCode=language).values(
        'dbfieldname__originFieldName',
        'customFieldName'):
        filterdic[fieldname['dbfieldname__originFieldName']] = fieldname['customFieldName']
    return filterdic

def get_feild_names(language, tablename, table):
    objs = DB_Modifying_Field_Name.objects.filter(languageCode=language, dbfieldname__tableName=tablename).values \
        ("dbfieldname__originFieldName", "customFieldName").order_by('languageCode')

    col = {}
    for obj in objs:
        for field in table._meta.get_fields():
            if obj["dbfieldname__originFieldName"] == field.name:
                col[field.name] = obj["customFieldName"]

        for field in table._meta.many_to_many:
            #manytomanyfield = DB_Modifying_Field_Name.objects.filter(countrycode=ci, dbfieldname__tableName=field.related_model._meta.object_name)
            if obj["dbfieldname__originFieldName"] == field.attname:
                col[field.attname] = obj["customFieldName"]
    return col

def getModifedFieldName(txt):
    if txt == '':
        return ['Application_Patent_Domestic_Master', 'Application_Patent_Domestic_Applicant', 'Person', 'Client', 'Work_Email', 'ApplicantInformation', 'Email_Detail', 'Message'
                , 'File', 'Main_Inventor', 'Agent', 'Case', 'Task', 'Revenue_Invoice', 'Purchase_Invoice', 'Transaction_History', 'BankAccount_Information'
                , 'Task_Template', 'Patent_Document', 'Patent_Document_Attachment']
    switcher = {
        'patent_domestic': ['Application_Patent_Domestic_Master', 'Application_Patent_Domestic_Applicant','Main_Inventor','Agent'],
        'people': ['Person', 'Client', 'Work_Email', 'ApplicantInformation'],
        'emailsystem': ['Email_Detail'],
        'message': ['Message', 'File', 'Patent_Document', 'Patent_Document_Attachment'],
        'case': ['Case', 'Task', 'Revenue_Invoice', 'Purchase_Invoice', 'Transaction_History', 'BankAccount_Information' ,'Task_Template'],
    }
    return switcher.get(txt, '')

def kr_field_name(txt):
    switcher = {
        'state': '상태',
        'applicants': '출원인',
        'client': '출원인',
        'clientCompany': '고객사',
        'ourCompanyNumber': '당소번호',
        'inventName': '발명의명칭',
        'mainInventor': '주발명자',
        'managerOfApplicant': '출원인담당',
        'managerOfOurCompany': '당소담당',
        'receptionDate': '접수일',
        'priorityClaim': '우선권주장',
        'priorityDate': '우선일',
        'applicationRequestFixedDate': '출원요청기일',
        'draftFixedDate': '초안기일',
        'draftSendDate': '초안발송일',
        'currentProgress': '현진행상태',
        'currentProgressDate': '현진행일자',
        'applicationNumber': '출원번호',
        'applicationDate': '출원일자',
        'remarks': '비고',
        'holder': '권리자',
        'agent': '대리인',
        'client': '고객'


   }
    return switcher.get(txt, "")

def make_initial_custom_field(fieldname, lang):
    if lang == 'ko':
        return kr_field_name(fieldname)
    else:
        return fieldname


def Insert_DBFIELD_and_ModifyingDB(model):

    for lang in LANGUAGE_CODE_INSERT:
        for fname in getModifedFieldName(''):
            if model._meta.object_name == fname:
                for field in model._meta.fields:
                    if field.name != 'id':
                        obj, created = DB_Field_Name.objects.get_or_create(tableName=model._meta.object_name,
                                                                           originFieldName=field.name)
                        #initcustomfield = make_initial_custom_field(field.name, lang.name)
                        obj, created = DB_Modifying_Field_Name.objects.get_or_create(dbfieldname=obj,
                                                                           languageCode=lang.name)
                        #obj.customFieldName = initcustomfield
                        obj.save()
                for field in model._meta.many_to_many:

                    obj, created = DB_Field_Name.objects.get_or_create(tableName=model._meta.object_name,
                                                                       originFieldName=field.attname)
                    #initcustomfield = make_initial_custom_field(field.attname, lang.name)
                    dobj, created = DB_Modifying_Field_Name.objects.get_or_create(dbfieldname=obj,
                                                                       languageCode=lang.name)
                    #dobj.customFieldName = initcustomfield
                    dobj.save()



def Insert_DB_Field_Name():
    # get appconfig
    app_patent_domestic = apps.get_app_config('application_patent_domestic')
    people = apps.get_app_config('People')
    email = apps.get_app_config('emailsystem')
    message = apps.get_app_config('message')
    mgrapp = apps.get_app_config('management')
    authapp = apps.get_app_config('auth')
    caseapp = apps.get_app_config('case')


    for table in TABLE_TYPE.list():
        obj, created = Search_Parameter.objects.get_or_create(tableType=table)

    for model in app_patent_domestic.get_models():
        Insert_DBFIELD_and_ModifyingDB(model)
    for model in people.get_models():
        Insert_DBFIELD_and_ModifyingDB(model)
    for model in email.get_models():
        Insert_DBFIELD_and_ModifyingDB(model)
    for model in message.get_models():
        Insert_DBFIELD_and_ModifyingDB(model)
    for model in caseapp.get_models():
        Insert_DBFIELD_and_ModifyingDB(model)




