import datetime
import json

from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.http import JsonResponse

from application_patent_domestic.models import Application_Patent_Domestic_Master
from case.models import Case, Revenue_Invoice, Task, Task_Template_Ref, Task_Template_Ref_MessageType, Task_Template, \
    Transaction_History, Deposit_List

#C[출원종류코드1자리][출원국가종류코드1자리][국가코드 2자리][사건종류코드3자리][연도2자리][월2자리][일2자리][일련번호3자리]
from common.codemultilanguage_choice import GetCodeMultpleLanguageForCase, GetCodeMultpleLanguageForTask
from message.models import Message
from utils.enums import ALL_CODE_TYPE


@login_required(login_url='People:login')
def Generate_CaseNumber(request):
    patentid = request.GET.get('patentid')
    casecodename = request.GET.get('casecodename')
    msgid = request.GET.get('msgid')
    dtime = request.GET.get('dtime')

    mobj = Application_Patent_Domestic_Master.objects.get(id=patentid)
    ptype = mobj.patentType
    if not ptype:
        data = {'state': 'fail', 'result': 'not exist patenttype in patentmaster'}
        return JsonResponse(data)
    ntype = mobj.nationType
    if not ntype:
        data = {'state': 'fail', 'result': 'not exist nationtype in patentmaster'}
        return JsonResponse(data)
    ccode = mobj.countryCode
    if ntype == 'O':
        if not ccode:
            data = {'state': 'fail', 'result': 'not exist countrycode in patentmaster'}
            return JsonResponse(data)
    meobj = Message.objects.get(id=msgid)
    if dtime:
        datestr = dtime[2:4] + dtime[5:7] + dtime[8:]
    else:
        datestr = meobj.createdDate.strftime('%Y%m%d')
        datestr = datestr[2:]
    ourlist = []
    if ntype == 'O':
        numberforsearch = ptype + ntype + casecodename + datestr
        f = Case.objects.filter(
            caseNumber__regex=r'(C'+numberforsearch+')(\d{3})'+'('+ccode+')')
        if f:
            for l in f:
                ourlist.append(int(l.caseNumber[11:14]))
            serialnum = numberforsearch + '-' + str(max(ourlist) + 1).zfill(3) + ccode
        else:
            serialnum = numberforsearch + '001' + ccode

    else:
        numberforsearch = ptype + ntype + casecodename + datestr
        f = Case.objects.filter(
            caseNumber__regex=r'(' + numberforsearch + ')(\d{3})')
        if f:
            for l in f:
                ourlist.append(int(l.caseNumber[11:14]))
            serialnum = numberforsearch + str(max(ourlist) + 1).zfill(3)
        else:
            serialnum = numberforsearch + '001'

    data = {'state': 'success', 'result': serialnum}
    return JsonResponse(data)

@login_required(login_url='People:login')
def SearchRevenueInvoideByOurComnum(request):
    ourcom = request.GET.get('searchournum')
    robj = Revenue_Invoice.objects.filter(
        ourCompanyNumber=ourcom).values('id', 'ourCompanyNumber')
    data = {'state': 'success', 'result': list(robj)}
    return JsonResponse(data)

@login_required(login_url='People:login')
def Generate_TaskNumber(request):
    taskcode = request.GET.get('taskcode')
    msgid = request.GET.get('msgid')
    dtime = request.GET.get('dtime')

    meobj = Message.objects.get(id=msgid)
    if dtime:
        datestr = dtime[2:4] + dtime[5:7] + dtime[8:]
    else:
        datestr = meobj.createdDate.strftime('%Y%m%d')
        datestr = datestr[2:]
    ourlist = []

    numberforsearch = 'T' + taskcode + datestr
    f = Task.objects.filter(
        taskNumber__regex=r'('+numberforsearch+')(\d{4})')
    if f:
        for l in f:
            ourlist.append(int(l.taskNumber[-4:]))
        serialnum = numberforsearch + str(max(ourlist) + 1).zfill(4)
    else:
        serialnum = numberforsearch + '0001'



    data = {'state': 'success', 'result': serialnum}
    return JsonResponse(data)


@login_required(login_url='People:login')
def GetTaskTemplateRef(request):
    patentid = request.GET.get('patentid')
    casecode = request.GET.get('casecode')
    casemessagetypecode = request.GET.getlist('casemessagetypecode[]')
    casestate = request.GET.get('casestate')
    cCode = ''

    mobj = Application_Patent_Domestic_Master.objects.get(id=patentid)
    if mobj.nationType == 'O':
        cCode = mobj.countryCode
    nationT = mobj.nationType
    patentT = mobj.patentType
    if cCode:
        obj = Task_Template_Ref.objects.filter(caseType=casecode, patentType=patentT, patentNationType=nationT,
                                                tasktemplaterefmessagetype_tasktemplateref__messageType__in=casemessagetypecode,
                                               tasktemplaterefcasestate_tasktemplateref__caseState=casestate,
                                                countryCode=cCode)
    else:
        obj = Task_Template_Ref.objects.filter(caseType=casecode, patentType=patentT, patentNationType=nationT,
                                               tasktemplaterefmessagetype_tasktemplateref__messageType__in=casemessagetypecode,
                                               tasktemplaterefcasestate_tasktemplateref__caseState=casestate,
                                               )
    res = []

    ctypemultiplelang = GetCodeMultpleLanguageForCase(request)[ALL_CODE_TYPE.Case_CaseType.name]
    for o in obj:
        for ctype in ctypemultiplelang:
            ct = ''
            if ctype[0] == o.caseType:
                ct = ctype[1]
                break

        objstr = o.patentType + o.patentNationType + o.countryCode + ct
        res.append({'id':o.id,'str':objstr})
    data = {'state': 'success', 'result': res}
    return JsonResponse(data)

@login_required(login_url='People:login')
def GetTaskTemplate(request):
    tasktemplaterefid = request.GET.get('tasktemplaterefid')
    tt = Task_Template_Ref.objects.get(id=tasktemplaterefid).tasktemplate_tasktemplateref.all()
    taskcodemultiplelang = GetCodeMultpleLanguageForTask(request)[ALL_CODE_TYPE.Task_TaskType.name]
    res = []
    for o in tt:
        for taskcode in taskcodemultiplelang:
            tmultiple = ''
            if taskcode[0] == o.taskCode:
                tmultiple = taskcode[1]
                break
        res.append({'id':o.id,'str':tmultiple})
    data = {'state': 'success', 'result': res}
    return JsonResponse(data)

@login_required(login_url='People:login')
def LoadTaskTemplate(request):
    tasktemplateid = request.GET.getlist('tasktemplateid[]')
    tt = Task_Template.objects.filter(id__in=tasktemplateid).values('id','taskCode','child_id')
    taskcodemultiplelang = GetCodeMultpleLanguageForTask(request)[ALL_CODE_TYPE.Task_TaskType.name]
    res = []
    def getmultiplecode(taskCode):
        for taskcode in taskcodemultiplelang:
            tmultiple = ''
            if taskcode[0] == taskCode:
                tmultiple = taskcode[1]
                break
        return tmultiple



    def makeedge(id, nodelist, edge, rootid, linking=False):
        tobj = Task_Template.objects.get(id=id)
        if tobj.child_id:
            if tobj.child_id in nodelist:
                if linking:
                    edge.append({'source': rootid,'target':tobj.child_id})
                else:
                    edge.append({'source': id, 'target': tobj.child_id})
            else:
                makeedge(tobj.child_id, nodelist, edge,id,True)

    nodelist = []
    nodeidlist = []
    for o in tt:
        tmultiple = getmultiplecode(o['taskCode'])
        nodelist.append({'id':o['id'],'taskCode':tmultiple})
        nodeidlist.append(o['id'])
    edge = []
    for node in nodelist:
        makeedge(node['id'],nodeidlist,edge,'')



    data = {'state': 'success', 'nodelist': nodelist, 'edges':edge}
    return JsonResponse(data)


@login_required(login_url='People:login')
def LinkCaseToRevenueInvoice(request):
    caseid = request.GET.get('caseid')
    invoiceid = request.GET.get('invoiceid', None)
    forlink = request.GET.get('forlink', None)
    if forlink == 'true':
        Revenue_Invoice.objects.filter(id=invoiceid).update(case_id=caseid)
    else:
        Revenue_Invoice.objects.filter(id=invoiceid).update(case_id=None)

    data = {'state': 'success', 'result': 'success'}
    return JsonResponse(data)

@login_required(login_url='People:login')
def LinkTransactionHistoryToRevenueInvoice(request):
    tranid = request.GET.get('tranid')
    invoiceid = request.GET.get('invoiceid', None)
    th = Transaction_History.objects.get(id=tranid)
    ri = Revenue_Invoice.objects.get(id=invoiceid)
    Deposit_List.objects.get_or_create(transaction=th, revenue_invoice=ri)
    data = {'state': 'success', 'result': 'success'}
    return JsonResponse(data)

@login_required(login_url='People:login')
def LinkFinalCaseToRevenueInvoice(request):
    caseid = request.GET.get('caseid')
    invoiceid = request.GET.get('invoiceid', None)

    if invoiceid:
        Case.objects.filter(id=caseid).update(final_revenue_invoice_id=invoiceid)
    else:
        Case.objects.filter(id=caseid).update(final_revenue_invoice_id=None)
    data = {'state': 'success', 'result': 'success'}
    return JsonResponse(data)

@login_required(login_url='People:login')
def GetFinalRevenueInvoiceByCase(request):
    caseid = request.GET.get('caseid')
    final_revenue_invoice = Case.objects.get(id=caseid).final_revenue_invoice
    data = {'state': 'success', 'result': 'success','final_revenue_invoice':serializers.serialize("json",[final_revenue_invoice])}
    return JsonResponse(data)

@login_required(login_url='People:login')
def UnlinkCaseToMessage(request):
    caseid = request.GET.get('caseid')
    msgid = request.GET.get('msgid')
    case = Case.objects.get(id=caseid)
    case.message.remove(Message.objects.get(id=msgid))

    data = {'state': 'success', 'result': 'success'}
    return JsonResponse(data)

@login_required(login_url='People:login')
def CaseAndPatentByPatentOurCom(request):
    ourcomno = request.GET.get('ourcomno')
    obj = Application_Patent_Domestic_Master.objects.get(ourCompanyNumber=ourcomno)
    cases = list(obj.case.all().values())
    ap = Application_Patent_Domestic_Master.objects.filter(ourCompanyNumber=ourcomno)
    patent = list(Application_Patent_Domestic_Master.objects.filter(ourCompanyNumber=ourcomno).values())

    data = {'state': 'success', 'result': 'success','case': cases, 'patent': patent}
    return JsonResponse(data)

@login_required(login_url='People:login')
def SetDepositInfo(request):
    depositinfoid = request.GET.get('depositinfoid')
    currency = request.GET.get('currency')
    connectedamount = request.GET.get('connectedamount')
    dp = Deposit_List.objects.get(id=depositinfoid)
    dp.connectedCurrency = currency
    dp.connectedAmount = connectedamount
    dp.save()

    data = {'state': 'success', 'result': 'success'}
    return JsonResponse(data)