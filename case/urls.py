from django.urls import path

from case import views, crudmodel, ajax, modelcontroller

app_name = 'case'
urlpatterns = [
    path('test', views.test, name='test'),
    path('addcasewithmsg/<pk>', views.CreateCaseWithMessage, name='addcasewithmsg'),
    path('addtaskwithmsg/<pk>', views.CreateTaskWithMessage, name='addtaskwithmsg'),
    path('case', views.CaseView.as_view(), name='cases'),
    path('case/<pk>', views.CaseDetailView.as_view(), name='cases_detail'),
    path('tasks', views.TaskView.as_view(), name='tasks'),
    path('revenue_invoices', views.RevenuInvoiceView.as_view(), name='revenue_invoices'),
    path('purchase_invoices', views.PurchaseInvoiceView.as_view(), name='purchase_invoices'),

    path('transactionhistory', views.TransactionHistoryView.as_view(), name='transactionhistory'),
    path('transactionhistory/detail/<pk>', views.TransactionHistoryDetailView.as_view(), name='transactionhistory_detail'),

    path('bankaccountinformation', views.BankAccountInformationView.as_view(), name='bankaccountinformation'),

    path('task/create', crudmodel.TaskCreate, name='createtask'),
    path('task/edit/<pk>', crudmodel.TaskEdit, name='edittask'),
    path('task/delete', crudmodel.TaskDelete, name='deletetask'),
    path('task/search/ournumner', crudmodel.Task_SearchOurCom, name='task_searchournumber'),
    path('task/create/withmasterandemailmsg', crudmodel.CreateTaskWithMasterandMsg, name='createtaskwithmasterandmsg'),
    path('task/create/popupform/<pk>', crudmodel.CustomTaskPopupForm, name='customtaskpopupform'),
    path('task/number/load', ajax.Generate_TaskNumber, name='task_number_load'),

    path('case/create', crudmodel.CaseCreate, name='createcase'),
    path('case/edit/<pk>', crudmodel.CaseEdit, name='editcase'),
    path('case/delete', crudmodel.CaseDelete, name='deletecase'),
    path('case/search/ournumner', crudmodel.Case_SearchOurCom, name='case_searchournumber'),
    path('case/number/load', ajax.Generate_CaseNumber, name='case_number_load'),
    path('case/tasktemplateref/ajax/get', ajax.GetTaskTemplateRef, name='case_tasktemplate_ref_get'),
    path('case/tasktemplate/ajax/get', ajax.GetTaskTemplate, name='case_tasktemplate_get'),
    path('case/tasktemplate/ajax/load', ajax.LoadTaskTemplate, name='case_tasktemplate_load'),

    path('case/create/withmasterandemailmsg', crudmodel.CreateCaseWithMasterandMsg, name='createcasewithmasterandmsg'),
    path('case/create/popupform/<pk>', crudmodel.CustomCasePopupForm, name='customcasepopupform'),
    path('caseortask/message/link', modelcontroller.LinkCaseOrTaskToMessage, name='linkcaseortaskmessage'),
    path('case/revenueinvoice/ajax/link', ajax.LinkCaseToRevenueInvoice, name='linkcasetorevenueinvoice'),
    path('case/finalrevenueinvoice/ajax/link', ajax.LinkFinalCaseToRevenueInvoice, name='linkfinalcasetorevenueinvoice'),
    path('case/finalrevenueinvoice/ajax/get', ajax.GetFinalRevenueInvoiceByCase, name='getfinalrevenueinvoicebycase'),
    path('case/message/ajax/unlink', ajax.UnlinkCaseToMessage, name='unlinkcasetomessage'),
    path('case/patent/bypatentourcom/ajax/get', ajax.CaseAndPatentByPatentOurCom, name='getcaseandpatent_bypatentourcom'),



    path('revenueinvoice/create', crudmodel.RevenueInvoiceCreate, name='createrevenueinvoice'),
    path('revenueinvoice/edit/<pk>', crudmodel.RevenueInvoiceEdit, name='editrevenueinvoice'),
    path('revenueinvoice/delete', crudmodel.RevenueInvoiceDelete, name='deleterevenueinvoice'),
    path('revenueinvoice/load/ourcomnumber', crudmodel.LoadOurComnumForRevenueInvoide, name='revenueinvoiceourcomnumberload'),
    path('revenueinvoice/search/ourcomnumber', ajax.SearchRevenueInvoideByOurComnum, name='revenueinvoice_search'),


    path('purchaseinvoice/create', crudmodel.PurchaseInvoiceCreate, name='createpurchaseinvoice'),
    path('purchaseinvoice/edit/<pk>', crudmodel.PurchaseInvoiceEdit, name='editpurchaseinvoice'),
    path('purchaseinvoice/delete', crudmodel.PurchaseInvoiceDelete, name='deletepurchaseinvoice'),

    path('transactionhistory/create', crudmodel.TransactionHistoryCreate, name='createtransactionhistory'),
    path('transactionhistory/edit/<pk>', crudmodel.TransactionHistoryEdit, name='edittransactionhistory'),
    path('transactionhistory/delete', crudmodel.TransactionHistoryDelete, name='deletetransactionhistory'),
    path('transactionhistory/revenue_invoce/link', ajax.LinkTransactionHistoryToRevenueInvoice, name='link_transactionhistory_revenueinvoice'),

    path('bankaccountinformation/create', crudmodel.BankAccountCreate, name='createbankaccountinformation'),
    path('bankaccountinformation/edit/<pk>', crudmodel.BankAccountEdit, name='editbankaccountinformation'),
    path('bankaccountinformation/delete', crudmodel.BankAccountDelete, name='deletebankaccountinformation'),

    path('depositinfo/ajax/set', ajax.SetDepositInfo, name='set_depositinfo'),






]