# Generated by Django 3.0.6 on 2020-08-26 06:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('case', '0017_auto_20200826_1316'),
    ]

    operations = [
        migrations.AddField(
            model_name='case',
            name='final_revenue_invoice',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='case_finalrevenueinvoice', to='case.Revenue_Invoice'),
        ),
    ]
