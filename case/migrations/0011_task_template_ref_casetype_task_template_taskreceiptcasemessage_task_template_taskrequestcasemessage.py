# Generated by Django 3.0.6 on 2020-08-21 07:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('case', '0010_auto_20200819_1751'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task_Template_TaskType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('taskType', models.CharField(blank=True, max_length=20, null=True)),
                ('tasktemplate', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tasktemplatetasktype_tasktemplate', to='case.Task_Template')),
            ],
            options={
                'db_table': 'task_template_tasktype',
            },
        ),
        migrations.CreateModel(
            name='Task_Template_TaskRequestCaseMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('taskRequestCaseMessage', models.CharField(blank=True, max_length=20, null=True)),
                ('tasktemplate', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tasktemplatetaskrequestcasemessage_tasktemplate', to='case.Task_Template')),
            ],
            options={
                'db_table': 'task_template_task_request_casemessage',
            },
        ),
        migrations.CreateModel(
            name='Task_Template_TaskReceiptCaseMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('taskReceiptCaseMessage', models.CharField(blank=True, max_length=20, null=True)),
                ('tasktemplate', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tasktemplatetaskreceiptcasemessage_tasktemplate', to='case.Task_Template')),
            ],
            options={
                'db_table': 'task_template_task_receipt_casemessage',
            },
        ),
        migrations.CreateModel(
            name='Task_Template_Ref_CaseType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('caseType', models.CharField(blank=True, max_length=20, null=True)),
                ('tasktemplate_ref', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tasktemplaterefcasetype_tasktemplateref', to='case.Task_Template_Ref')),
            ],
            options={
                'db_table': 'task_template_ref_casetype',
            },
        ),
    ]
