from django.forms import ModelForm
from django import forms

from People.models import Client
from application_patent_domestic.forms import ModelFormBase, BasicFormBase
from application_patent_domestic.models import Application_Patent_Domestic_Master
from case.models import Case, Task, Revenue_Invoice, Purchase_Invoice, Transaction_History, BankAccount_Information, \
    Task_Template
from common.codemultilanguage_choice import MakeTagChoiceMultipleLanguage
from management.models import Code_Information, CountryInformation
from message.models import File
from utils.enums import ALL_CODE_TYPE



class CaseModelForm(ModelFormBase):
    def __init__(self, *args, **kwargs):
        super(CaseModelForm, self).__init__(*args, **kwargs)

        if self.labeldic:
            for i, (k,v) in enumerate(self.labeldic.items()):
                self.fields[k].label = v
        if self.customcodechoice:
            self.fields['caseCode'].widget = forms.Select(choices=self.customcodechoice[ALL_CODE_TYPE.Case_CaseType.name])
            self.fields['state'].widget = forms.Select(choices=self.customcodechoice[ALL_CODE_TYPE.Case_CaseState.name])
            #self.fields['caseCode'].choices = self.customcodechoice[ALL_CODE_TYPE.Case_CaseType.name]
            #self.fields['state'].choices = self.customcodechoice[ALL_CODE_TYPE.Case_CaseState.name]
            MakeTagChoiceMultipleLanguage(self)
    class Meta:
        model = Case
        fields = '__all__'
        widgets = {
            'receptionDate': forms.DateInput(attrs={'type': 'date'}),
            'recentCaseDocumentSubmissionDate': forms.DateInput(attrs={'type': 'date'}),
            'recentSubmissionInstructionReceiptDate': forms.DateInput(attrs={'type': 'date'}),
            'recentCompleteReportDate': forms.DateInput(attrs={'type': 'date'}),
            'recentCaseDocumentSubmissionDueDate': forms.DateInput(attrs={'type': 'date'}),
            'initialCaseDocumentSubmissionDueDate': forms.DateInput(attrs={'type': 'date'}),

        }

class Custom_CaseForm(CaseModelForm):
    case_evidence_file = forms.ModelMultipleChoiceField(queryset=File.objects.all(), to_field_name='id', label='사건근거파일',
                                                        required=False)
    isReferencedClient = forms.BooleanField(required=False)
    isReferencedClientManager = forms.BooleanField(required=False)
    isReferencedourCompanyManager = forms.BooleanField(required=False)

class TaskModelForm(ModelFormBase):
    def __init__(self, *args, **kwargs):
        super(TaskModelForm, self).__init__(*args, **kwargs)

        if self.labeldic:
            for i, (k,v) in enumerate(self.labeldic.items()):
                self.fields[k].label = v
        if self.customcodechoice:
            #self.fields['taskCode'].choices = self.customcodechoice[ALL_CODE_TYPE.Task_TaskType.name]
            #self.fields['state'].choices = self.customcodechoice[ALL_CODE_TYPE.Task_TaskState.name]
            self.fields['taskCode'].widget = forms.Select(choices=self.customcodechoice[ALL_CODE_TYPE.Task_TaskType.name])
            self.fields['state'].widget = forms.Select(choices=self.customcodechoice[ALL_CODE_TYPE.Task_TaskState.name])
            MakeTagChoiceMultipleLanguage(self)
    class Meta:
        model = Task
        fields = '__all__'
        widgets = {
            'receptionDate': forms.DateInput(attrs={'type': 'date'}),
            'completionReportingDate': forms.DateInput(attrs={'type': 'date'}),
            'internalCompletionDueDate': forms.DateInput(attrs={'type': 'date'}),
            'completionDueDate': forms.DateInput(attrs={'type': 'date'}),
            'externalServiceRequest': forms.DateInput(attrs={'type': 'date'}),
            'externalServiceResultsReceptionDate': forms.DateInput(attrs={'type': 'date'}),
        }



class RevenueInvoiceModelForm(ModelFormBase):
    def __init__(self, *args, **kwargs):
        super(RevenueInvoiceModelForm, self).__init__(*args, **kwargs)


        if self.labeldic:
            for i, (k,v) in enumerate(self.labeldic.items()):
                self.fields[k].label = v
        if self.customcodechoice:
            self.fields['state'].widget = forms.Select(choices=self.customcodechoice[ALL_CODE_TYPE.Revenue_Invoice_State.name])
            MakeTagChoiceMultipleLanguage(self)
    class Meta:
        model = Revenue_Invoice
        fields = '__all__'
        widgets = {
            'claimDate': forms.DateInput(attrs={'type': 'date'}),
            'taxInvoiceIssueDate': forms.DateInput(attrs={'type': 'date'}),
            'depositDate': forms.DateInput(attrs={'type': 'date'}),
            'paymentCompletionDate': forms.DateInput(attrs={'type': 'date'}),
        }

class PurchaseInvoiceModelForm(ModelFormBase):
    def __init__(self, *args, **kwargs):
        super(PurchaseInvoiceModelForm, self).__init__(*args, **kwargs)

        if self.labeldic:
            for i, (k,v) in enumerate(self.labeldic.items()):
                self.fields[k].label = v
        if self.customcodechoice:
            self.fields['state'].widget = forms.Select(choices=self.customcodechoice[ALL_CODE_TYPE.Purchase_Invoice_State.name])
            MakeTagChoiceMultipleLanguage(self)
    class Meta:
        model = Purchase_Invoice
        fields = '__all__'
        widgets = {
            'claimDate': forms.DateInput(attrs={'type': 'date'}),
            'invoiceReceptionDate': forms.DateInput(attrs={'type': 'date'}),
            'depositDate': forms.DateInput(attrs={'type': 'date'}),
            'taxInvoiceIssueDate': forms.DateInput(attrs={'type': 'date'}),
            'paymentCompletionDate': forms.DateInput(attrs={'type': 'date'}),
        }

class TransactionHistoryModelForm(ModelFormBase):
    def __init__(self, *args, **kwargs):

        super(TransactionHistoryModelForm, self).__init__(*args, **kwargs)

        if self.labeldic:
            for i, (k,v) in enumerate(self.labeldic.items()):
                self.fields[k].label = v
        if self.customcodechoice:
            MakeTagChoiceMultipleLanguage(self)
    class Meta:
        model = Transaction_History
        fields = '__all__'
        widgets = {
            'transactionDate': forms.DateInput(attrs={'type': 'date'}),
        }

class BankAccountInformationModelForm(ModelFormBase):
    def __init__(self, *args, **kwargs):
        super(BankAccountInformationModelForm, self).__init__(*args, **kwargs)

        if self.labeldic:
            for i, (k,v) in enumerate(self.labeldic.items()):
                self.fields[k].label = v
        if self.customcodechoice:
            MakeTagChoiceMultipleLanguage(self)
    class Meta:
        model = BankAccount_Information
        fields = '__all__'



class TaskTemplateRefForm(BasicFormBase):
    patentType = forms.ModelChoiceField(queryset=Code_Information.objects.filter(codeType='PatentType'),
                                                to_field_name='id', required=True)
    nationType = forms.ModelChoiceField(queryset=Code_Information.objects.filter(codeType='PatentNationType'),
                                                to_field_name='id', required=True)
    countryCode = forms.ModelChoiceField(queryset=CountryInformation.objects.all(), to_field_name='id', required=False)
    caseCode = forms.ModelChoiceField(queryset=Code_Information.objects.filter(codeType='Case_CaseType'),
                                              to_field_name='id', required=True)

    client = forms.ModelChoiceField(queryset=Client.objects.filter(client_clienttype__typeName='OverseaAgent'),
                                      to_field_name='id', required=False)
    isSpecialTemplate = forms.ChoiceField(required=True, choices=[('nor', '일반'), ('spc', '특수')])
    specialTemplateName = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(TaskTemplateRefForm, self).__init__(*args, **kwargs)
        if self.labeldic:
            for key, value in self.fields.items():
                if key in self.labeldic.keys():
                    self.fields[key].label = self.labeldic[key]
            """       
            self.fields['countryCode'].label = self.labeldic['countryCode']
            self.fields['patentType'].label = self.labeldic['patentType']
            self.fields['nationType'].label = self.labeldic['nationType']
            self.fields['caseCode'].label = self.labeldic['caseCode']
            self.fields['taskCode'].label = self.labeldic['taskCode']
            self.fields['client'].label = self.labeldic['client']
            self.fields['specialTemplateName'].label = self.labeldic['specialTemplateName']
            """


        if self.customcodechoice:
            self.fields['countryCode'].choices = self.customcodechoice['countryCode']
            self.fields['patentType'].choices = self.customcodechoice[ALL_CODE_TYPE.PatentType.name]
            self.fields['nationType'].choices = self.customcodechoice[ALL_CODE_TYPE.PatentNationType.name]
            self.fields['caseCode'].choices = self.customcodechoice[ALL_CODE_TYPE.Case_CaseType.name]

            newchioce = []
            for i in self.fields['client'].choices:
                newchioce.append(i)
            newchioce.insert(1, ('na', '일반고객'))
            self.fields['client'].choices = newchioce





class CustomTaskTemplateForm(BasicFormBase):
    taskCode = forms.ModelMultipleChoiceField(queryset=Code_Information.objects.filter(codeType='Task_TaskType'),
                                              to_field_name='id', required=False)
    def __init__(self, *args, **kwargs):
        super(CustomTaskTemplateForm, self).__init__(*args, **kwargs)
        if self.labeldic:
            self.fields['taskCode'].label = self.labeldic['taskCode']
        if self.customcodechoice:
            self.fields['taskCode'].choices = self.customcodechoice[ALL_CODE_TYPE.Task_TaskType.name]

class TaskTemplateModelForm(ModelFormBase):
    def __init__(self, *args, **kwargs):
        super(TaskTemplateModelForm, self).__init__(*args, **kwargs)

        if self.labeldic:
            for i, (k,v) in enumerate(self.labeldic.items()):
                self.fields[k].label = v
        if self.customcodechoice:
            MakeTagChoiceMultipleLanguage(self)
    class Meta:
        model = Task_Template
        fields = ['tasktemplate_ref','child']