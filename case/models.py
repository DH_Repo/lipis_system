from django.core.exceptions import ValidationError
from django.db import models
# Create your models here.
from management.models import BaseModel
from utils.enums import INVOICE_TYPE, CASE_TASK_TYPE, EXTERNALSERVICE_MESSAGE_TYPE, \
    MakeCurrencyEnum, TASK_PROCEDURE_TYPE, CASE_TYPE, TASK_TYPE, CASE_STATE, TASK_STATE, REVENUE_INVOICE_STATE, \
    PURCHASE_INVOICE_STATE, MESSAGE_CASETASK_TYPE


class Case(BaseModel):

    master = models.ManyToManyField(
        'application_patent_domestic.Application_Patent_Domestic_Master',
        through='application_patent_domestic.Application_Patent_Domestic_Master_Case',
        related_name='case_master', blank=True
    )


    caseNumber = models.CharField(max_length=50, null=True, blank=True)
    caseCode = models.CharField(max_length=50, null=True, blank=True)
    state = models.CharField(max_length=30, null=True, blank=True)
    receptionDate = models.DateTimeField(null=True, blank=True)
    isNeedSendReport = models.BooleanField(null=True, blank=True)
    isNeedSubmissionReport = models.BooleanField(null=True, blank=True)
    recentCaseDocumentSubmissionDate = models.DateTimeField(null=True, blank=True)
    initialCaseDocumentSubmissionDueDate = models.DateTimeField(null=True, blank=True)
    recentCaseDocumentSubmissionDueDate = models.DateTimeField(null=True, blank=True)
    recentSubmissionInstructionReceiptDate = models.DateTimeField(null=True, blank=True)
    recentCompleteReportDate = models.DateTimeField(null=True, blank=True)

    case_client = models.ManyToManyField(
        'People.Client',
        through='Case_Task_Invoice_Management',
        related_name='case_client', blank=True
    )
    case_clientmanagement = models.ManyToManyField(
        'People.Person',
        through='Case_Task_Invoice_Management',
        related_name='case_client_manager', blank=True
    )
    case_ourcompanymanagement = models.ManyToManyField(
        'People.Person',
        through='Case_OurCompanyManagement',
        related_name='case_ourcompanymanager', blank=True
    )
    task = models.ManyToManyField(
        'Task',
        through='Case_Task',
        related_name='case_task', blank=True
    )
    message = models.ManyToManyField(
        'message.Message',
        through='Case_Task_Message',
        related_name='case_message', blank=True
    )

    final_revenue_invoice = models.ForeignKey('Revenue_Invoice', null=True, blank=True, on_delete=models.SET_NULL,
                              related_name='case_finalrevenueinvoice')
    def __str__(self):
        if self.caseNumber:
            return self.caseNumber
        else:
            return 'no case number. case id is {}'.format(self.id)


    class Meta:
        db_table = "case"

class Task(BaseModel):

    master = models.ManyToManyField(
        'application_patent_domestic.Application_Patent_Domestic_Master',
            through='application_patent_domestic.Application_Patent_Domestic_Master_Task',
        related_name='task_master', blank=True
    )

    taskNumber = models.CharField(max_length=50, null=True, blank=True)
    taskCode = models.CharField(max_length=50, null=True, blank=True)
    state = models.CharField(max_length=30, null=True, blank=True)
    receptionDate = models.DateTimeField(null=True, blank=True)
    completionReportingDate = models.DateTimeField(null=True, blank=True)
    neededCompletionReporting = models.BooleanField(null=True, blank=True)
    isChargedExpense = models.BooleanField(null=True, blank=True)
    internalCompletionDueDate = models.DateTimeField(null=True, blank=True)
    completionDueDate = models.DateTimeField(null=True, blank=True)
    chargedExpense = models.CharField(max_length=50, null=True, blank=True)
    chargedExpenseCurrency = models.CharField(max_length=50, null=True, blank=True)
    externalServiceRequest = models.DateTimeField(null=True, blank=True)
    externalServiceResultsReceptionDate = models.DateTimeField(null=True, blank=True)
    externalServicePrice = models.CharField(max_length=50, null=True, blank=True)
    externalServicePriceCurrency = models.CharField(max_length=50, null=True, blank=True)
    task_client = models.ManyToManyField('People.Client', through='Case_Task_Invoice_Management', related_name='task_client', blank=True)
    task_clientmanagement = models.ManyToManyField('People.Person', through='Case_Task_Invoice_Management', related_name='task_clientmanagement', blank=True)
    externalservice_client = models.ManyToManyField('People.Client', through='Task_ExternalService', related_name='task_externalservice_client', blank=True)
    externalservice_clientmanagement = models.ManyToManyField('People.Person', through='Task_ExternalService',
                                                    related_name='task_externalservice_clientmanagement', blank=True)

    externalService_message = models.ManyToManyField('message.Message', through='Task_ExternalService_Message', related_name='task_externalservice_message',blank=True)
    purchase_invoice = models.ForeignKey('Purchase_Invoice', null=True, blank=True, related_name='task_purchase_invoice', on_delete=models.SET_NULL)
    case = models.ManyToManyField(
        'Case',
        through='Case_Task',
        related_name='task_case', blank=True
    )
    child = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL,
                              related_name='task_child')
    def __str__(self):
        if self.taskNumber:
            return self.taskNumber
        else:
            return self.taskCode
    class Meta:
        db_table = "task"


class Case_Task(models.Model):
    case = models.ForeignKey('Case', blank=False, related_name='case_task_case', on_delete=models.CASCADE)
    task = models.ForeignKey('Task', blank=False, related_name='case_task_task', on_delete=models.CASCADE)
    class Meta:
        db_table = "case_task"



class Revenue_Invoice(BaseModel):
    ourCompanyNumber = models.CharField(max_length=50, null=True, blank=True)
    ourCompanyClaimNumber = models.CharField(max_length=50, null=True, blank=True)
    state = models.CharField(max_length=30, null=True, blank=True)
    case = models.ForeignKey('Case', on_delete=models.SET_NULL, related_name='case_revenueinvoice', null=True, blank=True)
    claimDate = models.DateTimeField(null=True, blank=True)
    totalCharges = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    chargesCurrency = models.CharField(max_length=50, null=True, blank=True, choices=[(tag.value, tag.value) for tag in MakeCurrencyEnum()])
    ourCompanyCharges = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    vat = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    taxInvoiceIssueDate = models.DateTimeField(null=True, blank=True)
    depositDate = models.DateTimeField(null=True, blank=True)
    totalDepositAmount = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    totalDepositAmountCurrency = models.CharField(max_length=50, null=True, blank=True,choices=[(tag.value, tag.value) for tag in MakeCurrencyEnum()])
    paymentCompletionDate = models.DateTimeField(null=True, blank=True)
    revenue_invoice_client = models.ManyToManyField('People.Client', through='Case_Task_Invoice_Management',
                                         related_name='revenue_invoice_client')
    revenue_invoice_clientmanagement = models.ManyToManyField('People.Person', through='Case_Task_Invoice_Management',
                                                   related_name='revenue_invoice_clientmanagement')
    revenue_deposit_info = models.ManyToManyField('Transaction_History', through='Deposit_List',
                                                   related_name='revenue_deposit_list', blank=True)
    class Meta:
        db_table = "revenue_invoice"

class Purchase_Invoice(BaseModel):
    purchaseCompanyClaimNumber = models.CharField(max_length=50, null=True, blank=True)
    state = models.CharField(max_length=30, null=True, blank=True)
    claimDate = models.DateTimeField(null=True, blank=True)
    invoiceReceptionDate = models.DateTimeField(null=True, blank=True)
    totalCharges = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    chargesCurrency = models.CharField(max_length=50, null=True, blank=True, choices=[(tag.value, tag.value) for tag in MakeCurrencyEnum()])
    vat = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    taxInvoiceIssueDate = models.DateTimeField(null=True, blank=True)
    depositDate = models.DateTimeField(null=True, blank=True)
    totalDepositAmount = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    totalDepositAmountCurrency = models.CharField(max_length=50, null=True, blank=True, choices=[(tag.value, tag.value) for tag in MakeCurrencyEnum()])
    paymentCompletionDate = models.DateTimeField(null=True, blank=True)
    purchase_invoice_client = models.ManyToManyField('People.Client', through='Case_Task_Invoice_Management',
                                         related_name='purchase_invoice_client')
    purchase_invoice_clientmanagement = models.ManyToManyField('People.Person', through='Case_Task_Invoice_Management',
                                                   related_name='purchase_invoice_clientmanagement')
    purchase_deposit_info = models.ManyToManyField('Transaction_History', through='Deposit_List',
                                                  related_name='purchase_deposit_list', blank=True)

    class Meta:
        db_table = "purchase_invoice"

class Case_Task_Invoice_Management(models.Model):
    case = models.ForeignKey('Case', on_delete=models.CASCADE, related_name='case_task_invoice_management_case',null=True, blank=True)
    task = models.ForeignKey('Task', on_delete=models.CASCADE, related_name='case_task_invoice_management_task',null=True, blank=True)
    revenue_invoice = models.ForeignKey('Revenue_Invoice', on_delete=models.CASCADE, related_name='case_task_invoice_management_revenue_invoice',null=True, blank=True)
    purchase_invoice = models.ForeignKey('Purchase_Invoice', on_delete=models.CASCADE, related_name='case_task_invoice_management_purchase_invoice',null=True, blank=True)
    client = models.ForeignKey('People.Client', on_delete=models.SET_NULL, related_name='case_task_invoice_management_client',null=True, blank=True)
    clientmanagement = models.ForeignKey('People.Person', on_delete=models.SET_NULL,
                               related_name='case_task_invoice_management_clientmanagement',null=True, blank=True)
    priority = models.IntegerField(null=True, blank=True)
    isReferencedpatent_client = models.BooleanField(null=True, blank=True, default='')
    isReferencedpatent_clientManager = models.BooleanField(null=True, blank=True, default='')

    def clean(self):
        if self.case and self.task:
            raise ValidationError('validation error')
        if self.task and self.revenue_invoice:
            raise ValidationError('validation error')
        if self.revenue_invoice and self.purchase_invoice:
            raise ValidationError('validation error')
        if self.revenue_invoice and self.case:
            raise ValidationError('validation error')
        if self.purchase_invoice and self.case:
            raise ValidationError('validation error')
        if self.task and self.purchase_invoice:
            raise ValidationError('validation error')
    class Meta:
        db_table = "case_task_invoice_management"

class Case_OurCompanyManagement(models.Model):
    case = models.ForeignKey('Case', on_delete=models.CASCADE, related_name='case_person_managerofourcompany')
    managerOfOurCompany = models.ForeignKey('People.Person', on_delete=models.CASCADE, related_name='case_person_managerofourcompany', null=True, blank=True)
    priority = models.IntegerField(null=True, blank=True)
    isReferencedpatent = models.BooleanField(null=True, blank=True, default=False)
    class Meta:
        db_table = "case_ourcompanymanagement"





class Task_ExternalService(models.Model):
    task = models.ForeignKey('Task', on_delete=models.CASCADE, related_name='task_externalservice_task')
    externalservice_client = models.ForeignKey('People.Client', on_delete=models.SET_NULL,
                                         related_name='task_externalservice_task_client', null=True, blank=True)
    externalservice_manager = models.ForeignKey('People.Person', on_delete=models.SET_NULL,
                                               related_name='task_externalservice_task_manager', null=True, blank=True)
    class Meta:
        db_table = "task_externalservice"

class Task_ExternalService_Message(models.Model):
    message = models.ForeignKey('message.Message', on_delete=models.CASCADE, related_name='task_externalservice_message_message')
    task = models.ForeignKey('Task', on_delete=models.CASCADE,
                                         related_name='task_externalservice_message_task', null=True, blank=True)
    messageType = models.CharField(max_length=50, null=True, blank=True,
                                      choices=[(tag.name, tag.value) for tag in EXTERNALSERVICE_MESSAGE_TYPE])
    class Meta:
        db_table = "task_externalservice_message"

class Case_Task_Message(models.Model):
    case_task_Type = models.CharField(max_length=50, null=True, blank=True, choices=[(tag.name, tag.value) for tag in MESSAGE_CASETASK_TYPE])
    message = models.ForeignKey('message.Message', on_delete=models.CASCADE, related_name='case_task_message_message')
    case = models.ForeignKey('Case', on_delete=models.CASCADE, related_name='case_task_message_case',null=True, blank=True)
    task = models.ForeignKey('Task', on_delete=models.CASCADE,
                                        related_name='case_task_message_task',null=True, blank=True)
    file = models.ManyToManyField('message.File', through='Case_Message_File'
                                  , related_name='case_message_file', blank=True)
    class Meta:
        db_table = "case_task_message"

class Case_Message_File(models.Model):
    case_message = models.ForeignKey(
        'Case_Task_Message'
        , on_delete=models.CASCADE, related_name='case_message_file_case_message')
    file = models.ForeignKey('message.File', on_delete=models.CASCADE, related_name='case_message_file_file')
    docuCode = models.CharField(max_length=30, blank=True, null=True)
    class Meta:
        db_table = "case_message_file"



class Invoice_Message(models.Model):
    invoiceType = models.CharField(max_length=50, null=True, blank=True, choices=[(tag.name, tag.value) for tag in INVOICE_TYPE])
    message = models.ForeignKey('message.Message', on_delete=models.CASCADE, related_name='invoice_message_message')
    revenue_invoice = models.ForeignKey('Revenue_Invoice', on_delete=models.CASCADE, related_name='invoice_message_revenue_invoice',null=True, blank=True)
    purchase_invoice = models.ForeignKey('Purchase_Invoice', on_delete=models.CASCADE,
                                        related_name='invoice_message_purchase_invoice',null=True, blank=True)
    class Meta:
        db_table = "invoice_message"

class Transaction_History(models.Model):
    order = models.IntegerField(null=True, blank=True)
    transactionDate = models.DateTimeField(null=True,blank=True)
    breifs = models.CharField(max_length=100,null=True,blank=True)
    currency = models.CharField(max_length=50,null=True,blank=True, choices=[(tag.value, tag.value) for tag in MakeCurrencyEnum()])
    depositAmount = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    withdrawalAmount = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    balance = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    type = models.CharField(max_length=50,null=True,blank=True)
    transactionOffice = models.CharField(max_length=50,null=True,blank=True)
    recipient = models.CharField(max_length=100, null=True, blank=True)
    client_receiver = models.ManyToManyField('BankAccount_Information', through='Client_Receiver_AccountInfo'
                                             ,related_name='transactionhistory_client_recevier_account', blank=True)
    revenue_deposit_info = models.ManyToManyField('Revenue_Invoice', through='Deposit_List',
                                                   related_name='transactionhistory_revenue_deposit_info', blank=True)
    class Meta:
        db_table = "transaction_history"

class BankAccount_Information(models.Model):
    bankName = models.CharField(max_length=50, null=True, blank=True)
    accountNumber = models.CharField(max_length=50, null=True, blank=True)
    depositorName = models.CharField(max_length=20, null=True, blank=True)
    isClientInfo = models.BooleanField(null=False, blank=False, default=True)

    def __str__(self):
        return self.depositorName
    class Meta:
        db_table = "bankaccount_information"


class Client_Receiver_AccountInfo(models.Model):
    accountinfo = models.ForeignKey('BankAccount_Information', on_delete=models.CASCADE, related_name='client_receiver_accountinfo_accountinfo')
    transactionhistory = models.ForeignKey('Transaction_History', on_delete=models.CASCADE, related_name='client_receiver_accountinfo_transactionhistory')
    isReceiver = models.BooleanField(null=False,blank=False,default=True)
    class Meta:
        db_table = "client_receiver_accountinfo"

class Deposit_List(models.Model):
    transaction = models.ForeignKey('Transaction_History', on_delete=models.CASCADE, related_name='depositlist_transaction')
    revenue_invoice = models.ForeignKey('Revenue_Invoice', on_delete=models.CASCADE,
                                    related_name='depositlist_revenue_invoice', null=True, blank=True)
    purchase_invoice = models.ForeignKey('Purchase_Invoice', on_delete=models.CASCADE,
                                    related_name='depositlist_purchase_invoice', null=True, blank=True)
    connectedAmount = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    connectedCurrency = models.CharField(max_length=50, null=True, blank=True,
                                choices=[(tag.value, tag.value) for tag in MakeCurrencyEnum()])
    class Meta:
        db_table = "deposit_list"







class Task_Template_Ref(BaseModel):
    patentType = models.CharField(max_length=20, null=True, blank=True)
    patentNationType = models.CharField(max_length=20, null=True, blank=True)
    countryCode = models.CharField(max_length=20, null=True, blank=True)
    caseType = models.CharField(max_length=20, null=True, blank=True)
    isGeneralClient = models.BooleanField(null=False, blank=False, default=True)
    isGeneralTemplate = models.BooleanField(null=False, blank=False, default=True)
    specialTemplateName = models.CharField(max_length=30, null=True, blank=True)
    client = models.ForeignKey('People.Client', null=True, blank=True, related_name='tasktemlpateref_client', on_delete=models.SET_NULL)

    def __str__(self):
        if self.countryCode:
            return self.patentType+self.patentNationType+self.countryCode+self.caseType
        else:
            return self.patentType + self.patentNationType + self.caseType
    class Meta:
        db_table = "task_template_ref"

class Task_Template(BaseModel):
    taskCode = models.CharField(max_length=20, null=True, blank=True)
    tasktemplate_ref = models.ForeignKey('Task_Template_Ref', null=True, blank=True, on_delete=models.CASCADE,
                                         related_name='tasktemplate_tasktemplateref')
    child = models.ForeignKey('Task_Template', null=True, blank=True, on_delete=models.SET_NULL,
                                         related_name='tasktemplate_child')
    def __str__(self):
        return self.taskCode
    class Meta:
        db_table = "task_template"

class Task_Template_Ref_MessageType(models.Model):
    messageType = models.CharField(max_length=20, null=True, blank=True)

    tasktemplate_ref = models.ForeignKey('Task_Template_Ref', null=True, blank=True, on_delete=models.CASCADE,
                                         related_name='tasktemplaterefmessagetype_tasktemplateref')

    class Meta:
        db_table = "task_template_ref_messagetype"

class Task_Template_Ref_CaseState(models.Model):
    caseState = models.CharField(max_length=20, null=True, blank=True)

    tasktemplate_ref = models.ForeignKey('Task_Template_Ref', null=True, blank=True, on_delete=models.CASCADE,
                                         related_name='tasktemplaterefcasestate_tasktemplateref')

    class Meta:
        db_table = "task_template_ref_casestate"

class Task_Template_Ref_CaseStateChangeCondition(models.Model):
    messageType = models.CharField(max_length=20, null=True, blank=True)
    caseState = models.CharField(max_length=20, null=True, blank=True)

    tasktemplate_ref = models.ForeignKey('Task_Template_Ref', null=True, blank=True, on_delete=models.CASCADE,
                                         related_name='tasktemplaterefcasestatechangecondition_tasktemplateref')

    class Meta:
        db_table = "task_template_ref_casestatechangecondition"



class Task_Template_TaskState(models.Model):
    taskState = models.CharField(max_length=20, null=True, blank=True)

    tasktemplate = models.ForeignKey('Task_Template', null=True, blank=True, on_delete=models.CASCADE,
                                         related_name='tasktemplatetaskstate_tasktemplate')

    class Meta:
        db_table = "task_template_taskstate"

class Task_Template_TaskRequestCaseMessage(models.Model):
    taskRequestCaseMessage = models.CharField(max_length=20, null=True, blank=True)

    tasktemplate = models.ForeignKey('Task_Template', null=True, blank=True, on_delete=models.CASCADE,
                                         related_name='tasktemplatetaskrequestcasemessage_tasktemplate')

    class Meta:
        db_table = "task_template_task_request_casemessage"

class Task_Template_TaskReceiptCaseMessage(models.Model):
    taskReceiptCaseMessage = models.CharField(max_length=20, null=True, blank=True)

    tasktemplate = models.ForeignKey('Task_Template', null=True, blank=True, on_delete=models.CASCADE,
                                         related_name='tasktemplatetaskreceiptcasemessage_tasktemplate')

    class Meta:
        db_table = "task_template_task_receipt_casemessage"