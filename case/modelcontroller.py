from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from case.models import Case, Task
from message.models import Message

@csrf_exempt
@login_required(login_url='People:login')
def LinkCaseOrTaskToMessage(request):
    if request.method == 'POST':
        msgid = request.POST.get('msgid')
        objid = request.POST.get('objid')
        mobj = Message.objects.get(id=msgid)

        func = request.POST.get('func')
        type = request.POST.get('type')
        if type == 'case':
            cmobj = Case.objects.get(id=objid)
            if func == 'link':
                mobj.case.add(cmobj)
            else:
                mobj.case.remove(cmobj)
        else:
            obj = Task.objects.get(id=objid)
            if func == 'link':
                mobj.task.add(obj)
            else:
                mobj.task.remove(obj)
        mobj.save()

    return HttpResponse('')