import ast
import json
import re

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Prefetch, Q
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.views.generic import TemplateView
from iso4217 import Currency

from Lipis_system_master.views import HomeMixin
from application_patent_domestic.models import Application_Patent_Domestic_Master
from case.forms import CaseModelForm, TaskModelForm
from case.models import *
from common.codemultilanguage_choice import GetCodeMultpleLanguageForCase, GetCodeMultpleLanguageForTask, \
    GetCodeInfoMultipleLanguage_all, GetCodeMultpleLanguageForPatentMaster
from common.tableview_function import get_all_model_context_for_table, get_model_query_context_for_table_search_page, \
    get_model_query_context_for_table_nopage_nosearch, get_model_query_context_for_table_page_nosearch, get_field_multilang
from emailsystem.models import Email_Detail
from message.models import Message, File, Patent_Document, Patent_Document_Attachment
from utils.api.api import callapi_readAppHistInfo
from utils.db_fieldset import get_fielddic, get_feild_names
from utils.enums import ALL_CODE_TYPE


def check_email_format(email):
    # pass the regualar expression
    # and the string in search() method
    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    if (re.search(regex, email)):
        return True

    else:
        return False
def test(request):

    ## prefetch 는 1:N에서 1의 상태에서 __set으로 조회할 수 있다.
    ## manytomany에서는 manytomany field가 없을 때 __set으로 조회하나 있으면 해당 필드명으로 접근하면 됨.
    ## https://docs.djangoproject.com/en/3.0/ref/models/querysets/ 참고
    """ 다수 prefetch_related 예시

        a = ProductPrices.objects.filter(country=country_obj)
    prefetch1 = Prefetch(
        'product_prices',
        queryset=a,
        to_attr='price_info'
    )

    b = ProductMeta.objects.filter(language=lang)
    prefetch2 = Prefetch(
        'product_meta',
        queryset=b,
        to_attr='meta_info'
    )

    products = products.prefetch_related(prefetch1, prefetch2)
    products = products.filter(id__in=a.values_list('product_id'))
    products = products.filter(id__in=b.values_list('product_id'))
    """
    #file = File.objects.first()
    #baselocation = file.path.storage.base_location
    #dir = file.path.path

    #File.objects.filter(email__emailType=EMAILTYPE.FromServer.name).update(place=FILETYPE.sync_email.name)

    # Task_Procedure_Template 테이블을 참조하고 있는 테이블의 relate 이름으로 prefetch queryset 생성.
    # => 참조하고 있는 테이블 접근 가능

    # Task_Procedure_Template에서 참조하는 related 이름과 Task_Procedure_Template queryset으로 prefetched object 생성.
    # Task_Procedure_Template에서 참조하는 테이블에서 Task_Procedure_Template 접근 하도록.
    # prefetch_related 의 파라미터 이름으로 root를 참조하는 테이블을 조회하는 queryset 리스트를 얻을 수 있다.
    # relatedname이 없을때 테이블이름(소문자?)_set을 쓰는 방법 있음.
    # prefetch_related 파라미터에서 root를 참조하는 테이블 및 그 테이블의 다른 항목까지 접근할수도 있고 __id 같이 쓰는 방법 있음.

    qs = Task_Procedure_Template.objects.prefetch_related('template_parent')
    prefetch1 = Prefetch(
        'task_procedure_template_procedure',
        queryset=qs,
        to_attr='tptp'
    )
    b = Task_Procedure.objects.filter(id=1).prefetch_related(prefetch1)
    e = b.first().tptp
    for l in e:
        for fil in l.template_parent.all():
            print(str(fil.parent.id) +'//' + str(fil.child.id))

    return render(request, 'management/commoninfo_form.html')


@login_required(login_url='People:login')
def CreateCaseWithMessage(request, pk):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Case')
    if request.method == 'POST':
        form = CaseModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            casemodel = form.save(commit=False)

            msgpk = request.POST['msgid']
            message = get_object_or_404(Message, id=msgpk)
            ThroughModel = message.case.through()
            ThroughModel.message = message
            ThroughModel.case = casemodel
            casetype = request.POST['casetype']
            ctype = [cttype for cttype in CASE_TASK_TYPE if cttype.value == casetype]
            ThroughModel.case_task_Type = ctype[0]
            casemodel.save()
            ThroughModel.save()

            return HttpResponse('<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeMultpleLanguageForCase(request)
        form = CaseModelForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)
        casetype = [CASE_TASK_TYPE.CaseReception, CASE_TASK_TYPE.CaseSubmissionInstruction]
    return render(request, 'case/add_case_form.html', {'form': form, 'msgid': pk,'casetype': casetype})

@login_required(login_url='People:login')
def CreateTaskWithMessage(request, pk):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Task')
    if request.method == 'POST':
        form = TaskModelForm(request.POST, labeldic=None, customcodechoice=None, request=request)
        if form.is_valid():
            taskmodel = form.save(commit=False)
            msgpk = request.POST['msgid']
            message = get_object_or_404(Message, id=msgpk)
            ThroughModel = message.task.through()
            ThroughModel.message = message
            ThroughModel.task = taskmodel
            tasktype = request.POST['tasktype']
            ttype = [cttype for cttype in CASE_TASK_TYPE if cttype.value == tasktype]
            ThroughModel.case_task_Type = ttype[0]
            taskmodel.save()
            ThroughModel.save()

            return HttpResponse('<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeMultpleLanguageForTask(request)
        form = TaskModelForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)
        tasktype = [CASE_TASK_TYPE.TaskReport, CASE_TASK_TYPE.TaskReception]
    return render(request, 'case/add_task_form.html', {'form': form, 'msgid': pk, 'tasktype':tasktype})


class CaseView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'case/cases.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(CaseView, self).get_context_data(**kwargs)
        casestate = self.request.GET.get('casestate', None)
        if not casestate and 'casestate' in self.request.session:
            casestate = self.request.session['casestate']
        elif not casestate and 'casestate' not in self.request.session:
            casestate = None
        elif casestate:
            self.request.session['casestate'] = casestate
        if casestate:
            context['caseState'] = casestate
            if casestate == 'progress':
                query = Case.objects.filter(~Q(state='CMPL')).order_by('-recentCaseDocumentSubmissionDate')
            elif casestate == 'complete':
                query = Case.objects.filter(Q(state='CMPL')).order_by('-recentCaseDocumentSubmissionDate')
            elif casestate == 'all':
                query = Case.objects.all().order_by('-recentCaseDocumentSubmissionDate')
            get_model_query_context_for_table_page_nosearch(self, Case, 'Case', context, query)
        else:
            context = get_model_query_context_for_table_search_page(self, Case,
                                                       'Case',
                                                       'Case', context, Case.objects.all().order_by('-recentCaseDocumentSubmissionDate'))
        context['revenue_invoice'] = Revenue_Invoice.objects.all()
        context['case_code_multiplelang'] = GetCodeMultpleLanguageForCase(self.request)
        context['ptype_code_multiplelang'] = GetCodeMultpleLanguageForPatentMaster(self.request)[ALL_CODE_TYPE.PatentType.name]
        context['ntype_code_multiplelang'] = GetCodeMultpleLanguageForPatentMaster(self.request)[ALL_CODE_TYPE.PatentNationType.name]
        return context

class CaseDetailView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'case/case_detail.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(CaseDetailView, self).get_context_data(**kwargs)

        return context
    def get(self, request, pk):
        context = self.get_context_data()
        case = Case.objects.get(id=pk)
        context['case'] = case
        get_model_query_context_for_table_nopage_nosearch(self, Revenue_Invoice, 'Revenue_Invoice',
                                                          context, Revenue_Invoice.objects.all(), 'revenue_invoice')
        get_model_query_context_for_table_nopage_nosearch(self, Task, 'Task',
                                                          context, case.task.all())
        get_model_query_context_for_table_nopage_nosearch(self, Message, 'Message',
                                                          context, case.message.all(), 'case_message')
        context['email_multilang'] = get_field_multilang(self, Email_Detail, 'Email_Detail')
        context['patent_document_multilang'] = get_field_multilang(self, Patent_Document, 'Patent_Document')
        context['patent_document_attach_multilang'] = get_field_multilang(self, Patent_Document_Attachment, 'Patent_Document_Attachment')
        context['task_type_multiplelang'] = GetCodeMultpleLanguageForTask(self.request)[ALL_CODE_TYPE.Task_TaskType.name]
        context['case_code_multiplelang'] = GetCodeMultpleLanguageForCase(self.request)


        return self.render_to_response(context)

class TaskView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'case/tasks.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(TaskView, self).get_context_data(**kwargs)
        context = get_all_model_context_for_table(self, Task,
                                                    'Task',
                                                    'Task', context)
        context['task_code_multiplelang'] = GetCodeMultpleLanguageForTask(self.request)
        return context

class RevenuInvoiceView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'case/revenue_invoices.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(RevenuInvoiceView, self).get_context_data(**kwargs)
        context = get_all_model_context_for_table(self, Revenue_Invoice,
                                                    'Revenue_Invoice',
                                                    'Revenue_Invoice', context)
        return context

class PurchaseInvoiceView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'case/purchase_invoices.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(PurchaseInvoiceView, self).get_context_data(**kwargs)
        context = get_all_model_context_for_table(self, Purchase_Invoice,
                                                    'Purchase_Invoice',
                                                    'Purchase_Invoice', context)
        return context

class TransactionHistoryView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'case/transactionhistory.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(TransactionHistoryView, self).get_context_data(**kwargs)
        qs = Transaction_History.objects.order_by('transactionDate')
        context = get_model_query_context_for_table_search_page(self, Transaction_History,
                                                    'Transaction_History',
                                                    'Transaction_History', context, qs)
        return context

class TransactionHistoryDetailView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'case/transactionhistory_detail.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(TransactionHistoryDetailView, self).get_context_data(**kwargs)

        return context
    def get(self, request, pk):
        context = self.get_context_data()
        ourcom = self.request.GET.get('ourcomno', None)
        if ourcom:
            language = self.request.session['django_language']
            context['ourcom'] = ourcom
            context['patent'] = Application_Patent_Domestic_Master.objects.filter(ourCompanyNumber=ourcom)
            context['masterfield_multiplelang'] = get_feild_names(language, 'Application_Patent_Domestic_Master',
                                                                  Application_Patent_Domestic_Master)

        context['tranid'] = pk
        dp_all = Transaction_History.objects.get(id=pk).depositlist_transaction.all()

        context = get_model_query_context_for_table_nopage_nosearch(self, Revenue_Invoice,
                                                             'Revenue_Invoice', context, Transaction_History.objects.get(id=pk).revenue_deposit_info.all(), 'last_revenue_list')
        context['deposit_info_list'] = dp_all
        context['currency'] = list(MakeCurrencyEnum())
        sum = 0
        dp_cur = ''
        for dp in dp_all:
            sum += dp.connectedAmount
            dp_cur = dp.connectedCurrency
        context['deposit_amount'] = sum
        context['deposit_amount_currency'] = dp_cur
        context['revenue_invoice_multilang'] = get_field_multilang(self, Revenue_Invoice, 'Revenue_Invoice')
        context['patent_multilang'] = get_field_multilang(self, Application_Patent_Domestic_Master, 'Application_Patent_Domestic_Master')

        return self.render_to_response(context)
    def post(self, request):
        context = self.get_context_data()

        return self.render_to_response(context)


class BankAccountInformationView(LoginRequiredMixin, HomeMixin, TemplateView):
    template_name = 'case/bankaccountinformation.html'
    login_url = 'People:login'
    def get_context_data(self, **kwargs):
        context = super(BankAccountInformationView, self).get_context_data(**kwargs)
        context = get_all_model_context_for_table(self, BankAccount_Information,
                                                    'BankAccount_Information',
                                                    'BankAccount_Information', context)
        return context