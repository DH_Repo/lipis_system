import datetime

from django.contrib.auth.decorators import login_required
from django.forms import forms
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt

from application_patent_domestic.models import Application_Patent_Domestic_Master
from case.forms import CaseModelForm, TaskModelForm, RevenueInvoiceModelForm, PurchaseInvoiceModelForm, TransactionHistoryModelForm, \
    BankAccountInformationModelForm, Custom_CaseForm
from case.models import Case, Task, Revenue_Invoice, Purchase_Invoice, Transaction_History, BankAccount_Information, \
    Task_Template, Case_Task
from common.codemultilanguage_choice import GetCodeMultpleLanguageForPatentMaster, \
    GetCodeMultpleLanguageForRevenueInvoice, GetCodeMultpleLanguageForCase, GetCodeMultpleLanguageForTask, \
    GetCodeMultpleLanguageForPurchaseInvoice, GetCodeMultpleLanguageForCaseTaskMessage
from management.models import Code_Information
from message.models import File, Message, Patent_Document
from utils.db_fieldset import get_fielddic
from utils.enums import ALL_CODE_TYPE


@login_required(login_url='People:login')
def CaseCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Case')
    if request.method == 'POST':
        form = CaseModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        nowdate = datetime.datetime.now()
        year = nowdate.strftime('%y')
        mon = nowdate.strftime('%m')
        day = nowdate.strftime('%d')
        ccodename = Code_Information.objects.filter(codeType=ALL_CODE_TYPE.Case_CaseType.name).first().codeName
        ourlist = []
        f = Case.objects.filter(
            caseNumber__regex=r'(^C)('+ccodename+')(' + year + ')(' + mon + ')('+ day +')(\d{3})')
        serialnum = ''
        if f:
            for l in f:

                ourlist.append(int(l.caseNumber[-3:]))
            serialnum = year + mon + day + str(max(ourlist) + 1).zfill(3)
        else:
            serialnum = year + mon + day + '001'



        serialnum = 'C' + ccodename + serialnum
        customcodechoice = GetCodeMultpleLanguageForCase(request)
        form = CaseModelForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)

    return render(request, 'case/case_form.html', {'form': form,'serialnum' : serialnum})

@login_required(login_url='People:login')
def CaseEdit(request, pk):
    case = get_object_or_404(Case, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Case')
    if request.method == 'POST':
        form = CaseModelForm(request.POST, request.FILES, instance=case, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeMultpleLanguageForCase(request)
        form = CaseModelForm(instance=case, labeldic=labeldic, customcodechoice=customcodechoice, request=request)
    return render(request, 'case/case_form.html', {
        'form': form,
    })

@login_required(login_url='People:login')
def Case_SearchOurCom(request):
    numberforsearch = request.GET.get('searchournum')
    ourlist = []
    f = Case.objects.filter(
        caseNumber__regex=r'('+numberforsearch+')(\d{3})')
    if f:
        for l in f:
            ourlist.append(int(l.ourCompanyNumber[-3:]))
        serialnum = numberforsearch + '-' + str(max(ourlist) + 1).zfill(3)
    else:
        serialnum = numberforsearch + '001'
    data = {'serialnum':serialnum}
    return JsonResponse(data)

@login_required(login_url='People:login')
@csrf_exempt
def CaseDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Case.objects.get(id=pk).delete()
        return redirect('case:cases')

@login_required(login_url='People:login')
def TaskCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Task')
    if request.method == 'POST':
        form = TaskModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        nowdate = datetime.datetime.now()
        year = nowdate.strftime('%y')
        mon = nowdate.strftime('%m')
        day = nowdate.strftime('%d')
        ccodename = Code_Information.objects.filter(codeType=ALL_CODE_TYPE.Task_TaskType.name).first().codeName
        ourlist = []
        f = Task.objects.filter(
            taskNumber__regex=r'(^T)(' + ccodename + ')(' + year + ')(' + mon + ')(' + day + ')(\d{4})')
        serialnum = ''
        if f:
            for l in f:
                ourlist.append(int(l.caseNumber[-4:]))
            serialnum = year + mon + day + str(max(ourlist) + 1).zfill(4)
        else:
            serialnum = year + mon + day + '0001'

        serialnum = 'T' + ccodename + serialnum
        customcodechoice = GetCodeMultpleLanguageForTask(request)
        form = TaskModelForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)

    return render(request, 'case/task_form.html', {'form': form,'serialnum':serialnum})

@login_required(login_url='People:login')
def TaskEdit(request, pk):
    task = get_object_or_404(Task, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Task')
    if request.method == 'POST':
        form = TaskModelForm(request.POST, request.FILES, instance=task, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeMultpleLanguageForTask(request)
        form = TaskModelForm(instance=task, labeldic=labeldic, customcodechoice=customcodechoice, request=request)
    return render(request, 'case/task_form.html', {
        'form': form,
    })

@login_required(login_url='People:login')
@csrf_exempt
def TaskDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Task.objects.get(id=pk).delete()
        return render(request, 'case/tasks.html')


@login_required(login_url='People:login')
def Task_SearchOurCom(request):
    numberforsearch = request.GET.get('searchournum')
    ourlist = []
    f = Task.objects.filter(
        taskNumber__regex=r'('+numberforsearch+')(\d{4})')
    if f:
        for l in f:
            ourlist.append(int(l.taskNumber[-4:]))
        serialnum = numberforsearch + '-' + str(max(ourlist) + 1).zfill(4)
    else:
        serialnum = numberforsearch + '0001'
    data = {'serialnum':serialnum}
    return JsonResponse(data)


@login_required(login_url='People:login')
def RevenueInvoiceCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Revenue_Invoice')
    if request.method == 'POST':
        form = RevenueInvoiceModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        nowdate = datetime.datetime.now()
        year = nowdate.strftime('%y')
        mon = nowdate.strftime('%m')
        ourlist = []
        pcode = Code_Information.objects.filter(codeType=ALL_CODE_TYPE.PatentType.name).first().codeName
        ncode = Code_Information.objects.filter(codeType=ALL_CODE_TYPE.PatentNationType.name).first().codeName
        f = Revenue_Invoice.objects.filter(
            ourCompanyClaimNumber__regex=r'('+ncode+')(' + year + ')(' + mon + ')(\d{4})('+pcode+')')
        # f = Application_Patent_Domestic_Master.objects.filter(ourCompanyNumber__regex=r'(^L)(P|T|D|R|I|C)(D|O|I)(1912)(-?)(\d{4})').all()
        serialnum = ''
        if f:
            for l in f:
                ourlist.append(int(l.ourCompanyClaimNumber[5:9]))
            serialnum = year + mon + str(max(ourlist) + 1).zfill(4)
        else:
            serialnum = year + mon + '0001'
        serialnum = ncode + serialnum + pcode
        customcodechoice = GetCodeMultpleLanguageForRevenueInvoice(request)

        form = RevenueInvoiceModelForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)

    return render(request, 'case/revenue_invoice_form.html', {'form': form,'serialnum':serialnum,
                                                              'patentType':customcodechoice[ALL_CODE_TYPE.PatentType.name]
                                                              ,'nationType':customcodechoice[ALL_CODE_TYPE.PatentNationType.name]})
@login_required(login_url='People:login')
def LoadOurComnumForRevenueInvoide(request):
    numberforsearch = request.GET.get('searchournum')
    frontnum = numberforsearch[0:5]
    lastnum = numberforsearch[-1]
    ourlist = []
    f = Revenue_Invoice.objects.filter(
        ourCompanyClaimNumber__regex=r'('+frontnum+')(\d{4})('+lastnum+')')
    serialnum = ''
    if f:
        for l in f:
            ourlist.append(int(l.ourCompanyClaimNumber[5:9]))
        serialnum = frontnum + str(max(ourlist) + 1).zfill(4) + lastnum
    else:
        serialnum = frontnum + '0001' + lastnum
    data = {'serialnum': serialnum}
    return JsonResponse(data)



@login_required(login_url='People:login')
def RevenueInvoiceEdit(request, pk):
    revenue = get_object_or_404(Revenue_Invoice, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Revenue_Invoice')
    if request.method == 'POST':
        form = RevenueInvoiceModelForm(request.POST, request.FILES, instance=revenue, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeMultpleLanguageForRevenueInvoice(request)
        form = RevenueInvoiceModelForm(instance=revenue, labeldic=labeldic, customcodechoice=customcodechoice, request=request)
    return render(request, 'case/revenue_invoice_form.html', {'form': form,
                                                              'patentType': customcodechoice[
                                                                  ALL_CODE_TYPE.PatentType.name]
        , 'nationType': customcodechoice[ALL_CODE_TYPE.PatentNationType.name]})

@login_required(login_url='People:login')
@csrf_exempt
def RevenueInvoiceDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Revenue_Invoice.objects.get(id=pk).delete()
        return render(request, 'case/revenue_invoices.html')


@login_required(login_url='People:login')
def PurchaseInvoiceCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Purchase_Invoice')
    if request.method == 'POST':
        form = PurchaseInvoiceModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeMultpleLanguageForPurchaseInvoice(request)
        form = PurchaseInvoiceModelForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)

    return render(request, 'case/purchase_invoice_form.html', {'form': form})

@login_required(login_url='People:login')
def PurchaseInvoiceEdit(request, pk):
    purchase = get_object_or_404(Purchase_Invoice, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Purchase_Invoice')
    if request.method == 'POST':
        form = PurchaseInvoiceModelForm(request.POST, request.FILES, instance=purchase, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        customcodechoice = GetCodeMultpleLanguageForPurchaseInvoice(request)
        form = PurchaseInvoiceModelForm(instance=purchase, labeldic=labeldic, customcodechoice=customcodechoice, request=request)
    return render(request, 'case/purchase_invoice_form.html', {
        'form': form,
    })

@csrf_exempt
@login_required(login_url='People:login')
def PurchaseInvoiceDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Purchase_Invoice.objects.get(id=pk).delete()
        return render(request, 'case/purchase_invoices.html')

@login_required(login_url='People:login')
def TransactionHistoryCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Transaction_History')
    if request.method == 'POST':
        form = TransactionHistoryModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = TransactionHistoryModelForm(labeldic=labeldic, request=request)

    return render(request, 'case/transactionhistory_form.html', {'form': form})

@login_required(login_url='People:login')
def TransactionHistoryEdit(request, pk):
    instance = get_object_or_404(Transaction_History, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Transaction_History')
    if request.method == 'POST':
        form = TransactionHistoryModelForm(request.POST, request.FILES, instance=instance, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = TransactionHistoryModelForm(instance=instance, labeldic=labeldic, request=request)
    return render(request, 'case/transactionhistory_form.html', {
        'form': form,
    })

@csrf_exempt
@login_required(login_url='People:login')
def TransactionHistoryDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        Transaction_History.objects.get(id=pk).delete()
        return render(request, 'case/transactionhistory.html')


@login_required(login_url='People:login')
def BankAccountCreate(request):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'BankAccount_Information')
    if request.method == 'POST':
        form = BankAccountInformationModelForm(request.POST, crud='c', request=request)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.save()
            form.save_m2m()
            form.save_create()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = BankAccountInformationModelForm(labeldic=labeldic, request=request)

    return render(request, 'case/bankaccountinformation_form.html', {'form': form})

@login_required(login_url='People:login')
def BankAccountEdit(request, pk):
    instance = get_object_or_404(BankAccount_Information, id=pk)
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'BankAccount_Information')
    if request.method == 'POST':
        form = BankAccountInformationModelForm(request.POST, request.FILES, instance=instance, crud='u', request=request)
        if form.is_valid():
            formmaster = form.save(commit=False)
            formmaster.save()
            form.save_m2m()
            return HttpResponse(
                '<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        form = BankAccountInformationModelForm(instance=instance, labeldic=labeldic, request=request)
    return render(request, 'case/bankaccountinformation_form.html', {
        'form': form,
    })

@csrf_exempt
@login_required(login_url='People:login')
def BankAccountDelete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        BankAccount_Information.objects.get(id=pk).delete()
        return render(request, 'case/bankaccountinformation.html')


def CreateTaskFromTemplate(tasktemplatelist, msgid):

    def setTaskCode(taskcode, msgid):
        meobj = Message.objects.get(id=msgid)
        datestr = meobj.createdDate.strftime('%Y%m%d')
        datestr = datestr[2:]
        ourlist = []

        numberforsearch = 'T' + taskcode + datestr
        f = Task.objects.filter(
            taskNumber__regex=r'(' + numberforsearch + ')(\d{4})')
        if f:
            for l in f:
                ourlist.append(int(l.taskNumber[-4:]))
            serialnum = numberforsearch + str(max(ourlist) + 1).zfill(4)
        else:
            serialnum = numberforsearch + '0001'
        return serialnum
    def makeedge(id, nodelist, edge, rootid, linking=False):
        tobj = Task_Template.objects.get(id=id)
        if tobj.child_id:
            if tobj.child_id in nodelist:
                if linking:
                    edge.append({'source': rootid, 'target': tobj.child_id})
                else:
                    edge.append({'source': id, 'target': tobj.child_id})
            else:
                makeedge(tobj.child_id, nodelist, edge, id, True)

    tt = Task_Template.objects.filter(id__in=tasktemplatelist)
    nodeidlist = []
    generatedtaskid = []
    generatedtasks = []
    for o in tt:
        taskname = setTaskCode(o.taskCode, msgid)
        tobj, created = Task.objects.get_or_create(taskCode=o.taskCode, taskNumber=taskname)
        tobj.state = 'RECP'
        tobj.save()
        generatedtasks.append(tobj)
        generatedtaskid.append(tobj.id)
        nodeidlist.append(o.id)
    edge = []
    for node in nodeidlist:
        makeedge(node, nodeidlist, edge, '')
    return generatedtaskid, edge, generatedtasks

@login_required(login_url='People:login')
def Post_CustomCase(request):
    form = Custom_CaseForm(request.POST, crud='c', request=request)
    # 모델폼에서 임의로 폼 필드 추가하고 choices를 이후 적용하면, form valid 가 false이므로 일단 무시한다.
    if form.is_valid():
        msgid = form.request.POST.get('msgid')
        tasktemplate = form.request.POST.getlist('tasktemplate')
        isReferencedClient = form.request.POST.get('isReferencedClient', None)
        isReferencedClientManager = form.request.POST.get('isReferencedClientManager', None)
        isReferencedourCompanyManager = form.request.POST.get('isReferencedourCompanyManager', None)
        generatedtaskid, edges, generatedtasks = CreateTaskFromTemplate(tasktemplate, msgid)
        myform = form.save(commit=False)
        if not myform.caseNumber:
            raise forms.ValidationError("case number(code) is none.")
        myform.state = "RECP"
        myform.save()
        form.save_m2m()
        form.save_create()
        if isReferencedClient == 'on':
            ccth = myform.case_client.through()
            ccth.case = myform
            ccth.isReferencedpatent_client = True
            ccth.save()
        if isReferencedClientManager == 'on':
            cctth = myform.case_clientmanagement.through()
            cctth.case = myform
            cctth.isReferencedpatent_clientManager = True
            cctth.save()
        if isReferencedourCompanyManager == 'on':
            coth = myform.case_ourcompanymanagement.through()
            coth.case = myform
            coth.isReferencedpatent = True
            coth.save()

        msg = Message.objects.get(id=msgid)
        th = msg.case.through()
        th.message = msg
        th.case = myform
        casemessagetype = form.request.POST.getlist('case_message_type')

        if casemessagetype and casemessagetype[0] != '':
            th.case_task_Type = casemessagetype[0]
        th.save()
        cef = request.POST.getlist('case_evidence_file')
        if cef:
            File.objects.filter(id__in=cef).update(case_evidence=myform)
        ri = request.POST.getlist('revenue_invoice')
        if ri and ri[0] != '':
            riobj = Revenue_Invoice.objects.get(id=ri[0])
            riobj.case = myform
            riobj.save()



        for ed in edges:
            sid = ed['source']
            tid = ed['target']
            sourceTaskCode = Task_Template.objects.get(id=sid).taskCode
            for t in generatedtasks:
                if t.taskCode == sourceTaskCode:
                    task = t
                    break
            #task, created = Task.objects.get_or_create(taskCode=sourceTaskCode)
            if tid:
                targetTaskCode = Task_Template.objects.get(id=tid).taskCode
                for t in generatedtasks:
                    if t.taskCode == targetTaskCode:
                        child = t
                        break
                #child, created = Task.objects.get_or_create(taskCode=targetTaskCode)
                task.child = child
                task.save()
        for generatedtid in generatedtaskid:
            Case_Task.objects.get_or_create(case=myform, task_id=generatedtid)




@login_required(login_url='People:login')
def Post_CustomTask(request):
    form = TaskModelForm(request.POST, crud='c', request=request)
    # 모델폼에서 임의로 폼 필드 추가하고 choices를 이후 적용하면, form valid 가 false이므로 일단 무시한다.
    if form.is_valid():
        myform = form.save(commit=False)
        myform.state = 'RECP'
        myform.save()
        form.save_m2m()
        form.save_create()
        msgid = form.request.POST.get('msgid')

        msg = Message.objects.get(id=msgid)
        th = msg.task.through()
        th.message = msg
        th.task = myform
        taskmessagetype = form.request.POST.getlist('task_message_type')
        if taskmessagetype and taskmessagetype[0] != '':
            th.case_task_Type = taskmessagetype[0]
        th.save()




@csrf_exempt
@login_required(login_url='People:login')
def CreateCaseWithMasterandMsg(request):
    if request.method == 'POST':
        Post_CustomCase(request)
        metaget = request.META.get('HTTP_REFERER')
        return redirect(
            metaget)



@csrf_exempt
@login_required(login_url='People:login')
def CustomCasePopupForm(request, pk):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Case')
    if request.method == 'POST':
        Post_CustomCase(request)
        return HttpResponse('<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        #emaildetail, form, master
        type = request.GET.get('type','')
        customcodechoice = GetCodeMultpleLanguageForCase(request)
        msgs = Message.objects.filter(id=pk).prefetch_related('master_message').all()
        mastermessage = msgs.first().master_message.all()
        if msgs.first().type == 'EM':
            caseevidencefile = msgs.first().email.email_file.all()
        else:
            caseevidencefile = msgs.first().patent_docu.patentofficedocu_attachment.all()
        for pd in msgs.first().patent_docu.patentofficedocu_attachment.all():
            if pd.deadLineDate:
                dd = pd.deadLineDate.strftime('%Y-%m-%d')
            else:
                dd = None

        form = Custom_CaseForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request)
        case_message_type = GetCodeMultpleLanguageForCaseTaskMessage(request)[
            ALL_CODE_TYPE.Message_CaseTaskType.name]
        case_state = GetCodeMultpleLanguageForCase(request)[ALL_CODE_TYPE.Case_CaseState.name]

    return render(request, 'case/linkcase_master_message_form.html', {'form': form, 'linkmsg': pk, 'master': mastermessage,
                                                         'case_task_message_type': case_message_type,'caseevidencefile':caseevidencefile
                                                                      , 'ispop':True, 'type': type, 'case_state':case_state, 'deadlinedate':dd})


@csrf_exempt
@login_required(login_url='People:login')
def CreateTaskWithMasterandMsg(request):
    if request.method == 'POST':
        Post_CustomTask(request)
        metaget = request.META.get('HTTP_REFERER')
        return redirect(
            metaget)

@csrf_exempt
@login_required(login_url='People:login')
def CustomTaskPopupForm(request, pk):
    language = request.session['django_language']
    labeldic = get_fielddic(language, 'Task')
    if request.method == 'POST':
        Post_CustomTask(request)
        return HttpResponse('<script type="text/javascript">window.close(); window.opener.location.reload();</script>')
    else:
        # emaildetail, form, master
        customcodechoice = GetCodeMultpleLanguageForTask(request)
        msgs = Message.objects.filter(id=pk).prefetch_related('master_message').all()
        mastermessage = msgs.first().master_message.all()
        form = TaskModelForm(labeldic=labeldic, customcodechoice=customcodechoice, request=request, auto_id="task_%s")
        task_message_type = GetCodeMultpleLanguageForCaseTaskMessage(request)[
            ALL_CODE_TYPE.Message_CaseTaskType.name]
        return render(request, 'case/linktask_master_message_form.html', {'form': form, 'linkmsg': pk, 'master': mastermessage,
                                                         'case_task_message_type': task_message_type, 'ispop':True})

@csrf_exempt
@login_required(login_url='People:login')
def CaseAndPatentByPatentOurCom(request):
    ourcomno = request.POST.get('ourcomno')
    obj = Application_Patent_Domestic_Master.objects.get(ourCompanyNumber=ourcomno)
    cases = obj.case.all()
    patent = Application_Patent_Domestic_Master.objects.filter(ourCompanyNumber=ourcomno)

    return render(request, 'case/transactionhistory_detail.html',
                  {'ourcom':ourcomno,'cases':cases,'patent':patent})