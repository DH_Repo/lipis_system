from django.contrib import admin

# Register your models here.
from case.models import *

admin.site.register(Case)
admin.site.register(Task)
admin.site.register(Revenue_Invoice)
admin.site.register(Purchase_Invoice)
admin.site.register(Case_Task_Invoice_Management)

#admin.site.register(Application_Patent_Domestic_Master_Case_Task)
